﻿namespace DTT.CodeGeneration
{
    /// <summary>
    /// Defines the reserved keywords in the C# language.
    /// </summary>
    public static class SourceKeywords
    {
        public const string PRIVATE = "private";
        public const string PROTECTED = "protected";
        public const string INTERNAL = "internal";
        public const string PUBLIC = "public";
        public const string CLASS = "class";
        public const string STRUCT = "struct";
        public const string READONLY = "readonly";
        public const string STATIC = "static";
        public const string CONST = "const";
        public const string ENUM = "enum";
    }
}