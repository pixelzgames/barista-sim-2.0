﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DTT.COLOR.Editor")]
[assembly: InternalsVisibleTo("DTT.COLOR.Tests.Runtime")]
namespace DTT.COLOR
{
    // Exposes internal modifiers to the editor assembly.
}