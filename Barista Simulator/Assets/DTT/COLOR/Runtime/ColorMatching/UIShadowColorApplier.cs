using Coffee.UIEffects;
using UnityEngine;

namespace DTT.COLOR.ColorMatching
{
    internal class UIShadowColorApplier : IColorApplier
    {
        /// <summary>
        /// The camera object to apply the color to.
        /// </summary>
        private readonly UIShadow _uiShadow;

        /// <summary>
        /// Creates a new instance of the color applier.
        /// </summary>
        /// <param name="uiShadow">The camera to apply the color to.</param>
        public UIShadowColorApplier(UIShadow uiShadow) => _uiShadow = uiShadow;

        public void Apply(Color color, bool overrideAlpha)
        {
            _uiShadow.effectColor =
                ColorPaletteManagerUtilities.ApplyColor(_uiShadow.effectColor, color, overrideAlpha);
        }
    }
}
