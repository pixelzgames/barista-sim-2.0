﻿namespace DTT.COLOR.Editor
{
    /// <summary>
    /// Implemented on objects that are able to draw themselves.
    /// </summary>
    internal interface IDrawable
    {
        /// <summary>
        /// Should draw the object.
        /// </summary>
        void Draw();
    }
}