using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace WeightedDistribution
{
    [System.Serializable]
    public class DistributionItem<T>
    {
        [FormerlySerializedAs("weight")] [SerializeField]
        float _weight;
        public float Weight { get => _weight;
            set => _weight = value;
        }

        public float CombinedWeight { get; set; }

        [FormerlySerializedAs("percentage")] [Range(0f, 100f), SerializeField]
        float _percentage;
        public float Percentage { get => _percentage;
            set { if (value is >= 0 and <= 100) _percentage = value; } }

        [FormerlySerializedAs("value")] [SerializeField]
        T _value;
        public T Value { get => _value;
            set => this._value = value;
        }
    }

    public abstract class Distribution<T, T_ITEM> : ScriptableObject
        where T_ITEM : DistributionItem<T>, new()
    {
        [FormerlySerializedAs("items")] [SerializeField]
        private List<T_ITEM> _items;
        public List<T_ITEM> Items => _items;

        private int _nbItems = 0;
        private float _combinedWeight;

        private void OnItemsChange(bool addedItem = false)
        {
            // On Add Component
            if (_items == null)
                return;
            
            // On Add Item
            if (!addedItem && _items.Count > _nbItems)
                _items[^1].Weight = 0;

            var atLeastOnePositiveValue = false;
            foreach (T_ITEM item in _items)
            {
                if (item.Weight < 0)
                    item.Weight = 0;
                if (item.Weight > 0)
                    atLeastOnePositiveValue = true;
            }
            
            if (_items.Count > 0 && (_items.Count == 1 || !atLeastOnePositiveValue))
                _items[0].Weight = 1;

            ComputePercentages();
            _nbItems = _items.Count;
        }

        private void ComputePercentages()
        {
            _combinedWeight = 0;

            foreach (var item in _items)
            {
                _combinedWeight += item.Weight;
                item.CombinedWeight = _combinedWeight;
            }

            foreach (var item in _items)
                item.Percentage = item.Weight * 100 / _combinedWeight;
        }

        private void Awake()
        {
            OnItemsChange();
        }

        private void OnValidate()
        {
            OnItemsChange();
        }

        public T Draw(bool removeItem = false)
        {
            if (_items.Count == 0)
                throw new UnityException("Can't draw an item from an empty distribution!");

            var random = Random.Range(0f, _combinedWeight);
            foreach (var item in _items.Where(item => random <= item.CombinedWeight))
            {
                if (removeItem)
                {
                    item.Weight = 0f;
                    ComputePercentages();
                }
                
                return item.Value;
            }

            throw new UnityException("Error while drawing an item.");
        }

        public void Add(T value, float weight)
        {
            _items.Add(new T_ITEM { Value = value, Weight = weight });
            OnItemsChange(true);
        }

        public void RemoveAt(int index)
        {
            if (_items.Count - 1 < index || index < 0)
                return;
            _items.RemoveAt(index);
            OnItemsChange();
        }
    }
}