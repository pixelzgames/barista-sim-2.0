%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: UpperBody_Mask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: ApronSupport
    m_Weight: 1
  - m_Path: ApronSupport/Apron
    m_Weight: 1
  - m_Path: Bone_Root
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_left
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_left/bone_leg_left
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_left/bone_leg_left/bone_foot_left
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_left/bone_leg_left/bone_foot_left/bone_toes_left
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_left/bone_leg_left/bone_foot_left/bone_toes_left/bone_toes_left_nub
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_right
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_right/bone_leg_right
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_right/bone_leg_right/bone_foot_right
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_right/bone_leg_right/bone_foot_right/bone_toes_right
    m_Weight: 1
  - m_Path: Bone_Root/bone_thigh_right/bone_leg_right/bone_foot_right/bone_toes_right/bone_toes_right_nub
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head/Closed
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head/Eyes
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head/Glasses
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head/Hat_test
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head/Head_Knob
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/Bone_Head/Mustach
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_left
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_left/bone_upperarm_left
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_left/bone_upperarm_left/bone_lowerarm_left
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_left/bone_upperarm_left/bone_lowerarm_left/bone_hand_left_nub
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_right
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_right/bone_upperarm_right
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_right/bone_upperarm_right/bone_lowerarm_right
    m_Weight: 1
  - m_Path: Bone_Root/Bone_Torso/bone_shoulder_right/bone_upperarm_right/bone_lowerarm_right/bone_hand_right_nub
    m_Weight: 1
  - m_Path: Character_Body
    m_Weight: 1
  - m_Path: Character_Body/Pants
    m_Weight: 1
  - m_Path: Character_Body/Pants002
    m_Weight: 1
  - m_Path: Character_Body/Tshirt
    m_Weight: 1
  - m_Path: IK Chain002
    m_Weight: 1
  - m_Path: IK Chain003
    m_Weight: 1
  - m_Path: IK Chain004
    m_Weight: 1
  - m_Path: IK Chain005
    m_Weight: 1
  - m_Path: Table
    m_Weight: 1
