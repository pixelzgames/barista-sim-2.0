using Newtonsoft.Json;
using System;
using System.IO;
using UnityEngine;

public static class FileGameDataHandler<T>
{
   private static readonly string _dataDirPath = Application.persistentDataPath;
   private static readonly string _dataFileName = "savegame";
   private static readonly bool _useEncryption;
   private static readonly string _encryptionCodeWord = "BigPeepee";

   public static bool HasSaveFile(string path = null)
   {
      if (string.IsNullOrEmpty(path))
      {
         path = Path.Combine(_dataDirPath, _dataFileName + ".brag");
      }

      return File.Exists(path);
   }
   
   public static T Load(string fileName = null)
   {
      if (string.IsNullOrEmpty(fileName))
      {
         fileName = _dataFileName;
      }
      
      string fullPath;
      
      if (GameConfig.GetConfigData().Gameplay.OverrideCafeGridSerialization)
      {
         fullPath = Path.Combine(Application.dataPath + "/StreamingAssets/" + fileName + ".brag");
      }
      else
      {
         fullPath = Path.Combine(_dataDirPath, fileName + ".brag");
      }
      
      object loadedData = null;

      if (!HasSaveFile(fullPath))
      {
         Debug.Log($"Trying to Load() {fileName}.brag but file does not exist on path {fullPath}");
         return default;
      }
      
      try
      {
         // Load the serialized game data from the file
         using var stream = new FileStream(fullPath, FileMode.Open);
         using var reader = new StreamReader(stream);
         var dataToLoad = reader.ReadToEnd();

         if (_useEncryption)
         {
            dataToLoad = EncryptDecrypt(dataToLoad); 
         }
         
         // Deserialize the data loaded in Json into c# objects
         loadedData = JsonConvert.DeserializeObject<T>(dataToLoad);
      }
      catch (Exception e)
      {
         Debug.LogError($"Error occured while trying to load data from file: {fullPath} \n {e}");
      }

      return (T)loadedData;
   }

   public static void SaveToDisk(T data, string fileName = null)
   {
      if (string.IsNullOrEmpty(fileName))
      {
         fileName = _dataFileName;
      }
      
      var fullPath = Path.Combine(_dataDirPath, fileName + ".brag");
      try
      {
         // Create the directory in case it does not exist
         Directory.CreateDirectory(Path.GetDirectoryName(fullPath) ?? string.Empty);
         
         // Serialize the Game Data to JSON (or other)
         var dataToSave = JsonConvert.SerializeObject(data, Formatting.Indented, new JsonSerializerSettings
         {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
         });

         if (_useEncryption)
         {
            dataToSave = EncryptDecrypt(dataToSave);
         }
         
         // Write the serialized data to the file
         using var stream = new FileStream(fullPath, FileMode.Create);
         using var writer = new StreamWriter(stream);
         writer.Write(dataToSave);
      }
      catch (Exception e)
      {
         Debug.LogError($"Error occured while trying to save data to file: {fullPath} \n {e}");
      }
      
      Debug.Log($"Data {fileName}.brag saved to {fullPath}");
   }
   
   public static void DeleteSaveFile()
   {
      Debug.Log("Deleting save files...");
      
      var bragFiles = Directory.GetFiles(_dataDirPath, "*.brag");
      foreach (var file in bragFiles)
      {
         File.Delete(file);
      }
   }

   /// <summary>
   /// Simple implementation of XOR encryption method
   /// </summary>
   /// <param name="data">JSON to encrypt</param>
   /// <returns>Encrypted data</returns>
   private static string EncryptDecrypt(string data)
   {
      var modifiedData = string.Empty;
      for (var i = 0; i < data.Length; i++) 
      {
         modifiedData += (char) (data[i] ^ _encryptionCodeWord[i % _encryptionCodeWord.Length]);
      }
      
      return modifiedData;
   }
}
