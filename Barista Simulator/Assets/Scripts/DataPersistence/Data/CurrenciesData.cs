public class CurrenciesData
{
    public CurrenciesData()
    {
        CurrentMoney = 5000;
        CurrentXP = 0;
        CurrentBeanPoints = 0;
        CurrentLevel = 0;
    }
    
    public int CurrentMoney { get; set; }
    public int CurrentXP { get; set; }
    public int CurrentBeanPoints{ get; set; }
    public int CurrentLevel { get; set; }
}
