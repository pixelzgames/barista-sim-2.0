using UnityEngine;

[System.Serializable]
public class BRAGObjectData
{
    public string Name;
    public byte? PrefabID;
    public SerializedVector3 Position;
    public Quaternion Rotation;
    public SerializedVector3 Scale;
    public bool IsKinematic;
    public bool IsColliderEnabled;
    public Recipe CurrentRecipe;

    public PowerableData PowerableData;
    public EspressoMachineData EspressoMachineData;
    public MovableData MovableData;
    public ContainableData ContainableData;
}

[System.Serializable]
public struct SerializedVector3
{
    public float X, Y, Z;
}