using AI.Feedback;

[System.Serializable]
public class PlayerData
{
    /// <summary>
    /// Default constructor when no data is found.
    /// Initialize entry game data here for first launch.
    /// </summary>
    public PlayerData()
    {
        GameManagerData = new GameManagerData();
        CurrenciesData = new CurrenciesData();
        Appearance = new Appearance();
        FeedbackCatalogData = new FeedbackCatalogData();
        PlayerProgressionData = new ProgressionManagerData();
        BeanOSManagerData = new BeanOSManagerData();
        UnlockableItemData = new UnlockableItemData();
        TaskManagerData = new TaskManagerData();
        GridLevelData = new GridLevelData();
        GameData = new GameData();
    }
    
    public GameManagerData GameManagerData { get; set; }
    public CurrenciesData CurrenciesData { get; set; }
    public Appearance Appearance { get; set; }
    public FeedbackCatalogData FeedbackCatalogData { get; set; }
    public ProgressionManagerData PlayerProgressionData { get; set; }
    public BeanOSManagerData BeanOSManagerData { get; set; }
    public UnlockableItemData UnlockableItemData { get; set; }
    public TaskManagerData TaskManagerData { get; set; }
    public GridLevelData GridLevelData { get; set; }
    public GameData GameData { get; set; }
}