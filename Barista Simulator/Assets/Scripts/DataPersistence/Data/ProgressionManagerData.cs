#define USE_DEBUG_DATA
using System;
using System.Collections.Generic;
using UnityEngine;

public class ProgressionManagerData
{
    public Level LastGameplayScene { get; set; }
    public ProgressionBaseLevel CurrentCafeBaseLevel { get; set; }
    public ProgressionBaseLevel CurrentHomeBaseLevel { get; set; }
    public int CurrentDay { get; set; }
    public List<ProgressionEvent> CurrentActiveDailyEvents { get; set; }
    public bool HasPhone { get; set; }
    public CharacterWorldData CharacterWorldData { get; set; }

    public ProgressionManagerData()
    {
        CurrentDay = 0;
        LastGameplayScene = Level.Home;
        CurrentHomeBaseLevel = new HomeBaseLevel();
        CurrentCafeBaseLevel = new CafeBaseLevel();
        CurrentActiveDailyEvents = new List<ProgressionEvent>();
        HasPhone = false;
        CharacterWorldData = new CharacterWorldData();
#if USE_DEBUG_DATA
        var data = GameConfig.GetConfigData()?.DebugConfig;
        if (data == null)
        {
            return;
        }
        
        CurrentDay = data.CurrentDay;
        HasPhone = data.HasPhoneUnlocked;
        CurrentHomeBaseLevel = new HomeBaseLevel(data.HomeLevelName);
        CurrentCafeBaseLevel = new CafeBaseLevel(data.CafeLevelName);
#endif
    }
}

[Serializable]
public abstract class ProgressionBaseLevel
{
    public string SceneName { get; set; }
}

[Serializable]
public class HomeBaseLevel : ProgressionBaseLevel
{
    public HomeBaseLevel(string sceneName = "Home_01")
    {
        SceneName = sceneName;
    }
}

[Serializable]
public class CafeBaseLevel : ProgressionBaseLevel
{
    public CafeBaseLevel(string sceneName = "Cafe_01")
    {
        SceneName = sceneName;
    }
}

[Serializable]
public class CharacterWorldData
{
    public DataPerSceneType CafeData;
    public DataPerSceneType HomeData;
    
    [Serializable]
    public class DataPerSceneType
    {
        public Level LevelType;
        public SerializedVector3 Position;
        public Quaternion Rotation;
    }
}
