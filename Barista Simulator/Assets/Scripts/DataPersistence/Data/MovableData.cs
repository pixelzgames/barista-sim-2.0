using UnityEngine;

[System.Serializable]
public struct MovableData
{
    public Vector2Int GridPosition;
}