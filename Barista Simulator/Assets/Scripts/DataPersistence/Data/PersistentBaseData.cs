using UnityEngine;

public class PersistentBaseData<T> : MonoBehaviour
{
    public T Data { get; protected set; }

    protected virtual void OnEnable()
    {
        SingletonManager.Instance.DataPersistenceManager.PlayerDataLoaded += LoadData;
    }

    protected virtual void OnDisable()
    {
        SingletonManager.Instance.DataPersistenceManager.PlayerDataLoaded -= LoadData;
    }

    protected virtual void LoadData(PlayerData data){ }
}
