using System.Collections.Generic;

public class TaskManagerData
{
    public List<TaskInstance> ActiveTasks { get; set; }

    public TaskManagerData()
    {
        ActiveTasks = new List<TaskInstance>();
    }
}
