public class BeanOSManagerData
{
    public BeanOSManagerData()
    {
        Conversations = new BeanOSConversations();
    }
    
    public BeanOSConversations Conversations { get; set; }
    public SocialPostStubber[] CurrentSocialPosts { get; set; }
}

//TODO replace with Scriptable Object Config?
public struct SocialPostStubber
{
    public string ContentText;
    public int LikesAmount;
    public string UserTag;
}
