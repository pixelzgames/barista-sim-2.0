public interface IDataPersistence<T>
{
    void LoadData(T data);
    void SaveData(T data);
}

public interface IWorldDataPersistence
{
    void InitializeData();
    void LoadData(BRAGObjectData data);
    BRAGObjectData UpdateObjectData(); 
}
