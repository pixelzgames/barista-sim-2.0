using System;
using UnityEngine;

public class DataPersistenceManager : MonoBehaviour
{
    public event Action<PlayerData> PlayerDataLoaded = delegate {  };
    private PlayerData _playerData;

    private void Start()
    {
        LoadAllPlayerData(true);
        SingletonManager.Instance.GameManager.OnLevelLoadRequest += OnAnyLevelLoadRequest;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.GameManager.OnLevelLoadRequest -= OnAnyLevelLoadRequest;
    }

    public void NewGame()
    {
        FileGameDataHandler<object>.DeleteSaveFile();
        ResetAllPlayerData();
    }

    private void LoadAllPlayerData(bool fromDisk)
    {
        // Load data from file using the game Data handler
        if (fromDisk)
        {
            _playerData = FileGameDataHandler<PlayerData>.Load();
        }
        
        if (_playerData == null)
        {
            Debug.LogWarning("No Game Data Found When Trying To Load.");
            NewGame();
            return;
        }
        
        PlayerDataLoaded?.Invoke(_playerData);
    }
    
    public void SaveAllPlayerData()
    {
        FileGameDataHandler<PlayerData>.SaveToDisk(_playerData);
    }

    public void ResetAllPlayerData()
    {
        _playerData = new PlayerData();
        LoadAllPlayerData(false);
    }

    public void SaveGrid(Level levelToSave)
    {
        var grid = SingletonManager.Instance.GridManager.SaveGrid();
        FileGameDataHandler<GridData>.SaveToDisk(grid, $"grid_{levelToSave.ToString()}");
    }

    public void SaveWorld(Level levelToSave)
    {
        var worldObjectsToSave = SingletonManager.Instance.WorldObjectsHandler.WorldObjects;
        var allWorldObjectsToSave = new WorldObjectsData();
        foreach (var worldObject in worldObjectsToSave)
        {
            allWorldObjectsToSave.WorldObjects.Add(worldObject.UpdateObjectData());
        }
        
        FileGameDataHandler<WorldObjectsData>.SaveToDisk(allWorldObjectsToSave, $"world_{levelToSave.ToString()}");
    }

    private void OnAnyLevelLoadRequest(Level currentLevel)
    {
        if (currentLevel is Level.None or Level.MainMenu)
        {
            return;
        }
        
        SaveAllPlayerData();
        SaveGrid(currentLevel);
        SaveWorld(currentLevel);
    }

    private void OnApplicationQuit()
    {
        var currentSceneType = SingletonManager.Instance.GameManager.CurrentSceneType;
        if (currentSceneType == Level.MainMenu)
        {
            return;
        }
        
        SaveGrid(currentSceneType);
        SaveWorld(currentSceneType);
        SaveAllPlayerData();
    }
}
