using UnityEngine;

public class BRAGPrefabHandler : MonoBehaviour
{
    public Interactable DataObject => _dataObject;
    
    [SerializeReference] private Interactable _dataObject;

    public void PassDataThrough(BRAGObjectData data)
    {
        _dataObject.LoadData(data);
    }
}
