using System;
using UnityEngine;

public class GridLoader : ILoaderWaitable
{
    public event Action Completed = delegate { };

    private const string _gridFileName = "grid_";
    private readonly Level _gridToLoad;
    
    public GridLoader(Level gridToLoad)
    {
        _gridToLoad = gridToLoad;
    }
    
    void ILoaderWaitable.Execute()
    {
        if (_gridToLoad is Level.Cafe or Level.Home)
        {
            GridData data;
            if (_gridToLoad is Level.Cafe && GameConfig.GetConfigData().Gameplay.OverrideCafeGridSerialization)
            {
                data = FileGameDataHandler<GridData>.Load(GameConfig.GetConfigData().Gameplay.CafeGridSaveFile);
            }
            else
            {
                data = FileGameDataHandler<GridData>.Load(_gridFileName + _gridToLoad);
            }
           
            SingletonManager.Instance.GridManager.LoadGrid(data);
        }
        
        Debug.Log($"{this} is completed!");
        Completed?.Invoke();
    }
}
