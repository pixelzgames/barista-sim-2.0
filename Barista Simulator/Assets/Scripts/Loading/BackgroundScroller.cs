using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    [SerializeField] private Transform[] _walls;
    [SerializeField] private float _xValueToDespawn = -10f;
    [SerializeField] private float _xValueToAdd = 30f;
    [SerializeField] private float _scrollingSpeed = 2f;
    

    // Update is called once per frame
    private void Update()
    {
        foreach (var wall in _walls)
        {
            wall.Translate(Vector3.left * _scrollingSpeed * Time.deltaTime);
            if (!(wall.position.x <= _xValueToDespawn))
            {
                continue;
            }
            
            var position = wall.position;
            position = new Vector3(position.x + _xValueToAdd, position.y, position.z);
            wall.position = position;
        }
    }
}
