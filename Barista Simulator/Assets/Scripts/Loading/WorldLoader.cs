using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class WorldLoader : ILoaderWaitable
{
    public event Action Completed = delegate { };

    private readonly Level _worldToLoad;
    
    public WorldLoader(Level worldToLoad)
    {
        _worldToLoad = worldToLoad;
    }
    
    void ILoaderWaitable.Execute()
    {
        if (_worldToLoad is Level.Cafe or Level.Home)
        {
            WorldObjectsData data;
            if (_worldToLoad is Level.Cafe && GameConfig.GetConfigData().Gameplay.OverrideCafeGridSerialization)
            {
                data = FileGameDataHandler<WorldObjectsData>.Load(GameConfig.GetConfigData().Gameplay.CafeWorldSaveFile);
            }
            else
            {
                data = FileGameDataHandler<WorldObjectsData>.Load("world_" + _worldToLoad);    
            }
            
            if (data != null)
            {
                foreach (var worldObject in data.WorldObjects)
                {
                    var go = PrefabsUtility.GetPrefabFromList(worldObject.PrefabID);
                    Object.Instantiate(go).PassDataThrough(worldObject);
                }
            }
        }
    
        Debug.Log($"{this} is completed!");
        Completed?.Invoke();
    }
}
