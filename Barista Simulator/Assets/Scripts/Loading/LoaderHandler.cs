using System;
using UnityEngine;

public class LoaderHandler
{
   public event Action LoadingCompleted = delegate { };

   private readonly ILoaderWaitable[] _loaderWaitables;
   private int _index;
   
   public LoaderHandler(ILoaderWaitable[] loaders)
   {
      _loaderWaitables = loaders;
   }

   public void RunChainLoaders()
   {
      if (_loaderWaitables.Length <= 0)
      {
         Debug.LogError($"Trying to load an empty chain of loaders in {_loaderWaitables}");
         return;
      }

      RunLoader(0);
   }

   private void RunLoader(int index)
   {
      _loaderWaitables[index].Completed += Next;
      _loaderWaitables[index].Execute();
   }

   private void Next()
   {
      _loaderWaitables[_index].Completed -= Next;
      _index++;
      
      if (_loaderWaitables.Length <= _index)
      {
         LoadingCompleted?.Invoke();
         Debug.Log($"{this} loading Chain Completed!");
         return;
      }
      
      RunLoader(_index);
   }
}
