using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class SceneLoader : ILoaderWaitable
{
    public event Action Completed = delegate { };

    private readonly LevelData _levelData;
    
    public SceneLoader(LevelData levelData)
    {
        _levelData = levelData;
    }
    
    void ILoaderWaitable.Execute()
    {
        Debug.Log($"{this} is completed!");
        SingletonManager.Instance.StartCoroutine(LoadLevelAsync(_levelData));
    }

    private IEnumerator LoadLevelAsync(LevelData levelData)
    {
        //TODO This method should get the PlayerData to be able to know which scene to load
        var operation = SceneManager.LoadSceneAsync(levelData.LevelName);
        while (!operation.isDone)
        {
            yield return null;
        }

        var waitables = Object.FindObjectsByType<MonoBehaviour>(FindObjectsSortMode.None)
            .OfType<ILevelLoadingWaitable>().ToArray();
        
        var shouldWait = waitables.Length > 0;
        while (shouldWait)
        {
            foreach (var script in waitables)
            {
                if (!script.IsReady())
                {
                    yield return null;
                }

                shouldWait = false;
            }
        }
        
        Completed?.Invoke();
    }
}
