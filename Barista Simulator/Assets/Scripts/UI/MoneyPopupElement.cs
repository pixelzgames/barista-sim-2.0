using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MoneyPopupElement : MonoBehaviour
{
    [SerializeField] private Color _lossColor, _gainColor;
    [SerializeField] private Image _backgroundImage;
    [SerializeField] private TextMeshProUGUI _moneyText;
    [SerializeField] private CanvasGroup _canvasGroup;

    public void InitializeData(int value)
    {
        var textToDisplay = "+";
        if (value < 0)
        {
            textToDisplay = "";
            _backgroundImage.color = _lossColor;
        }
        else
        {
            _backgroundImage.color = _gainColor;
        }

        _moneyText.text = textToDisplay + value;

        gameObject.SetActive(true);
        var mySequence = DOTween.Sequence();
        mySequence.Append(_canvasGroup.DOFade(1f, 0.2f))
            .AppendInterval(2f)
            .Append(_canvasGroup.DOFade(0f, 1f));
        mySequence.onComplete = () => Destroy(gameObject);
        mySequence.Play();
    }
}
