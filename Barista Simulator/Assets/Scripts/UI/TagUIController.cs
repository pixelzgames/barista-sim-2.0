using Coffee.UIEffects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UIGradient = JoshH.UI.UIGradient;

public class TagUIController : MonoBehaviour
{
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private UIGradient _gradient;
    [SerializeField] private TMP_Text _text;
    [SerializeField] private UIShadow _uiShadow;
    [SerializeField] private UIDropShadow _dropShadow;
    [SerializeField] private UIShiny _shiny;
  
    public void InitWithData(TagData data)
    {
        // Gradient colors
        _gradient.LinearColor1 = data.GradiantColorA;
        _gradient.LinearColor2 = data.GradientColorB;
        
        // Text
        _text.color = data.NameColor;
        _text.text = data.TagName;

        // UI 3D Shadow
        _uiShadow.enabled = data.HasShadow;
        _uiShadow.effectColor = data.ShadowColor;
        
        // Drop shadow
        _dropShadow.enabled = data.HasDropShadow;
        
        // Shiny
        _shiny.enabled = data.IsShiny;
        
        // Refresh size fitter with text
        LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
    }
    
    public void SetVisibility(bool visible)
    {
        gameObject.SetActive(visible);
    }
}