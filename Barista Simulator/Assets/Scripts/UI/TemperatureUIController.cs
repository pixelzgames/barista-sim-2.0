using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TemperatureUIController : MonoBehaviour
{
    [Tooltip("Needs 4 color")] 
    [SerializeField] private Gradient _colors;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private Image _fillImage;

    private TemperatureState _currentState;

    private void Awake()
    {
        Hide();
    }

    public void ShowTemperature(TemperatureState temp, float currentTemperature, float maxTemperature)
    {
        return;
        _currentState = temp;
        _fillImage.color = GetColorByTemperature();
        _fillImage.fillAmount = currentTemperature/maxTemperature;
        StopAllCoroutines();
        StartCoroutine(Show());
    }

    private IEnumerator Show()
    {
        _canvasGroup.DOFade(1f, 0.5f);
        yield return new WaitForSeconds(1f);
        Hide();
    }

    private void Hide()
    {
        _canvasGroup.alpha = 0f;
    }

    private Color GetColorByTemperature()
    {
        float value;

        switch (_currentState)
        {
            case TemperatureState.Boiling:
                value = 0.8f;
                break;
            case TemperatureState.Hot:
                value = 0.65f;
                break;
            case TemperatureState.Warm:
                value = 0.35f;
                break;
            case TemperatureState.Cold:
                value = 0.2f;
                break;
            case TemperatureState.None:
                value = 0f;
                break;
            default:
                value = 0f;
                break;
        }
        
        return _colors.Evaluate(value);
    }

    private void OnDestroy()
    {
        _canvasGroup.DOKill();
    }
}
