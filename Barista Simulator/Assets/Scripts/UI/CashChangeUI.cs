﻿using UnityEngine;
using DG.Tweening;
using TMPro;
using System.Collections;

public class CashChangeUI : MonoBehaviour
{
    private TextMeshProUGUI _text;

    public void InitCashChangeComponent(string newText, Color color)
    {
        _text = GetComponent<TextMeshProUGUI>();
        _text.text = newText + "$";
        _text.color = color;
        _text.alpha = 0f;
        StartCoroutine(FadeInAndKill());
    }

    private IEnumerator FadeInAndKill()
    {
        _text.DOFade(1f, 0.2f);
        yield return new WaitForSecondsRealtime(2f);
        yield return _text.DOFade(0f, 0.2f).WaitForCompletion();
        Destroy(gameObject);
    }


}
