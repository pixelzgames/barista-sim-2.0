using TMPro;
using UnityEngine;

public class NewDayCanvasController : MonoBehaviour
{
    public bool IsDoneRunning { get; private set; }
    public bool HasFadedIn { get; private set; }
    public bool HasFadedOut {get; private set;}

    [SerializeField] private TMP_Text _oldDayText;
    [SerializeField] private TMP_Text _newDayText;

    private void OnEnable()
    {
        var currentDay = SingletonManager.Instance.ProgressionManager.Data.CurrentDay;

        _oldDayText.text = currentDay.ToString();
        _newDayText.text = currentDay+1.ToString();
    }

    public void FinishFadingIn()
    {
        HasFadedIn = true;
    }
    
    public void FinishPlaying()
    {
        IsDoneRunning = true;
    }
    
    public void FinishFadingOut()
    {
        HasFadedOut = true;
    }
}
