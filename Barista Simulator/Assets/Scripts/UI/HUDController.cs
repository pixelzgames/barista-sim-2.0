using UnityEngine;

public class HUDController : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private CanvasGroup _xpCanvasGroup;
    [SerializeField] private CanvasGroup _dayTimeCanvasGroup;
    [SerializeField] private CanvasGroup _currencyCanvasGroup;
    [SerializeField] private CinematicModeUIController _cinematicModeUIController;
    
    private void Start()
    {
        SingletonManager.Instance.GameManager.CinematicMode += OnCinematicMode;
        SingletonManager.Instance.GameManager.MechanicMode += OnCloseUpMechanic;
    }

    private void OnDisable()
    {
        SingletonManager.Instance.GameManager.CinematicMode -= OnCinematicMode;
        SingletonManager.Instance.GameManager.MechanicMode -= OnCloseUpMechanic;
    }

    private void OnCinematicMode(bool isOn)
    {
        _cinematicModeUIController.CinematicModeChanged(isOn);
        _canvasGroup.alpha = isOn ? 0f : 1f;
    }

    private void OnCloseUpMechanic(bool isOn) 
    {
        _dayTimeCanvasGroup.alpha = isOn ? 0f : 1f;
        _currencyCanvasGroup.alpha = isOn ? 0f : 1f;
    }
}
