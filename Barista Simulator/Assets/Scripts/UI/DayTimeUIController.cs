using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DayTimeUIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _cafeStateText;
    [SerializeField] private Sprite _openSprite, _closedSprite;
    [SerializeField] private Image _imageCafeState;
    [SerializeField] private Slider _fillingTimeDay;
    [SerializeField] private TextMeshProUGUI _timeOfDayStateText;

    private void Start()
    {
        ResetTime();
        OnTimeStateChanged(SingletonManager.Instance.GameManager.CurrentTimeOfDayState);
        SingletonManager.Instance.GameManager.ChangedDay += OnChangedDay;
        SingletonManager.Instance.GameManager.ChangedTimeState += OnTimeStateChanged;
        SingletonManager.Instance.CafeManager.CafeStateChange += OnCafeStateChanged;
    }

    private void OnTimeStateChanged(TimeOfDay timeOfDay)
    {
        _timeOfDayStateText.text = timeOfDay.ToString();
    }

    private void OnDisable()
    {
        SingletonManager.Instance.GameManager.ChangedDay -= OnChangedDay;
        SingletonManager.Instance.CafeManager.CafeStateChange -= OnCafeStateChanged;
        SingletonManager.Instance.GameManager.ChangedTimeState -= OnTimeStateChanged;
    }

    private void Update()
    {
        UpdateTime(SingletonManager.Instance.CafeManager.TimeLeftInDayNormalized);
    }

    private void OnChangedDay(int day)
    {
        Debug.Log("Day should change to " + day);
        ResetTime();
    }

    private void OnCafeStateChanged(bool open)
    {
        if (open)
        {
            _imageCafeState.sprite = _openSprite;
            _cafeStateText.text = "OPEN";
        }
        else
        {
            _imageCafeState.sprite = _closedSprite;
            _cafeStateText.text = "CLOSED";
        }
    }

    private void UpdateTime(float time)
    {
        _fillingTimeDay.value = time;
    }

    private void ResetTime()
    {
        _fillingTimeDay.value = 0;
    }
}
