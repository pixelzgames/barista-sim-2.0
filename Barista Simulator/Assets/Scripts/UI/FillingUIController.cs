using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FillingUIController : MonoBehaviour
{
    [SerializeField] private Image _fillingCircle;
    
    public void StartFilling(float timeToCompletion, AnimationCurve easing = null)
    {
        _fillingCircle.fillAmount = 0f;
        if (easing == null)
        {
            _fillingCircle.DOFillAmount(1f, timeToCompletion).SetEase(Ease.Linear);
            return;
        }
        
        _fillingCircle.DOFillAmount(1f, timeToCompletion).SetEase(easing);
    }
    
    public void StopFilling()
    {
        _fillingCircle.DOKill();
        _fillingCircle.fillAmount = 0f;
    }

    private void OnDestroy()
    {
        _fillingCircle.DOKill();
    }
}
