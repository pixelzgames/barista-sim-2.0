using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "InteractionButtonFXConfig", menuName = "BRAG/InteractionButtonFX")]
public class InteractionButtonFXConfig : ScriptableObject
{
    [field:SerializeField] public AnimationCurve FillingBarAnimationCurve { get; private set; }
}
