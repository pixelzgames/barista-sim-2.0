using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

[RequireComponent(typeof(EventTrigger))]
public class ButtonBehaviourEffect : MonoBehaviour
{
    [FormerlySerializedAs("_effectConfigData")] [SerializeField] private ButtonBehaviourEffectConfig _effectConfig;
    
    private Button _button;

    private void Awake()
    {
        _button = GetComponent<Button>();
        
        var trigger = GetComponent<EventTrigger>();
        var entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Select
        };
        entry.callback.AddListener(OnSelected);
        trigger.triggers.Add(entry);
        
        entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Deselect
        };
        entry.callback.AddListener(OnDeselect);
        trigger.triggers.Add(entry);
        
        _button.onClick.AddListener(OnClick);
    }

    private void OnSelected(BaseEventData eventData)
    {
        _button.transform.DOScale(Vector3.one * _effectConfig.ScaleFactor, _effectConfig.TimeToScale).SetEase(_effectConfig.Ease);
    }

    private void OnDeselect(BaseEventData eventData)
    {
        _button.transform.DOScale(Vector3.one, _effectConfig.TimeToScale).SetEase(_effectConfig.Ease);
    }

    private void OnClick()
    {
        _button.transform.DOPunchScale(Vector3.one * 0.2f, 0.2f, 1, 0f).onComplete = () => 
        {
            _button.transform.localScale = Vector3.one;
        };
    }
}
