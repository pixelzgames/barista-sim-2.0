using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultButtonBehaviourEffect", menuName = "BRAG/New Button Behaviour Effect Config")]
public class ButtonBehaviourEffectConfig : ScriptableObject
{
    [field: SerializeField]
    public float ScaleFactor { get; private set; } = 1.1f;
    [field: SerializeField]
    public float TimeToScale { get; private set; } = 0.2f;
    [field: SerializeField]
    public Ease Ease { get; private set; } = Ease.OutBack;
}
