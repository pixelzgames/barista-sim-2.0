using UnityEngine;

public class UILookAtCamera : MonoBehaviour
{
    private Camera _mainCam;

    // Start is called before the first frame update
    private void Start()
    {
        _mainCam = Camera.main;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (_mainCam == null)
        {
            return;
        }

        transform.LookAt(_mainCam.transform);
    }
}
