using System;
using UnityEngine;
using UnityEngine.UI;

public class BRAGCanvasScaler : CanvasScaler
{
    [SerializeField] private UIOption _uiType;
    
    protected override void Awake()
    {
        base.Awake();
        GameOptions.GameOptionsChange += RefreshScale;
        RefreshScale();
    }

    private void RefreshScale()
    {
        var scale = _uiType switch
        {
            UIOption.Hud => GameOptions.UI_HUD_SCALER,
            UIOption.Gameplay => GameOptions.UI_GAMEPLAY_SCALER,
            _ => throw new ArgumentOutOfRangeException()
        };

        m_ScaleFactor = scale;
    }
}
