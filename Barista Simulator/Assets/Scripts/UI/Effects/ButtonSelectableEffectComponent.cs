using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class ButtonSelectableEffectComponent : MonoBehaviour
{
    [SerializeReference] private ButtonSelectableBase[] _effects;
    
    private void Awake()
    {
        var trigger = GetComponent<EventTrigger>();
      
        var entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Select
        };
        foreach (var effect in _effects)
        {
            entry.callback.AddListener(effect.OnSelected);
        }
        trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Deselect
        };
        foreach (var effect in _effects)
        {
            entry.callback.AddListener(effect.OnDeselected);
        }
        trigger.triggers.Add(entry);
    }
}
