using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonSelectedImageAppear : ButtonSelectableBase
{
   [SerializeField] private Image _image;

   private void OnDestroy()
   {
      _image.DOKill();
   }

   public override void OnSelected(BaseEventData data)
   {
      _image.DOFade(1f, 0.1f);
   }

   public override void OnDeselected(BaseEventData data)
   {
      _image.DOFade(0f, 0.1f);
   }
}
