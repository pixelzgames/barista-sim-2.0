using DG.Tweening;
using UnityEngine;

/// <summary>
/// Used as a way of auto killing tweens on Transform of a gameobject when added by another entity that can't track its tweens' lifecycle.
/// ie:
/// --> Object X starts a tween on object Y's transform
/// --> OnDestroy is called
/// --> Object Y Destroys
/// --> Object X still runs a Tween on loop on Object Y and throws
/// </summary>
public class TweenTransformHandler : MonoBehaviour
{
    private void OnDestroy()
    {
        transform.DOKill();
    }
}
