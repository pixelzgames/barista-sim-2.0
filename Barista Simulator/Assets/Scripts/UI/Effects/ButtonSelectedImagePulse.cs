using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSelectedImagePulse : ButtonSelectableBase
{
    [SerializeField] private Transform _transform;
    [SerializeField] private float _scale = 1.2f;
    [SerializeField] private float _duration = 1f;
    [SerializeField] private Ease _easing;

    private Sequence _sequence;

    private void OnEnable()
    {
        _sequence = DOTween.Sequence();
        _sequence.Append(_transform.DOScale(Vector3.one * _scale, _duration).SetEase(_easing))
            .Append(_transform.DOScale(Vector3.one , _duration).SetEase(_easing));
        _sequence.SetLoops(-1);
    }

    private void OnDestroy()
    {
        _sequence.Kill();
    }

    public override void OnSelected(BaseEventData data)
    {
        _sequence.Restart();
    }

    public override void OnDeselected(BaseEventData data)
    {
        _sequence.Pause();
    }
}
