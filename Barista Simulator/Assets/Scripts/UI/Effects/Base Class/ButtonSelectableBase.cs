using UnityEngine;
using UnityEngine.EventSystems;

public abstract class ButtonSelectableBase : MonoBehaviour
{
    public abstract void OnSelected(BaseEventData data);
    public abstract void OnDeselected(BaseEventData data);
}
