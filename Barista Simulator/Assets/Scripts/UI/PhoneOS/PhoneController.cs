using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PhoneController : MonoBehaviour
{
    public Action<bool> PhoneStatusUpdated = delegate {}; 
    
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject _blurryComponentPrefab;
    [SerializeField] private CanvasGroup _phoneCanvasGroup;
    [SerializeField] private BeanOSMainUIController _beanOSMainUIController;
    
    private bool _phoneOut;
    private static readonly int State = Animator.StringToHash("Out");

    private void Awake()
    {
        PlayPhoneAnimation();
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.Phone.performed += PhoneButton;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.Power.performed += PhoneButton;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.Phone.performed -= PhoneButton;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.Power.performed -= PhoneButton;
    }

    public void PhoneButton(InputAction.CallbackContext callbackContext)
    {
        OpenPhone();
    }

    public void OpenPhone(BeanOSAppName appToOpen = BeanOSAppName.None)
    {
        if (!PlayerInteractionManager.Instance.CanUsePhone)
        {
            return;
        }
        
        _phoneOut = !_phoneOut;
        _phoneCanvasGroup.interactable = _phoneOut;
        
        PlayPhoneAnimation();
        PhoneStatusUpdated?.Invoke(_phoneOut);
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.PhoneOpened));
        SingletonManager.Instance.InputManager.ChangeInputMapping(_phoneOut
            ? SingletonManager.Instance.InputManager.InputMaster.BeanOS
            : SingletonManager.Instance.InputManager.InputMaster.GenericGameplay);

        if (appToOpen == BeanOSAppName.None)
        {
            return;
        }
        
        OpenBeanOSApp(appToOpen);
    }

    private void OpenBeanOSApp(BeanOSAppName appName)
    {
        _beanOSMainUIController.OpenApp(appName);
    }

    private void PlayPhoneAnimation()
    {
        _blurryComponentPrefab.SetActive(_phoneOut);
        _animator.SetBool(State, _phoneOut);
    }
}

[Serializable]
public class BeanOSAppMapping
{
    public BeanOSAppName App;
    public BeanOSApp AppInstance;
}

public enum BeanOSAppName
{
    None,
    Messages,
    Settings,
    Social,
    Store,
    Reports
}
