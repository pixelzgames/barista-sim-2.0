using System.Collections.Generic;
using Backend.Grid;
using UnityEngine;
using UnityEngine.UI;

public class VerticalUIController : MonoBehaviour
{
   [SerializeField] private CanvasGroup _canvasGroup;
   [SerializeField] private Image _pointReference;
   [SerializeField] private float _unselectedScale = 0.5f;
   [SerializeField] private Transform _background;
   [SerializeField] private Color _unoccupiedColor;

   private GridCell _currentCell;
   private int _verticalIndex;
   private readonly List<Image> _currentVerticalPoints = new ();

   private void PopulateUIWithData()
   {
      CleanUIAndResetData();
      ShowVerticalUI(_currentCell.GridSockets.Count > 1);
      
      for (var i = 0; i < _currentCell.GridSockets.Count; i++)
      {
         var point = Instantiate(_pointReference, _background);
         if (i != _verticalIndex)
         {
            point.transform.localScale = Vector3.one * _unselectedScale;
         }

         if (!_currentCell.GridSockets[i].IsOccupied())
         {
            point.color = _unoccupiedColor;
         }
         
         _currentVerticalPoints.Add(point);
         point.gameObject.SetActive(true);
      }
   }

   private void CleanUIAndResetData()
   {
      foreach (var point in _currentVerticalPoints)
      {
         Destroy(point.gameObject);
      }
      
      _currentVerticalPoints.Clear();
   }
   
   public void InitializeUIWithGridCellData(GridCell cell, int verticalIndex)
   {
      _currentCell = cell;
      _verticalIndex = verticalIndex;
      PopulateUIWithData();
   }

   public void ShowVerticalUI(bool show)
   {
      _canvasGroup.alpha = show ? 1f : 0f;
   }

   public void UpdateCurrentVerticalIndex()
   {
      foreach (var point in _currentVerticalPoints)
      {
         point.transform.localScale = Vector3.one * _unselectedScale;
      }
      
      _currentVerticalPoints[_verticalIndex].transform.localScale = Vector3.one;
   }
}
