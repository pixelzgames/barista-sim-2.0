using Coffee.UIEffects;
using TMPro;
using UnityEngine;
using UIGradient = JoshH.UI.UIGradient;

public class FlavorDescriptorUIController : MonoBehaviour
{
    [SerializeField] private TMP_Text _nameTagText;
    [SerializeField] private UIGradient _gradient;
    [SerializeField] private UIShadow _shadow;
    

    public void InitWithData(CoffeeDescriptor coffeeDescriptorData)
    {
        _nameTagText.text = coffeeDescriptorData.TastingNote.ToString();
        _nameTagText.color = coffeeDescriptorData.ColorScheme.FontColor;
        _gradient.LinearGradient.colorKeys = coffeeDescriptorData.ColorScheme.Gradient.colorKeys;
        _shadow.effectColor = coffeeDescriptorData.ColorScheme.ShadowColor;
    }
}
