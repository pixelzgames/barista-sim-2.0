﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class CurrenciesUIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _cashText;
    [SerializeField] private TextMeshProUGUI _beanPointsText;
    [SerializeField] private MoneyPopupElement _popupElementPrefabMoney;
    [SerializeField] private Transform _verticalMoneyGrid;
    [SerializeField] private Transform _verticalBeanPointsGrid;
    [SerializeField] private MoneyPopupElement _popupElementPrefabBeanPoints;

    private void Start()
    {
        _cashText.text = SingletonManager.Instance.CurrenciesManager.Data.CurrentMoney.ToString();
        SingletonManager.Instance.CurrenciesManager.MoneyChangedBy += UpdateCashUI;
        SingletonManager.Instance.CurrenciesManager.BeanPointsChangedBy += UpdateBeanPointsUI;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.CurrenciesManager.MoneyChangedBy -= UpdateCashUI;
        SingletonManager.Instance.CurrenciesManager.BeanPointsChangedBy -= UpdateBeanPointsUI;
    }

    private void UpdateCashUI(int value)
    {
        var animatedValue = SingletonManager.Instance.CurrenciesManager.Data.CurrentMoney - value;
        Instantiate(_popupElementPrefabMoney, _verticalMoneyGrid).InitializeData(value);

        DOTween.To(() => animatedValue, x => animatedValue = x, SingletonManager.Instance.CurrenciesManager.Data.CurrentMoney, 1f)
            .onUpdate = () =>
        {
            _cashText.text = animatedValue.ToString();
        };
    }
    
    private void UpdateBeanPointsUI(int value)
    {
        var animatedValue = SingletonManager.Instance.CurrenciesManager.Data.CurrentBeanPoints - value;
        Instantiate(_popupElementPrefabBeanPoints, _verticalBeanPointsGrid).InitializeData(value);

        DOTween.To(() => animatedValue, x => animatedValue = x, SingletonManager.Instance.CurrenciesManager.Data.CurrentBeanPoints, 1f)
            .onUpdate = () =>
        {
            _beanPointsText.text = animatedValue.ToString();
        };
    }
}
