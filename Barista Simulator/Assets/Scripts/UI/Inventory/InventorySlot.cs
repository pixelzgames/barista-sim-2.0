using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Pickable PickableInSlot => _pickableInSlot;
    public int SlotIndex { get; private set; }

    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private Image _icon;
    
    private Pickable _pickableInSlot;

    public void InitSlotData(Pickable pickable, int index)
    {
        SlotIndex = index;
        if (pickable != null)
        {
            _pickableInSlot = pickable;
            _text.text = _pickableInSlot.InteractableName;
            _icon.sprite = _pickableInSlot.InteractableIcon;
        }
        
        gameObject.SetActive(true);
    }
}
