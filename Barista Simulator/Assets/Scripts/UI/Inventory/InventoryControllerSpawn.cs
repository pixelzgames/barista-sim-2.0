using System;

public class InventoryControllerSpawn : InventoryControllerBase
{

    public Action<InventorySlot> OnButtonPressed = delegate {  };
    
    public override void OnInventoryButton(InventorySlot slot)
    { 
        if (slot.PickableInSlot == null)
        {
            return;
        }
        
        if (PlayerInteractionManager.Instance.CurrentlyInMainHand != null)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Hands are already occupied!"));
            return;
        }
        
        OnButtonPressed.Invoke(slot);
        OnBackButton();
    }
}
