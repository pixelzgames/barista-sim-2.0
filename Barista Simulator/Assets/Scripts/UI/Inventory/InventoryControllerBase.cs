using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public abstract class InventoryControllerBase : MonoBehaviour
{
    public Action OnClosed = delegate {  };

    [SerializeField] private Image _background;
    [SerializeField] private GameObject _backButton;
    [SerializeField] private GameObject _slot;

    /// <summary>
    /// Initialize this inventory UI with the provided interactables
    /// </summary>
    /// <param name="interactablesInSlot">Objects contained in the inventory</param>
    /// <param name="slotCount">Number of slots</param>
    public void InitializeUI(Pickable[] interactablesInSlot, int slotCount)
    {
        _background.transform.localScale = Vector3.zero;
        _background.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);
        
        SingletonManager.Instance.InputManager.InputMaster.UI.Back.performed += JoystickBackPressed;

        for (var i = 0; i < slotCount; i++)
        {
            var newSlot = Instantiate(_slot, _slot.transform.parent);
            newSlot.GetComponent<InventorySlot>()
                .InitSlotData(i >= interactablesInSlot.Length ? null : interactablesInSlot[i],i);

            if (i == 0)
            {
                EventSystem.current.SetSelectedGameObject(newSlot);
            }
        }
    }

    public abstract void OnInventoryButton(InventorySlot slot);

    public void OnBackButton()
    {
        SingletonManager.Instance.InputManager.InputMaster.UI.Back.performed -= JoystickBackPressed;
        OnClosed.Invoke();
        Destroy(gameObject);
    }

    private void JoystickBackPressed(InputAction.CallbackContext obj)
    {
        OnBackButton();
    }
}
