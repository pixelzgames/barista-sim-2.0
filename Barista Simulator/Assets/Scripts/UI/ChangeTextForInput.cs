﻿using System;
using UnityEngine;
using TMPro;
// ReSharper disable RedundantTernaryExpression

public class ChangeTextForInput : MonoBehaviour
{
    private InputManager _inputManager;
    private TextMeshProUGUI[] _texts;


    private void Start()
    {
        _inputManager = FindFirstObjectByType<InputManager>();
        _inputManager.DeviceChanged += ChangeTextValue;
        _texts = GetComponentsInChildren<TextMeshProUGUI>();
        ChangeTextValue(_inputManager.CurrentDevice);
    }

    private void OnDestroy()
    {
        if(_inputManager)
            _inputManager.DeviceChanged -= ChangeTextValue;
    }

    private void ChangeTextValue(InputManager.Devices device)
    {
        switch (device)
        {
            case InputManager.Devices.Keyboard:
                for (int i = 0; i < _texts.Length; i++)
                {
                    _texts[i].gameObject.SetActive(i == 0 ? true : false);
                }
                break;
            case InputManager.Devices.Xbox:
                for (int i = 0; i < _texts.Length; i++)
                {
                    _texts[i].gameObject.SetActive(i == 1 ? true : false);
                }
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(device), device, null);
        }
    }
}
