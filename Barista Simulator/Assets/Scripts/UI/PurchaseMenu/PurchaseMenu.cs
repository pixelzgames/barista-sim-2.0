using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PurchaseMenu : MonoBehaviour
{
    [SerializeField] [Tooltip("Product list item prefab to instantiate in product purchase list.")]
    private GameObject _productListItemPrefab;

    [SerializeField, Tooltip("Parent game object where we instantiate the PurchaseItems.")]
    private GameObject _viewport;

    [SerializeField, Tooltip("PurchaseItem prefab to instantiate for each product in menu.")]
    private GameObject _purchaseItemObject;
    
    [SerializeField, Tooltip("GameObject to instantiate purchase items in.")]
    private GameObject _purchaseItemListGO;
    
    [SerializeField, Tooltip("Reference for total price text.")]
    private TextMeshProUGUI _totalPriceText;

    [SerializeField, Tooltip("Reference to menu go.")]
    private GameObject _menu;

    [SerializeField]
    private List<ProductConfig> _productList;

    private readonly List<ProductConfig> _purchaseList = new();
    private int _totalPrice;

    #region Singleton
    public static PurchaseMenu Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }
    #endregion

    private void OnEnable()
    {
        foreach (var product in _productList)
        {
            CreatePurchaseItem(product, _purchaseItemObject, _viewport);
        }
    }
    
    public void AddProductToList(ProductConfig productConfig)
    {
        CreatePurchaseItem(productConfig, _productListItemPrefab, _purchaseItemListGO);
        _purchaseList.Add(productConfig);
        UpdatePrice(_totalPrice + productConfig.Product.Price);
    }

    public void ConfirmPurchase()
    {
        // Check player's current cash for current product price.
        if (SingletonManager.Instance.CurrenciesManager.Data.CurrentMoney >= _totalPrice)
        {
            // Purchase items in purchase list.
            foreach (var product in _purchaseList)
            {
                SingletonManager.Instance.CafeManager.AddShipment(product.Product);
              
                SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData(
                    "Bought " + product.Product.ProductName, "", "Arriving in"  + product.Product.DeliveryTimeInDays + " days.", 5f, NotificationSize.Main));
            }
            
            SingletonManager.Instance.CurrenciesManager.ChangeCurrentMoneyBy(-_totalPrice);
            ActivateMenu(false);
        }
        else
        {
            //Display insufficient funds message.
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Insufficient funds."));
        }
    }

    public void ActivateMenu(bool on)
    {
        _menu.SetActive(on);
        ResetPurchaseList();
        UpdatePrice(0);
    }

    private void ResetPurchaseList()
    {
        foreach(Transform child in _purchaseItemListGO.transform)
        {
            Destroy(child.gameObject);
        }

        _purchaseList.Clear();
    }

    private void UpdatePrice(int price)
    {
        _totalPrice = price;
        _totalPriceText.text = "$" + _totalPrice;
    }

    private void CreatePurchaseItem(ProductConfig productConfig, GameObject prefab,  GameObject parent)
    {
        var newPurchaseItem= Instantiate(prefab, parent.transform).GetComponent<PurchaseItem>();

        // Set product data
        newPurchaseItem.ProductConfig = productConfig;
        newPurchaseItem.UpdateInfo();
    }
}
