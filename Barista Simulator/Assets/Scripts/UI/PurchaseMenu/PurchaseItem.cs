using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

/// <summary>
/// Component to hold UI elements of purchase item.
/// </summary>
public class PurchaseItem : MonoBehaviour
{
    [FormerlySerializedAs("ProductImage")] [SerializeField]
    private Image _productImage;

    [FormerlySerializedAs("ProductNameText")] [SerializeField]
    private TextMeshProUGUI _productNameText;

    [FormerlySerializedAs("PriceText")] [SerializeField]
    private TextMeshProUGUI _priceText;

    [FormerlySerializedAs("_product")] [FormerlySerializedAs("Product")] [SerializeField]
    private ProductConfig _productConfig;

    public ProductConfig ProductConfig
    {
        get => _productConfig;
        set => _productConfig = value;
    }

    public void UpdateInfo()
    {
        if (_productConfig == null) 
            return;
        
        if (_productImage != null)
            _productImage.sprite = _productConfig.Product.Icon;
        if (_productNameText != null)
            _productNameText.text = _productConfig.Product.ProductName;
        if (_priceText != null)
            _priceText.text = _productConfig.Product.Price.ToString();
    }

    public void DisplayConfirmation()
    {
        PurchaseMenu.Instance.AddProductToList(_productConfig);
    }   
}
