using UnityEngine;
using UnityEngine.UI;

public class LevelDataButton : MonoBehaviour
{
    [SerializeField] private LevelData _levelToLoad;
    
    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(Execute);
    }

    private void Execute()
    {
        Time.timeScale = 1f;
        SingletonManager.Instance.GameManager.LoadALevel(_levelToLoad);
    }
}
