﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UI;

/// <summary>
/// Handles the interaction UI elements.
/// </summary>
public class InteractionUIController : MonoBehaviour
{
    public event Action<float> AnyInteractionHoldStart = delegate {  };
    public event Action AnyInteractionHoldStop = delegate {  };

    public VerticalUIController VerticalUIController => _verticalUI;
    
    [Header("Canvas Groups")]
    [SerializeField]
    [Tooltip("Reference to the contextual button UI that appears on objects themselves.")]
    private CanvasGroup _contextualUI;

    [SerializeField]
    [Tooltip("Reference to the corner button UI that appears when objects are in the player's hands.")]
    private CanvasGroup _lowerActionsCanvas;
    
    [Header("Button Elements")]
    [SerializeField]
    private InteractionButtonUI[] _mainButtons = new InteractionButtonUI[4];
    
    //TODO a faire esti la
    [Header("Corner Elements")]
    [SerializeField]
    private InteractionButtonUI[] _lowerButtons = new InteractionButtonUI[2];

    [Header("Info Elements")] 
    [SerializeField] private InfoPanelController _heldCornerInfo;

    [SerializeField] private InfoPanelController _highlightedCornerInfo;
    
    [Header("Vertical UI")]
    [SerializeField] private VerticalUIController _verticalUI;
    
    [FormerlySerializedAs("_radialUIController")]
    [Header("Duplicate UI")]
    [SerializeField] private DuplicateInteractionUIController _duplicateUIController;
    
    private Pickable _heldInteractable;
    private Interactable _highlightedInteractable;
    private bool _duplicatedShown;

    private void Awake()
    {
        SingletonManager.Instance.SetInteractionUIController(this);

        foreach (var button in _mainButtons)
        {
            button.AnyInputHoldStart += OnAnyInteractionStarted;
            button.AnyInputHoldStop += OnAnyInteractionStopped;
        }
    }

    private void OnDestroy()
    {
        _contextualUI.DOKill();
        
        foreach (var button in _mainButtons)
        {
            button.AnyInputHoldStart -= AnyInteractionHoldStart;
            button.AnyInputHoldStop -= AnyInteractionHoldStop;
            button.HideUI();
        }
    }

    private void OnAnyInteractionStarted(float holdTime)
    {
        AnyInteractionHoldStart?.Invoke(holdTime);
    }
    
    private void OnAnyInteractionStopped()
    {
        AnyInteractionHoldStop?.Invoke();
    }
    
    private void Start()
    {
        ShowContextualUI(null,Array.Empty<ActionInputConfig>());
    }

    /// <summary>
    /// Sets the current held interactable.
    /// </summary>
    /// <param name="interactable"></param>
    public void SetHeldInteractable(Pickable interactable)
    {
        _heldInteractable = interactable;
        ShowHeldInteractions(interactable != null);
        ShowHeldObjectInfo(_heldInteractable != null);
    }

    /// <summary>
    /// Show a set of static interactions at the bottom center for currently held Pickable (Throw, Drop etc.)
    /// </summary>
    /// <param name="show"></param>
    private void ShowHeldInteractions(bool show)
    {
        //TODO connect Front-end with buttons
        if (show && _heldInteractable.CanThrow)
        {
            _lowerActionsCanvas.DOFade(1f, 0.2f);
        }
        else
        {
            _lowerActionsCanvas.DOFade(0f, 0.1f);
        }
    }

    public void ShowDuplicateInteractions(bool show, ActionInputConfig[] duplicateActions)
    {
        if (duplicateActions == null || duplicateActions.Length == 0)
        {
            HideDuplicateInteractions();
            return;
        }
        
        if (show && !_duplicatedShown)
        {
            _duplicateUIController.Show(duplicateActions);
            _duplicatedShown = true;
            var buttonType = duplicateActions[0].Button;
            foreach (var button in _mainButtons)
            {
                if (button.Config == null || button.Config.Button == buttonType)
                {
                    button.SetStatus(InteractionButtonUI.ButtonStatus.Locked);
                    continue;
                }
                
                button.SetStatus(InteractionButtonUI.ButtonStatus.Hidden);
            }
        }
        else if (_duplicatedShown)
        {
            HideDuplicateInteractions();
        }
    }

    private void HideDuplicateInteractions()
    {
        _duplicateUIController.Hide();
        _duplicatedShown = false;
        foreach (var button in _mainButtons)
        {
            button.SetStatus(InteractionButtonUI.ButtonStatus.None);
        }
    }

    public ActionInputConfig GetSelectedDuplicateAction(float inputTime)
    {
        return _duplicateUIController.GetActionSelection(inputTime);
    }
    
    /// <summary>
    /// Show the contextual UI for interactions based on provided Actions
    /// </summary>
    /// <param name="interactable">Which interactable are we looking at</param>
    /// <param name="actions">Which actions can we execute</param>
    public void ShowContextualUI(Interactable interactable, ActionInputConfig[] actions)
    {
        //Turn off all the UI buttons to enable only the ones we need
        foreach (var button in _mainButtons)
        {
            button.HideUI();
        }
        
        if (_contextualUI == null || interactable == null)
        {
            return;
        }

        //Set the current highlighted interactable
        _highlightedInteractable = interactable;
        
        foreach (var actionInput in actions)
        {
            switch (actionInput.Button)
            {
                case ActionInputConfig.ActionButton.MainAction:
                    _mainButtons[0].UpdateButtonUI(actionInput);
                    break;
                case ActionInputConfig.ActionButton.SecondaryAction:
                    _mainButtons[1].UpdateButtonUI(actionInput);
                    break;
                case ActionInputConfig.ActionButton.ThirdAction:
                    _mainButtons[2].UpdateButtonUI(actionInput);
                    break;
                case ActionInputConfig.ActionButton.FourthAction:
                    _mainButtons[3].UpdateButtonUI(actionInput);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        _contextualUI.DOKill();
        _contextualUI.DOFade(1f, 0.2f);
        
        _highlightedCornerInfo.ShowContextualUI(true, _highlightedInteractable);
    }

    public void HideContextualUI()
    {
        _highlightedInteractable = null;
        _highlightedCornerInfo.ShowContextualUI(false);
        
        foreach (var button in _mainButtons)
        {
            button.UpdateButtonUI(null);
        }
        
        _contextualUI.DOKill();
        _contextualUI.DOFade(0f, 0.1f);
    }
    
    private void ShowHeldObjectInfo(bool show)
    {
       _heldCornerInfo.ShowContextualUI(show,_heldInteractable);
    }

    /// <summary>
    /// Should be called everytime a data or state changes on any interactable. Refreshes the front end only
    /// </summary>
    public void RefreshInfoCanvases()
    {
        _heldCornerInfo.SetCanvasInfo(_heldInteractable);
        _highlightedCornerInfo.SetCanvasInfo(_highlightedInteractable);
    }

    [Serializable]
    public class InteractionButtonUI
    {
        public event Action<float> AnyInputHoldStart = delegate {  };
        public event Action AnyInputHoldStop = delegate {  };
        
        public ActionInputConfig Config => _config;
        
        [Tooltip("Reference to parent of button.")]
        //make these private
        public GameObject gameObject;

        [SerializeField] private TextMeshProUGUI _keyboardLetter, _xboxLetter;
        [SerializeField] private TextMeshProUGUI _actionText;
        [SerializeField] private  Image _ring;
        [SerializeField] private  Image _fillBar;
        [SerializeField, Required] private InteractionButtonFXConfig _interactionButtonFXConfig;

        private ActionInputConfig _config;
        private ButtonStatus _buttonStatus;
        
        public enum ButtonStatus
        {
            None = 0,
            Locked = 1,
            Hidden = 2
        }
        
        public void SetStatus(ButtonStatus buttonStatus)
        {
            _buttonStatus = buttonStatus;
        }
        
        public void UpdateButtonUI(ActionInputConfig config)
        {
            if (_buttonStatus == ButtonStatus.Hidden)
            {
                HideUI();
                return;
            }
            
            if (config == null)
            {
                AnyInputHoldStop?.Invoke();
                SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= InteractionStarted;
                SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.started -= Interaction2Started;
                SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.started -= Interaction3Started;
                SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.started -= Interaction4Started;
                return;
            }
            
            _config = config;
            _actionText.text = config.ActionDisplayName;
            switch (_config.Button)
            {
                case ActionInputConfig.ActionButton.MainAction:
                    _keyboardLetter.text =  "E";
                    _xboxLetter.text = "A";
                    
                    SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started += InteractionStarted;
                    break;
                
                case ActionInputConfig.ActionButton.SecondaryAction:
                    _keyboardLetter.text =  "R";
                    _xboxLetter.text = "B";
                    
                    SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.started += Interaction2Started;
                    break;
                
                case ActionInputConfig.ActionButton.ThirdAction:
                    _keyboardLetter.text =  "Q";
                    _xboxLetter.text = "X";
                    
                    SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.started += Interaction3Started;
                    break;
                
                case ActionInputConfig.ActionButton.FourthAction:
                    _keyboardLetter.text =  "F";
                    _xboxLetter.text = "Y";
                    
                    SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.started += Interaction4Started;
                    break;
                
                default:
                    return;
            }
            
            gameObject.SetActive(true);

            if (_buttonStatus == ButtonStatus.Locked)
            {
                LockUI();
            }
        }
        
        private void InteractionStarted(InputAction.CallbackContext context)
        {
            _ring.fillAmount = 0f;
            _ring.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve).onKill = OnComplete;
            _fillBar.fillAmount = 0f;
            _fillBar.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve);
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= InteractionStarted;

            AnyInputHoldStart?.Invoke(_config.HoldTime);
        }
        private void Interaction2Started(InputAction.CallbackContext context)
        {
            _ring.fillAmount = 0f;
            _ring.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve).onKill = OnComplete;
            _fillBar.fillAmount = 0f;
            _fillBar.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve);
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= InteractionStarted;

            AnyInputHoldStart?.Invoke(_config.HoldTime);
        }
        private void Interaction3Started(InputAction.CallbackContext context)
        {
            _ring.fillAmount = 0f;
            _ring.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve).onKill = OnComplete;
            _fillBar.fillAmount = 0f;
            _fillBar.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve);
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= InteractionStarted;

            AnyInputHoldStart?.Invoke(_config.HoldTime);
        }
        private void Interaction4Started(InputAction.CallbackContext context)
        {
            _ring.fillAmount = 0f;
            _ring.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve).onKill = OnComplete;
            _fillBar.fillAmount = 0f;
            _fillBar.DOFillAmount(1f, _config.HoldTime).SetEase(_interactionButtonFXConfig.FillingBarAnimationCurve);
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= InteractionStarted;

            AnyInputHoldStart?.Invoke(_config.HoldTime);
        }

        private void OnComplete()
        {
            gameObject.transform.DOPunchScale(Vector3.one * 0.2f, 0.2f, 1, 0f);
        }

        public void InteractionStopped(InputAction.CallbackContext context)
        {
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= InteractionStarted;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.canceled -= InteractionStopped;

            _ring.DOKill();
            _ring.fillAmount = 0f;
            _fillBar.DOKill();
            _fillBar.fillAmount = 0f;
            AnyInputHoldStop?.Invoke();
        }
        
        private void Interaction2Stopped(InputAction.CallbackContext context)
        {
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.started -= Interaction2Started;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.canceled -= Interaction2Stopped;

            _ring.DOKill();
            _ring.fillAmount = 0f;
            _fillBar.DOKill();
            _fillBar.fillAmount = 0f;
            AnyInputHoldStop?.Invoke();
        }

        private void Interaction3Stopped(InputAction.CallbackContext context)
        {
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.started -= Interaction3Started;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.canceled -= Interaction3Stopped;

            _ring.DOKill();
            _ring.fillAmount = 0f;
            _fillBar.DOKill();
            _fillBar.fillAmount = 0f;
            AnyInputHoldStop?.Invoke();
        }

        private void Interaction4Stopped(InputAction.CallbackContext context)
        {
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.started -= Interaction4Started;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.canceled -= Interaction4Stopped;

            _ring.DOKill();
            _ring.fillAmount = 0f;
            _fillBar.DOKill();
            _fillBar.fillAmount = 0f;
            AnyInputHoldStop?.Invoke();
        }

        public void HideUI()
        {
            LockUI();
            gameObject.SetActive(false);
            gameObject.transform.localScale = Vector3.one;
        }
        
        public void LockUI()
        {
            InteractionStopped(default);
            Interaction2Stopped(default);
            Interaction3Stopped(default);
            Interaction4Stopped(default);
        }
    }
}
