using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class MainMenuController : BRAGBehaviour
{
    [SerializeField] private BRAGCanvasController _mainMenuCanvas;
    [SerializeField] private CinemachineVirtualCamera _mainCamera;
    [SerializeField] private BRAGCanvasController _settingCanvas;
    [SerializeField] private CinemachineVirtualCamera _settingsCamera;

    private MainMenuState _currentState = MainMenuState.MainScreen;
    private InputMaster _inputMaster;

    [SerializeField]
    private UnityEvent _onBackButtonPressed;

    [SerializeField] private Button _continueButton;
    [SerializeField] private GameObject _disabledContinueButton;

    private bool _isLoading => _currentState == MainMenuState.Loading;

    private void Start()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        SingletonManager.Instance.InputManager.ChangeInputMapping(
            SingletonManager.Instance.InputManager.InputMaster.UI);
        _inputMaster.UI.Back.performed += OnBackButton;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        _inputMaster.UI.Back.performed -= OnBackButton;
    }

    protected override void OnLevelLoaded(LevelData levelData)
    {
        if (!FileGameDataHandler<PlayerData>.HasSaveFile())
        {
            _continueButton.gameObject.SetActive(false);
            _disabledContinueButton.SetActive(true);
            return;
        }
        
        EventSystem.current.SetSelectedGameObject(_continueButton.gameObject);
    }

    public void OnContinueButton()
    {
        if (_isLoading)
        {
            return;
        }
        
        _currentState = MainMenuState.Loading;
        
        var lastActiveLevelType = SingletonManager.Instance.ProgressionManager.Data.LastGameplayScene;
        var levelNameToLoad = 
            SingletonManager.Instance.ProgressionManager.GetCurrentSceneToLoadType(lastActiveLevelType);
        
        var data = new LevelData
        {
            LevelName = levelNameToLoad,
            LevelType = lastActiveLevelType,
            CommutingType = CommutingType.None
        };
        
        SingletonManager.Instance.GameManager.LoadALevel(data);
    }
    
    public void NewGameButton()
    {
        if (_isLoading)
        {
            return;
        }
        
        var modalWindow = new ModalWindowData("New Game",
            "Are you sure you want to start a new game and overwrite the previous save file?", null, false,
            () =>
            {
                var data = new LevelData
                {
                    LevelName = SingletonManager.Instance.ProgressionManager.GetCurrentSceneToLoadType(Level.Home),
                    LevelType = Level.Home,
                    CommutingType = CommutingType.None
                };

                _currentState = MainMenuState.Loading;
                SingletonManager.Instance.DataPersistenceManager.NewGame();
                SingletonManager.Instance.GameManager.LoadALevel(data);
            },
            OnMainMenuButton);
        
            SingletonManager.Instance.GameManager.ShowModalWindow(modalWindow);
    }

    public void OnMainMenuButton()
    {
        _currentState = MainMenuState.MainScreen;
        CameraSwitch();
    }
    
    public void OnSettingsButton()
    {
        _currentState = MainMenuState.Settings;
        CameraSwitch();
    }

    private void CameraSwitch()
    {
        switch (_currentState)
        {
            case MainMenuState.MainScreen:
                _settingCanvas.CanvasStateChange += SettingCanvasStateChange;
                _settingCanvas.DisableCanvas(1f);
                _settingsCamera.Priority = 49;
                _mainCamera.Priority = 100;
                break;
            case MainMenuState.Settings:
                _settingCanvas.EnableCanvas();
                _settingsCamera.Priority = 100;
                _mainCamera.Priority = 49;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void SettingCanvasStateChange(bool isEnabled)
    {
        if (!isEnabled)
        {
            _settingCanvas.CanvasStateChange -= SettingCanvasStateChange;
            _mainMenuCanvas.EnableCanvas(0.5f);
        }
    }

    public void OnExitGameButton()
    {
        var modalWindow = new ModalWindowData("Exit Game", "Are you sure you want to exit the game?", null, false, () => QuitGame(true), () => QuitGame(false));
        SingletonManager.Instance.GameManager.ShowModalWindow(modalWindow);
    }

    private void QuitGame(bool shouldQuit)
    {
        if (shouldQuit)
        {
            Application.Quit();
        }
    }

    private void OnBackButton(InputAction.CallbackContext obj)
    {
        _onBackButtonPressed?.Invoke();
    }

    private enum MainMenuState
    {
        None = 0, 
        MainScreen = 1,
        Settings = 2,
        Loading = 3
    }
}
