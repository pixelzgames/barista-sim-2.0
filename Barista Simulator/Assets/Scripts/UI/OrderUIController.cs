﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class OrderUIController : MonoBehaviour
{
    [SerializeField] private Image _timerBar;
    [SerializeField] private TextMeshProUGUI _recipeNameText;
    [SerializeField] private TextMeshProUGUI _recipeTypeSize;
    [SerializeField] private FlavorDescriptorUIController _tastingNoteController;

    [SerializeField]
    private DTT.COLOR.PaletteColorSelection _successColor;
    
    [SerializeField]
    private DTT.COLOR.PaletteColorSelection _failColor;

    private CanvasGroup _canvasGroup;
    private Transform _element;
    private bool _inited;
    private Order _myOrder;

    private void Awake()
    {
        _myOrder = GetComponent<Order>();
        _myOrder.OnInitialized += GetOrderData;
        _myOrder.OnFailure += Fail;
        _myOrder.OnSuccess += OrderDelivered;
        _element = transform.GetChild(0);
        _canvasGroup = _element.GetComponent<CanvasGroup>();
        _timerBar.color = DTT.HUD.Order_OnTime;
    }

    private void TimerStateChanged(TimerState state)
    {
        var color = Color.white;
        switch (state)
        {
            case TimerState.Started:
                color = DTT.HUD.Order_OnTime;
                break;
            case TimerState.Warning:
                color = DTT.HUD.Order_Moderate;
                break;
            case TimerState.EndingImminent:
                color = DTT.HUD.Order_Late;
                break;
            case TimerState.Ended:
                break;
        }
        
        _timerBar.color = color;
    }

    private void OnDisable()
    {
        _element.DOKill();
        _myOrder.OnInitialized -= GetOrderData;
        _myOrder.OnFailure -= Fail;
        _myOrder.OnSuccess -= OrderDelivered;
        _myOrder.OrderTimer.TimerStateChanged -= TimerStateChanged;
    }

    private void GetOrderData()
    {
        _timerBar.fillAmount = _myOrder.OrderTimer.TimeRemaining / _myOrder.InitialTime;
        _recipeNameText.text = _myOrder.Recipe.RecipeName;
        _recipeTypeSize.text = _myOrder.Type.ToString();

        if (_myOrder.RequiredTastingNoteByCustomer != TastingDescriptor.OPENED)
        {
            var coffeeDescriptor = new CoffeeDescriptor(_myOrder.RequiredTastingNoteByCustomer);
            _tastingNoteController.InitWithData(coffeeDescriptor);
            _tastingNoteController.gameObject.SetActive(true);
        }
        
        ShowOrder();
        _inited = true;
        _myOrder.OrderTimer.TimerStateChanged += TimerStateChanged;
    }

    private void Update()
    {
        if (!_inited)
        {
            return;
        }
        
        UpdateFillBar();
    }

    private void UpdateFillBar()
    {
        _timerBar.fillAmount = _myOrder.OrderTimer.TimeRemaining / _myOrder.InitialTime;
    }

    private void Fail()
    {
        _element.GetComponent<Image>().DOColor(_failColor.PaletteColor.Color, 0.2f);
        HideOrder();
    }

    private void OrderDelivered()
    {
        _timerBar.enabled = false;
        _element.GetComponent<Image>().DOColor(_successColor.PaletteColor.Color, 0.2f);
        _element.DOPunchScale(Vector3.one * 0.2f, 0.2f, 1, 0f);
        HideOrder();
    }

    private void ShowOrder()
    {
        _element.DOPunchScale(Vector3.one * 0.2f, 0.2f, 1,0f);
    }

    private void HideOrder(float delay = 2f)
    {
        _element.DOMoveX(transform.position.x - 200f, 0.5f).SetDelay(delay);
        _canvasGroup.DOFade(0f, 0.5f).SetDelay(delay);
        Destroy(gameObject, delay + 2f);
    }
}
