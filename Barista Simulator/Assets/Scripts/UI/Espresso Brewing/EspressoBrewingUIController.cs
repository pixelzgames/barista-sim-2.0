using UnityEngine;
using UnityEngine.UI;

public class EspressoBrewingUIController : MonoBehaviour
{
    [Header("Image properties")] 
    [SerializeField]
    private Image _espressoImage;
    [SerializeField] 
    private Image _lungoImage;
    [SerializeField] 
    private Image _godshotImage;
    [SerializeField] 
    private Image _dialedInImage;
    [SerializeField] 
    private FillingUIController _fillingController;
    
    public void SetData(EspressoBrewingConfig data, bool isDialedIn = true)
    {
        _fillingController.StartFilling(data.ExtractionTime, data.ExtractionCurve);
        
        // Espresso data
        _espressoImage.rectTransform.localRotation = Quaternion.Euler(0, 0, -data.EspressoRangePosition);
        var espressoRange = data.EspressoRangeAnglePercentage / 100f;
        _espressoImage.fillAmount = espressoRange;

        // Lungo data
        // +1 is just to avoid a little gap in the UI
        _lungoImage.rectTransform.localRotation = Quaternion.Euler(0, 0, (espressoRange * -360f) + 1);
        _lungoImage.fillAmount = data.LungoRangeAnglePercentage / 100f;

        if (isDialedIn)
        {
            _dialedInImage.fillAmount = 0f;
        }
        else
        {
            _dialedInImage.fillAmount = _lungoImage.fillAmount + _espressoImage.fillAmount;
            return;
        }
        
        //Godshot data
        var godShotAngle = data.GodshotAngleAngleAndPosition.y - data.GodshotAngleAngleAndPosition.x;
        _godshotImage.fillAmount = godShotAngle * espressoRange;
        _godshotImage.rectTransform.localRotation =
            Quaternion.Euler(0, 0, data.GodshotAngleAngleAndPosition.x * 360 * -espressoRange);
    }
}
