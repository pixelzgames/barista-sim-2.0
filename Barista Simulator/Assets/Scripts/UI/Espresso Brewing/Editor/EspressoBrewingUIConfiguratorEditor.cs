using UnityEditor;

[CustomEditor(typeof(EspressoBrewingUIConfigurator))]
public class EspressoBrewingUIConfiguratorEditor : Editor
{
   public override void OnInspectorGUI()
   {
      DrawDefaultInspector();

      var script = target as EspressoBrewingUIConfigurator;
      if (!script)
      {
         return;
      }
      
      script.RunConfiguration();
      Repaint();
      EditorUtility.SetDirty(script);
   }
}
