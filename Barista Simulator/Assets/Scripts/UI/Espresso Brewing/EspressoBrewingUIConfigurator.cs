using UnityEngine;

[RequireComponent(typeof(EspressoBrewingUIController))]
public class EspressoBrewingUIConfigurator : MonoBehaviour
{
    [SerializeField] private EspressoBrewingUIController _controller;
    
    [Header("Configuration"), Space(3)]
    [SerializeField] private EspressoBrewingConfig _config;

    [SerializeField] private bool _isDialedIn;
    public void RunConfiguration()
    {
        if (!_config || !_controller)
        {
            return;
        }
        
        _controller.SetData(_config, _isDialedIn);
    }
}
