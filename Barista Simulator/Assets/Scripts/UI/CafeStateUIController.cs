using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CafeStateUIController : MonoBehaviour
{
   public event Action OnAnimationFinished = delegate { };
   
   [SerializeField] private CanvasGroup _canvasGroup;
   
   [SerializeField] private Color _openColor, _closeColor;
   [SerializeField] private string _openText, _closeText;
   [SerializeField] private Sprite _openImage, _closeImage;

   [SerializeField] private TextMeshProUGUI _text;
   [SerializeField] private Image _backgroundImage;
   [SerializeField] private Image _spriteIcon;

   [SerializeField] private Animator _animator;
   private static readonly int Show = Animator.StringToHash("Show");
   
   public void PlayCanvasAnimation(bool open)
   {
      if (open)
      {
         _backgroundImage.color = _openColor;
         _text.text = _openText;
         _spriteIcon.sprite = _openImage;
      }
      else
      {
         _backgroundImage.color = _closeColor;
         _text.text = _closeText;
         _spriteIcon.sprite = _closeImage;
      }

      StartCoroutine(WaitToEndAnimation(3f, !open));
   }

   private IEnumerator WaitToEndAnimation(float time, bool closed)
   {
      _animator.SetTrigger(Show);
      _canvasGroup.alpha = 1f;
      yield return new WaitForSecondsRealtime(time);
      _canvasGroup.alpha = 0f;

      if (closed)
      {
         OnAnimationFinished.Invoke();
      }
   }
}
