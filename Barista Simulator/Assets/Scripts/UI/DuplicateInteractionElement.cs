﻿using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DuplicateInteractionElement : MonoBehaviour
{
    public ActionInputConfig ActionInputConfig => _actionInputConfig;
    
    [SerializeField] private TMP_Text _text;
    [SerializeField] private Image[] _fillImages;
    [SerializeField] private GameObject _inputGameObject;
    [SerializeField, Required] private InteractionButtonFXConfig _interactionButtonFXConfig;
    
    private ActionInputConfig _actionInputConfig;

    public void InitWithData(ActionInputConfig config)
    {
        _actionInputConfig = config;
        _text.text = config.ActionDisplayName;
        gameObject.SetActive(true);
    }

    public void SelectElement()
    {
        _inputGameObject.SetActive(true);
        transform.DOScale(Vector3.one * 1.2f, 0.2f).SetEase(Ease.OutBack);
    }

    public void DeselectElement(float animationTime = 0.2f)
    {
        _inputGameObject.SetActive(false);
        foreach (var fillImage in _fillImages)
        {
            fillImage.fillAmount = 0f;
        }

        if (Mathf.Approximately(animationTime, 0f))
        {
            transform.localScale = Vector3.one;
            return;
        }
        
        transform.DOScale(Vector3.one, animationTime).SetEase(Ease.OutBack);
    }

    public void UpdateSelection(float heldTime)
    {
        foreach (var fillImage in _fillImages)
        {
            if (Mathf.Approximately(heldTime, 0f))
            {
                fillImage.fillAmount = 0f;
                continue;
            }

            fillImage.fillAmount =
                _interactionButtonFXConfig.FillingBarAnimationCurve.Evaluate(
                    Mathf.Clamp(heldTime / _actionInputConfig.HoldTime, 0f, 1f));
        }
    }
    
    public void Reset()
    {
        DeselectElement(0f);
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        transform.DOKill();
    }
}
