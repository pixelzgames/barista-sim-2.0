using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CinematicModeUIController : MonoBehaviour
{
    [SerializeField] private CanvasGroup _cinematicModeCanvasGroup;
    [SerializeField] private TMP_Text _textImage;
    [SerializeField] private Image _fillingImage;
    
    public void CinematicModeChanged(bool isOn)
    {
        _cinematicModeCanvasGroup.DOFade(isOn ? 1f : 0f, 0.3f);
        
        if (isOn)
        {
            SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.performed += SkipPerformed;
            SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.started += SkipOnStarted;
            SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.canceled += SkipOnCanceled;
        }
        else
        {
            SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.performed -= SkipPerformed;
            SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.started -= SkipOnStarted;
            SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.canceled -= SkipOnCanceled;
        }
    }
    
    private void SkipPerformed(InputAction.CallbackContext obj)
    {
        _fillingImage.DOKill();
        _textImage.DOKill();
        
        SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.performed -= SkipPerformed;
        SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.started -= SkipOnStarted;
        SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.canceled -= SkipOnCanceled;
    }

    private void SkipOnStarted(InputAction.CallbackContext obj)
    {
        _fillingImage.DOKill();
        _textImage.DOKill();
        
        _textImage.DOFade(1f, 0.2f);
        _fillingImage.DOFillAmount(1f, 2f); // magic number from input config
    }
    
    private void SkipOnCanceled(InputAction.CallbackContext obj)
    {
        _fillingImage.DOKill();
        _textImage.DOKill();
        
        _textImage.DOFade(0f, 0.3f);
        _fillingImage.DOFillAmount(0f, 0.1f);
    }
}
