using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class SettingMenuController : MonoBehaviour
{
    [SerializeField] private MainMenuController _mainMenuController;
    [SerializeField] private BRAGCanvasController _myCanvas;
    [SerializeField] private TextMeshProUGUI _uiScaletext;
    [SerializeField] private TextMeshProUGUI _hudScaletext;
    [SerializeField] private TextMeshProUGUI _musicVolumeText;
    [SerializeField] private TextMeshProUGUI _sfxVolumeText;
    [SerializeField] private TextMeshProUGUI _menuSFXVolumeText;
    [SerializeField] private TMP_Dropdown _resolutionDropdown;
    [SerializeField] private TMP_Dropdown _screenModeDropdown;
    [SerializeField] private TMP_Dropdown _vsyncDropDown;
    
    private readonly List<Resolution> _resolutions = new();
    private int _currentResolutionIndex;
    private InputMaster _inputMaster;
    
    private readonly string[] _fullscreenOptions =
    {
        FullScreenMode.Windowed.ToString(),
        "Fullscreen",
        "Borderless Window"
    };
    
    private void OnEnable()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        _myCanvas.CanvasStateChange += OnCanvasStateChanged;
        PopulateResolutionDropdown();
        PopulateScreenModeDropdown();
        PopulateVsyncDropDown();
    }

    private void OnDisable()
    {
        _myCanvas.CanvasStateChange -= OnCanvasStateChanged;
    }

    private void OnCanvasStateChanged(bool on)
    {
        if (on)
        {
            _inputMaster.UI.Back.performed += OnBackButton;
            return;
        }

        _inputMaster.UI.Back.performed -= OnBackButton;
        _resolutionDropdown.Hide();
        _screenModeDropdown.Hide();
        _vsyncDropDown.Hide();
    }

    private void OnBackButton(InputAction.CallbackContext obj)
    {
        if (_resolutionDropdown.IsExpanded || _screenModeDropdown.IsExpanded || _vsyncDropDown.IsExpanded)
        {
            _resolutionDropdown.Hide();
            _screenModeDropdown.Hide();
            _vsyncDropDown.Hide();
            return;
        }
        
        _mainMenuController.OnMainMenuButton();
    }

    private void PopulateResolutionDropdown()
    {
        var resolutions = Screen.resolutions;
        
        var currentResolution = new Resolution
        {
            width = Screen.width,
            height = Screen.height,
            refreshRateRatio = Screen.currentResolution.refreshRateRatio
        };

        var optionsList = new List<string>();
        var currentResolutionCounter = 0;
        foreach (var resolution in resolutions)
        {
            if (!resolution.refreshRateRatio.Equals(currentResolution.refreshRateRatio))
            {
                continue;
            }
            
            if (resolution.Equals(currentResolution))
            {
                _currentResolutionIndex = currentResolutionCounter;
            }
            
            _resolutions.Add(resolution);
            optionsList.Add(resolution.ToString());
            currentResolutionCounter++;
        }

        _resolutionDropdown.ClearOptions();
        _resolutionDropdown.AddOptions(optionsList);
        _resolutionDropdown.value = _currentResolutionIndex;
        _resolutionDropdown.RefreshShownValue();
    }

    private void PopulateScreenModeDropdown()
    {
        var screenMode = Screen.fullScreenMode;
        _screenModeDropdown.ClearOptions();

        var index = GetScreenModeIndex(screenMode);

        _screenModeDropdown.AddOptions(_fullscreenOptions.ToList());
        _screenModeDropdown.value = index;
        _screenModeDropdown.RefreshShownValue();
    }
    
    private void PopulateVsyncDropDown()
    {
        QualitySettings.vSyncCount = PlayerPrefs.GetInt("vsync");
        var vsync = QualitySettings.vSyncCount;
        _vsyncDropDown.ClearOptions();

        var options = new List<string>
        {
            VsyncState.Disabled.ToString(),
            VsyncState.Enabled.ToString()
        };
        
        _vsyncDropDown.AddOptions(options);
        _vsyncDropDown.value = vsync;
        _vsyncDropDown.RefreshShownValue();
    }
    
    public void HudUIScaleChanged(float value)
    {
        GameOptions.UI_HUD_SCALER = value;
        GameOptions.GameOptionsChange();
        _hudScaletext.text = value.ToString("0.0");
    }
    
    public void GameplayUIScaleChanged(float value)
    {
        GameOptions.UI_GAMEPLAY_SCALER = value;
        GameOptions.GameOptionsChange();
        _uiScaletext.text = value.ToString("0.0");
    }
    
    public void MusicVolumeChanged(float value)
    {
        GameOptions.MUSIC_VOLUME = value;
        GameOptions.GameOptionsChange();
        _musicVolumeText.text = value.ToString("0.0");
    }
    
    public void SFXVolumeChanged(float value)
    {
        GameOptions.SFX_VOLUME = value;
        GameOptions.GameOptionsChange();
        _sfxVolumeText.text = value.ToString("0.0");
    }
    
    public void MenuSFXVolumeChanged(float value)
    {
        GameOptions.MENU_SFX_VOLUME = value;
        GameOptions.GameOptionsChange();
        _menuSFXVolumeText.text = value.ToString("0.0");
    }

    public void OnChangedScreenResolution(int value)
    {
        _currentResolutionIndex = value;
        var width = _resolutions[_currentResolutionIndex].width;
        var height = _resolutions[_currentResolutionIndex].height;
        var refreshRate = _resolutions[_currentResolutionIndex].refreshRateRatio;
        Screen.SetResolution(width, height, Screen.fullScreenMode, refreshRate);
    }
    
    public void OnChangedScreenMode(int value)
    {
        Screen.fullScreenMode = GetFullScreenModeByIndex(value);
    }
    
    public void OnChangedVsyncState(int value)
    {
        QualitySettings.vSyncCount = value;
        PlayerPrefs.SetInt("vsync", value);
    }

    private int GetScreenModeIndex(FullScreenMode mode)
    {
        return mode switch
        {
            FullScreenMode.Windowed => 0,
            FullScreenMode.ExclusiveFullScreen => 1,
            FullScreenMode.FullScreenWindow => 2,
            _ => 0
        };
    }

    private FullScreenMode GetFullScreenModeByIndex(int index)
    {
        return index switch
        {
            0 => FullScreenMode.Windowed,
            1 => FullScreenMode.ExclusiveFullScreen,
            2 => FullScreenMode.FullScreenWindow,
            _ => 0
        };
    }
    
    private enum VsyncState
    {
        Disabled = 0,
        Enabled = 1
    }
}
