using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ModalWindowController : BRAGCanvasController
{
    [SerializeField] private Transform _panel;
    
    [Header("Header")] [SerializeField] private Transform _header;
    [SerializeField] private TextMeshProUGUI _titleText;

    [Header("Content")] [SerializeField] private Transform _content, _verticalContent, _horizontalContent;
    [SerializeField] private TextMeshProUGUI _verticalContentText, _horizontalContentText, _contentText, _alternativeButtonText;
    [SerializeField] private Image _verticalImage, _horizontalImage;

    [Header("Footer")] [SerializeField] private Transform _footer;
    [SerializeField] private Button  _cancelButton, _otherButton;

    private Action _confirmationAction;
    private Action _cancelAction;
    private Action _alternativeAction;
    
    private InputActionMap _lastInputMap;
    private GameObject _lastSelectedGameObject;

    public void ShowWindow(ModalWindowData data)
    {
        _lastSelectedGameObject = EventSystem.current.currentSelectedGameObject;
        EventSystem.current.SetSelectedGameObject(null);
        
        _lastInputMap = SingletonManager.Instance.InputManager.CurrentActionMapping;
        SingletonManager.Instance.InputManager.ChangeInputMapping(SingletonManager.Instance.InputManager.InputMaster.Modal);

        // Handle header
        var withHeader = !string.IsNullOrEmpty(data.Title);
        _header.gameObject.SetActive(withHeader);
        _titleText.text = data.Title;
        
        // Handle content
        var withContent = !string.IsNullOrEmpty(data.Message);
        _content.gameObject.SetActive(withContent);
        _contentText.text = data.Message;
        
        // Handle Footer
        var hasAlternativeButton = data.Alternative != null;
        _otherButton.gameObject.SetActive(hasAlternativeButton);
        if (hasAlternativeButton)
        {
            SingletonManager.Instance.InputManager.InputMaster.Modal.Alternate.performed += Alternative;
            _alternativeAction = data.Alternative;
            _alternativeButtonText.text = data.AlternativeLabel;
        }
        
        var hasCancelButton = data.Cancel != null;
        _cancelButton.gameObject.SetActive(hasCancelButton);
        if (hasCancelButton)
        {
            SingletonManager.Instance.InputManager.InputMaster.Modal.Back.performed += Cancel;
            _cancelAction = data.Cancel;
        }

        SingletonManager.Instance.InputManager.InputMaster.Modal.Confirm.performed += Confirm;
        _confirmationAction = data.Confirm;

        EnableCanvas();
        _panel.DOPunchScale(Vector3.one * 0.2f, 0.2f, 1, 0f);
    }

    protected override void OnDestroy()
    {
        SingletonManager.Instance.InputManager.InputMaster.Modal.Confirm.performed -= Confirm;
        SingletonManager.Instance.InputManager.InputMaster.Modal.Back.performed -= Cancel;
        SingletonManager.Instance.InputManager.InputMaster.Modal.Alternate.performed -= Alternative;
        _panel.DOKill();
        base.OnDestroy();
    }

    public void ShowWindowWithImage(ModalWindowData data)
    {
        _content.gameObject.SetActive(false);
        _verticalContent.gameObject.SetActive(data.Vertical);
        _horizontalContent.gameObject.SetActive(!data.Vertical);
        
        if (data.Vertical)
        {
            _verticalImage.sprite = data.Image;
            _verticalContentText.text = data.Message;
        }
        else
        {
            _horizontalImage.sprite = data.Image;
            _horizontalContentText.text = data.Message;
        }
        
        ShowWindow(data);
    }
    
    public void Confirm(InputAction.CallbackContext callbackContext)
    {
        ResetInputMap();
        _confirmationAction?.Invoke();
        Close();
    }
    
    public void Cancel(InputAction.CallbackContext callbackContext)
    {
        ResetInputMap();
        _cancelAction?.Invoke();
        Close();
    }
    
    public void Alternative(InputAction.CallbackContext callbackContext)
    {
        ResetInputMap();
        _alternativeAction?.Invoke();
        Close();
    }

    private void ResetInputMap()
    {
        SingletonManager.Instance.InputManager.ChangeInputMapping(_lastInputMap);
    }

    private void Close()
    {
        // Check if current selected object changed since opening modal window and if so, re-cache current selection
        if (_lastSelectedGameObject != null)
        {
            EventSystem.current.SetSelectedGameObject(_lastSelectedGameObject);
        }
        
        CanvasStateChange += DestroyCanvas;
        DisableCanvas();
    }

    private void DestroyCanvas(bool shouldEnable)
    {
        if (shouldEnable)
        {
            return;
        }
        
        CanvasStateChange -= DestroyCanvas;
        Destroy(gameObject);
    }
}

public class ModalWindowData
{
    public ModalWindowData(string title, string message, Sprite image, bool vertical, Action confirm, Action cancel = null, Action alternative = null, string alternativeButtonLabel = "Other Action")
    {
        Title = title;
        Message = message;
        Vertical = vertical;
        Image = image;
        Confirm = confirm;
        Cancel = cancel;
        Alternative = alternative;
        AlternativeLabel = alternativeButtonLabel;
    }

    public string Title { get; private set; }
    public string Message { get; private set; }
    public string AlternativeLabel { get; private set; }
    public Sprite Image { get; private set; }
    public bool Vertical { get; private set; }
    public Action Confirm { get; private set; }
    public Action Cancel { get; private set; }
    public Action Alternative { get; private set; }
}