public class DailyGameData
{
    public int Day;
    public CustomerData CustomerData;
    public FinanceData FinanceData;
    public BrewData BrewData;

    public DailyGameData(int day)
    {
        Day = day;
        CustomerData = new CustomerData();
        FinanceData = new FinanceData();
    }
}
