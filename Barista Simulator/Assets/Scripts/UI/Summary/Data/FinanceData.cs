public class FinanceData
{
    public int Revenue;
    public int Expenses;
    public int RentPaid;
    public int Tips;
    public int DrinksSold;
    public int BadDrinks;
    public int EndOfDayBalance;
}
