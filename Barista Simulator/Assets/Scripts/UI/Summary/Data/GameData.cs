using System.Collections.Generic;

public class GameData
{
    public AllTimeGameData AllTimeGameData;
    public Dictionary<int, DailyGameData> DailyGameDataDictionary;
    public GridData GridData;

    public GameData()
    {
        DailyGameDataDictionary = new Dictionary<int, DailyGameData>();
        AllTimeGameData = new AllTimeGameData();
    }
}
