using System.Linq;
using UnityEngine;

public static class ReportDataProcessor
{
    public static int CalculateStarRating(DailyGameData gameData)
    {
        return Random.Range(0, 6);
    }

    public static int CalculateCustomerSatisfaction(DailyGameData gameData)
    {
        // TODO find algo
        var feedbackEntries = SingletonManager.Instance.FeedbackCatalogManager.Data.FeedbackDays[gameData.Day];
        var totalCustomersWhoOrdered = gameData.CustomerData.CustomersOrdered;

        // Loop through all customers feedback that day and SUM the scores
        var totalScore = feedbackEntries.CustomerFeedbackList.SelectMany(feedbackEntry => feedbackEntry.FeedbackList).Sum(feedback => feedback.Score);
        // Create the average score for all customers
        var averageScoreForAllCustomers = totalScore / totalCustomersWhoOrdered;
        
        return Mathf.RoundToInt(averageScoreForAllCustomers);
    }

    public static int CalculateDailyChangeInMoney(GameData data, int currentDay)
    {
        data.DailyGameDataDictionary.TryGetValue(currentDay, out DailyGameData currentDailyData);

        if (currentDay > 0 && data.DailyGameDataDictionary.Count > currentDay)
        {
            data.DailyGameDataDictionary.TryGetValue(currentDay - 1, out DailyGameData previousDayDailyData);

            if (currentDailyData != null && previousDayDailyData != null)
            {
                return previousDayDailyData.FinanceData.EndOfDayBalance - currentDailyData.FinanceData.EndOfDayBalance;
            }
        }

        // First day 
        return currentDailyData.FinanceData.EndOfDayBalance;
    }

    public static int CalculateProfit(FinanceData data)
    {
        return data.Revenue - data.Expenses;
    }

    public static float CalculateVisitRate(float customersVisited, float totalPassersby)
    {
        if (totalPassersby <= 0)
        {
            return 0;
        }
        
        return customersVisited / totalPassersby;
    }
}
