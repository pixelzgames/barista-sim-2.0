public class GameStatsTracker : PersistentBaseData<GameData>
{
    private void Start()
    {
        SingletonManager.Instance.GameManager.ChangedDay += NewDailyGameData;
    }
    
    private void OnDestroy()
    {
        SingletonManager.Instance.GameManager.ChangedDay -= NewDailyGameData;
    }

    public DailyGameData GetCurrentDailyGameData()
    {
        var currentDay = SingletonManager.Instance.ProgressionManager.Data.CurrentDay;
        Data.DailyGameDataDictionary.TryGetValue(SingletonManager.Instance.ProgressionManager.Data.CurrentDay, out DailyGameData gameData);
        if (gameData == null)
        {
            gameData = new DailyGameData(currentDay);
            Data.DailyGameDataDictionary.Add(currentDay, gameData);
        }
        
        return gameData;
    }

    protected override void LoadData(PlayerData data)
    {
        Data = data.GameData;
    }

    private void NewDailyGameData(int day)
    {
        Data.DailyGameDataDictionary.Add(day, new DailyGameData(day));
    }
}
