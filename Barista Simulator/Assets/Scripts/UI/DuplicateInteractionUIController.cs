using UnityEngine;
using UnityEngine.InputSystem;

public class DuplicateInteractionUIController : MonoBehaviour
{
   // LOL
   private static readonly int[] INDEX_ORDER = { 0, 2, 1, 3};
   
   [SerializeField] 
   private DuplicateInteractionElement[] _mainElements;
   [SerializeField] 
   private DuplicateInteractionElement[] _secondaryElements;
   [SerializeField] 
   private DuplicateInteractionElement[] _thirdElements;
   [SerializeField] 
   private DuplicateInteractionElement[] _fourthElements;

   private DuplicateInteractionElement[] _elements;
   private InputMaster _input;
   private DuplicateInteractionElement _selectedElement;
   private int _currentIndex;

   private void OnEnable()
   {
      _input = SingletonManager.Instance.InputManager.InputMaster;
   }

   private void InitWithData(ActionInputConfig[] actionInputConfigs)
   {
      switch (actionInputConfigs[0].Button)
      {
         case ActionInputConfig.ActionButton.MainAction:
            _elements = _mainElements;
            break;
         case ActionInputConfig.ActionButton.SecondaryAction:
            _elements = _secondaryElements;
            break;
         case ActionInputConfig.ActionButton.ThirdAction:
            _elements = _thirdElements;
            break;
         case ActionInputConfig.ActionButton.FourthAction:
            _elements = _fourthElements;
            break;
      }
      
      for (var i = 0; i < actionInputConfigs.Length; i++)
      {
         _elements[i].InitWithData(actionInputConfigs[i]); 
      }
      
      _selectedElement = _elements[INDEX_ORDER[_currentIndex]];
      _selectedElement.SelectElement();
      _input.GenericGameplay.GridUp.performed += OnDPadUp;
      _input.GenericGameplay.GridDown.performed += OnDPadDown;
   }

   private void OnDPadDown(InputAction.CallbackContext obj)
   {
      // Find the next active in hierarchy button with the static index mapper
      for (var i = _currentIndex + 1; i < _elements.Length; i++)
      {
         if (!_elements[INDEX_ORDER[i]].gameObject.activeInHierarchy)
         {
            continue;
         }

         _selectedElement.DeselectElement();
         _currentIndex = i;
         _selectedElement = _elements[INDEX_ORDER[_currentIndex]];
         _selectedElement.SelectElement();
         break;
      }
   }
   
   private void OnDPadUp(InputAction.CallbackContext obj)
   {
      // Find the next active in hierarchy button with the static index mapper
      for (var i = _currentIndex - 1; i >= 0; i--)
      {
         if (!_elements[INDEX_ORDER[i]].gameObject.activeInHierarchy)
         {
            continue;
         }

         _selectedElement.DeselectElement();
         _currentIndex = i;
         _selectedElement = _elements[INDEX_ORDER[_currentIndex]];
         _selectedElement.SelectElement();
         break;
      }
   }

   public ActionInputConfig GetActionSelection(float inputTime)
   {
      if (_elements == null || _elements.Length == 0 || _selectedElement == null)
      {
         Debug.LogError("ERROR: No elements to select in RadialInteraction. Probably miss-initialized");
         return null;
      }
      
      _selectedElement.UpdateSelection(inputTime);
      return _selectedElement.ActionInputConfig;
   }

   public void Hide()
   {
      if (_elements != null && _elements.Length > 0)
      {
         foreach (var element in _elements)
         {
            element.Reset();
         }
      }

      _input.GenericGameplay.GridUp.performed -= OnDPadUp;
      _input.GenericGameplay.GridDown.performed -= OnDPadDown;
      
      _currentIndex = 0;
      
      PlayerInteractionManager.Instance.ToggleVerticality(true);
   }

   public void Show(ActionInputConfig[] duplicateActions)
   {
      PlayerInteractionManager.Instance.ToggleVerticality(false);
      InitWithData(duplicateActions);
   }
}