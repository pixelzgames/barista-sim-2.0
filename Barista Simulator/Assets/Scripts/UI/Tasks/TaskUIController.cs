using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TaskUIController : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private TaskUIElement _taskPrefab;
    [SerializeField] private Transform _taskPanel;

    private readonly List<TaskUIElement> _taskUIElements = new();
    
    private void Start()
    {
        SingletonManager.Instance.TaskManager.OnTaskAdded += AddTask;
        SingletonManager.Instance.TaskManager.OnTaskRemoved += RemoveTask;

        foreach (var task in SingletonManager.Instance.TaskManager.Data.ActiveTasks)
        {
            AddTask(task);
        }
        
        if (_taskUIElements.Count == 0)
        {
            HideTaskUI();
        }
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.TaskManager.OnTaskAdded -= AddTask;
        SingletonManager.Instance.TaskManager.OnTaskRemoved -= RemoveTask;
    }

    private void AddTask(TaskInstance task)
    {
        var taskElement = Instantiate(_taskPrefab, _taskPanel);
        taskElement.InitializeTask(task.TaskData.TaskName, task);
        _taskUIElements.Add(taskElement);
        ShowTaskUI();
    }
    
    private void RemoveTask(TaskData task)
    {  
        foreach (var taskUIElement in _taskUIElements)
        {
            if (taskUIElement.TaskInstance.TaskData != task)
            {
                continue;
            }
            
            _taskUIElements.Remove(taskUIElement);
            StartCoroutine(taskUIElement.DestroyElement());
            break;
        }
        
        if (_taskUIElements.Count == 0)
        {
            HideTaskUI();
        }
    }

    private void HideTaskUI()
    {
        _canvasGroup.DOFade(0f, 0.5f);
    }
    
    private void ShowTaskUI()
    {
        _canvasGroup.DOFade(1f, 0.5f);
    }
}
