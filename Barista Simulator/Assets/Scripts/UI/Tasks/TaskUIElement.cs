using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class TaskUIElement : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _taskName;
    [SerializeField] private Transform _element;
    [SerializeField] private Transform _subTaskContainer;
    [SerializeField] private SubTaskUIElement _subTaskPrefab;

    private readonly float _timeAfterCompletion = 3f;

    public TaskInstance TaskInstance { get; private set; }

    public void InitializeTask(string taskName, TaskInstance taskInstance)
    {
        _taskName.text = taskName;
        TaskInstance = taskInstance;

        foreach (var subtask in taskInstance.SubTasks)
        {
            var subtaskUIElement = Instantiate(_subTaskPrefab, _subTaskContainer);
            subtaskUIElement.InitializeTask(subtask.SubTaskData.TaskDescription, subtask);
        }
    }

    public IEnumerator DestroyElement()
    {
        yield return new WaitForSeconds(_timeAfterCompletion);

        _element.DOMoveX(transform.position.x + 500f, 0.5f).SetDelay(0.5f).onComplete = () =>
        {
            Destroy(gameObject);
        };
    }
}
