using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SubTaskUIElement : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _taskDescription;
    [SerializeField] private TextMeshProUGUI _progressText;
    [SerializeField] private GameObject _progressTextElement;
    [SerializeField] private GameObject _progressBarElement;
    [SerializeField] private GameObject _checkmarkContainerElement;
    [SerializeField] private Image _fillBar;
    [SerializeField] private Image _completedImage;

    public SubTaskInstance SubTaskInstance { get; private set; }
    
    private void OnDestroy()
    {
        SubTaskInstance.SubTaskCompleted -= OnCompletedTask;
        SubTaskInstance.ProgressionIncremented -= OnProgressionIncremented;
    }

    public void InitializeTask(string taskDescription, SubTaskInstance subTaskInstance)
    {
        _taskDescription.text = taskDescription;
        SubTaskInstance = subTaskInstance;
        SubTaskInstance.SubTaskCompleted += OnCompletedTask;
        SubTaskInstance.ProgressionIncremented += OnProgressionIncremented;
        SetStyle(subTaskInstance.SubTaskData.DisplayConfig);
        if (SubTaskInstance.Completed)
        {
            OnCompletedTask(SubTaskInstance);
        }
    }

    private void SetStyle(TaskProgressDisplayConfig config)
    {
        switch (config)
        {
            case TaskProgressDisplayConfig.None:
                _progressTextElement.SetActive(false);
                _checkmarkContainerElement.SetActive(true);
                break;
            case TaskProgressDisplayConfig.Percentage:
                _progressText.text = SubTaskInstance.CurrentValue + "%";
                _checkmarkContainerElement.SetActive(true);
                break;
            case TaskProgressDisplayConfig.Fraction:
                _progressText.text = SubTaskInstance.CurrentValue + "/" + SubTaskInstance.SubTaskData.ProgressCompletionAmount;
                _checkmarkContainerElement.SetActive(true);
                break;
            case TaskProgressDisplayConfig.FillBar:
                _progressTextElement.SetActive(false);
                _progressBarElement.SetActive(true);
                _checkmarkContainerElement.SetActive(false);
                break;
        }
    }

    private void OnProgressionIncremented(int progressValue)
    {
        switch (SubTaskInstance.SubTaskData.DisplayConfig)
        {
            case TaskProgressDisplayConfig.None:
                _progressTextElement.SetActive(false);
                break;
            case TaskProgressDisplayConfig.Percentage:
                _progressText.text = Mathf.RoundToInt((float) SubTaskInstance.CurrentValue / SubTaskInstance.SubTaskData.ProgressCompletionAmount * 100f) + "%";
                break;
            case TaskProgressDisplayConfig.Fraction:
                _progressText.text = SubTaskInstance.CurrentValue + "/" + SubTaskInstance.SubTaskData.ProgressCompletionAmount;
                break;
            case TaskProgressDisplayConfig.FillBar:
                _fillBar.fillAmount = (float) SubTaskInstance.CurrentValue / SubTaskInstance.SubTaskData.ProgressCompletionAmount;
                break;
        }
    }

    private void OnCompletedTask(SubTaskInstance subTask)
    {
        _completedImage.gameObject.SetActive(true);
        StrikeThroughText(_taskDescription);
        FadeText(_taskDescription);
    }

    private void StrikeThroughText(TextMeshProUGUI text)
    {
        text.text = "<s>" + text.text + "</s>";
    }

    private void FadeText(TextMeshProUGUI text)
    {
        text.alpha = 0.5f;
    }
}
