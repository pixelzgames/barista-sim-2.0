using UnityEngine;

public class PlayerInteractionFillingUIController : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private FillingUIController _fillingController;
    
    private void Awake()
    {
        _canvasGroup.alpha = 0f;
        SingletonManager.Instance.InteractionUIController.AnyInteractionHoldStart += Show;
        SingletonManager.Instance.InteractionUIController.AnyInteractionHoldStop += Hide;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.InteractionUIController.AnyInteractionHoldStart -= Show;
        SingletonManager.Instance.InteractionUIController.AnyInteractionHoldStop -= Hide;
    }

    private void Show(float holdTime)
    {
        if (holdTime <= 0.1f)
        {
            return;
        }

        _canvasGroup.alpha = 1f;
        _fillingController.StartFilling(holdTime);
    }

    private void Hide()
    {
        _canvasGroup.alpha = 0f;
        _fillingController.StopFilling();
    }
}
