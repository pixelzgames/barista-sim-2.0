using System.Collections.Generic;
using UnityEngine;

public class InteractionUIFlavorDescriptorController : MonoBehaviour
{
    [SerializeField] private RectTransform _tagsRow;
    [SerializeField] private FlavorDescriptorUIController _tagPrefab;
    
    private readonly List<FlavorDescriptorUIController> _tagsSpawned = new ();

    public void AddNoteTag(CoffeeDescriptor descriptor)
    {
        var tagNotePrefab = Instantiate(_tagPrefab, _tagsRow);
        tagNotePrefab.InitWithData(descriptor);
        tagNotePrefab.gameObject.SetActive(true);
        _tagsSpawned.Add(tagNotePrefab);
    }

    public void ClearAll()
    {
        foreach (var tagSpawned in _tagsSpawned)
        {
            Destroy(tagSpawned.gameObject);
        }
        
        _tagsSpawned.Clear();
    }
}
