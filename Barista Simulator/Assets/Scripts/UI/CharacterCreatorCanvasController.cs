using DG.Tweening;
using UnityEngine;

public class CharacterCreatorCanvasController : BRAGCanvasController
{
    [SerializeField] private RectTransform _mainPanel;
    
    public override void EnableCanvas(float fadeInTime = 0f)
    {
        base.EnableCanvas(fadeInTime);
        var inputMapping = SingletonManager.Instance.InputManager.InputMaster.UI;
        SingletonManager.Instance.InputManager.ChangeInputMapping(inputMapping);

        // Animate panels in
        var anchorMin = _mainPanel.anchorMin;
        anchorMin.x = -0.5f;
        _mainPanel.anchorMin = anchorMin;
        var anchorMax = _mainPanel.anchorMax;
        anchorMax.x = -0.5f;
        _mainPanel.anchorMax = anchorMax;
        
        _mainPanel.DOAnchorMin(new Vector2(0f, anchorMin.y), 0.3f).SetEase(Ease.OutBack);
        _mainPanel.DOAnchorMax(new Vector2(0f, anchorMax.y), 0.3f).SetEase(Ease.OutBack);
    }

    public override void DisableCanvas(float fadeOutTime = 0f)
    {
        // Animate panels out
        _mainPanel.DOAnchorMin(new Vector2(-0.5f, _mainPanel.anchorMin.y), 0.2f).SetEase(Ease.InBack);
        _mainPanel.DOAnchorMax(new Vector2(-0.5f, _mainPanel.anchorMax.y), 0.2f).SetEase(Ease.InBack)
            .onComplete = () =>
        {
            base.DisableCanvas(fadeOutTime);
        };
    }
}
