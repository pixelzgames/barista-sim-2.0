using UnityEngine;
using UnityEngine.UI;

public class OrderListUIController : MonoBehaviour
{
    [SerializeField] private VerticalLayoutGroup _verticalLayoutGroup;

    private void Start()
    {
        SingletonManager.Instance.OrderManager.OrderGenerated += OnOrderGenerated;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.OrderManager.OrderGenerated -= OnOrderGenerated;
    }

    private void OnOrderGenerated(Order addedOrder)
    {
        addedOrder.gameObject.transform.SetParent(_verticalLayoutGroup.transform);
    }
}
