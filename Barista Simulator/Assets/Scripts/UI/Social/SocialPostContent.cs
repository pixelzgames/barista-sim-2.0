using System;
using UnityEngine;

[Serializable]
public class SocialPostContent
{
    [field: SerializeField]
    public string ContentText { get; private set; }
    [field: SerializeField]
    public int Priority { get; private set; }
}
