using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SocialPostDisplay : MonoBehaviour
{
   [SerializeField] private TextMeshProUGUI _contentText;
   [SerializeField] private TextMeshProUGUI _userTagText;
   [SerializeField] private TextMeshProUGUI _likesText;
   [SerializeField] private Image _userPictureImage;
   [SerializeField] private Image _postPictureImage;
   [SerializeField] private RectTransform[] _rectToRefresh;
   
   public void PopulateDataUI(SocialPostStubber data)
   {
      _contentText.text = data.ContentText;
      _userTagText.text = data.UserTag;
      _likesText.text = data.LikesAmount.ToString("N0");
      gameObject.SetActive(true);

      foreach (var rect in _rectToRefresh)
      {
         LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
      }
      
      AnimateIn();
   }

   private void AnimateIn()
   {
      transform.DOPunchScale(Vector3.one * 0.2f, 0.2f, 1, 0f);
   }
}
