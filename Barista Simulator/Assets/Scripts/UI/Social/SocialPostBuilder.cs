using System.Collections.Generic;
using System.Linq;
using AI.Feedback;

public static class SocialPostBuilder
{
   /// <summary>
   /// generate a tweet based on the customers feedback entries
   /// </summary>
   /// <param name="allEntries">All the entries</param>
   /// <param name="dataTable">Data set to use to map entry to post</param>
   /// <param name="maxCharacter">Max tweet character count</param>
   /// <returns></returns>
   public static string GenerateSocialFeedbackString(List<FeedbackEntry> allEntries, FeedbackEntryTypeDisplayConfigDictionary dataTable , int maxCharacter = 85)
   {
      // Get the processed feedback
      var processedFeedback = SocialFeedbackProcessor.ProcessFeedback(allEntries, dataTable);
      
      // Order them by priorities
      var orderedByPriorityFeedback = processedFeedback.OrderBy(o => o.Priority);
      
      // Iterate by priority and see how many we can fit
      var builtString = string.Empty;
      foreach (var feedback in orderedByPriorityFeedback)
      {
         if (builtString.Length + feedback.ContentText.Length >= maxCharacter)
         {
            continue;
         }

         builtString += $" {feedback.ContentText}";
      }
      
      return builtString;
   }
}

internal static class SocialFeedbackProcessor
{
   public static List<SocialPostContent> ProcessFeedback(List<FeedbackEntry> allEntries, FeedbackEntryTypeDisplayConfigDictionary dataSet)
   {
      var orderedEntries = allEntries.OrderByDescending(o => o.Score).ToList();
      var socialPostContents = ConditionalProcessing(orderedEntries, dataSet);
      socialPostContents = socialPostContents.OrderBy(o => o.Priority).ToList();
      
      return socialPostContents;
   }
   
   private static List<SocialPostContent> ConditionalProcessing(List<FeedbackEntry> orderedEntries, FeedbackEntryTypeDisplayConfigDictionary dataSet)
   {
      var belowEntries = orderedEntries.Where(o => o.Result == FeedbackEntryResult.BelowExpected);
      if (belowEntries.Count() >= 3)
      {
         var list = new List<SocialPostContent>();
         var randomPostData = dataSet[FeedbackEntryType.Generics]
            .SocialResultsMapping[FeedbackEntryResult.BelowExpected].GetRandomInPool();
         list.Add(randomPostData);
         return list;
      }
      
      var aboveEntries = orderedEntries.Where(o => o.Result == FeedbackEntryResult.AboveExpected);
      if (aboveEntries.Count() >= 3)
      {
         var list = new List<SocialPostContent>();
         var randomPostData = dataSet[FeedbackEntryType.Generics]
            .SocialResultsMapping[FeedbackEntryResult.AboveExpected].GetRandomInPool();
         list.Add(randomPostData);
         return list;
      }

      return orderedEntries.Select(entry => dataSet[entry.Type].SocialResultsMapping[entry.Result].GetRandomInPool()).ToList();
   }
}