using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanelNotchUIController : MonoBehaviour
{
    [SerializeField] private Image _fillImage;
    [SerializeField] private TMP_Text _servingsText;
    
    public void ShowWithData(LiquidContainer liquidContainer)
    {
        gameObject.SetActive(true);
        var hasContent = liquidContainer.Content != null;
        
        if (!hasContent)
        {
            _fillImage.fillAmount = 0f;
            _servingsText.text = 0.ToString();
            return;
        }
        
        _servingsText.text = liquidContainer.UnlimitedServings ? "∞" : liquidContainer.Content.Servings.ToString();
        _fillImage.fillAmount = liquidContainer.GetFullPercentage() / 100f;
    }
    
    public void ShowWithData(SolidContainer solidContainer)
    {
        gameObject.SetActive(true);
        var hasContent = solidContainer.Content != null;
        
        if (!hasContent)
        {
            _fillImage.fillAmount = 0f;
            _servingsText.text = 0.ToString();
            return;
        }
        
        _servingsText.text = solidContainer.Content.Servings.ToString();
        _fillImage.fillAmount = solidContainer.GetFullPercentage() / 100f;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
