using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class BRAGCanvasController : BRAGBehaviour
{
   public event Action<bool> CanvasStateChange = delegate { };

   [SerializeField] private bool _shouldStartActive;
   [SerializeField] private CanvasGroup _canvasGroup;
   [SerializeField] private GameObject _firstSelectedGameObject;
   
   private bool _isCanvasActive;

   protected override void OnLevelLoaded(LevelData levelData)
   {
      base.OnLevelLoaded(levelData);
      
      _isCanvasActive = _shouldStartActive;
      
      if (_shouldStartActive)
      {
         EnableCanvas();
         return;
      }
      
      InitCanvas();
   }
    
   public virtual void EnableCanvas(float fadeInTime = 0f)
   {
      if (!_canvasGroup)
      {
         return;
      }
      
      if (_firstSelectedGameObject)
      {
         EventSystem.current.SetSelectedGameObject(_firstSelectedGameObject);
      }
      
      _canvasGroup.interactable = true;
      _canvasGroup.blocksRaycasts = true;
      
      if (Mathf.Approximately(fadeInTime, 0f))
      {
         OnCanvasGroupVisible();
         return;
      }
      
      _canvasGroup.DOFade(1f, fadeInTime).onComplete = OnCanvasGroupVisible;
   }

   private void OnCanvasGroupVisible()
   {
      if (_isCanvasActive)
      {
         return;
      }
      
      _canvasGroup.alpha = 1f;
      _canvasGroup.DOComplete();
      CanvasStateChange?.Invoke(true);
      _isCanvasActive = true;
   }
   
   private void OnCanvasGroupHidden()
   {
      if (!_isCanvasActive)
      {
         return;
      }
         
      _canvasGroup.alpha = 0f;
      _canvasGroup.DOComplete();
      CanvasStateChange?.Invoke(false);
      _isCanvasActive = false;
   }

   private void InitCanvas()
   {
      _canvasGroup.alpha = 0f;
      _canvasGroup.interactable = false;
      _canvasGroup.blocksRaycasts = false;
      _isCanvasActive = false;
   }

   public virtual void DisableCanvas(float fadeOutTime = 0f)
   {
      if (!_canvasGroup)
      {
         return;
      }
      
      _canvasGroup.interactable = false;
      _canvasGroup.blocksRaycasts = false;
      
      if (Mathf.Approximately(fadeOutTime, 0f))
      {
         OnCanvasGroupHidden();
         return;
      }
      
      _canvasGroup.DOFade(0f, fadeOutTime).onComplete = OnCanvasGroupHidden;
   }

   protected override void OnDestroy()
   {
      base.OnDestroy();
      _canvasGroup.DOKill();
   }
}