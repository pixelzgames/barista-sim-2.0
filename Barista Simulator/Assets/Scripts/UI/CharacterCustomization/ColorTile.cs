using System.Collections.Generic;
using System.Linq;
using Coffee.UIEffects;
using DG.Tweening;
using UnityEngine;
using UIGradient = JoshH.UI.UIGradient;

public class ColorTile : MonoBehaviour, IUnlockable
{
    [SerializeField] private UIGradient _selectedGradient;
    [SerializeField] private UIShadow _normalShadow;
    [SerializeField] private UIShadow _selectedShadow;
    [SerializeField] private GameObject _equippedIcon;
    [SerializeField] private GameObject _newIcon;
    [SerializeField] private ColorTileGenerator _colorTileGenerator;
    [SerializeField] private AudioPlayer _audioPlayer;
    
    private int _themeIndex;
    private int _colorIndex;
    private int _customizableIndex;
    private CustomizableType _customizableType;
    private UnlockableItem _unlockableItem; 
    
    private void OnDestroy()
    {
        transform.DOKill();
    }
    
    public void SetColor(Customizable customizable, int themeIndex)
    {
        var listColors = GenerateListColors(customizable, themeIndex);
        _colorTileGenerator.InitWithData(listColors);
        
        _customizableType = customizable.CustomizableType;
        _themeIndex = themeIndex;
    }

    public void SetUnlockedItem(UnlockableItem unlockableItem)
    {
        _unlockableItem = unlockableItem;
        SetPurchasedStyle(_unlockableItem.Purchased);
        SetVisibleStyle(_unlockableItem.Visible);
        SetIsNewStyle(_unlockableItem.New);
    }

    private Color32[] GenerateListColors(Customizable customizable, int themeIndex)
    {
        var colorsToAdd = new HashSet<Color32>();
        for (var i = 0; i < customizable.AvailableThemes[themeIndex].MaterialsPerRenderer.Count; i++)
        {
            if (colorsToAdd.Count == 4)
            {
                break;
            }

            foreach (var material in customizable.AvailableThemes[themeIndex].MaterialsPerRenderer[i].Materials)
            {
                if (material == null)
                {
                    continue;
                }

                var color = material.color;
                color.a = 255;
                colorsToAdd.Add(color);
                if (colorsToAdd.Count == 4)
                {
                    break;
                }
            }
        }

        return colorsToAdd.ToArray();
    }

    public void OnTileSelected(bool playSound = true)
    {
        SingletonManager.Instance.UnlockableItemManager.UpdateUnlockable(_unlockableItem.Id, _unlockableItem.Purchased, _unlockableItem.Visible, false);
        
        SetPurchasedStyle(_unlockableItem.Purchased);
        SetVisibleStyle(_unlockableItem.Visible);
        SetIsNewStyle(_unlockableItem.New);
        
        _selectedGradient.enabled = true;
        _selectedShadow.enabled = true;
        _normalShadow.enabled = false;
        
        SingletonManager.Instance.CharacterAppearanceManager.SetCustomizableColor(_customizableType, _themeIndex);
        
        if (playSound)
        {
            _audioPlayer.PlayAudio();
        }
    }

    public void OnTileDeselected()
    {
        _selectedGradient.enabled = false;
        _selectedShadow.enabled = false;
        _normalShadow.enabled = true;
    }
    
    public void SetEquipped(bool equipped)
    {
        _equippedIcon.SetActive(equipped);
    }
    
    public void SetIsNewStyle(bool isNew)
    {
        _newIcon.SetActive(isNew);
    }

    public void SetPurchasedStyle(bool purchased) { } 
    public void SetVisibleStyle(bool locked) { }
}