using Cinemachine;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CharacterCustomizationUIController : MonoBehaviour
{
    [Header("UI Elements")]
    [SerializeField]
    private TextMeshProUGUI _titleText;

    [SerializeField]
    private Button _leftArrow;

    [SerializeField]
    private Button _rightArrow;
    [SerializeField]
    private Button _downArrow;

    [SerializeField]
    private Button _upArrow;

    [SerializeField]
    private GameObject _colorPicker;

    [SerializeField]
    private GameObject _colorContainer;

    [SerializeField]
    private GameObject _customizablePanel;

    [SerializeField] 
    private Button _randomizeButton;
    
    [SerializeField]
    private Button _exitButton;
    
    [SerializeField] 
    private Button _selectButton;
    
    [SerializeField]
    private BRAGCanvasController _canvasController; 

    [SerializeField]
    private List<DottedTabButton> _pageTabs;
    
    [SerializeField] 
    private UIScrollToSelection _uiScrollToSelection;

    [Header("Prefab references")]
    [SerializeField]
    private ColorTile _colorTilePrefab;

    [SerializeField]
    private CustomizableTile _customizableItemPrefab;

    [Header("Scene References")]
    [SerializeField]
    private CinemachineVirtualCamera _characterCreatorCamera;
    
    [SerializeField] 
    private GameObject _characterLighting;

    [Header("Tweening")]
    [SerializeField]
    private float _easeTime = 0.25f;

    [SerializeField]
    private Ease _ease = Ease.OutBack;

    private int _currentPage;
    private int _currentColorTileIndex;
    private int _currentCustomizableTileIndex;
    private int _lastEquippedColorIndex;
    
    private List<CustomizableCatalog> _customizableCatalogs;
    private readonly List<ColorTile> _colorTiles = new();
    private CustomizableTile _lastEquippedCustomizableTile;
    
    private bool _modalWindowOpen;
    private bool _appearanceHasChanges;

    private Appearance _lastAppearance;

    private void Start()
    {
        _customizableCatalogs = SingletonManager.Instance.CharacterAppearanceManager.GetCustomizableCatalogs();
        _canvasController.CanvasStateChange += OnCanvasStateChanged;
    }

    private void OnDestroy()
    {
        _canvasController.CanvasStateChange -= OnCanvasStateChanged;
    }

    public void SetAppearanceHasChanges()
    {
        _appearanceHasChanges = true;
    }

    public void SetCurrentCustomizableTile(int index)
    {
        _currentCustomizableTileIndex = index;
    }

    public void ChooseColors(Customizable customizable, bool isCurrentlyEquipped)
    {
        ResetColorPanel();
        
        _currentColorTileIndex = isCurrentlyEquipped ? SingletonManager.Instance.CharacterAppearanceManager.Data.GetColor(_customizableCatalogs.Count - 1 - _currentPage) : 0;
        
        _colorPicker.SetActive(true);
        _colorPicker.transform.DOScale(1, _easeTime).SetEase(_ease);

        InitializeColorTiles(customizable);
    }
    
    public void ResetColorPanel()
    {
        // Destroy all color items
        foreach (Transform child in _colorContainer.transform)
        {
            Destroy(child.gameObject);
        }

        _colorTiles.Clear();
        _currentColorTileIndex = 0;
    }
    
    public void EquipNewCustomizable(CustomizableTile customizable)
    {
        if (_lastEquippedCustomizableTile != null && _lastEquippedCustomizableTile != customizable)
        {
            _lastEquippedCustomizableTile.SetEquipped(false);
        }

        if (_colorTiles.Count != 0)
        {
            _colorTiles[_lastEquippedColorIndex].SetEquipped(false);
            _colorTiles[_currentColorTileIndex].SetEquipped(true);
        }
        
        _lastEquippedCustomizableTile = customizable;
        _lastEquippedColorIndex = _currentColorTileIndex;
        
        if (customizable.Customizable.CustomizableType == CustomizableType.Apron && customizable.Index == 1 && _currentColorTileIndex == 3)
        {
            SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.ApronUniformEquipped));
        } 
        
        if (customizable.Customizable.CustomizableType == CustomizableType.Hat && customizable.Index == 1 && _currentColorTileIndex == 3)
        {
            SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.CapUniformEquipped));
        }
    }

    private void OnDPadPressed(InputAction.CallbackContext obj)
    {
        var input = obj.ReadValue<Vector2>();

        if (input.y > 0)
        {
            MoveColorSelectionUp();
            _upArrow.onClick?.Invoke();
        }
        else if (input.y < 0)
        {
            MoveColorSelectionDown();
            _downArrow.onClick?.Invoke();
        }
    }
    
    private void OnAlternatePressed(InputAction.CallbackContext obj)
    {
        if (_modalWindowOpen)
        {
            return;
        }
        
        if (_appearanceHasChanges)
        {
            var modalWindow = new ModalWindowData("Randomize Character?", "Are you sure you want to discard your changes and randomize your character?", null, false, () => RandomizeCharacter(true), () => RandomizeCharacter(false));
            SingletonManager.Instance.GameManager.ShowModalWindow(modalWindow);
            _modalWindowOpen = true;
            return;
        }
        
        RandomizeCharacter(true);
    }
    
    private void OnSelectPressed(InputAction.CallbackContext obj)
    {
        if (_modalWindowOpen)
        {
            return;
        }
        
        _selectButton.onClick?.Invoke();
    }
    
    private void EnableCharacterCreator()
    {
        SingletonManager.Instance.InputManager.InputMaster.UI.Confirm.performed += OnSelectPressed;
        SingletonManager.Instance.InputManager.InputMaster.UI.Right.performed += GoToNextPage;
        SingletonManager.Instance.InputManager.InputMaster.UI.Left.performed += GoToPreviousPage;
        SingletonManager.Instance.InputManager.InputMaster.UI.Back.performed += OnBackPressed;
        SingletonManager.Instance.InputManager.InputMaster.UI.DPadMove.performed += OnDPadPressed;
        SingletonManager.Instance.InputManager.InputMaster.UI.Alternate.performed += OnAlternatePressed;

        _lastAppearance = new Appearance(SingletonManager.Instance.CharacterAppearanceManager.Data);
    }

    private void DisableCharacterCreator()
    {
        SingletonManager.Instance.InputManager.InputMaster.UI.Confirm.performed -= OnSelectPressed;
        SingletonManager.Instance.InputManager.InputMaster.UI.Right.performed -= GoToNextPage;
        SingletonManager.Instance.InputManager.InputMaster.UI.Left.performed -= GoToPreviousPage;
        SingletonManager.Instance.InputManager.InputMaster.UI.Back.performed -= OnBackPressed;
        SingletonManager.Instance.InputManager.InputMaster.UI.DPadMove.performed -= OnDPadPressed;
        SingletonManager.Instance.InputManager.InputMaster.UI.Alternate.performed -= OnAlternatePressed;
    }
    
    private void RandomizeCharacter(bool shouldRandomize)
    {
        if (shouldRandomize)
        {
            _randomizeButton.onClick?.Invoke();
            SingletonManager.Instance.CharacterAppearanceManager.AppearanceChangeRequested.Invoke(SingletonManager.Instance.CharacterAppearanceManager.GenerateRandomAppearance());
            _lastEquippedCustomizableTile = null;
            _appearanceHasChanges = false;
            RefreshPage();
        }

        _modalWindowOpen = false;
    }

    private void MoveColorSelectionDown()
    {
        if (_colorTiles.Count == 0 || _currentColorTileIndex == _colorTiles.Count - 1)
        {
            return;
        }
        
        _colorTiles[_currentColorTileIndex].OnTileDeselected();
        
        _currentColorTileIndex = Mathf.Clamp(_currentColorTileIndex + 1, 0, _colorTiles.Count - 1);
        _colorTiles[_currentColorTileIndex].OnTileSelected();
        _uiScrollToSelection.SetSelection(_colorTiles[_currentColorTileIndex].gameObject);
    }

    private void MoveColorSelectionUp()
    {
        if (_colorTiles.Count == 0 || _currentColorTileIndex == 0)
        {
            return;
        }
        
        _colorTiles[_currentColorTileIndex].OnTileDeselected();
        
        _currentColorTileIndex = Mathf.Clamp(_currentColorTileIndex - 1, 0, _colorTiles.Count - 1);
        _colorTiles[_currentColorTileIndex].OnTileSelected();
        _uiScrollToSelection.SetSelection(_colorTiles[_currentColorTileIndex].gameObject);
    }

    private void OnCanvasStateChanged(bool isEnabled)
    {
        if (isEnabled)
        {
            EnableCharacterCreator();
            RefreshPage();
        }
        else
        {
            DisableCharacterCreator();
        }
        
        _characterLighting.SetActive(isEnabled);
    }

    private void OnBackPressed(InputAction.CallbackContext obj)
    {
        if (!_modalWindowOpen)
        {
            OnExit();
        }
    }

    /// <summary>
    /// Switches to previous page.
    /// </summary>
    private void GoToPreviousPage(InputAction.CallbackContext obj)
    {
        if (_colorTiles.Count > 0)
        {
            _colorTiles[_lastEquippedColorIndex]?.OnTileSelected();
        }
        
        _currentPage = (_currentPage - 1 + _customizableCatalogs.Count) % _customizableCatalogs.Count;
        RefreshPage();
        _leftArrow.onClick?.Invoke();
    }

    /// <summary>
    /// Switches to next page.
    /// </summary>
    private void GoToNextPage(InputAction.CallbackContext obj)
    {
        if (_colorTiles.Count > 0)
        {
            _colorTiles[_lastEquippedColorIndex]?.OnTileSelected();
        }
        
        _currentPage = (_currentPage + 1 + _customizableCatalogs.Count) % _customizableCatalogs.Count;
        RefreshPage();
        _rightArrow.onClick?.Invoke();
    }

    private void OnExit()
    {
        _exitButton.onClick?.Invoke();
        var modalWindow = new ModalWindowData("Exit Character Creator", 
            "Are you sure you want to exit the Character Creator? Changes will be saved.",
            null, false, () => ExitCharacterCreator(true), () => ExitCharacterCreator(false), ResetAppearanceToLast, "Discard Changes");
        SingletonManager.Instance.GameManager.ShowModalWindow(modalWindow);
        _modalWindowOpen = true;
    }

    private void ExitCharacterCreator(bool shouldExit)
    {
        if (shouldExit)
        {
            _canvasController.DisableCanvas();
            SingletonManager.Instance.InputManager.ChangeInputMapping(SingletonManager.Instance.InputManager.InputMaster.GenericGameplay);
            SetCameraState(false);
            SingletonManager.Instance.DataPersistenceManager.SaveAllPlayerData();
        }
        
        _modalWindowOpen = false;
    }
    
    private void ResetAppearanceToLast()
    {
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceChangeRequested.Invoke(_lastAppearance);
        ExitCharacterCreator(true);
    }

    private void RefreshPage() 
    {
        _lastEquippedCustomizableTile?.SetAppearance();
        
        SetCameraState(true);

        ResetColorPanel();

        for (var i = 0; i < _pageTabs.Count; i++)
        {
            if (i == _currentPage)
            {
                _pageTabs[_pageTabs.Count - 1 - i].SelectTab();
            }
            else
            {
                _pageTabs[_pageTabs.Count - 1 - i].DeselectTab();
            }
        }

        _titleText.text = _customizableCatalogs[_customizableCatalogs.Count - 1 - _currentPage].CatalogName;

        // Destroy all customizable items
        foreach (Transform child in _customizablePanel.transform)
        {
            Destroy(child.gameObject);
        }

        InitializeCustomizableTiles();
    }

    private void SetCameraState(bool on)
    {
        SingletonManager.Instance.CameraManager.ChangeCamera(on
            ? _characterCreatorCamera
            : SingletonManager.Instance.CameraManager.MainGameplayCamera, false);
    }

    private void InitializeColorTiles(Customizable customizable)
    {
        var equippedColorSelected = _currentCustomizableTileIndex == _lastEquippedCustomizableTile.Index;
        var currentColor = equippedColorSelected ? SingletonManager.Instance.CharacterAppearanceManager.Data.GetColor(_customizableCatalogs.Count - 1 - _currentPage) : 0;

        for (var index = 0; index < customizable.AvailableThemes.Count; index++)
        {
            var objectTheme = customizable.AvailableThemes[index];
            var item = Instantiate(_colorTilePrefab, _colorContainer.transform);
            item.gameObject.transform.localScale = Vector3.zero;
            item.gameObject.transform.DOScale(1, _easeTime).SetEase(_ease);
            item.GetComponent<ColorTile>().SetColor(customizable, index);
            item.SetUnlockedItem(SingletonManager.Instance.UnlockableItemManager.GetUnlockable(objectTheme.Id));

            _colorTiles.Add(item);

            if (index != currentColor)
            {
                item.SetEquipped(false);
                continue;
            }
            
            item.SetEquipped(equippedColorSelected);
            item.OnTileSelected(false);
            _lastEquippedColorIndex = _currentColorTileIndex;
        }
    }

    private void InitializeCustomizableTiles()
    {
        var currentStyle = SingletonManager.Instance.CharacterAppearanceManager.Data.GetStyle(_customizableCatalogs.Count - 1 - _currentPage);
        var counter = 0;

        foreach (var customizable in _customizableCatalogs[_customizableCatalogs.Count - 1 - _currentPage].Customizables)
        {
            var item = Instantiate(_customizableItemPrefab, _customizablePanel.transform);
            item.gameObject.transform.localScale = Vector3.zero;
            item.gameObject.transform.DOScale(1, _easeTime).SetEase(_ease);
            
            var unlockableItem = SingletonManager.Instance.UnlockableItemManager.GetUnlockable(customizable.Id);
            item.SetDetails(customizable, counter, this, unlockableItem, counter == currentStyle);

            if (counter == currentStyle)
            {
                EquipNewCustomizable(item);
            }
            
            if (counter == currentStyle)
            {
                EventSystem.current.SetSelectedGameObject(item.gameObject);
            }
            
            counter++;
        }
    }
}
