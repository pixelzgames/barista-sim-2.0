using System;

[Serializable]
public class Appearance
{
    public int GlassesStyle;
    public int GlassesColor;

    public int EyeStyle;
    public int EyeColor;

    public int FacialHairStyle;
    public int FacialHairColor;

    public int ApronStyle;
    public int ApronColor;

    public int ShirtStyle;
    public int ShirtColor;

    public int PantsStyle;
    public int PantsColor;

    public int HatStyle;
    public int HatColor;

    public int SkinStyle;
    public int SkinColor;

    public int ShoeStyle;
    public int ShoeColor;

    public Appearance()
    {
        // Default Appearance
        GlassesStyle = 0;
        GlassesColor = 0;

        EyeStyle = 0;
        EyeColor = 0;

        FacialHairStyle = 0;
        FacialHairColor = 0;

        ApronStyle = 0;
        ApronColor = 0;

        ShirtStyle = 0;
        ShirtColor = 0;

        PantsStyle = 0;
        PantsColor = 0;

        HatStyle = 0;
        HatColor = 0;

        SkinStyle = 0;
        SkinColor = 0;

        ShoeStyle = 0;
        ShoeColor = 0;
    }

    /// <summary>
    /// Copy constructor
    /// </summary>
    /// <param name="appearance">Appearance to copy</param>
    public Appearance(Appearance appearance)
    {
        GlassesStyle = appearance.GlassesStyle;
        GlassesColor = appearance.GlassesColor;
        EyeStyle = appearance.EyeStyle;
        EyeColor = appearance.EyeColor;
        FacialHairStyle = appearance.FacialHairStyle;
        FacialHairColor = appearance.FacialHairColor;
        ApronStyle = appearance.ApronStyle;
        ApronColor = appearance.ApronColor;
        ShirtStyle = appearance.ShirtStyle;
        ShirtColor = appearance.ShirtColor;
        PantsStyle = appearance.PantsStyle;
        PantsColor = appearance.PantsColor;
        HatStyle = appearance.HatStyle;
        HatColor = appearance.HatColor;
        SkinStyle = appearance.SkinStyle;
        SkinColor = appearance.SkinColor;
        ShoeStyle = appearance.ShoeStyle;
        ShoeColor = appearance.ShoeColor;
    }

    public Appearance(int glassesStyle, int glassesColor, int eyeStyle, int eyeColor, int facialHairStyle, int facialHairColor, int apronStyle, int apronColor, int shirtStyle, int shirtColor, int pantsStyle, int pantsColor, int hatStyle, int hatColor, int skinStyle, int skinColor, int shoeStyle, int shoeColor)
    {
        GlassesStyle = glassesStyle;
        GlassesColor = glassesColor;
        EyeStyle = eyeStyle;
        EyeColor = eyeColor;
        FacialHairStyle = facialHairStyle;
        FacialHairColor = facialHairColor;
        ApronStyle = apronStyle;
        ApronColor = apronColor;
        ShirtStyle = shirtStyle;
        ShirtColor = shirtColor;
        PantsStyle = pantsStyle;
        PantsColor = pantsColor;
        HatStyle = hatStyle;
        HatColor = hatColor;
        SkinStyle = skinStyle;
        SkinColor = skinColor;
        ShoeStyle = shoeStyle;
        ShoeColor = shoeColor;
    }

    public void SetAppearanceByValue(Appearance appearance)
    {
        GlassesStyle = appearance.GlassesStyle;
        GlassesColor = appearance.GlassesColor;
        EyeStyle = appearance.EyeStyle;
        EyeColor = appearance.EyeColor;
        FacialHairStyle = appearance.FacialHairStyle;
        FacialHairColor = appearance.FacialHairColor;
        ApronStyle = appearance.ApronStyle;
        ApronColor = appearance.ApronColor;
        ShirtStyle = appearance.ShirtStyle;
        ShirtColor = appearance.ShirtColor;
        PantsStyle = appearance.PantsStyle;
        PantsColor = appearance.PantsColor;
        HatStyle = appearance.HatStyle;
        HatColor = appearance.HatColor;
        SkinStyle = appearance.SkinStyle;
        SkinColor = appearance.SkinColor;
        ShoeStyle = appearance.ShoeStyle;
        ShoeColor = appearance.ShoeColor;
    }

    public void SetColor(CustomizableType customizableType, int themeIndex)
    {
        switch (customizableType)
        {
            case CustomizableType.Glasses:
                GlassesColor = themeIndex;
                break;
            case CustomizableType.Eyes:
                EyeColor = themeIndex;
                break;
            case CustomizableType.FacialHair:
                FacialHairColor = themeIndex;
                break;
            case CustomizableType.Apron:
                ApronColor = themeIndex;
                break;
            case CustomizableType.Shirt:
                ShirtColor = themeIndex;
                break;
            case CustomizableType.Pants:
                PantsColor = themeIndex;
                break;
            case CustomizableType.Hat:
                HatColor = themeIndex;
                break;
            case CustomizableType.Skin:
                SkinColor = themeIndex;
                break;
            case CustomizableType.Shoes:
                ShoeColor = themeIndex;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(customizableType), customizableType, null);
        }
    }

    public int GetStyle(int customizableIndex)
    {
        return customizableIndex switch
        {
            0 => GlassesStyle,
            1 => EyeStyle,
            2 => FacialHairStyle,
            3 => ApronStyle,
            4 => ShirtStyle,
            5 => PantsStyle,
            6 => HatStyle,
            7 => SkinStyle,
            8 => ShoeStyle,
            _ => -1
        };
    }

    public int GetColor(int customizableIndex)
    {
        return customizableIndex switch
        {
            0 => GlassesColor,
            1 => EyeColor,
            2 => FacialHairColor,
            3 => ApronColor,
            4 => ShirtColor,
            5 => PantsColor,
            6 => HatColor,
            7 => SkinColor,
            8 => ShoeColor,
            _ => -1
        };
    }
}
