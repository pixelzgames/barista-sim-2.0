using System;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAppearanceManager : PersistentBaseData<Appearance>
{
    public Action<Appearance> AppearanceChangeRequested = delegate { };
    public Action<Appearance> AppearanceDataChanged = delegate { };

    [Header("Catalog")] [SerializeField] private AppearanceCatalog _customizableCatalog;

    public List<CustomizableCatalog> GetCustomizableCatalogs()
    {
        return _customizableCatalog.CustomizableCatalogs;
    }

    public Appearance GenerateRandomAppearance()
    {
        return _customizableCatalog.GenerateRandomAppearance();
    }

    private int _selectedCustomizableIndex;

    private void Start()
    {
        AppearanceDataChanged += UpdateAppearanceData;
    }

    private void OnDestroy()
    {
        AppearanceDataChanged -= UpdateAppearanceData;
    }

    private void UpdateAppearanceData(Appearance appearance)
    {
        Data.SetAppearanceByValue(appearance);
    }

    public void SetEyes(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.EyeStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetGlasses(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.GlassesStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetSkin(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.SkinStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetShirt(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.ShirtStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }
    
    public void SetShoes(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.ShoeStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetPants(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.PantsStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetApron(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.ApronStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetFacialHair(CustomizationReceiver facialHairPrefab, int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.FacialHairStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetHat(int index)
    {
        if (Data == null)
        {
            return;
        }

        Data.HatStyle = index;
        AppearanceChangeRequested.Invoke(Data);
    }

    public void SetCustomizableColor(CustomizableType customizableType, int colorIndex)
    {
        if (Data == null)
        {
            return;
        }

        Data.SetColor(customizableType, colorIndex);
        AppearanceChangeRequested.Invoke(Data);
    }

    protected override void LoadData(PlayerData data)
    {
        Data = data.Appearance;
        AppearanceChangeRequested.Invoke(Data);
    }
}
