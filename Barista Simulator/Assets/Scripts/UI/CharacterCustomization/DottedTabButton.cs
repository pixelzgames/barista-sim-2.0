using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DottedTabButton : MonoBehaviour
{
    [SerializeField] private Image _background;
    [SerializeField] private float _selectedSize = 1.5f;
    [SerializeField] private Color32 _selectedColor;

    private Color _baseColor;
    private Tweener _tween;
    
    private void OnEnable()
    {
        _baseColor = _background.color;
    }

    public void SelectTab()
    {
        _tween = transform.DOScale(Vector3.one * _selectedSize, 0.2f).SetEase(Ease.OutBack);
        _background.color = _selectedColor;
    }

    public void DeselectTab()
    {
        transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);
        _background.color = _baseColor;
    }

    private void OnDestroy()
    {
        _tween?.Kill();
    }
}
