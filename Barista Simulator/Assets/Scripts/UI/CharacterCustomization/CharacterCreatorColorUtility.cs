using UnityEngine;

public class CharacterCreatorColorUtility
{
   private const string CONFIG_PATH = "CurrentCharacterCreatorColor";
   public static CharacterCreatorColorConfig ColorConfig { get; private set; }

   [RuntimeInitializeOnLoadMethod]
   private static void LoadColorData()
   {
      ColorConfig = (CharacterCreatorColorConfig) Resources.Load(CONFIG_PATH);
   }
}
