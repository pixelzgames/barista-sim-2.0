using Coffee.UIEffects;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UIGradient = JoshH.UI.UIGradient;

public class CustomizableTile : MonoBehaviour, IUnlockable
{
    [SerializeField] private Image _icon;
    [SerializeField] private UIGradient _selectedGradient;
    [SerializeField] private UIShadow _normalShadow;
    [SerializeField] private UIShadow _selectedShadow;
    [SerializeField] private GameObject _equippedIcon;
    [SerializeField] private GameObject _lockedIcon;
    [SerializeField] private GameObject _newIcon;
    [SerializeField] private GameObject _priceIcon;
    [SerializeField] private TextMeshProUGUI _itemName;
    [SerializeField] private TextMeshProUGUI _costText;

    public Customizable Customizable { get; private set; }
    private bool _isCurrentlyEquipped;
    public int Index { get; private set; }

    private UnlockableItem _unlockableItem; 

    private CharacterCustomizationUIController _uiController;

    private void OnDestroy()
    {
        transform.DOKill();
    }
    
    public void SetDetails(Customizable customizable, int index, CharacterCustomizationUIController uiController, UnlockableItem unlockableItem, bool equipped)
    {
        Customizable = customizable;
        if (customizable.Icon != null)
        {
            _icon.sprite = customizable.Icon;
        }
        else
        {
            _icon.color = Color.clear;
        }
  
        _itemName.text = customizable.ItemName;
        Index = index;
        _uiController = uiController;

        _unlockableItem = unlockableItem;
        SetPurchasedStyle(_unlockableItem.Purchased);
        SetVisibleStyle(_unlockableItem.Visible);
        SetIsNewStyle(_unlockableItem.New);
            
        SetEquipped(equipped);
    }

    public void OnCustomizableEquipped()
    {
        if (!_unlockableItem.Purchased)
        {
            return;
        }
        
        _uiController.EquipNewCustomizable(this);
        _uiController.SetAppearanceHasChanges();
        SetEquipped(true);
    }

    public void PreviewCustomizable()
    {
        if (_unlockableItem.New)
        {
            SingletonManager.Instance.UnlockableItemManager.UpdateUnlockable(_unlockableItem.Id, _unlockableItem.Purchased, _unlockableItem.Visible, false);
            SetIsNewStyle(_unlockableItem.New);
        }

        _uiController.SetCurrentCustomizableTile(Index);
        if (Customizable.AvailableThemes.Count == 0)
        {
            _uiController.ResetColorPanel();
            Customizable.SetAppearance(Index);
            return;
        }
        
        _uiController.ChooseColors(Customizable, _isCurrentlyEquipped);
        Customizable.SetAppearance(Index);
    }

    public void SetAppearance()
    {
        Customizable.SetAppearance(Index);
    }

    public void OnSelected()
    {
        _selectedGradient.enabled = true;
        _selectedShadow.enabled = true;
        _normalShadow.enabled = false;
    }

    public void OnDeselected()
    {
        _selectedGradient.enabled = false;
        _selectedShadow.enabled = false;
        _normalShadow.enabled = true;
    }

    public void SetEquipped(bool equipped)
    {
        _isCurrentlyEquipped = equipped;
        _equippedIcon.SetActive(equipped);
    }
    
    public void SetVisibleStyle(bool visible)
    {
        //gameObject.SetActive(visible);
    }

    public void SetIsNewStyle(bool isNew)
    {
        _newIcon.SetActive(isNew);
    }

    public void SetPurchasedStyle(bool purchased)
    {
        _lockedIcon.SetActive(!purchased);
        if (purchased)
        {
            _priceIcon.SetActive(false);
        }
        else
        {
            _costText.text = $"{Customizable.Price}$";
        }
    }
}
