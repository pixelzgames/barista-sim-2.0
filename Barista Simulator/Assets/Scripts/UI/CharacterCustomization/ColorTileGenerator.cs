using UnityEngine;
using UnityEngine.UI;

public class ColorTileGenerator : MonoBehaviour
{
    [SerializeField] private Image[] _images;
    
    public void InitWithData(Color32[] colors)
    {
        for (var i = 0; i < colors.Length; i++)
        {
            _images[i].color = colors[i];
            if (i == 0)
            {
                continue;
            }
            
            _images[i].gameObject.SetActive(true);
        }
    }
}
