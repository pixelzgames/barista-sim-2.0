using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class InfoPanelController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Reference to canvas group for the InfoPanel")]
    private CanvasGroup _canvasGroup;
    
    [SerializeField]
    [Tooltip("Reference to Interactable name text.")]
    private TextMeshProUGUI _infoInteractableNameText;

    [SerializeField]
    [Tooltip("Reference to container type.")]
    private TextMeshProUGUI _infoTypeText;

    [SerializeField]
    [Tooltip("Reference to any other info in container")]
    private TextMeshProUGUI _infoText;

    [SerializeField]
    [Tooltip("Reference to Interactable name text.")]
    private Image _interactableIcon;
    
    [SerializeField]
    private Image _cornerIcon;

    [SerializeField]
    [Tooltip("Reference to Contents text.")]
    private GameObject _contentTitleText;

    [SerializeField] private TagUIController _coffeeQualityTagUIController;
    [SerializeField] private TagUIController _coffeeDialedInTag;

    [SerializeField] private TagConfig[] _qualityTagData;

    [SerializeField] private InfoPanelNotchUIController _recipeNotch;
    [SerializeField] private InfoPanelNotchUIController _waterNotch; 
    [SerializeField] private InfoPanelNotchUIController _milkNotch;
    [SerializeField] private InfoPanelNotchUIController _beansNotch;

    [SerializeField] private InteractionUIFlavorDescriptorController _flavorDescriptorController;

    /// <summary>
    /// Shows or hide and sets info of the canvas
    /// </summary>
    /// <param name="show"></param>
    /// <param name="interactable"></param>
    public void ShowContextualUI(bool show , Interactable interactable = null)
    {
        SetCanvasInfo(interactable);
        SetCanvasNotchesInfo(interactable);
        SetCanvasCoffeeDescriptor(interactable);
        
        if (_canvasGroup != null)
        {
            _canvasGroup.DOKill();
            _canvasGroup.DOFade(show ? 1f : 0f, show ? 0.2f : 0.1f);
        }
    }

    /// <summary>
    /// Only update the text and other info without showing or hiding
    /// </summary>
    /// <param name="interactable"></param>
    public void SetCanvasInfo(Interactable interactable = null)
    {
        if (!interactable)
        {
            return;
        }
        
        if (_infoTypeText != null)
        {
            var cup = interactable as Cup;
            if (cup != null)
            {
                _infoTypeText.text = cup.OrderType.ToString();
            }
            else
            {
                _infoTypeText.text = "";
            }
        }

        if (_infoInteractableNameText != null && interactable != null)
        {
            _infoInteractableNameText.text = interactable.CurrentRecipe != null && interactable.CurrentRecipe.Config
                ? interactable.InteractableName + " of " + (interactable.CurrentRecipe.IsBadQuality ? "Bad " : "") +
                  interactable.CurrentRecipe.Config.RecipeName
                : interactable.InteractableName;
        }
            
        if (_infoText != null)
        {
            _contentTitleText.SetActive(true);
            _infoText.text = interactable.GetInfoToDisplay();
        }
                    
        if (_interactableIcon == null)
        {
            return;
        }
        
        _interactableIcon.sprite = interactable.InteractableIcon != null ? interactable.InteractableIcon : null;
    }

    /// <summary>
    /// Show the different info about this interactable with icons
    /// </summary>
    private void SetCanvasNotchesInfo(Interactable interactable)
    {
        if (!interactable)
        {
            return;
        }

        SetQualityOfCoffee(interactable);

        // Check for Coffee Liquid Container
        var recipe = interactable.GetLiquidCoffeeData();
        if (recipe != null)
        {
            _recipeNotch.ShowWithData(recipe);
        }
        else
        {
            _recipeNotch.Hide();
        }
        
        // Check for Water Container
        var water = interactable.GetLiquidWaterData();
        if (water != null)
        {
            _waterNotch.ShowWithData(water);
        }
        else
        {
            _waterNotch.Hide();
        }
        
        // Check for Milk Container
        var milk = interactable.GetLiquidMilkData();
        if (milk != null)
        {
            _milkNotch.ShowWithData(milk);
        }
        else
        {
            _milkNotch.Hide();
        }

        // Check for Solid Container
        var beans = interactable.GetSolidBeansContainer();
        if (beans != null)
        {
            _beansNotch.ShowWithData(beans);
        }
        else
        {
            _beansNotch.Hide();
        }
    }

    private void SetQualityOfCoffee(Interactable interactable)
    {
        var quality = interactable.GetBeansDataQuality();
        if (quality != Quality.None)
        {
            var tagData = new TagData();
            switch (quality)
            {
                case Quality.None:
                    throw new ArgumentOutOfRangeException();
                case Quality.Commodity:
                    tagData = _qualityTagData[0].TagData;
                    break;
                case Quality.Premium:
                    tagData = _qualityTagData[1].TagData;
                    break;
                case Quality.Specialty:
                    tagData = _qualityTagData[2].TagData;
                    break;
                case Quality.Exotic:
                    tagData = _qualityTagData[3].TagData;
                    break;
                case Quality.Competition:
                    tagData = _qualityTagData[4].TagData;
                    break;
            }

            _coffeeQualityTagUIController.InitWithData(tagData);
            _coffeeQualityTagUIController.SetVisibility(true);
            _coffeeDialedInTag.gameObject.SetActive(interactable.GetCoffeeData() != null && interactable.GetCoffeeData().IsEspressoDialedIn);
        }
        else
        {
            _coffeeDialedInTag.gameObject.SetActive(false);
            _coffeeQualityTagUIController.SetVisibility(false);
        }
    }
    
    private void SetCanvasCoffeeDescriptor(Interactable interactable)
    {
        _flavorDescriptorController.ClearAll();
        
        if (!interactable)
        {
            return;
        }
        
        var coffeeData = interactable.GetCoffeeData();
        if (coffeeData == null)
        {
            return;
        }

        if (interactable.CurrentRecipe is { IsBadQuality: true })
        {
            if (CoffeeConfigUtility.CoffeeDescriptorsDatabaseConfig.CoffeeDescriptorsDictionary.TryGetValue(
                    TastingDescriptor.Bad, out var badTag))
            {
                _flavorDescriptorController.AddNoteTag(new CoffeeDescriptor(TastingDescriptor.Bad, badTag));
            }

            return;
        }
        
        foreach (var slot in coffeeData.CoffeeQuality.TastingDecriptorSlots)
        {
            _flavorDescriptorController.AddNoteTag(slot.CoffeeDescriptor);
        }
    }
}
