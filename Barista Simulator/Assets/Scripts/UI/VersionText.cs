﻿using UnityEngine;
using TMPro;

public class VersionText : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        GetComponentInChildren<TextMeshProUGUI>().text =  "version " + Application.version;
        DontDestroyOnLoad(gameObject);
    }
}
