using UnityEngine;

public class PauseMenuUIController : BRAGCanvasController
{
    public override void EnableCanvas(float fadeInTime = 0f)
    {
        Time.timeScale = 0f;
        base.EnableCanvas(fadeInTime);
        var inputMapping = SingletonManager.Instance.InputManager.InputMaster.UI;
        SingletonManager.Instance.InputManager.ChangeInputMapping(inputMapping);
    }

    public override void DisableCanvas(float fadeOutTime = 0f)
    {
        Time.timeScale = 1f;
        base.DisableCanvas(fadeOutTime);
        var inputMapping = SingletonManager.Instance.InputManager.InputMaster.GenericGameplay;
        SingletonManager.Instance.InputManager.ChangeInputMapping(inputMapping);
    }
}
