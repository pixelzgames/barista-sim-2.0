using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RadialMenuElement : MonoBehaviour
{
    public Image Icon => _icon;
    public Image RadialSection => _radialSectionBackground;
    public InteractionMode OnAction { get; private set; }
    
    [SerializeField] private Image _icon;
    [SerializeField] private Image _radialSectionBackground;

    private Sprite _normalSprite;
    private Sprite _selectedSprite;
    public void InitializeWithData(RadialMenuElementConfig config)
    {
        _normalSprite = config.Icon;
        _icon.sprite = _normalSprite;
        OnAction = config.OnAction;

        if (config.SelectedIcon != null)
        {
            _selectedSprite = config.SelectedIcon;
        }
    }

    public void SelectEffect(bool selected)
    {
        if (selected)
        {
            if (_selectedSprite != null)
            {
                _icon.sprite = _selectedSprite;
            }
            
            _icon.transform.DOScale(Vector3.one * 1.5f, 0.1f).SetUpdate(UpdateType.Normal, true);
        }
        else
        {
            _icon.transform.DOScale(Vector3.one, 0.1f).SetUpdate(UpdateType.Normal, true);
            _icon.sprite = _normalSprite;
        }
    }
    
    /// <summary>
    /// Checks if the selection is valid/selectable for the current gameplay state, and updates the UI accordingly.
    /// </summary>
    public void RefreshIconStatus()
    {
        var tempColor = _icon.color;
        tempColor.a = SingletonManager.Instance.GameManager.IsValidMode(OnAction) ? 1f : 0.5f;
        _icon.color = tempColor;
    }
}
