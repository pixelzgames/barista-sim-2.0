using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class RadialMenuController : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup; 
    [SerializeField] private Color _unselectedColor, _selectedColor; 
    [SerializeField] private RadialMenuElementConfig[] _data;
    [SerializeField] private RadialMenuElement _radialMenuElementPrefab;
    [SerializeField] private float _spaceBetweenElement;
    [SerializeField] private Volume _blurVolume;
    [SerializeField] private TextMeshProUGUI _modeText;

    private RadialMenuElement[] _elements;
    private InputMaster _input;
    private bool _isShown;
    private int _selectedIndex = -1;
    
    // Start is called before the first frame update
    private void Start()
    {
        _input = SingletonManager.Instance.InputManager.InputMaster;
        _input.GenericGameplay.ShowRadialUI.started += OnOpen;
        _input.GenericGameplay.ShowRadialUI.canceled += OnClose;

        var stepLength = 360f / _data.Length;
        var thisRect = transform.GetComponent<RectTransform>().rect;
        //var iconDist = Vector3.Distance(_radialMenuElementPrefab.Icon.transform.position, _radialMenuElementPrefab.RadialSection.transform.position);

        _elements = new RadialMenuElement[_data.Length];
        for (var i = 0; i < _data.Length; i++)
        {
            _elements[i] = Instantiate(_radialMenuElementPrefab, transform);
            _elements[i].transform.localPosition = Vector3.zero;
            _elements[i].transform.localRotation = Quaternion.identity;
     
            var rect = _elements[i].GetComponent<RectTransform>().rect;
            rect.width = thisRect.width;
            rect.height = thisRect.height;

            _elements[i].RadialSection.fillAmount = 1f / _data.Length - _spaceBetweenElement / 360f;
            _elements[i].RadialSection.transform.localRotation = Quaternion.Euler(0f,0f, -stepLength / 2f + _spaceBetweenElement / 2f + i * stepLength);

            _elements[i].Icon.transform.localPosition = _elements[i].RadialSection.transform.localPosition +
                Quaternion.AngleAxis(i * stepLength, Vector3.forward) * Vector3.up * 240f /*iconDist*/;
            
            _elements[i].InitializeWithData(_data[i]);
            
            _elements[i].gameObject.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        _input.GenericGameplay.ShowRadialUI.started -= OnOpen;
        _input.GenericGameplay.ShowRadialUI.canceled -= OnClose;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_isShown)
        {
            return;
        }
        
        var stepLength = 360f / _data.Length;
        
        // Setting to -1 as default so it has no index selected
        float angle;
        
        switch (SingletonManager.Instance.InputManager.CurrentDevice)
        {
            case InputManager.Devices.Keyboard:
                angle = BRAGUtilities.RadialMousePosition(stepLength, _input.GenericGameplay.MousePosition.ReadValue<Vector2>(), transform);
                break;
            case InputManager.Devices.Xbox:
            {
                angle = BRAGUtilities.RadialJoystickPosition(stepLength, _input.GenericGameplay.Move.ReadValue<Vector2>());
                break;
            }
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        // -1 refer to nothing selected
        _selectedIndex = angle < 0f ? -1 : (int)(angle / stepLength);

        for (var i = 0; i < _data.Length; i++)
        {
            if (i == _selectedIndex)
            {
                _elements[i].RadialSection.color = _selectedColor;
                _elements[i].transform.DOScale(Vector3.one * 1.05f, 0.1f).SetUpdate(UpdateType.Normal, true);
                _elements[i].SelectEffect(true);
                _modeText.text = _elements[i].OnAction.ToString();
            }
            else
            {
                _elements[i].RadialSection.color = _unselectedColor;
                _elements[i].transform.DOScale(Vector3.one , 0.1f).SetUpdate(UpdateType.Normal, true);
                _elements[i].SelectEffect(false);
            }
        }
        
        // Hide text if nothing is selected
        if (_selectedIndex == -1)
        {
            _modeText.text = string.Empty;
        }
    }

    private void OnOpen(InputAction.CallbackContext callbackContext)
    {
        if (!PlayerInteractionManager.Instance)
        {
            return;
        }
        
        foreach (var element in _elements)
        {
            element.RefreshIconStatus();
        }
        
        _selectedIndex = -1;
        PlayerInteractionManager.Instance.PlayerController.ToggleMovement(false);
        _blurVolume.weight = 1f;
        _canvasGroup.DOFade(1f, 0.2f).SetUpdate(UpdateType.Normal,true);
        _isShown = true;
    }
    
    private void OnClose(InputAction.CallbackContext callbackContext)
    {
        if (!PlayerInteractionManager.Instance)
        {
            return;
        }
        
        PlayerInteractionManager.Instance.PlayerController.ToggleMovement(true);
        _canvasGroup.DOFade(0f, 0.2f);
        _isShown = false;
        _blurVolume.weight = 0f;
        
        if (_selectedIndex < 0)
        {
            return;
        }
        
        SingletonManager.Instance.GameManager.ChangeInteractionMode(_elements[_selectedIndex].OnAction);
    }
}
