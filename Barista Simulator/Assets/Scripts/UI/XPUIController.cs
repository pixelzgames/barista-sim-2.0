using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class XPUIController : MonoBehaviour
{
    [SerializeField] private Slider _xpProgressSlider;
    [SerializeField] private CanvasGroup _barCanvasGroup;
    [SerializeField] private CanvasGroup _effectsCanvasGroup;
    [SerializeField] private TMP_Text _levelValueText;
    [SerializeField] private TMP_Text _xpValueText;
    [SerializeField] private TMP_Text _xpReceivedText;
    
    // Visual properties
    [Header("Visual properties")]
    [SerializeField] private float _barFadedAlpha = 0.2f;
    [SerializeField] private float _xpValueTextScaleWhenAnimated = 1.3f;
    [SerializeField] private float _fillingBarRate = 0.02f;
    [SerializeField] private float _canvasGroupFadeInDuration = 0.2f;

    private Tweener _xpBarTween;
    
    private void Start()
    {
        SetUpBarByLevel();
        _barCanvasGroup.alpha = _barFadedAlpha;
        _effectsCanvasGroup.alpha = 0f;
        SingletonManager.Instance.CurrenciesManager.XPChangedBy += UpdateUI;
    }

    private void OnDestroy()
    {
        _xpBarTween?.Kill();
        SingletonManager.Instance.CurrenciesManager.XPChangedBy -= UpdateUI;
    }

    private void SetUpBarByLevel()
    {
        var currentLevel = SingletonManager.Instance.CurrenciesManager.Data.CurrentLevel;
        var minXP = Mathf.RoundToInt(GameConfig.Data.Gameplay.XPSystemConfig.XPNeededPerLevelAnimationCurve.Evaluate(currentLevel));
        var maxXP = Mathf.RoundToInt(GameConfig.Data.Gameplay.XPSystemConfig.XPNeededPerLevelAnimationCurve.Evaluate(
            currentLevel + 1));

        // Reached max level
        if (minXP == maxXP)
        {
            minXP--;
        }

        _xpProgressSlider.minValue = minXP;
        _xpProgressSlider.maxValue = maxXP;

        var currentXP = SingletonManager.Instance.CurrenciesManager.Data.CurrentXP;
        _xpProgressSlider.value = currentXP;
        _levelValueText.text = currentLevel.ToString();
        _xpValueText.text = currentXP.ToString();
    }
    
    private void UpdateUI(int amountToAdd)
    {
        var killedTween = _xpReceivedText.DOKill();
        if (killedTween > 0)
        {
            HideXPTextAdded();
        }
        
        _xpBarTween?.Kill();
        var finalValue = SingletonManager.Instance.CurrenciesManager.Data.CurrentXP;

        _xpReceivedText.text = $"+ {amountToAdd}";
        _xpReceivedText.DOFade(1f, 0.2f);
        _xpReceivedText.transform.DOPunchScale(Vector3.one * 0.4f, 0.2f, 1, 0f);
        
        _effectsCanvasGroup.DOFade(1f, _canvasGroupFadeInDuration);
        _barCanvasGroup.DOFade(1f, _canvasGroupFadeInDuration);
        
        var duration = Mathf.Clamp(_fillingBarRate * amountToAdd , 0.5f, 5f);
        
        _xpValueText.transform.DOScale(Vector3.one * _xpValueTextScaleWhenAnimated, 0.2f);
        _xpBarTween = _xpProgressSlider.DOValue(finalValue, duration);
        _xpBarTween.OnUpdate(FillingBarOnUpdate).onComplete = () =>
        {
            StartCoroutine(AnimationComplete());
        };
    }

    private void HideXPTextAdded()
    {
        _xpReceivedText.transform.localScale = Vector3.one;
        _xpReceivedText.DOFade(0f, 0.2f);
    }

    private void FillingBarOnUpdate()
    {
        _xpValueText.text = _xpProgressSlider.value.ToString("0");
        if (_xpProgressSlider.value < _xpProgressSlider.maxValue)
        {
            return;
        }
        
        LevelUp();
        var modalWindow = new ModalWindowData("LEVEL UP!",
            $"Level {SingletonManager.Instance.CurrenciesManager.Data.CurrentLevel} reached!", null, 
            false, ResumeTween);
        SingletonManager.Instance.GameManager.ShowModalWindow(modalWindow);
    }

    private void LevelUp()
    {
        if (SingletonManager.Instance.CurrenciesManager.LevelUp())
        {
            _xpBarTween.Pause();
            return;
        }
   
        _xpBarTween.Kill();
        StartCoroutine(AnimationComplete());
        SetUpBarByLevel();
    }
    
    private void ResumeTween()
    {
        SetUpBarByLevel();
        _xpBarTween.Play();
    }
    
    private IEnumerator AnimationComplete()
    {
        _xpValueText.transform.DOScale(Vector3.one, 0.3f);
        _effectsCanvasGroup.DOFade(0f, 0.5f);
        yield return new WaitForSeconds(2f);
        _barCanvasGroup.DOFade(_barFadedAlpha, 1f);
        HideXPTextAdded();
    }
}
