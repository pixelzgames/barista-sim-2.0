public enum Quality
{
    None = -1,
    Commodity = 0,
    Premium = 1,
    Specialty = 2,
    Exotic = 3,
    Competition = 4
}

public enum TastingDescriptor
{
    Bad = -1,
    OPENED = 0,
    Fruity,
    Floral,
    Chocolatey,
    Winey,
    Acidic,
    Vegetative,
    Malty,
    Tobacco,
    Smokey,
    Spiced,
    Nutty,
    Sweet,
    Clean,
    Creamy,
    Vibrant,
    Earthy,
    Caramel
}