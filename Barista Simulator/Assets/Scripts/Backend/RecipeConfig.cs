﻿using GD.MinMaxSlider;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "NewRecipeConfig", menuName = "BRAG/Recipe/New Recipe Config")]
[SuppressMessage("ReSharper", "InconsistentNaming")]
public class RecipeConfig: ScriptableObject
{
    public string RecipeName;
    public RecipeType RecipeType;
    public Sprite Icon;
    public float NPCWaitTimeMinimum;

    [Header("Quality")]
    [MinMaxSlider(0, 100)]
    public Vector2Int IdealTemperature;

    public RecipeConfig(string recipeName)
    {
        RecipeName = recipeName;
    }
    
    public static RecipeConfig ValidateRecipe(RecipeConfig recipe)
    {
        foreach (var t in GameConfig.GetConfigData().Gameplay.RecipesOnMenu.Items.Where(recipe.Equals))
        {
            Debug.Log("Equal to recipe: " + t.Value.RecipeName);
            recipe.RecipeName = t.Value.RecipeName;

            return recipe;
        }

        return null;
    }
}

public enum RecipeType
{
    Americano,
    BlackCoffee,
    Cortado, // GG
    InstantCoffee, // GG
    Espresso,
    DoubleEspresso,
    Ristretto, // GG
    Lungo, 
    Latte,
    FlatWhite,
    LongBlack, // GG
    Cappuccino,
    Macchiato, // GG
    Cupping // GG
}
