using System;
using System.Collections.Generic;
using System.Linq;
using HighlightPlus;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

public class Interactable : MonoBehaviour, IWorldDataPersistence
{
    public event Action<ActionInputConfig> BeginInteraction = delegate {  };
    public event Action<ActionInputConfig> EndInteraction = delegate {  };

    public Collider Collider => _collider;
    
    [SerializeField] private bool _isWorldSerializable = true;
    [SerializeField] private bool _ignoreFromInteraction;
    [SerializeField] protected ObjectStateEventController _objectStateEventController;

    [SerializeField, Range(-0.2f,0.2f)] private float _screenWidthPercentageUIOffset;
    [SerializeField, Range(-0.2f,0.2f)] private float _screenHeightPercentageUIOffset;

    [SerializeField]
    protected InteractDirection _interactionDirection = InteractDirection.Front | InteractDirection.Left | InteractDirection.Right | InteractDirection.Back;
    
    protected Collider _collider;
    public Recipe CurrentRecipe => GetLiquidCoffeeData()?.Content as Recipe;
    public string InteractableName => _interactableName;
    public Sprite InteractableIcon => _interactableIcon;
    public bool IgnoreFromInteraction
    {
        get => _ignoreFromInteraction;
        set => _ignoreFromInteraction = value;
    }
    public Vector3 UIOffsetInScreenPercentage => new Vector2(_screenWidthPercentageUIOffset, _screenHeightPercentageUIOffset);
    
    /// <summary>
    /// Last time an input was started on this interactable
    /// </summary>
    protected double _lastInputStartedTime;
    /// <summary>
    /// List of all the possible current interactions at the current time / state
    /// </summary>
    protected List<ActionInputConfig> _currentInteractions = new ();
    
    /// <summary>
    /// Is this object currently the one highlighted by the player?
    /// </summary>
    protected bool _currentlyHighlighted;
    protected Rigidbody _rb;

    private bool _interacting;
    private InputAction.CallbackContext _lastInput;
    private float _currentInputTime;
    private HighlightEffect _highlightEffect;
    private bool _animating;
    private TemperatureUIController _temperatureUIController;
    protected Pickable _inHand;
    protected InteractionMode _interactionMode;

    /// <summary>
    /// All available interactions possible with this interactable
    /// </summary>
    [SerializeField, Required]
    protected InteractableActionsConfig _interactionsConfig;

    /// <summary>
    /// Actions that are currently available based on the interaction mode.
    /// </summary>
    protected readonly List<ActionInputConfig> _availableActions = new();

    [Header("Interactable front-end properties")] 
    [SerializeField] protected Sprite _interactableIcon;
    [SerializeField] protected string _interactableName;
    [field: SerializeField] public InteractionSocket InteractionSocket { get; private set; }

    private ActionInputConfig _currentAction;
    private byte? _prefabID;
    protected BRAGObjectData _data = new();
    
    private List<ActionInputConfig> _mainActionDuplicates = new();
    private List<ActionInputConfig> _secondaryActionDuplicates = new();
    private List<ActionInputConfig> _thirdActionDuplicates = new();
    private List<ActionInputConfig> _fourthActionDuplicates = new();
    
    private bool _duplicateInteractionUIShown;
    private List<ActionInputConfig> _currentDuplicateInteractions = new();
    private ActionInputConfig.ActionButton _currentDuplicateCategoryButtonType;

    #region Monobehaviour methods

    protected virtual void Start()
    {
       if (_isWorldSerializable)
       {
           SingletonManager.Instance.WorldObjectsHandler.RegisterWorldObject(this);
       }
    }

    protected virtual void Awake()
    {
        _collider = GetComponent<Collider>();
        _rb = GetComponent<Rigidbody>();
        _highlightEffect = GetComponent<HighlightEffect>();
        _temperatureUIController = GetComponentInChildren<TemperatureUIController>();
    }

    private void OnEnable()
    {
        SingletonManager.Instance.GameManager.InteractionModeChanged += OnInteractionModeChanged;
        UpdateAvailableActions(SingletonManager.Instance.GameManager.CurrentInteractionMode);
    }

    protected virtual void Update()
    {
        TemperateLiquid();
        
        if (_interacting)
        {
            _currentInputTime += Time.deltaTime;
            
            if (_duplicateInteractionUIShown)
            {
                UpdateDuplicateInteractions();
                return;
            }
            
            UpdateInteractions();
            return;
        }
        
        _currentInputTime = 0f;
        // Feed null inputs to duplicate UI if it is shown but nothing is pressed
        if (_duplicateInteractionUIShown)
        {
            SingletonManager.Instance.InteractionUIController.GetSelectedDuplicateAction(_currentInputTime);
        }
    }

    private void UpdateDuplicateInteractions()
    {
        if (!_lastInput.action.name.Equals(_currentDuplicateCategoryButtonType.ToString()))
        {
            return;
        }
        
        var selectedAction = SingletonManager.Instance.InteractionUIController.GetSelectedDuplicateAction(_currentInputTime);
        if (selectedAction == null)
        {
            return;
        }
        
        ExecuteInteraction(selectedAction, _currentInputTime);
    }

    protected virtual void OnDestroy()
    {
        UnbindInput();
        if (_isWorldSerializable)
        {
            SingletonManager.Instance.WorldObjectsHandler.UnregisterWorldObject(this);
        }
        
        SingletonManager.Instance.GameManager.InteractionModeChanged -= OnInteractionModeChanged;
    }

    #endregion
    
    #region Private methods
    
    private void UpdateInteractions()
    {
        if (!_currentlyHighlighted || _currentInteractions.Count == 0)
        {
            return;
        }
        
        foreach (var currentInteraction in _currentInteractions)
        {
            // Skip if not pressing input relevant to this element
            // (comparing their string name from the INPUT to Scriptable object)
            if (!_lastInput.action.name.Equals(currentInteraction.Button.ToString()))
            {
                continue;
            }

            if (ExecuteInteraction(currentInteraction, _currentInputTime))
            {
                continue;
            }
            
            break;
        }
    }

    private void EnterDuplicateMode(List<ActionInputConfig> duplicates)
    {
        _currentDuplicateInteractions = duplicates;
        SingletonManager.Instance.InteractionUIController.ShowDuplicateInteractions(true, _currentDuplicateInteractions.ToArray());
        _duplicateInteractionUIShown = true;
    }

    private void ExitDuplicateMode()
    {
        _currentDuplicateInteractions.Clear();
        SingletonManager.Instance.InteractionUIController.ShowDuplicateInteractions(false, _currentDuplicateInteractions.ToArray());
        _duplicateInteractionUIShown = false;
    }

    /// <summary>
    /// Reads the data from the action and check whether to execute animations and so on...
    /// </summary>
    /// <param name="currentInteraction">Pressed action</param>
    /// <param name="inputTime">Action current held time</param>
    /// <returns></returns>
    private bool ExecuteInteraction(ActionInputConfig currentInteraction, float inputTime)
    {
        if (currentInteraction.AnimationConfig &&
            currentInteraction.AnimationConfig.AnimationDelay == AnimationActionDelayType.Instant)
        {
            BeginInteraction?.Invoke(currentInteraction);
            _currentAction = currentInteraction;
        }

        if (inputTime <= currentInteraction.HoldTime)
        {
            if (currentInteraction.AnimationConfig && currentInteraction.AnimationConfig.AnimationDelay == AnimationActionDelayType.InputLength)
            {
                BeginInteraction?.Invoke(currentInteraction);
                _currentAction = currentInteraction;
            }

            return true;
        }

        if (currentInteraction.IsDuplicateCategory)
        {
            ExecuteDuplicateAction(currentInteraction.Button);
            _currentAction = null;
            return false;
        }
        
        ExitDuplicateMode();
        _currentAction = null;
        ExecuteAction(currentInteraction.name);
        return false;
    }

    private void OnInteractionModeChanged(InteractionMode mode)
    {
        _interactionMode = mode;
        UpdateAvailableActions(mode);
    }

    protected virtual void UpdateAvailableActions(InteractionMode mode)
    {
        _availableActions.Clear();

        if (!_interactionsConfig || _interactionsConfig.Actions.Length <= 0)
        {
            return;
        }
        
        // Update available actions list to reflect mode change
        foreach (var action in _interactionsConfig.Actions)
        {
            if (action.InteractionMode.HasFlag(mode))
            {
                _availableActions.Add(action);
            }
        }
    }
    
    private void TemperateLiquid()
    {
        if (this is not IContainer<Liquid> script || script.GetContainer() == null || script.GetContainer().Content == null)
        {
            return;
        }
        
        script.GetContainer().Content.ChangeTemperature();
        if (_temperatureUIController != null &&
            script.GetContainer().Content.Temperature.TemperatureChangeState == TemperatureChangeState.Moving &&
            script.GetContainer().Content.Servings > 0f)
        {
            _temperatureUIController.ShowTemperature(script.GetContainer().Content.GetTemperatureState(),
                script.GetContainer().Content.Temperature.Current,
                Liquid.MAX_TEMP);
            RefreshUI();
        }
    }
    
    #endregion

    #region Protected methods
    
    protected ActionInputConfig GetActionByName(string input)
    {
        if (_availableActions.Count == 0)
        {
            return null;
        }
        
        var valueToReturn = _availableActions.FirstOrDefault(action => action.name.Contains(input));
        if(valueToReturn == null)
        {
            Debug.LogWarning($"No action matched the string provided: {input} on the game object {gameObject.name}");
        }
        
        return valueToReturn;
    }

    protected void ShowInteractionUI()
    {
        ExtractDuplicateActions();
        
        SingletonManager.Instance.InteractionUIController.ShowDuplicateInteractions(true, _currentDuplicateInteractions.ToArray());
        
        SingletonManager.Instance.InteractionUIController.ShowContextualUI(_currentlyHighlighted ? this : null,
            _currentInteractions.ToArray());
        RefreshUI();
    }

    private void ExtractDuplicateActions()
    {
        // Group interactions by button type
        var interactionsByButton = _currentInteractions
            .GroupBy(x => x.Button)
            .ToDictionary(g => g.Key, g => g.ToList());

        // Identify and store duplicates for each action
        _mainActionDuplicates = GetDuplicates(interactionsByButton, ActionInputConfig.ActionButton.MainAction,
            out var mainCategoryActionConfig);
        _secondaryActionDuplicates = GetDuplicates(interactionsByButton, ActionInputConfig.ActionButton.SecondaryAction,
            out var secondaryCategoryActionConfig);
        _thirdActionDuplicates = GetDuplicates(interactionsByButton, ActionInputConfig.ActionButton.ThirdAction,
            out var thirdCategoryActionConfig);
        _fourthActionDuplicates = GetDuplicates(interactionsByButton, ActionInputConfig.ActionButton.FourthAction,
            out var fourthCategoryActionConfig);

        // Remove all duplicates from _currentInteractions
        _currentInteractions = _currentInteractions            .Except(_mainActionDuplicates
                .Concat(_secondaryActionDuplicates)
                .Concat(_thirdActionDuplicates)
                .Concat(_fourthActionDuplicates))
            .ToList();

        if (mainCategoryActionConfig)
        {
            _currentInteractions.Add(mainCategoryActionConfig);
        }
        
        if (secondaryCategoryActionConfig)
        {
            _currentInteractions.Add(secondaryCategoryActionConfig);
        }
        
        if (thirdCategoryActionConfig)
        {
            _currentInteractions.Add(thirdCategoryActionConfig);
        }
        
        if (fourthCategoryActionConfig)
        {
            _currentInteractions.Add(fourthCategoryActionConfig);
        }
    }
    
    private List<ActionInputConfig> GetDuplicates(
        Dictionary<ActionInputConfig.ActionButton, List<ActionInputConfig>> interactionsByButton,
        ActionInputConfig.ActionButton button, out ActionInputConfig categoryActionConfig)
    {
        if (!interactionsByButton.TryGetValue(button, out var interactions) || interactions.Count <= 1)
        {
            categoryActionConfig = null;
            return new List<ActionInputConfig>();
        }

        categoryActionConfig = ScriptableObject.CreateInstance<ActionInputConfig>();
        categoryActionConfig.Button = button;
        categoryActionConfig.IsDuplicateCategory = true;
        categoryActionConfig.ActionDisplayName = interactions[0].Category + "..."; //TODO agglomerate all configs to find category name
        categoryActionConfig.name = $"{button}";
        return interactions;
    }

    protected void RefreshUI()
    {
        if (SingletonManager.Instance.InteractionUIController == null)
        {
            return;
        }
        
        SingletonManager.Instance.InteractionUIController.RefreshInfoCanvases();
    }
    
    #endregion Protected methodsm
    
    #region Virtual methods

    public virtual void InitializeData()
    {
        _prefabID = _data.PrefabID;
        var myTransform = transform;
        myTransform.position = _data.Position.ToVector3();
        myTransform.rotation = _data.Rotation;
        myTransform.localScale = _data.Scale.ToVector3();
        if (_rb)
        {
            _rb.isKinematic = _data.IsKinematic;
        }

        if (_collider)
        {
            _collider.enabled = _data.IsColliderEnabled;
        }

        if (GetLiquidCoffeeData() != null)
        {
            GetLiquidCoffeeData().Content = _data.CurrentRecipe;
        }
    }
    
    public virtual Interactable GetInteractableDownTheChain()
    {
        return this;
    }
    
    public virtual void Highlight(bool highlighted)
    {
        _currentlyHighlighted = highlighted;
        
        if (!_currentlyHighlighted)
        {
            SingletonManager.Instance.InteractionUIController.HideContextualUI();
            _lastInputStartedTime = Mathf.Infinity;
            
            if (_interacting)
            {
                OnAnyActionButtonStopped(default);
            }
        }
        
        if (_highlightEffect != null)
        {
            _highlightEffect.highlighted = _currentlyHighlighted;
        }
        
        if (!_currentlyHighlighted)
        {
            if (!_duplicateInteractionUIShown)
            {
                return;
            }
            
            ExitDuplicateMode();
            return;
        }
        
        SetCurrentPossibleInteractions();
    }

    public bool CompareDirection(InteractDirection interactDirection)
    {
        return _interactionDirection.HasFlag(interactDirection);
    }
    
    protected virtual void ExecuteAction(string actionName)
    {
        // if (actionName == "")
        // {
        //     Foo();
        // }
        
        _interacting = false;
        StopAnimation();
        SetCurrentPossibleInteractions();
    }

    private void ExecuteDuplicateAction(ActionInputConfig.ActionButton button)
    {
        var duplicates = button switch
        {
            ActionInputConfig.ActionButton.MainAction => _mainActionDuplicates,
            ActionInputConfig.ActionButton.SecondaryAction => _secondaryActionDuplicates,
            ActionInputConfig.ActionButton.ThirdAction => _thirdActionDuplicates,
            ActionInputConfig.ActionButton.FourthAction => _fourthActionDuplicates,
            _ => null
        };

        _currentDuplicateCategoryButtonType = button;
        EnterDuplicateMode(duplicates);
        
        _interacting = false;
        StopAnimation();
        SetCurrentPossibleInteractions();
    }
    
    protected virtual void OnAnyActionButtonStarted(InputAction.CallbackContext obj)
    {
        if (!_currentlyHighlighted)
        {
            return;
        }
        
        _interacting = true;
        _lastInput = obj;
        _lastInputStartedTime = obj.startTime;
    }
    
    protected virtual void OnAnyActionButtonStopped(InputAction.CallbackContext obj)
    {
        _interacting = false;
        _lastInput = default;
        if (!_currentlyHighlighted)
        {
            return;
        }

        if (_currentAction != null)
        {
            EndInteraction?.Invoke(_currentAction);
        }

        _currentAction = null;
        StopAnimation();

        if (obj.action.name != _currentDuplicateCategoryButtonType.ToString() && _duplicateInteractionUIShown)
        {
            ExitDuplicateMode();
        }
        
        //HERE 
        //To be overriden
        SetCurrentPossibleInteractions();
    }

    /// <summary>
    /// Refreshes the info canvases upon any interactions or data/state changes
    /// </summary>
    protected virtual void SetCurrentPossibleInteractions()
    {
        _currentInteractions.Clear();
        _inHand = PlayerInteractionManager.Instance.CurrentlyInMainHand;
    }
    
    /// <summary>
    /// Return all the info formatted to be displayed on the front-end UI
    /// </summary>
    /// <returns>A string that will be display to players</returns>
    public virtual string GetInfoToDisplay()
    {
        return string.Empty;
    }
    #endregion

    #region Public methods

    public void BindInput()
    {
        if (SingletonManager.Instance.InputManager == null)
        {
            return;
        }
        
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started += OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.canceled += OnAnyActionButtonStopped;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.started += OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.canceled += OnAnyActionButtonStopped;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.started += OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.canceled += OnAnyActionButtonStopped;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.started += OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.canceled += OnAnyActionButtonStopped;
    }
    
    public void UnbindInput()
    {
        if (SingletonManager.Instance.InputManager == null)
        {
            return;
        }

        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.started -= OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.MainAction.canceled -= OnAnyActionButtonStopped;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.started -= OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.SecondaryAction.canceled -= OnAnyActionButtonStopped;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.started -= OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.ThirdAction.canceled  -= OnAnyActionButtonStopped;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.started -= OnAnyActionButtonStarted;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.FourthAction.canceled -= OnAnyActionButtonStopped;
    }

    #endregion
    
    private void StopAnimation()
    {
        if (!PlayerInteractionManager.Instance)
        {
            return;
        }
        
        PlayerInteractionManager.Instance.StopInteractionAnimation();
    }
    
    public virtual void ToggleCollider(bool on)
    {
        if (_collider != null)
        {
            _collider.enabled = on;
        }
    }

    public virtual Quality GetBeansDataQuality()
    {
        var data = GetCoffeeData();
        return data != null ? data.CoffeeQuality.Quality : Quality.None;
    }

    public CoffeeData GetCoffeeData()
    {
        var solidContainer = GetSolidBeansContainer();
        if (solidContainer != null && !solidContainer.IsEmpty() && solidContainer.Content is GreenBeans beans)
        {
            return beans.CoffeeData;
        }

        var coffeeLiquidData = GetLiquidCoffeeData();
        if (coffeeLiquidData != null && !coffeeLiquidData.IsEmpty() && coffeeLiquidData.Content is Recipe recipe)
        {
            return recipe.Data;
        }
        
        return null;
    }
    
    public virtual LiquidContainer GetLiquidWaterData()
    {
        return null;
    }

    public virtual LiquidContainer GetLiquidMilkData()
    {
        return null;
    }
    
    public virtual LiquidContainer GetLiquidCoffeeData()
    {
        return null;
    }

    public virtual SolidContainer GetSolidBeansContainer()
    {
        return null;
    }

    public void LoadData(BRAGObjectData data)
    {
        _data = data;
        InitializeData();
    }

    public virtual BRAGObjectData UpdateObjectData()
    {
        if (_prefabID == null)
        {
            _prefabID = PrefabsUtility.GetKeyFromPrefab(gameObject);
            _data.PrefabID = _prefabID;
        }

        _data.Name = InteractableName;
        var myTransform = transform;
        _data.Position = myTransform.position.ToSerializedVector3();
        _data.Rotation = myTransform.rotation;
        _data.Scale = myTransform.localScale.ToSerializedVector3();
        if (_rb)
        {
            _data.IsKinematic = _rb.isKinematic;
        }

        if (_collider)
        {
            _data.IsColliderEnabled = _collider.enabled;
        }
        
        _data.CurrentRecipe = CurrentRecipe;
        return _data;
    }
}

[Flags]
public enum InteractDirection
{
    Front = 1,
    Left = 2,
    Right = 4,
    Back = 8
}