﻿using Newtonsoft.Json;
using UnityEngine;

[System.Serializable]
public class Liquid : Ingredient
{
    public const float DEFAULT_ROOMTEMP_CHANGE_RATE = 0f;
    public const float MIN_TEMP = -10f;
    public const float MAX_TEMP = 100f;
    
    [field: SerializeField] 
    public Temperature Temperature { get; protected set; }
    [field: SerializeField] 
    public int Servings { get; set; }
    public float TemperatureGoal { get; private set; } = Temperature.ROOM_TEMP;
    public float TemperatureChangeRatePerSecond { get; private set; } = DEFAULT_ROOMTEMP_CHANGE_RATE;
    public TemperatureState GetTemperatureState()
    {
        return Temperature.Current switch
        {
            <= 22f => TemperatureState.Cold,
            <= 65f => TemperatureState.Warm,
            <= 85f => TemperatureState.Hot,
            <= 120f => TemperatureState.Boiling,
            _ => TemperatureState.None
        };
    }

    public Liquid (Temperature temperature, int servings = 0)
    {
        Temperature = temperature;
        Servings = servings;
    }

    public void SetTemperatureGoal(float newGoal, float newRate)
    {
        TemperatureGoal = newGoal;
        TemperatureChangeRatePerSecond = newRate;
    }
    
    public void ChangeTemperature()
    {
        Temperature.TemperateToByRate(TemperatureGoal,TemperatureChangeRatePerSecond);
        Temperature.Current = Mathf.Clamp(Temperature.Current, MIN_TEMP, MAX_TEMP);
    }
}

[System.Serializable]
public class Water : Liquid
{
    public Water(float waterTemperature, int servings):base(new Temperature(waterTemperature),servings)
    {
        _ingredientName = GetTemperatureState() + " " + "water";
    }

    public void SetData(Temperature temperature)
    {
        Temperature = new Temperature(temperature.Current);
    }
}

public class Milk : Liquid
{
    public FoamType FoamType { get; private set; }
    public bool IsFrothed { get; private set; }
    
    public Milk(float temperature, int servings):base(new Temperature(temperature),servings)
    {
        _ingredientName = GetTemperatureState() + " " + "milk";
    }

    public void SetIsFrothed(bool frothed, FoamType foamType)
    {
        IsFrothed = frothed;
        FoamType = foamType;
    }

    public void SetData(Temperature temperature, bool frothed, FoamType foamType)
    {
        SetIsFrothed(frothed, foamType);
        Temperature = new Temperature(temperature.Current);
    }
}

public enum FoamType
{
    None,
    HotMilk,
    Large,
    Micro,
    Yogurt
}

[System.Serializable]
public class Recipe : Liquid
{
    [JsonIgnore]
    public RecipeConfig Config { get; }
    public CoffeeData Data { get; }
    public bool IsBadQuality { get; private set; }

    //TODO Make this work with serialization
    [JsonConstructor]
    public Recipe(): base(new Temperature(0f)){}
    
    public Recipe(Recipe recipe, int servings) : base(new Temperature(recipe.Temperature.Current), servings)
    {
        Config = recipe.Config;
        Data = recipe.Data;
        IsBadQuality = recipe.IsBadQuality;
    }
    
    public Recipe(RecipeConfig config, float recipeTemperature, int servings, CoffeeData coffeeData, bool isBadQuality = false) : base(new Temperature(recipeTemperature), servings)
    {
        _ingredientName = GetTemperatureState() + " " + "coffee";
        Config = config;
        Data = coffeeData;
        IsBadQuality = isBadQuality;
    }

    public Recipe(Recipe recipe, bool isBadQuality = false):base(new Temperature(recipe.Temperature.Current), recipe.Servings)
    {
        Config = recipe.Config;
        IsBadQuality = isBadQuality;
        Data = recipe.Data;
    }
}
