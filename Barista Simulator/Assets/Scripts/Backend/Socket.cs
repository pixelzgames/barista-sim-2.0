﻿using System;
using DG.Tweening;
using UnityEngine;

public class Socket : MonoBehaviour
{
    public Action StateChange = delegate {  };
    
    [field: SerializeField]
    public Interactable CurrentInteractable { get; protected set; }

    protected virtual void Awake()
    {
        var child = GetComponentInChildren<Interactable>();
        if (child != null)
        {
            AddToSocket(child, false);
        }
    }

    public bool IsEmpty => CurrentInteractable == null;
    
    private void RemoveFromSocket(Pickable pick, PlayerHand hand)
    {
        if (CurrentInteractable is Pickable pickable)
        {
            pickable.OnPickUp -= RemoveFromSocket;
        }
        
        // Remove from parent
        CurrentInteractable.transform.SetParent(null);
        
        pick.IgnoreFromInteraction = false;
        SingletonManager.Instance.WorldObjectsHandler.UnregisterWorldObject(CurrentInteractable);
        CurrentInteractable = null;
        StateChange?.Invoke();
    }

    public void AddToSocket(Interactable interactable, bool moveToSocketPosition = true)
    {
        CurrentInteractable = interactable;
        if (interactable is Pickable pickable)
        {
            pickable.OnPickUp += RemoveFromSocket;
        }

        interactable.IgnoreFromInteraction = true;
        var interactableTransform = CurrentInteractable.transform;
        var localTransform = transform;
        
        // Add to parent
        interactable.transform.SetParent(transform);

        if (moveToSocketPosition)
        {
            interactableTransform.DOMove(localTransform.position, 0.2f);
        }
        
        SingletonManager.Instance.WorldObjectsHandler.UnregisterWorldObject(interactable);
        StateChange?.Invoke();
    }
}
