﻿using UnityEngine;

[System.Serializable]
public class Temperature
{
    public float Current { get; set; }
    public TemperatureChangeState TemperatureChangeState{ get; private set; }
    
    public const float ROOM_TEMP = 25f;
    
    /// <summary>
    /// Temperature of anything
    /// </summary>
    /// <param name="currentTemperature">Current Temperature</param>
    public Temperature(float currentTemperature)
    {
        Current = currentTemperature;
    }
    
    public void TemperateToByRate(float goalTemperature, float ratePerSecond)
    {
        if (Current > goalTemperature)
        {
            ratePerSecond *= -1;
        }
        
        if (Current > goalTemperature + 0.1f || Current < goalTemperature - 0.1f)
        {
            Current += ratePerSecond * Time.deltaTime;
            TemperatureChangeState = TemperatureChangeState.Moving;
        }
        else
        {
            TemperatureChangeState = TemperatureChangeState.Constant;
        }
    }
}

public enum TemperatureState
{
    Boiling,
    Hot,
    Warm,
    Cold,
    None
}

public enum TemperatureChangeState
{
    Moving,
    Constant
}
