using System;

public class ProgressionControllerBase
{
   public event Action<ProgressionControllerBase> OnProgressionEventFinished;
   public ProgressionEvent ProgressionEvent => _progressionEvent;
   
   protected readonly ProgressionEvent _progressionEvent;
   
   public ProgressionControllerBase(ProgressionEvent progressionEvent)
   {
      _progressionEvent = progressionEvent;
      SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted += GameEventReceived;
   }

   private void GameEventReceived(GameEvent gameEvent)
   {
      if (gameEvent.Key != _progressionEvent.EventToTrigger.Key)
      {
         return;
      }
         
      if (gameEvent.Value == _progressionEvent.EventToTrigger.Value &&
          gameEvent.ID == _progressionEvent.EventToTrigger.ID)
      {
         ExecuteEvent();
      }
   }

   protected virtual void ExecuteEvent()
   {
      SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted -= GameEventReceived;
   }
    
   protected void Finish()
   {
      OnProgressionEventFinished?.Invoke(this);
   }

   public void Destroy()
   {
      OnProgressionEventFinished = null;
      SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted -= GameEventReceived;
   }
}
