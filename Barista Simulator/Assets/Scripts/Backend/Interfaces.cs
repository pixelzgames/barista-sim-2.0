﻿using UnityEngine;

public abstract class Interfaces
{
   
}

public interface IInteractable
{
    void Highlight(bool on);
}

public interface IPickable : IInteractable
{
    void PickUp();
    void Pickup(PlayerHand hand);
    void Drop(PlayerHand.HandType hand, bool unEquip);
    void PlaceOnASocket(Socket socket, bool alignRotation, bool disableCollider);
    void Throw(float power);
    void ToggleCollider(bool on);
}

public interface IContainer<T>
{
    void InitContainer();
    Container<T> GetContainer();
}

public interface IGridSocket
{
    bool IsOccupied();
    void SetOccupation(Movable movable);
    Vector3 GetWorldPosition();
    Interactable GetInteractableInSocket();
    Interactable GetSocketInteractable();
    void RemoveInteractableFromSocket();
}

public interface IAnimatableSocket
{
    public AnimationSocketDictionary AnimationSocketDictionary { get; set; }
    public void AddToAnimatableSocket(InteractableAnimationType animationType, Pickable pickable);
    public Pickable RemoveFromAnimatableSocket();

    public void AnimateSocketInteractable();
}

public interface IInteractionTransformSocket
{
    Transform GetRandomInteractionPoint();
    Transform GetClosestInteractionPoint(Vector3 position);
}

public interface IBreakable
{
    bool IsBroken();
    void Break();
    void Repair();
}

public interface IGameplayMechanic
{
    void OnGameplayTick();
}

public interface ILevelLoadingWaitable
{
    bool IsReady();
}

public interface ILoaderWaitable
{
    event System.Action Completed;
    void Execute();
}

public interface IUnlockable
{
    void SetVisibleStyle(bool visible);
    void SetIsNewStyle(bool isNew);
    void SetPurchasedStyle(bool purchased);
}

public interface IBeanOSPage
{
    public void MakePageActive();
    public void ClosePage();
}

public interface IKnockableMechanic
{
    public Interactable GetInteractable();
    public int GetKnocksRequiredToEmpty();
}

public interface ICleanable
{
    public bool Dirty { get; set; }
    public void Clean();
}

public enum LevelResult
{
    Low,
    Empty,
    None
}
