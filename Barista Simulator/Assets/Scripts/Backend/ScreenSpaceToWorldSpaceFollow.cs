﻿using UnityEngine;
using Screen = UnityEngine.Device.Screen;

public class ScreenSpaceToWorldSpaceFollow : MonoBehaviour
{
    [SerializeField] private Collider _boundsToFollow;
    
    [SerializeField] private float _worldSpaceHorizontalUIOffset;
    [SerializeField] private float _worldSpaceVerticalUIOffset;
    
    [SerializeField, Range(-0.2f,0.2f)] private float _screenWidthPercentageUIOffset;
    [SerializeField, Range(-0.2f,0.2f)] private float _screenHeightPercentageUIOffset;
    
    
    private RectTransform _rect;
    private Vector2 _screenResolution;

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _screenResolution = new Vector2(Screen.width, Screen.height);
    }
    
    private void FixedUpdate()
    {
        if (_boundsToFollow == null)
        {
            return;
        }

        var screenPosition = Camera.main.WorldToScreenPoint(_boundsToFollow.bounds.center +
                                                            new Vector3(_worldSpaceHorizontalUIOffset,
                                                                _worldSpaceVerticalUIOffset));
        _rect.position = screenPosition + new Vector3(_screenWidthPercentageUIOffset * _screenResolution.x,
            _screenHeightPercentageUIOffset * _screenResolution.y, 0f);
    }
}
