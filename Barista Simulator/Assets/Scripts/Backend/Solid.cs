[System.Serializable]
public class Solid : Ingredient
{
    public int Servings { get; set; }
    public Solid(int servings)
    {
        Servings = servings;
    }
}
