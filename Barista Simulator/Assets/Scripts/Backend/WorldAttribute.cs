using UnityEngine;

public class WorldAttribute : MonoBehaviour
{
    public WorldAttributeType WorldAttributeType = WorldAttributeType.None;
    public int Value;

    private void OnEnable()
    {
        if (SingletonManager.Instance.CafeManager == null)
        {
            return;
        }
        
        SingletonManager.Instance.CafeManager.AddRemoveWorldAttribute(this);
    }

    private void OnDisable()
    {
        if (SingletonManager.Instance.CafeManager == null)
        {
            return;
        }
        
        SingletonManager.Instance.CafeManager.AddRemoveWorldAttribute(this);
    }
}

public enum WorldAttributeType
{
    Attractiveness,
    Cleanliness,
    Comfort,
    Wifi,
    Music,
    None
}
