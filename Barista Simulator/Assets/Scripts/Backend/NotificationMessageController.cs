using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotificationMessageController : MonoBehaviour
{
    public Action<NotificationSize> NotificationDisappeared = delegate {  };
    
    [SerializeField] private TMP_Text _title;
    [SerializeField] private TMP_Text _subTitle;
    [SerializeField] private TMP_Text _description;
    
    [SerializeField] private Image _minorPanel;
    [SerializeField] private CanvasGroup _canvasGroup;

    private NotificationData _data;

    private void Awake()
    {
        _canvasGroup.alpha = 0f;
    }

    public void InitMessage(NotificationData data)
    {
        _data = data;
        
        _title.text = _data.Title;

        if (_data.Size == NotificationSize.Main)
        {
            _subTitle.text = _data.Subtitle;
            _description.text = _data.Description;
        }
        
        StartCoroutine(ShowNotification());
    }
    
    private IEnumerator ShowNotification()
    {
        switch (_data.Size)
        {
            case NotificationSize.Minor:
            {
                _minorPanel.rectTransform.anchoredPosition = Vector2.zero;
                _canvasGroup.DOFade(1f, 0.2f);
                _minorPanel.rectTransform.DOAnchorPosX(-_minorPanel.rectTransform.rect.width, 0.2f);
                yield return new WaitForSecondsRealtime(_data.TimeToShow);
                _canvasGroup.DOFade(0f, 0.2f);
                yield return _minorPanel.rectTransform.DOAnchorPosX(_minorPanel.rectTransform.rect.width, 0.2f)
                    .WaitForCompletion();
            }
                break;
            case NotificationSize.Main:
            {
                _canvasGroup.DOFade(1f, 1.5f);
                yield return new WaitForSecondsRealtime(_data.TimeToShow);
                yield return _canvasGroup.DOFade(0f, 1.5f).WaitForCompletion();
            }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        NotificationDisappeared?.Invoke(_data.Size);
        DeleteMessage();
    }

    private void OnDestroy()
    {
        DeleteMessage();
    }

    private void DeleteMessage()
    {
        _canvasGroup.DOKill();
        Destroy(gameObject);
    }
}
