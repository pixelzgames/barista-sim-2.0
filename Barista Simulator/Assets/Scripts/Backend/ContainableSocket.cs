using UnityEngine;
using Grid = Backend.Grid.Grid;

public class ContainableSocket : Socket, IGridSocket
{
    private Interactable _socketInteractable;
    private Grid _grid;

    protected override void Awake()
    {
        base.Awake();
        _socketInteractable = GetComponentInParent<Interactable>();
        _grid = FindFirstObjectByType<Grid>();
        _grid.GeneratedGrid += RegisterInGridCell;
        
        if (!_grid.IsGenerated)
        {
            return;
        }
        
        _grid.GeneratedGrid -= RegisterInGridCell;
        RegisterInGridCell();
    }
     
    private void RegisterInGridCell()
    {
        var gridCell = _grid.GetGridCellAtPosition(transform.position);
        gridCell.AddSocketToGridCell(this);
    }

    public bool IsOccupied()
    {
        return !IsEmpty;
    }

    public void SetOccupation(Movable movable)
    {
        if (!IsEmpty)
        {
            return;
        }
        
        AddToSocket(movable);
    }

    public Vector3 GetWorldPosition()
    {
        return transform.position;
    }

    public Interactable GetInteractableInSocket()
    {
        return CurrentInteractable;
    }
    
    /// <summary>
    /// Get the interactable that this socket is owned by
    /// </summary>
    public Interactable GetSocketInteractable()
    {
        return _socketInteractable;
    }

    public void RemoveInteractableFromSocket()
    {
        CurrentInteractable = null;
        StateChange?.Invoke();
    }
}
