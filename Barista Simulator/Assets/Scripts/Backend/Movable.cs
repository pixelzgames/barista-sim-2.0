using System;
using System.Collections.Generic;
using UnityEngine;

public class Movable : Interactable
{
    public MovableClearance MovablePlacementClearanceRequirement => _movablePlacementClearanceRequirement;
    public MovableSurface MovableSurfaceRequirement => _movableSurfaceRequirement;
    public Vector2Int GridPosition { get; set; }
    
    [SerializeField] 
    private MovableClearance _movablePlacementClearanceRequirement;
    [SerializeField] 
    private MovableSurface _movableSurfaceRequirement = MovableSurface.Floor;
    
    private MeshRenderer[] _allMeshRenderers;
    private readonly Dictionary<Renderer, Material[]> _originalMaterials = new();
    private CursorController _cursorController;
    
    
    protected override void Awake()
    {
        base.Awake();

        // TODO: Does this make sense always? probably not 
        GridPosition = new Vector2Int(Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.z));

        _allMeshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (var rend in _allMeshRenderers)
        {
            _originalMaterials.Add(rend, rend.materials);
        }
        
        _cursorController = FindFirstObjectByType<CursorController>();
    }

    public override void InitializeData()
    {
        base.InitializeData();
        GridPosition = _data.MovableData.GridPosition;
        SingletonManager.Instance.GridManager.AddMovableToGrid(this);
    }

    public override BRAGObjectData UpdateObjectData()
    {
        _data = base.UpdateObjectData();
        _data.MovableData.GridPosition = GridPosition;
        return _data;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        AddMovableActions();
    }
    
    private void AddMovableActions()
    {
        if (_interactionMode != InteractionMode.Edit)
        {
            return;
        }
        
        var action = GetActionByName("Move");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        
        action = GetActionByName("Delete");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        
        action = GetActionByName("Place");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Move":
                Select();
                break;
            case "Delete":
                DeleteMovable();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    public void DeleteMovable()
    {
        Destroy(gameObject);
    }

    public void Select(bool fromProductBox = false)
    {
        ToggleCollider(false);
        Highlight(false);
        SingletonManager.Instance.InputManager.ChangeInputMapping(
            SingletonManager.Instance.InputManager.InputMaster.Moveable);

        _cursorController.SelectMovable(this, fromProductBox);
        SingletonManager.Instance.CameraManager.AssignCameraTarget(transform);
    }

    public void Deselect()
    {
        ToggleCollider(true);
        SingletonManager.Instance.InputManager.ChangeInputMapping(
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay);
        
        SetDefaultMaterial();
        SingletonManager.Instance.CameraManager.AssignCameraTarget(PlayerInteractionManager.Instance.transform, 0.5f);
    }

    public void SetMaterial(Material material)
    {
        foreach (var rend in _allMeshRenderers)
        {
            var mats = new Material[rend.materials.Length];
            for (var j = 0; j < rend.materials.Length; j++)
            {
                mats[j] = material;
            }
            
            rend.materials = mats;
        }
    }

    private void SetDefaultMaterial()
    {
        foreach (var (rend, value) in _originalMaterials)
        {
            rend.materials = value;
        }
    }
    
    protected override void UpdateAvailableActions(InteractionMode mode)
    {
        base.UpdateAvailableActions(mode);
        
        foreach (var action in GameConfig.GetConfigData().Gameplay.MovableActionsConfig.Actions)
        {
            if (action.InteractionMode.HasFlag(mode))
            {
                _availableActions.Add(action);
            }
        }
    }
}

[Serializable, Flags]
public enum MovableClearance
{
    Nothing = 0,
    Forward = 1,
    Back = 2,
    Left = 4,
    Right = 8,
    Everything = ~0
}

[Serializable, Flags]
public enum MovableSurface
{
    Nothing = 0,
    Floor = 1,
    Containable = 2,
    Wall = 4
}