using UnityEngine;

public abstract class BRAGBehaviour : MonoBehaviour
{
    protected virtual void Awake()
    {
        SingletonManager.Instance.GameManager.LevelLoaded += OnLevelLoaded;
    }

    protected virtual void OnDestroy()
    {
        SingletonManager.Instance.GameManager.LevelLoaded -= OnLevelLoaded;
    }
    
    protected virtual void OnLevelLoaded(LevelData levelData)
    {
        SingletonManager.Instance.GameManager.LevelLoaded -= OnLevelLoaded;
    }
}
