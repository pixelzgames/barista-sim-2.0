﻿/// <summary>
/// Interactable object that can hold one interactable on/in it. 
/// </summary>
public abstract class Containable : Movable
{
    public ContainableSocket Socket { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        // Find sockets that are currently attached to the containable.
        Socket = gameObject.GetComponentInChildren<ContainableSocket>();
        Socket.StateChange += OnSocketStateChange;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        Socket.StateChange -= OnSocketStateChange;
    }

    public override void InitializeData()
    {
        base.InitializeData();
        // TODO: IDK IF THIS MAKES SENSE, SEE IF MAKES SENSE AND THEN MAKE IT MAKE SENSE
        // Containable has a reference to the BRAGObjectData in its socket, spawns it itself? Will we need to prevent it from spawning on its own? idk...
        
        if (_data.ContainableData.ObjectInSocket == null)
        {
            return;
        }
        
        var go = PrefabsUtility.GetPrefabFromList(_data.ContainableData.ObjectInSocket.PrefabID);
        var bragPrefabHandler = Instantiate(go);
        bragPrefabHandler.PassDataThrough(_data.ContainableData.ObjectInSocket);
        
        if (bragPrefabHandler.TryGetComponent<Movable>(out var movable))
        {
            Socket.SetOccupation(movable);
        }
    }

    protected abstract void OnSocketStateChange();

    protected virtual void Place()
    {
        PlayerInteractionManager.Instance.TryPlaceObject(Socket, false);
    }

    public override Interactable GetInteractableDownTheChain()
    {
        return !Socket.IsEmpty ? Socket.CurrentInteractable.GetInteractableDownTheChain() : base.GetInteractableDownTheChain();
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        //If the containable's socket is empty
        if (Socket.IsEmpty)
        {
            //if hands are not empty
            if (_inHand != null && _inHand.CanPlaceOnContainable)
            {
                var action = GetActionByName("Place");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Place":
                Place();
                if (!Socket.IsEmpty)
                {
                    base.Highlight(false);
                }
                
                break;
        }
        base.ExecuteAction(actionName);
    }
}
