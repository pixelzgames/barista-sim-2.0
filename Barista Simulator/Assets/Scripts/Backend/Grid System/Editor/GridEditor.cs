using Backend.Grid;
using UnityEditor;
using UnityEngine;
using Grid = Backend.Grid.Grid;

[CustomEditor(typeof(Grid), true)]
public class GridEditor : Editor
{
    private int _nearestHandle = -1;
    private bool _controlHeld;
    private bool _drawing;
    private Vector2 _previousMousePosition;
    private Grid _grid;

    public override void OnInspectorGUI()
    {
        _grid = target as Grid;
        
        serializedObject.Update();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("Length"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_cellSize"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_gridLineMaterial"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_cursorController"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_showDebugGrid"));

        EditorGUILayout.Space(20);
        
        var style = new GUIStyle
        {
            fontStyle = FontStyle.Bold,
            normal =
            {
                textColor = Color.white
            },
            fontSize = 12,
            wordWrap = true
        };

        GUILayout.Label("Grid Cell Editor", style);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_gridCellList"));
        
        if (GUILayout.Button(_drawing ? "Stop Drawing " + _grid?.TypeToDraw + " Cells" : "Start Drawing " + _grid?.TypeToDraw + " Cells"))
        {
            _drawing = !_drawing;
        
            if (_drawing)
            {
                _grid?.GenerateGrid();
            }
            else
            {
                EditorUtility.SetDirty(target);
            }
        }
        
        EditorGUILayout.Space(20);
        
        GUILayout.Label("While Drawing is enabled, hold CTRL and mouse over cells to apply the " + _grid?.TypeToDraw + " type to them. Change style with the enum below", style);
        
        EditorGUILayout.Space(15);
        
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_typeToDraw"));
        
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        _grid = target as Grid;
        if (!_grid || !_grid.IsGenerated)
        {
            return;
        }
        
        _nearestHandle = -1;
        _nearestHandle = HandleUtility.nearestControl;
        _controlHeld = Event.current.control;

        if (Event.current.type == EventType.Layout && _drawing)
        {
            var counter = 0;
            for (var i = 0; i < _grid.GridArray.GetLength(0); i++)
            {
                for (var j = 0; j < _grid.GridArray.GetLength(1); j++)
                {
                    Handles.SphereHandleCap(counter, _grid.GridArray[i, j].GetWorldPosition(), Quaternion.Euler(Vector3.up), 0.25f, EventType.Layout);
                    counter++;
                }
            }
        }

        HandleUtility.Repaint();
        
        if (Event.current.type == EventType.Repaint && _drawing)
        {
            if (_drawing && _controlHeld && _nearestHandle <= _grid.GridArray.Length)
            {
                var clickedCoords = GetCoords(_nearestHandle, _grid.GridArray.GetLength(0), _grid.GridArray.GetLength(1));
                _grid.GridArray[clickedCoords.x, clickedCoords.y].CellType = _grid.TypeToDraw;
                _grid.UpdateGridCellData(_nearestHandle, _grid.TypeToDraw);
            }
            
            var counter = 0;
            for (var i = 0; i < _grid.GridArray.GetLength(0); i++)
            {
                for (var j = 0; j < _grid.GridArray.GetLength(1); j++)
                {
                    Handles.color = _nearestHandle == counter ? Color.cyan : Color.blue;

                    switch (_grid.GridArray[i,j].CellType)
                    {
                        case CellType.Cafe:
                            Handles.color = Color.green;
                            break;
                        case CellType.Street:
                            Handles.color = Color.grey;
                            break;
                        case CellType.OutOfBounds:
                            Handles.color = Color.red;
                            break;
                    }
                    
                    Handles.SphereHandleCap(counter, _grid.GridArray[i, j].GetWorldPosition(), Quaternion.Euler(Vector3.up), 0.25f, EventType.Repaint);
                    counter++;
                }
            }
        }
        
        for (var i = 0; i < _grid.GridArray.GetLength(0); i++)
        {
            for (var j = 0; j < _grid.GridArray.GetLength(1); j++)
            {
                GUI.skin.label.fontSize = 30;
                GUI.skin.label.fontStyle = FontStyle.Bold;
                Handles.Label(_grid.GridArray[i, j].GetWorldPosition() + Vector3.up * 0.2f,
                    _grid.GridArray[i, j].GridSockets.Count.ToString());
            }
        }
    }
    
    private Vector2Int GetCoords(int index, int xLength, int yLength)
    {
        int remainder = index % yLength;
        int quotient = (index - remainder) / yLength;
        
        return new Vector2Int(quotient, remainder);
    }
}