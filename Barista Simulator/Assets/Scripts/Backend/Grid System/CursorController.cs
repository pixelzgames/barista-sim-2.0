using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Grid;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;

public class CursorController : MonoBehaviour
{
    public event Action Placed = delegate { };
    public event Action Cancelled = delegate { };
    
    private static readonly int _good = Shader.PropertyToID("_Good");
    
    public bool IsCursorActive { get; private set; }
    
    [SerializeField] private Material _ghostMaterial;
    [SerializeField] private GameObject _cursorGO;
    [SerializeField] private float _cursorSpeed = 5f;

    private Movable _currentMovableSelected;
    private InputMaster _inputMaster;
    private Backend.Grid.Grid _grid;
    private Vector3 _cursorPos;
    private Camera _cam;
    private float _h;
    private float _v;
    private Vector3 _cameraRelativeInput;
    private Vector3 _directionalInputs;
    private bool _canMove;
    private bool _isCurrentPositionValid;
    private IGridSocket _lastGridSocketSelected;
    private GridCell _lastMovableGridCell;
    private Tweener _positionTweener;
    private readonly VerticalityHandler _verticalityHandler = new();

    // Initial OnSelect data
    private MoveableGridSnapshotData _moveableGridSnapshotData;
    
    // Current GridCell data
    private GridCell _currentCursorGridCell;
    private IGridSocket _currentGridSocketSelected;

    private void Awake()
    {
        _cam = Camera.main;
    }
    
    private void OnEnable()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        SingletonManager.Instance.GameManager.InteractionModeChanged += OnInteractionStateChanged;
        _inputMaster.Moveable.RotateRight.performed += RotateMovableRight;
        _inputMaster.Moveable.RotateLeft.performed += RotateMovableLeft;
        _inputMaster.Moveable.Place.performed += PlaceMovableOnGrid;
        _inputMaster.Moveable.Cancel.performed += CancelMovingMovable;
        _inputMaster.Moveable.GridUp.performed += MoveCursorUpGrid;
        _inputMaster.Moveable.GridDown.performed += MoveCursorDownGrid;
    }
    
    private void OnDisable()
    {
        SingletonManager.Instance.GameManager.InteractionModeChanged -= OnInteractionStateChanged;
        _inputMaster.Moveable.RotateRight.performed -= RotateMovableRight;
        _inputMaster.Moveable.RotateLeft.performed -= RotateMovableLeft;
        _inputMaster.Moveable.Place.performed -= PlaceMovableOnGrid;
        _inputMaster.Moveable.Cancel.performed -= CancelMovingMovable;
        _inputMaster.Moveable.GridUp.performed -= MoveCursorUpGrid;
        _inputMaster.Moveable.GridDown.performed -= MoveCursorDownGrid;
    }
    
    private void OnInteractionStateChanged(InteractionMode mode)
    {
        _canMove = mode == InteractionMode.Edit;
    }
    
    private void Start()
    {
        _grid = FindFirstObjectByType<Backend.Grid.Grid>();
    }
    
    private void Update()
    {
        if (!_grid || !_canMove || !_currentMovableSelected)
        {
            return;
        }
        
        CalculateInputDirection();
        MoveCursor();
    }

    private void MoveCursorUpGrid(InputAction.CallbackContext callbackContext)
    {
        if (_currentCursorGridCell == null)
        {
            return;
        }
        
        _currentCursorGridCell.MoveUpGridSocket(_verticalityHandler.MoveIndexUp(_currentCursorGridCell, true));
    }
    
    private void MoveCursorDownGrid(InputAction.CallbackContext callbackContext)
    {
        if (_currentCursorGridCell == null)
        {
            return;
        }

        _currentCursorGridCell.MoveDownGridSocket(_verticalityHandler.MoveIndexDown(_currentCursorGridCell, true));
    }

    private void CalculateInputDirection()
    {
        var movementVector = _inputMaster.Moveable.Move.ReadValue<Vector2>();
        _h = movementVector.x;
        _v = movementVector.y;
        
        //Calculate Camera relative inputs
        var camFacing = _cam.transform.eulerAngles.y; 
        _directionalInputs = new Vector3(_h, 0f, _v);
        _cameraRelativeInput = Quaternion.Euler(0f, camFacing, 0f) * _directionalInputs;
        
        _cursorPos += _cameraRelativeInput * (Time.deltaTime * _cursorSpeed);

        var gridSize = _grid.GetSize;
        
        var x = Mathf.Clamp(_cursorPos.x,0f,gridSize.x);
        var y = Mathf.Clamp(_cursorPos.z, 0f, gridSize.y);
        _cursorPos = new Vector3(x, _cursorPos.y, y);
    }

    private void MoveCursor()
    {
        var gridCell = _grid.GetGridCellAtPosition(_cursorPos);
        if (_currentCursorGridCell != gridCell)
        {
            _currentCursorGridCell = gridCell;

            var index = _verticalityHandler.VerticalIndex;
            if (!_verticalityHandler.IsCurrentIndexValidAndUnoccupied(_currentCursorGridCell))
            {
                index = _verticalityHandler.GetFirstAvailableIndexInGridCell(_currentCursorGridCell);
            }
            
            _currentCursorGridCell.CursorSelected(index);
        }
        
        _currentGridSocketSelected = _currentCursorGridCell.GridSockets[_verticalityHandler.VerticalIndex];
        _cursorGO.transform.position = _currentGridSocketSelected.GetWorldPosition();

        _isCurrentPositionValid = IsPositionValidForMovable(_currentCursorGridCell);
        _ghostMaterial.SetInt(_good, _isCurrentPositionValid ? 1 : 0);

        if (!_currentMovableSelected)
        {
            SingletonManager.Instance.InteractionUIController.VerticalUIController.ShowVerticalUI(false);
            return;
        }

        SingletonManager.Instance.InteractionUIController.VerticalUIController.ShowVerticalUI(true);
            
        if (_lastMovableGridCell != _currentCursorGridCell || _currentGridSocketSelected != _lastGridSocketSelected)
        {
            _positionTweener = _currentMovableSelected.transform.DOMove(_currentGridSocketSelected.GetWorldPosition(), 0.1f);
            _lastMovableGridCell = _currentCursorGridCell;
            _lastGridSocketSelected = _currentGridSocketSelected;
            
            // If on a wall, set wall socket rotation
            if (_currentCursorGridCell.WallSocket == _currentGridSocketSelected && _currentGridSocketSelected is GridSocket gridSocket)
            {
                _currentMovableSelected.transform.rotation = gridSocket.transform.rotation;
            }
        }
    }

    private void CancelMovingMovable(InputAction.CallbackContext callbackContext)
    {
        if (_currentMovableSelected == null)
        {
            return;
        }
        
        _positionTweener?.Kill();
        _currentCursorGridCell = _moveableGridSnapshotData.GridCell;
        _currentGridSocketSelected = _moveableGridSnapshotData.GridSocket;
        _currentMovableSelected.transform.position = _moveableGridSnapshotData.Position;
        _currentMovableSelected.transform.rotation = _moveableGridSnapshotData.Rotation;
        
        PlaceSelectedMovable(true);
    }
    
    public void SelectMovable(Movable movable, bool fromProductBox = false)
    {
        if (movable == null)
        {
            return;
        }

        IsCursorActive = true;

        var gridCell = _grid.GetGridCellAtPosition(movable.transform.position);
        _moveableGridSnapshotData = new MoveableGridSnapshotData
        {
            ProductBoxPlacement = fromProductBox,
            GridCell = gridCell,
            Position = movable.transform.position,
            Rotation = movable.transform.rotation,
            GridSocket = gridCell.GetGridSocketWithInteractable(movable)
        };

        if (_moveableGridSnapshotData.GridSocket != null && _moveableGridSnapshotData.GridSocket.GetInteractableInSocket() == movable)
        {
            _moveableGridSnapshotData.GridSocket.RemoveInteractableFromSocket();
        }
        
        if (movable is Containable contain)
        {
            _moveableGridSnapshotData.GridCell.RemoveSocketFromGridCell(contain.Socket);
        }
        
        var nextCursorPosition = _moveableGridSnapshotData.GridCell.GetWorldPosition();
        _cursorGO.transform.position = nextCursorPosition;
        _cursorPos = nextCursorPosition;
        _currentMovableSelected = movable;
        
        _currentMovableSelected.transform.DOPunchScale(Vector3.one * 0.4f, 0.2f, 1, 0f).onComplete = () =>
        {
            movable.SetMaterial(_ghostMaterial);
        };
    }
    
    private void PlaceMovableOnGrid(InputAction.CallbackContext callbackContext)
    {
        if (_currentMovableSelected == null || !_isCurrentPositionValid)
        {
            return;
        }

        PlaceSelectedMovable();
    }

    private void PlaceSelectedMovable(bool isACancellationPlacement = false)
    {
        // Check if it comes from a product box and if it's a cancellation so it doesn't get place on grid again
        if ((isACancellationPlacement && !_moveableGridSnapshotData.ProductBoxPlacement) || !isACancellationPlacement)
        {
            _currentMovableSelected.transform.DOPunchScale(Vector3.one * 0.3f, 0.2f, 1, 0f);
        
            if (_currentMovableSelected is Containable containable)
            {
                _currentCursorGridCell.AddSocketToGridCell(containable.Socket);
            }

            if (_currentGridSocketSelected != null && !_currentGridSocketSelected.IsOccupied())
            {
                _currentGridSocketSelected.SetOccupation(_currentMovableSelected);
            }
        }
        
        _currentMovableSelected.Deselect();

        _moveableGridSnapshotData = default;
        _currentMovableSelected = null;
        _currentGridSocketSelected = null;
        _currentCursorGridCell = null;
        _isCurrentPositionValid = false;
        IsCursorActive = false;
        
        if (isACancellationPlacement)
        {
            Cancelled?.Invoke();
            return;
        }
        
        Placed?.Invoke();
    }

    public void SpawnMovableOnGrid(Movable movable)
    {
        if (!movable)
        {
            return;
        }

        var gridCell = _grid.GetGridCellAtPosition(_cursorPos); 
        if (gridCell.IsOccupied())
        {
            _currentMovableSelected = Instantiate(movable, gridCell.GetWorldPosition(), Quaternion.identity);
        }
    }
    
    private bool IsPositionValidForMovable(GridCell cell)
    {
        // Get the first valid placement
        //var socketIndex = cell.GetFirstAvailableSocketIndex();
        if (cell.CellType is CellType.OutOfBounds or CellType.Street)
        {
            return false;
        }
        
        if (cell.GridSockets[_verticalityHandler.VerticalIndex].IsOccupied())
        {
            return false;
        }
        
        if (cell.GetObjectOwningGridSocketAtIndex(_verticalityHandler.VerticalIndex) is Containable)
        {
            return _currentMovableSelected.MovableSurfaceRequirement.HasFlag(MovableSurface.Containable);
        }
        
        if (_currentCursorGridCell.WallSocket == _currentGridSocketSelected && !_currentMovableSelected.MovableSurfaceRequirement.HasFlag(MovableSurface.Wall))
        {
            return false;
        }
        
        if (_currentCursorGridCell.FloorSocket == _currentGridSocketSelected && !_currentMovableSelected.MovableSurfaceRequirement.HasFlag(MovableSurface.Floor))
        {
            return false;
        }

        var neighborCells = GetAdjacentCells(cell);
        GridCell gridCell;
        switch (_currentMovableSelected.MovablePlacementClearanceRequirement)
        {
            case MovableClearance.Everything:
            {
                return neighborCells.Count >= 4 && neighborCells.All(neighbor => !neighbor.IsOccupied());
            }
            case MovableClearance.Right:
            {
                gridCell = GetCellInSpecificDirectionFromCell(cell, MovableClearance.Right);
                if (gridCell != null && !gridCell.IsOccupied())
                {
                    return true;
                }

                break;
            }
            case MovableClearance.Forward:
                gridCell = GetCellInSpecificDirectionFromCell(_currentCursorGridCell, MovableClearance.Forward);
                if (gridCell != null && !gridCell.IsOccupied())
                {
                    return true;
                }
                
                break;
            case MovableClearance.Back:
                gridCell = GetCellInSpecificDirectionFromCell(_currentCursorGridCell, MovableClearance.Back);
                if (gridCell != null && !gridCell.IsOccupied())
                {
                    return true;
                }
                
                break;
            case MovableClearance.Left:
                gridCell = GetCellInSpecificDirectionFromCell(_currentCursorGridCell, MovableClearance.Left);
                if (gridCell != null && !gridCell.IsOccupied())
                {
                    return true;
                }
                
                break;
            case MovableClearance.Nothing:
                return true;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        return false;
    }
    
    private GridCell GetCellInSpecificDirectionFromCell(GridCell centerCell, MovableClearance direction)
    {
        switch (direction)
        {
            case MovableClearance.Forward:
                var forward = _currentMovableSelected.transform.forward;
                return _grid.GetGridCellInGrid((int)centerCell.GetWorldPosition().x + Mathf.CeilToInt(forward.x), (int)centerCell.GetWorldPosition().y + Mathf.CeilToInt(forward.y));
            case MovableClearance.Back:
                var back = -_currentMovableSelected.transform.forward;
                return _grid.GetGridCellInGrid((int)centerCell.GetWorldPosition().x + Mathf.CeilToInt(back.x), (int)centerCell.GetWorldPosition().y + Mathf.CeilToInt(back.y));
            case MovableClearance.Left:
                var left = -_currentMovableSelected.transform.right;
                return _grid.GetGridCellInGrid((int)centerCell.GetWorldPosition().x + Mathf.CeilToInt(left.x), (int)centerCell.GetWorldPosition().y + Mathf.CeilToInt(left.y));
            case MovableClearance.Right:
                var right = _currentMovableSelected.transform.right;
                return _grid.GetGridCellInGrid((int)centerCell.GetWorldPosition().x + Mathf.CeilToInt(right.x), (int)centerCell.GetWorldPosition().y + Mathf.CeilToInt(right.y));
            default:
                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
        }
    }
    
    // Function that returns all the adjacent cells
    private List<GridCell> GetAdjacentCells(GridCell cell)
    {
        var cellPosition = cell.GetWorldPosition();
        List<GridCell> neighbors = new();
        for (var xx = -1; xx <= 1; xx++)
        {
            for (var yy = -1; yy <= 1; yy++)
            {
                if (xx == 0 && yy == 0)
                {
                    // You are not neighbor to yourself
                    continue; 
                }

                if (Mathf.Abs(xx) + Mathf.Abs(yy) > 1)
                {
                    continue;
                }

                if (IsValidPosition((int) cellPosition.x + xx, (int) cellPosition.y + yy))
                {
                    neighbors.Add(_grid.GetGridCellInGrid((int) cellPosition.x + xx, (int) cellPosition.y + yy));
                }
            }
        }

        return neighbors;
    }
    
    private bool IsValidPosition(int x, int y) 
    {
        var gridSize = _grid.GetSize;
        return x >= 0 && y >= 0 && x < gridSize.x && y < gridSize.y;
    }

    private void RotateMovableLeft(InputAction.CallbackContext context)
    {
        if (_currentMovableSelected == null || DOTween.IsTweening(_currentMovableSelected.gameObject.transform))
        {
            return;
        }

        // Avoid rotation on Wall socket
        if (_currentCursorGridCell.WallSocket == _currentGridSocketSelected)
        {
            return;
        }

        _currentMovableSelected.gameObject.transform.DORotate(_currentMovableSelected.gameObject.transform.rotation.eulerAngles + new Vector3(0, 90, 0), 0.2f);
    }

    private void RotateMovableRight(InputAction.CallbackContext context)
    {
        if (_currentMovableSelected == null || DOTween.IsTweening(_currentMovableSelected.gameObject.transform))
        {
            return;
        }
        
        // Avoid rotation on Wall socket
        if (_currentCursorGridCell.WallSocket == _currentGridSocketSelected)
        {
            return;
        }

        _currentMovableSelected.gameObject.transform.DORotate(_currentMovableSelected.gameObject.transform.rotation.eulerAngles + new Vector3(0, -90, 0), 0.2f);
    }
}

public struct MoveableGridSnapshotData
{
    public bool ProductBoxPlacement;
    public GridCell GridCell;
    public Vector3 Position;
    public Quaternion Rotation;
    public IGridSocket GridSocket;
}
