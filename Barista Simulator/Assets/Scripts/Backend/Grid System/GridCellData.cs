using System;
using Backend.Grid;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class GridCellData 
{
    // 1,0 -> Floor: Counter -> Machine, Wall: Shelf -> Radio
    // 1,0 -> Floor: Counter -> Machine, Wall: null
    [JsonProperty]
    public CellType CellType;
    [JsonProperty]
    public Vector2Int Position;
    [JsonProperty]
    public BRAGObjectData FloorBRAGObjectData;
    [JsonProperty]
    public BRAGObjectData WallBRAGObjectData;

    public GridCellData(CellType cellType, Vector2Int position, BRAGObjectData floorObjectData, BRAGObjectData wallObjectData)
    {
        CellType = cellType;
        Position = position;
        FloorBRAGObjectData = floorObjectData;
        WallBRAGObjectData = wallObjectData;
    }
}
