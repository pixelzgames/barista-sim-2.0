using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class GridData
{
    public int GridSize;
    
    [JsonProperty]
    public List<GridCellData> GridCellList;

    public GridData()
    {
        GridSize = 0;
        GridCellList = new List<GridCellData>();
    }
}
