using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Backend.Grid
{
    [Serializable]
    public class GridCell : IGridSocket
    {
        public Action<GridCell, int> MovedOnGridCell = delegate {  };
        
        public CellType CellType;
        public List<IGridSocket> GridSockets => _gridSockets;
        public IGridSocket FloorSocket;
        public IGridSocket WallSocket;
        public int PosX;
        public int PosZ;
        
        private Movable _objectOccupyingCellRoot;
        private List<IGridSocket> _gridSockets = new();

        public GridCell(int x, int z)
        {
            PosX = x;
            PosZ = z;
            CellType = CellType.OutOfBounds;
            AddSocketToGridCell(this);
            FloorSocket = this;
        }

        public Vector3 GetWorldPosition()
        {
            // Offset by 0.5f to be in middle of grid cell in world space
            return new Vector3(PosX + 0.5f, 0, PosZ + 0.5f);
        }

        public void SetPosition(int x, int z)
        {
            PosX = x;
            PosZ = z;
        }

        public IGridSocket GetGridSocketWithInteractable(Interactable interactable)
        {
            foreach (var socket in _gridSockets)
            {
                if (socket.GetInteractableInSocket() != interactable)
                {
                    continue;
                }

                return socket;
            }

            return null;
        }

        public int? GetFirstAvailableSocketIndex()
        {
            IGridSocket firstAvailableSocket = null;
            foreach (var socket in _gridSockets.Where(socket => !socket.IsOccupied()))
            {
                firstAvailableSocket = socket;
                break;
            }

            return firstAvailableSocket == null ? null : _gridSockets.IndexOf(firstAvailableSocket);
        }
        
        public int? GetIndexOfFirstOccupiedSocket()
        {
            IGridSocket firstAvailableSocket = null;
            foreach (var socket in _gridSockets.Where(socket => socket.IsOccupied()))
            {
                firstAvailableSocket = socket;
                break;
            }

            return firstAvailableSocket == null ? null : _gridSockets.IndexOf(firstAvailableSocket);
        }

        public Interactable GetInteractableInSocket()
        {
            return _objectOccupyingCellRoot;
        }

        public Interactable GetSocketInteractable()
        {
            return null;
        }

        public void RemoveInteractableFromSocket()
        {
            _objectOccupyingCellRoot = null;
        }

        public Interactable GetObjectOwningGridSocketAtIndex(int socketIndex)
        {
            return socketIndex == 0 ? _objectOccupyingCellRoot : _gridSockets[socketIndex].GetSocketInteractable();
        }

        public bool IsOccupied()
        {
            return _objectOccupyingCellRoot != null;
        }

        public void SetOccupation(Movable movable)
        {
            if (movable.MovableSurfaceRequirement == MovableSurface.Floor)
            {
                _objectOccupyingCellRoot = movable;
                movable.GridPosition = new Vector2Int(PosX, PosZ);
            }
        }

        public void AddSocketToGridCell(IGridSocket socket)
        {
            if (socket is GridSocket && socket != this)
            {
                WallSocket = socket;
            }

            if (_gridSockets.Contains(socket))
            {
                return;
            }
            
            // TODO: Cleanup? as many movables can be there
            // Add the lowest Movable as the _currentOccupyingMovable on the grid
            if (_gridSockets.Count == 1 && socket.GetSocketInteractable() is Movable movable)
            {
                SetOccupation(movable);
            }

            _gridSockets.Add(socket);
            _gridSockets = _gridSockets.OrderBy(o => o.GetWorldPosition().y).ToList();
        }

        public void RemoveSocketFromGridCell(IGridSocket socket)
        {
            if (!_gridSockets.Contains(socket))
            {
                return;
            }

            if (_objectOccupyingCellRoot == socket.GetSocketInteractable())
            {
                _objectOccupyingCellRoot = null;
            }
            
            _gridSockets.Remove(socket);
            _gridSockets = _gridSockets.OrderBy(o => o.GetWorldPosition().y).ToList();
        }

        public void CursorSelected(int verticalIndex)
        {
            MovedOnGridCell(this, verticalIndex); // TODO is this needed HERE? Seems like front end only
        }

        public int? GetIndexOfInteractable(Interactable interactable)
        {
            for (var i = 0; i < _gridSockets.Count; i++)
            {
                if (_gridSockets[i].GetInteractableInSocket() != interactable)
                {
                    continue;
                }

                return i;
            }

            return null;
        }

        public void MoveUpGridSocket(int index)
        {
            MovedOnGridCell(this, index);
        }
        
        public void MoveDownGridSocket(int index)
        {
            MovedOnGridCell(this, index);
        } 
    }

    public enum CellType
    {
        Cafe,
        Street,
        OutOfBounds
    }
}