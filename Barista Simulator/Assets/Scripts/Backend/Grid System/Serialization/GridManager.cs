using System.Collections.Generic;
using UnityEngine;
using Grid = Backend.Grid.Grid;

public class GridManager : MonoBehaviour
{
    private Grid _currentGrid;
    
    public void AddMovableToGrid(Movable movable)
    {
        Debug.Log("Adding movable to grid " + movable.InteractableName + " at " + movable.GridPosition);
        // TODO: Use Current grid here, forcing CafeGrid to test
        //CurrentGrid.GridArray[movable.GridPosition.x, movable.GridPosition.y].SetOccupation(movable);
        if (movable.GridPosition.x < 0 || movable.GridPosition.x > _currentGrid.GridArray.Length)
        {
            Debug.LogWarning("Grid position outside of grid bounds.");
            return;
        }
        
        _currentGrid.GridArray[movable.GridPosition.x, movable.GridPosition.y].SetOccupation(movable);
    }

    private void LoadGridCells(GridData gridData)
    {
        foreach (var cell in gridData.GridCellList)
        {
            ApplyGridCellData(cell);
        }
    }

    private void ApplyGridCellData(GridCellData gridCellData)
    {
        if (gridCellData.FloorBRAGObjectData != null && _currentGrid.GridArray[gridCellData.Position.x, gridCellData.Position.y].FloorSocket != null)
        {
            var prefab = PrefabsUtility.GetPrefabFromList(gridCellData.FloorBRAGObjectData.PrefabID);
            var bragPrefabHandler = Instantiate(prefab, gridCellData.FloorBRAGObjectData.Position.ToVector3(),
                gridCellData.FloorBRAGObjectData.Rotation);
            bragPrefabHandler.PassDataThrough(gridCellData.FloorBRAGObjectData);
            
            if (bragPrefabHandler.DataObject is Movable movable)
            {
                _currentGrid.GridArray[gridCellData.Position.x, gridCellData.Position.y].FloorSocket?.SetOccupation(movable);
            }

            if (bragPrefabHandler.DataObject is Containable containable)
            {
                _currentGrid.GridArray[gridCellData.Position.x, gridCellData.Position.y].AddSocketToGridCell(containable.Socket);
            }
        }
        
        if (gridCellData.WallBRAGObjectData != null && _currentGrid.GridArray[gridCellData.Position.x, gridCellData.Position.y].WallSocket != null)
        {
            var prefab = PrefabsUtility.GetPrefabFromList(gridCellData.WallBRAGObjectData.PrefabID);
            var bragPrefabHandler = Instantiate(prefab, gridCellData.WallBRAGObjectData.Position.ToVector3(),
                gridCellData.WallBRAGObjectData.Rotation);
            bragPrefabHandler.PassDataThrough(gridCellData.WallBRAGObjectData);
            
            if (bragPrefabHandler.DataObject is Movable movable)
            {
                _currentGrid.GridArray[gridCellData.Position.x, gridCellData.Position.y].WallSocket?.SetOccupation(movable);
            }

            if (bragPrefabHandler.DataObject is Containable containable)
            {
                _currentGrid.GridArray[gridCellData.Position.x, gridCellData.Position.y].AddSocketToGridCell(containable.Socket);
            }
        }
    }

    public GridData SaveGrid()
    {
        Debug.Log("Saving cafe grid.");
        
        var gridData = new GridData
        {
            GridSize = _currentGrid.Length
        };

        var cellDataList = new List<GridCellData>();

        for (var x = 0; x < _currentGrid.Length; x++)
        {
            for (var y = 0; y < _currentGrid.Length; y++)
            {
                var gridCell = _currentGrid.GridArray[x, y];
                var floorSocketObject =
                    gridCell.FloorSocket != null && gridCell.FloorSocket.GetInteractableInSocket() != null
                        ? gridCell.FloorSocket.GetInteractableInSocket()
                        : null;
                var wallSocketObject =
                    gridCell.WallSocket != null && gridCell.WallSocket.GetInteractableInSocket() != null
                        ? gridCell.WallSocket.GetInteractableInSocket()
                        : null;
                
                var cellData = new GridCellData(gridCell.CellType, 
                    new Vector2Int(gridCell.PosX, gridCell.PosZ),
                    floorSocketObject?.UpdateObjectData(),
                    wallSocketObject?.UpdateObjectData());
                
                cellDataList.Add(cellData);
            }
        }
        
        gridData.GridCellList = cellDataList;
        return gridData;
    }

    public void SetGrid(Grid grid)
    {
        _currentGrid = grid;
    }

    public void LoadGrid(GridData data)
    {
        data ??= SaveGrid();
        LoadGridCells(data);
    }
}
