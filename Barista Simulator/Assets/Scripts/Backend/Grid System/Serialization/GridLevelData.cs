using System;

[Serializable]
public class GridLevelData
{
    public GridData HomeGridData;
    public GridData CafeGridData;

    public GridLevelData()
    {
        HomeGridData = new GridData();
        CafeGridData = new GridData();
    }
}
