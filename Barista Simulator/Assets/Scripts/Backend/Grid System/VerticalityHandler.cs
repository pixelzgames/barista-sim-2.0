using Backend.Grid;
using UnityEngine;

public class VerticalityHandler
{
    public int VerticalIndex => _verticalIndex;

    private int _verticalIndex;
    
    public int MoveIndexUp(GridCell cell, bool allowEmptyIndex = false)
    {
        for (var i = _verticalIndex + 1; i < cell.GridSockets.Count; i++)
        {
            if (!allowEmptyIndex && !cell.GridSockets[i].IsOccupied())
            {
                continue;
            }

            _verticalIndex = i;
            break;
        }

        _verticalIndex = Mathf.Clamp(_verticalIndex, 0, cell.GridSockets.Count - 1);
        return _verticalIndex;
    }

    public int MoveIndexDown(GridCell cell, bool allowEmptyIndex = false)
    {
        for (var i = _verticalIndex - 1; i >= 0; i--)
        {
            if (!allowEmptyIndex && !cell.GridSockets[i].IsOccupied())
            {
                continue;
            }

            _verticalIndex = i;
            break;
        }
        
        return _verticalIndex;
    }

    public int GetFirstAvailableIndexInGridCell(GridCell cell)
    {
        _verticalIndex = cell.GetFirstAvailableSocketIndex() ?? Mathf.Clamp(_verticalIndex, 0, cell.GridSockets.Count - 1);
        return _verticalIndex;
    }

    public int GetIndexOfFirstOccupiedSocket(GridCell cell)
    {
        _verticalIndex = cell.GetIndexOfFirstOccupiedSocket() ?? Mathf.Clamp(_verticalIndex, 0, cell.GridSockets.Count);
        return _verticalIndex;
    }
    
    public bool IsCurrentIndexValidAndOccupied(GridCell cell)
    {
        return cell.GridSockets.Count > _verticalIndex && cell.GridSockets[_verticalIndex].IsOccupied();
    }

    public bool IsCurrentIndexValidAndUnoccupied(GridCell cell)
    {
        return cell.GridSockets.Count > _verticalIndex && !cell.GridSockets[_verticalIndex].IsOccupied();
    }

    public int GetIndexOfInteractable(GridCell cell, Interactable interactable)
    {
        var index = cell.GetIndexOfInteractable(interactable);

        if (index.HasValue)
        {
            _verticalIndex = index.Value;
        }
        
        return _verticalIndex;
    }
}
