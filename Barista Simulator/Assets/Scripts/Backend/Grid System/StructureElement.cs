using System;
using UnityEngine;
using DG.Tweening;

public class StructureElement : MonoBehaviour
{
    private const float ANIM_TIME = 0.3f;
    public enum StructureType
    {
        Wall,
        Corner
    }
    
    [SerializeField] private Direction _directionToLower;
    [SerializeField] private StructureType _typeOfStructure;

    [Header("Walls")] 
    [SerializeField] private GameObject _fullWall;
    [SerializeField] private GameObject _lowWall;
    
    [Header("Corners")] 
    [SerializeField] private GameObject _fullCorner;
    [SerializeField] private GameObject _lowCorner;
    
    private GameObject _fullSize;
    private GameObject _loweredSize;
    
    private bool _shouldLower;
    private CameraController _cameraController;

    private void Awake()
    {
        for (var i = 0; i < transform.childCount; i++)
        {
           transform.GetChild(i).gameObject.SetActive(false);
        }
        
        switch (_typeOfStructure)
        {
            case StructureType.Wall:
                _fullSize = _fullWall;
                _loweredSize = _lowWall;
                break;
            case StructureType.Corner:
                _fullSize = _fullCorner;
                _loweredSize = _lowCorner;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        _cameraController = FindFirstObjectByType<CameraController>();
    }

    private void Update()
    {
        if (_cameraController == null)
        {
            return;
        }
        
        _shouldLower = _cameraController.CameraDirection.ToString().Contains(_directionToLower.ToString());

        switch (_shouldLower)
        {
            case true when !_loweredSize.activeInHierarchy:
                ShowLoweredSize();
                break;
            case false when !_fullSize.activeInHierarchy:
                ShowFullSize();
                break;
        }
    }

    public void SetStructure(Direction direction, StructureType structureType)
    {
        _directionToLower = direction;
        _typeOfStructure = structureType;
        Awake();
    }

    private void ShowFullSize()
    {
        _loweredSize.transform.DOScaleY(0f, ANIM_TIME).SetEase(Ease.OutQuint).onComplete = () => {_loweredSize.SetActive(false);};
        _fullSize.transform.localScale = new Vector3(1f, 0f, 1f);
        _fullSize.SetActive(true);
        _fullSize.transform.DOScaleY(1f, ANIM_TIME).SetEase(Ease.OutBack);
    }

    private void ShowLoweredSize()
    {
        _fullSize.transform.DOScaleY(0f, ANIM_TIME).SetEase(Ease.OutQuint).onComplete = () => {_fullSize.SetActive(false);};
        _loweredSize.transform.localScale = new Vector3(1f, 0f, 1f);
        _loweredSize.SetActive(true);
        _loweredSize.transform.DOScaleY(1f, ANIM_TIME).SetEase(Ease.OutBack);
    }
}
