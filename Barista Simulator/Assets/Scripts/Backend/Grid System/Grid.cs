using System;
using System.Collections.Generic;
using UnityEngine;

namespace Backend.Grid
{
    public class Grid : MonoBehaviour
    {
        private static readonly int GridTiling = Shader.PropertyToID("_GridTiling");
        public event Action GeneratedGrid = delegate {  };

        public CursorController CursorController => _cursorController;
        public GridCell[,] GridArray { get; private set; }
        public bool IsGenerated { get; private set; }
        public Vector2 GetSize => new(Length, Length);
        public CellType TypeToDraw => _typeToDraw;
        public int Length;
        
        [SerializeField] private float _cellSize = 1f;
        [SerializeField] private Material _gridLineMaterial;
        [SerializeField] private List<GridCell> _gridCellList;
        [SerializeField] private CellType _typeToDraw;
        [SerializeField] private CursorController _cursorController;

        private const float GRID_RENDER_HEIGHT_OFFSET = 0.01f;
        private MeshRenderer _meshRenderer;
        
        private void Awake()
        {
            GenerateGrid();
            _meshRenderer.enabled = false;
            SingletonManager.Instance.GridManager.SetGrid(this);
        }

        private void OnEnable()
        {
            SingletonManager.Instance.GameManager.InteractionModeChanged += OnChangedInteractionMode;
        }

        private void OnDisable()
        {
            SingletonManager.Instance.GameManager.InteractionModeChanged -= OnChangedInteractionMode;
        }

        private void OnChangedInteractionMode(InteractionMode state)
        {
            _meshRenderer.enabled = state == InteractionMode.Edit;
        }
        
        public void GenerateGrid()
        {
            CreateGrid();
            DrawWorldGrid();
            IsGenerated = true;
            GeneratedGrid.Invoke();
        }
        
        private void DrawWorldGrid()
        {
            var meshFilter = gameObject.GetComponent<MeshFilter>();
            var mesh = new Mesh();
            
            // Define vertices
            var vertices = new Vector3[]
            {
                new (0f, GRID_RENDER_HEIGHT_OFFSET, 0f),          // Bottom-left
                new (Length, GRID_RENDER_HEIGHT_OFFSET, 0f),      // Bottom-right
                new (0f, GRID_RENDER_HEIGHT_OFFSET, Length),      // Top-left
                new (Length, GRID_RENDER_HEIGHT_OFFSET, Length)   // Top-right
            };

            // Define triangles
            var triangles = new []
            {
                0, 2, 1, // First triangle
                1, 2, 3  // Second triangle
            };

            // Define UVs
            var uvs = new Vector2[]
            {
                new (0, 0), // Bottom-left
                new (1, 0), // Bottom-right
                new (0, 1), // Top-left
                new (1, 1)  // Top-right
            };

            // Assign data to the mesh
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uvs;
            mesh.RecalculateNormals();

            _meshRenderer = gameObject.GetComponent<MeshRenderer>();
            _meshRenderer.material = _gridLineMaterial;
            meshFilter.mesh = mesh;
            transform.position += Vector3.up * GRID_RENDER_HEIGHT_OFFSET;
            _meshRenderer.material.SetVector(GridTiling, new Vector2(Length, Length));
        }

        private void CreateGrid()
        {
            GridArray = new GridCell[Length, Length];
            
            var counter = 0;
            for (var x = 0; x < Length; x++)
            {
                for (var y = 0; y < Length; y++)
                {
                    var gridItem = new GridCell(x, y)
                    {
                        MovedOnGridCell = OnMovedOnGridCell
                    };
                    
                    if (_gridCellList.Count <= counter)
                    {
                        _gridCellList.Add(gridItem);
                    }

                    _gridCellList[counter].SetPosition(x, y);
                    
                    gridItem.CellType = _gridCellList[counter].CellType;
                    GridArray[x, y] = gridItem;
                    
                    counter++;
                }
            }
        }

        private void OnMovedOnGridCell(GridCell cell, int index)
        {
            SingletonManager.Instance.InteractionUIController.VerticalUIController.InitializeUIWithGridCellData(cell, index);
            SingletonManager.Instance.InteractionUIController.VerticalUIController.UpdateCurrentVerticalIndex();
        }

        [ContextMenu("Reset Grid Data")]
        private void ResetGridData()
        {
            _gridCellList.Clear();

            foreach (var gridCell in GridArray)
            {
                gridCell.CellType = CellType.OutOfBounds;
            }
        }

        public void UpdateGridCellData(int index, CellType type)
        {
            _gridCellList[index].CellType = type;
        }

        private Vector2Int GetGridPositionFromWorld(Vector3 worldPos)
        {
            var x = Mathf.FloorToInt(worldPos.x / _cellSize);
            var z = Mathf.FloorToInt(worldPos.z / _cellSize);

            //Check if object position is outside the grid size
            if (x > Length || z > Length)
            {
                Debug.LogError($"Position {worldPos} is outside the current grid length of {Length}. This will lead to flawed Grid Editor numbers");
            }
            
            x = Mathf.Clamp(x, 0, Length);
            z = Mathf.Clamp(z, 0, Length);

            return new Vector2Int(x, z);
        }

        public GridCell GetGridCellInGrid(int x, int y)
        {
            if (x > Length || x < 0 || y > Length || y < 0)
            {
                return null;
            }
            
            return GridArray[x, y];
        }
        
        public GridCell GetGridCellAtPosition(Vector3 worldPos)
        {
            var gridPos = GetGridPositionFromWorld(new Vector3(worldPos.x, 0f, worldPos.z));
            return GridArray[Mathf.Clamp(gridPos.x, 0, Length - 1), Mathf.Clamp(gridPos.y, 0, Length - 1)];
        }

#if UNITY_EDITOR

        [SerializeField] private bool _showDebugGrid;
        
        private void OnGUI()
        {
            if (!IsGenerated || !_showDebugGrid)
            {
                return;
            }
            
            foreach (var gridCell in GridArray)
            {
                if (gridCell.GridSockets == null)
                {
                    continue;
                }
                
                var screenPosition = Camera.main.WorldToScreenPoint(gridCell.GetWorldPosition());
                screenPosition.y = Screen.height - screenPosition.y;
                
                var offset = new Vector2(0, 20);
                Vector2 labelPosition = screenPosition; 
                
                GUI.color = Color.white;
                GUI.backgroundColor = Color.black;
                
                GUIStyle labelStyle = new (GUI.skin.label)
                {
                    fontSize = 12,
                    alignment = TextAnchor.MiddleCenter,
                    
                };

                foreach (var socket in gridCell.GridSockets)
                {
                    var labelText = socket.IsOccupied() ? socket.GetInteractableInSocket().InteractableName : "Empty";
                    var textSize = labelStyle.CalcSize(new GUIContent(labelText));
                    var labelRect = new Rect(labelPosition.x, labelPosition.y, textSize.x + 10, textSize.y + 5);
                    GUI.Box(labelRect, GUIContent.none);
                    GUI.Label(labelRect, labelText, labelStyle);
                    labelPosition += offset;
                }
            }
        }
#endif
    }
}
