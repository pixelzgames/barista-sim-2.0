using UnityEngine;

public class InteractionSocket : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        DebugExtension.DrawArrow(transform.position, transform.forward * 0.5f, Color.blue);
    }
}
