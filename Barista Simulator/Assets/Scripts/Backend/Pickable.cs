﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

public class Pickable : Interactable, IPickable
{
    public event Action<Pickable, PlayerHand> OnPickUp = delegate { };
    
    [SerializeField] protected AnimationCurve _throwingCurve;

    [FormerlySerializedAs("_handSocket")] [SerializeField]
    protected Transform _ikHandle;

    public Transform IKHandle => _ikHandle;
    [field: SerializeField] public bool CanThrow { get; private set; } = true;
    [field: SerializeField] public bool CanTrash { get; private set; }
    [field: SerializeField] public bool CanPlaceOnContainable { get; private set; } = true;

    private bool _pickingUp;
    private Coroutine _placingTweenCoroutine;

    public virtual void Drop(PlayerHand.HandType hand = PlayerHand.HandType.Main, bool unEquip = true)
    {
        if (_placingTweenCoroutine != null)
        {
            StopCoroutine(_placingTweenCoroutine);
            _placingTweenCoroutine = null;
        }
        
        ToggleCollider(true);
        TogglePhysics(true);

        // Detach interactable from its parent.
        gameObject.transform.parent = null;
        gameObject.SetLayerRecursively(0);

        if (unEquip)
        {
            PlayerInteractionManager.Instance.UnEquipObjectFromHands(hand);
        }
    }

    public void OnCompletedInteraction()
    {
        var hitColliders = new Collider[15];
        var colCount = Physics.OverlapSphereNonAlloc(PlayerInteractionManager.Instance.SocketForInteraction.position, 1f, hitColliders,
            PlayerInteractionManager.Instance.InteractionLayerMask);
        var closest = FindClosestEmptyContainable(hitColliders, colCount);

        if (closest != null)
        {
            var availSocket = closest.Socket;
            if (availSocket != null)
            {
                PlaceOnASocket(availSocket);
                return;
            }
        }

        Drop(PlayerHand.HandType.Second);
    }
    
    void IPickable.Pickup(PlayerHand hand)
    {
        if (_pickingUp)
        {
            return;
        }

        if (_placingTweenCoroutine != null)
        {
            StopCoroutine(_placingTweenCoroutine);
            Drop(hand.Hand);
            _placingTweenCoroutine = null;
        }
        
        _pickingUp = true;
        
        OnPickUp?.Invoke(this, hand);
        
        ToggleCollider(false);
        TogglePhysics(false);
        
        // TODO find a better system to match objects orientation to hands 
        // if (hand.Hand == PlayerHand.HandType.Main)
        // {
        //     transform.localRotation = Quaternion.identity;
        // }
        // else
        // {
        //     transform.localRotation = Quaternion.identity * Quaternion.Euler(new Vector3(180, 0, 0));
        // }
        
        _placingTweenCoroutine = StartCoroutine(AnimatePlacement(hand.TransformSocket));
        
        gameObject.SetLayerRecursively(LayerMask.NameToLayer("Player"));
        _pickingUp = false;
    }
    
    private IEnumerator AnimatePlacement(Transform target, float duration = 0.1f, bool alignRotation = true)
    {
        yield return new WaitForSeconds(0.1f);
        var elapsedTime = 0f;
        var startingPosition = transform.position;
        var startingRotation = transform.rotation;

        while (elapsedTime < duration)
        {
            transform.position = Vector3.Lerp(startingPosition, target.position, elapsedTime / duration);
            transform.rotation = Quaternion.Lerp(startingRotation, alignRotation ?
                target.rotation : Quaternion.Euler(new Vector3(0f, transform.rotation.eulerAngles.y, 0f)), elapsedTime / duration);

            elapsedTime += Time.deltaTime;
            yield return null;
        }
        
        transform.position = target.position;
        transform.rotation = target.rotation;
        transform.SetParent(target);
        _placingTweenCoroutine = null;
    }

    public virtual void PlaceOnASocket(Socket socket, bool alignRotation = true, bool colliderOn = false)
    {
        socket.AddToSocket(this);
        PlaceOnAPoint(socket.transform, alignRotation, colliderOn);
    }

    private void PlaceOnAPoint(Transform point, bool alignRotation = true, bool colliderOn = false)
    {
        if (_placingTweenCoroutine != null)
        {
            StopCoroutine(_placingTweenCoroutine);
            _placingTweenCoroutine = null;
        }
        
        ToggleCollider(colliderOn);
        TogglePhysics(false);
        gameObject.SetLayerRecursively(0);

        _placingTweenCoroutine = StartCoroutine(AnimatePlacement(point, alignRotation: true));
    }

    public virtual void Throw(float power)
    {
        StartCoroutine(ThrowTween(power));
    }

    private IEnumerator ThrowTween(float power)
    {
        yield return new WaitForSeconds(0.05f);

        var force = _throwingCurve.Evaluate(power);
        //Letting go of the object
        Drop();
        //Adding forces on the object based on the hold timer and curve
        var directionVector =
            ((PlayerInteractionManager.Instance.transform.forward * (force * 2f)) - transform.position).normalized;
        _rb.AddForce(directionVector * force, ForceMode.VelocityChange);
        //add a constant upward force
        _rb.AddForce(Vector3.up * 4.5f, ForceMode.VelocityChange);
        //Add rotation speed
        _rb.angularVelocity = transform.right * 5000f;
    }

    public virtual void PickUp()
    {
        if (PlayerInteractionManager.Instance.TryAddObjectsToHands(this, out var freeHand))
        {
            ((IPickable)this).Pickup(freeHand);
        }
    }

    private void TogglePhysics(bool on)
    {
        _rb.isKinematic = !on;
    }

    private Containable FindClosestEmptyContainable(Collider[] cols, int iterationCount)
    {
        Containable closest = null;
        var distance = Mathf.Infinity;

        for (var i = 0; i < iterationCount; i++)
        {
            var con = cols[i].GetComponent<Containable>();

            if (!con || !con.Socket.IsEmpty ||
                !((cols[i].bounds.center - transform.position).magnitude < distance))
            {
                continue;
            }

            closest = con;
            distance = (cols[i].bounds.center - transform.position).magnitude;
        }

        return closest;
    }

    protected override void UpdateAvailableActions(InteractionMode mode)
    {
        base.UpdateAvailableActions(mode);
        
        foreach (var action in GameConfig.GetConfigData().Gameplay.PickableActionsConfig.Actions)
        {
            if (action.InteractionMode.HasFlag(mode))
            {
                _availableActions.Add(action);
            }
        }
    }
}