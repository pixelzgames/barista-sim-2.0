using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class Container<T>
{
    public Action ContentChanged = delegate { };

    public List<Ingredient> Ingredients { get; protected set; } = new();
    public T Content
    {
        get => _content;
        set
        {
            Ingredients.Clear();
            _content = value;
            ContentChanged?.Invoke();
        }
    }
    
    public bool UnlimitedServings { get; private set; }
    public int ServingsCapacity => _servingsCapacity;
    protected readonly int _servingsCapacity;
    public bool IsFillable { get; }

    private T _content;
    
    protected Container(T content, int servingsCapacity, bool isFillable, bool unlimitedServings = false)
    {
        _servingsCapacity = servingsCapacity;
        Content = content;
        IsFillable = isFillable;
        UnlimitedServings = unlimitedServings;
    }
    
    public LevelResult GetLevel()
    {
        if (IsEmpty() || GetFullPercentage() <= 0f)
        {
            return LevelResult.Empty;
        }
        
        return GetFullPercentage() <= ObjectStateConfig.LOW_PERCENTAGE ? LevelResult.Low : LevelResult.None;
    }

    public abstract int GetFullPercentage();
    
    public abstract void AddToContainer(int amount);

    public abstract void RemoveFromContainer(int amount);
    public abstract void RemoveAllContent();

    public abstract bool IsEmpty();
    public abstract bool IsFull();
}

public class LiquidContainer : Container<Liquid>
{
    public LiquidContainer(Liquid liquid, int servingsCapacity, bool isFillable, bool unlimitedServings = false) : base(liquid, servingsCapacity, isFillable, unlimitedServings)
    {
        // empty
    }

    public override int GetFullPercentage()
    {
        return Mathf.FloorToInt(Mathf.Clamp((float)Content.Servings / _servingsCapacity * 100f, 0f, 100f));
    }
    
    public override void AddToContainer(int amount)
    {
        Content.Servings += amount;
        Content.Servings = Mathf.Clamp(Content.Servings, 0, _servingsCapacity);
        ContentChanged?.Invoke();
    }

    public override void RemoveFromContainer(int amount)
    {
        Content.Servings -= amount;
        Content.Servings = Mathf.Clamp(Content.Servings, 0, _servingsCapacity);
        ContentChanged?.Invoke();
    }

    public override void RemoveAllContent()
    {
        Content.Servings = 0;
        ContentChanged?.Invoke();
    }

    public override bool IsEmpty()
    {
        if (Content == null)
        {
            return true;
        }
        
        return Content.Servings <= 0f;
    }

    public override bool IsFull()
    {
        if (IsEmpty())
        {
            return false;
        }

        return Content.Servings >= _servingsCapacity;
    }
}

public class SolidContainer : Container<Solid>
{
    public SolidContainer(Solid solid, int servingsCapacity, bool isFillable) : base(solid, servingsCapacity, isFillable)
    {
        
    }
    
    public override int GetFullPercentage()
    {
        return Mathf.FloorToInt(Mathf.Clamp((float)Content.Servings / _servingsCapacity * 100f, 0f, 100f));
    }

    public override void AddToContainer(int amount)
    {
        Content.Servings += amount;
        Content.Servings = Mathf.Clamp(Content.Servings, 0, _servingsCapacity);
        ContentChanged?.Invoke();
    }

    public override void RemoveFromContainer(int amount)
    {
        Content.Servings -= amount;
        Content.Servings = Mathf.Clamp(Content.Servings, 0, _servingsCapacity);
        ContentChanged?.Invoke();
    }

    public override void RemoveAllContent()
    {
        Content.Servings = 0;
        ContentChanged?.Invoke();
    }

    public override bool IsEmpty()
    {
        if (Content == null)
        {
            return true;
        }
        
        return Content.Servings <= 0;
    }
    
    public override bool IsFull()
    {
        if (IsEmpty())
        {
            return false;
        }

        return Content.Servings >= _servingsCapacity;
    }
}