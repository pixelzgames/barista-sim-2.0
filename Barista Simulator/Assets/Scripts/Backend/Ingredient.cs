﻿using System;

[Serializable]
public class Ingredient
{
    public string IngredientName => _ingredientName;
    protected string _ingredientName = "";
}
