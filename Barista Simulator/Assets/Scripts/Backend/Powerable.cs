using System;
using System.Collections;
using UnityEngine;

public class Powerable : Movable
{
    public event Action<PowerState> OnPoweredOn = delegate {  };
    public bool IsPowered => CurrentPowerState == PowerState.Powered;
    protected PowerState CurrentPowerState { get; private set; }

    [SerializeField] protected float _timeForPowering = 10f;
    [SerializeField] private MeshRenderer[] _powerLights;
    [SerializeField] private AnimationCurve _flickerCurve;
    
    protected override void Start()
    {
        base.Start();
        Power(false);
    }
    
    protected virtual bool TryPowerOn()
    {
        if (CurrentPowerState == PowerState.Off)
        {
            StartCoroutine(Powering());
            return true;
        }
        
        Debug.LogWarning($"{InteractableName} is already powered on.");
        return false;
    }
    
    protected virtual bool TryPowerOff()
    {
        if (CurrentPowerState == PowerState.Powered)
        {
            Power(false);
            return true;
        }
        
        Debug.LogWarning($"{InteractableName} is already off.");
        return false;
    }

    private void Power(bool on) 
    {
        if (on)
        {
            CurrentPowerState = PowerState.Powered;
            _objectStateEventController.RaiseEvent(ObjectState.PoweredOn);
            _objectStateEventController.RaiseEvent(ObjectState.Powering, false);
        }
        else
        {
            CurrentPowerState = PowerState.Off;
            _objectStateEventController.RaiseEvent(ObjectState.PoweredOff);
        }
        
        OnPoweredOn.Invoke(CurrentPowerState);
        
        ToggleMachineLights(on);
    }
    
    private void ToggleMachineLights(bool on)
    {
        foreach (var buttons in _powerLights)
        {
            foreach (var mat in buttons.materials)
            {
                StartCoroutine(FlickerLights(mat, on));
            }
        }
    }

    private IEnumerator FlickerLights(Material mat, bool on)
    {
        float time = 0f;

        while (time < 1f)
        {
            if (_flickerCurve.Evaluate(time) < 0f)
            {
                mat.EnableKeyword("_EMISSION");
            }
            else
            {
                mat.DisableKeyword("_EMISSION");
            }
            
            time += Time.deltaTime;
            
            yield return null;
        }

        if (on)
        {
            mat.EnableKeyword("_EMISSION");
        }
        else
        {
            mat.DisableKeyword("_EMISSION");
        }
    }

    protected virtual IEnumerator Powering()
    {
        CurrentPowerState = PowerState.Powering;
        _objectStateEventController.RaiseEvent(ObjectState.Powering, true, _timeForPowering);
        _objectStateEventController.RaiseEvent(ObjectState.PoweredOff, false);
        yield return new WaitForSeconds(_timeForPowering);
        Power(true);
    }

    public override void InitializeData()
    {
        base.InitializeData();
        CurrentPowerState = _data.PowerableData.CurrentPowerState;
        switch (CurrentPowerState)
        {
            case PowerState.Off:
                Power(false);
                break;
            case PowerState.Powering:
                CurrentPowerState = PowerState.Off;
                TryPowerOn();
                break;
            case PowerState.Powered:
                Power(true);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public override BRAGObjectData UpdateObjectData()
    {
        _data.PowerableData.CurrentPowerState = CurrentPowerState;
        return base.UpdateObjectData();
    }
    
    protected override void UpdateAvailableActions(InteractionMode mode)
    {
        base.UpdateAvailableActions(mode);
        
        foreach (var action in GameConfig.GetConfigData().Gameplay.PowerableActionsConfig.Actions)
        {
            if (action.InteractionMode.HasFlag(mode))
            {
                _availableActions.Add(action);
            }
        }
    }
}

public enum PowerState
{
    Off,
    Powering,
    Powered
}