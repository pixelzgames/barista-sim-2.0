[System.Serializable]
public class GreenBeans : Solid
{
    public CoffeeData CoffeeData { get; protected set; }
    
    public GreenBeans(int servings, CoffeeData coffeeData) : base(servings)
    {
        _ingredientName = "Green seeds";
        CoffeeData = coffeeData;
    }

    public GreenBeans() : base(0)
    {
        _ingredientName = "Green seeds";
        CoffeeData = new CoffeeData();
    }
    
    public void SetData(CoffeeData coffeeData)
    {
        CoffeeData = coffeeData;
    }
}
