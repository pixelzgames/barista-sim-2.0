[System.Serializable]
public class InstantGrounds : Solid
{
    public CoffeeData CoffeeData { get; private set; }
    public InstantGrounds(int servings) : base(servings)
    {
        _ingredientName = "Instant grounds";
        var coffeeOrigin = new CoffeeOrigin(Origin.Various, null);
        

        CoffeeData = new CoffeeData(coffeeOrigin, GrindSize.FineB, new GreenCoffeeData(), null);
    }
}