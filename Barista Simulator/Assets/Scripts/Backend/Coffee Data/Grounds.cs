[System.Serializable]
public class Grounds : WholeBeans
{
    public GrindSize Size { get; private set; }
    public Grounds(int servings, CoffeeData coffeeData, GrindSize size) : base(servings, coffeeData)
    {
        _ingredientName = "Coffee grounds";
        Size = size;
    }
    
    public Grounds()
    {
        _ingredientName = "Coffee grounds";
    }

    public void SetData(CoffeeData coffeeData, GrindSize size)
    {
        CoffeeData = coffeeData;
        Size = size;
    }
}

public enum GrindSize
{
    ExtraCoarse = 0,
    Coarse = 1,
    Medium = 2,
    MediumFineA = 3,
    MediumFineB = 4,
    MediumFineC = 5,
    FineA = 6,
    FineB = 7,
    FineC = 8,
    ExtraFine = 9 
}