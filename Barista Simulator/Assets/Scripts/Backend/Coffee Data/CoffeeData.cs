using System.Linq;
using UnityEngine;

[System.Serializable]
public class CoffeeData
{
    public CoffeeOrigin CoffeeOrigin { get; private set; }
    public GreenCoffeeData PossibleNotesPerRoast { get; private set; }
    public CoffeeQuality[] PossibleQualities { get; private set; } 
    
    // Roasted properties
    public RoastType Roast { get; private set; }
    public CoffeeQuality CoffeeQuality { get; private set; }
    
    // Espresso properties
    public GrindSize IdealEspressoGrindSize { get; private set; }
    public bool IsEspressoDialedIn { get; private set; }
    
    // Brewing property
    public GrindSize IdealBrewingGrindSize { get; private set; }

    public CoffeeData()
    {
        //Empty constructor kk ty
    }
    
    public CoffeeData(CoffeeOrigin coffeeOrigin, GrindSize grindSizeIdealEspresso, GreenCoffeeData possibleNotesPerRoast, 
        CoffeeQuality[] possibleQualities, bool isEspressoDialedIn = true)
    {
        IsEspressoDialedIn = isEspressoDialedIn;
        CoffeeOrigin = coffeeOrigin;
        IdealEspressoGrindSize = grindSizeIdealEspresso;
        IdealBrewingGrindSize = GenerateBrewingGrindSize();
        PossibleNotesPerRoast = possibleNotesPerRoast;
        PossibleQualities = possibleQualities;
    }

    private GrindSize GenerateBrewingGrindSize()
    {
        var randomNumber = Random.Range((int)GrindSize.ExtraCoarse, (int)GrindSize.MediumFineB);
        return (GrindSize)randomNumber;
    }
    
    public void SetRoast(RoastType roastType)
    {
        Roast = roastType;
    }

    // TODO with roast score
    public void SetQualityFromRoastScore(float score)
    {
        CoffeeQuality = PossibleQualities[0];
    }
    
    public void SetFlavorNote()
    {
        
    }
    
    public void SetRandomFlavourNotes()
    {
        var allPossibleNotes = PossibleNotesPerRoast.PossibleLightRoastNotes.ToList();
        allPossibleNotes.AddRange(PossibleNotesPerRoast.PossibleMediumRoastNotes);
        allPossibleNotes.AddRange(PossibleNotesPerRoast.PossibleDarkRoastNotes);
        
        foreach (var noteSlot in CoffeeQuality.TastingDecriptorSlots)
        {
            var randomIndex = Random.Range(0, allPossibleNotes.Count);
            noteSlot.CoffeeDescriptor = new CoffeeDescriptor(allPossibleNotes[randomIndex]);
            allPossibleNotes.Remove(allPossibleNotes[randomIndex]);
        }
    }

    public void RevealEspressoDialIn()
    {
        IsEspressoDialedIn = true;
    }
}