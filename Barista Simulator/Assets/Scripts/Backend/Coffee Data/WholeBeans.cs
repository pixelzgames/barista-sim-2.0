[System.Serializable]
public class WholeBeans : GreenBeans
{
    public WholeBeans(int servings, CoffeeData coffeeData) : base(servings, coffeeData)
    {
        _ingredientName = "Whole seeds";
    }

    public WholeBeans()
    {
        _ingredientName = "Whole seeds";
    }
}

[System.Serializable]
public enum RoastType
{
    Raw = -1,
    Light = 0,
    Medium = 2,
    Dark = 3,
    Burned = 4
}
