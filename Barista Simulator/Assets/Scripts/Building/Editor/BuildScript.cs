using System.IO;
using System.IO.Compression;
using System.Linq;
using UnityEditor;
using UnityEngine.Rendering;
using File = UnityEngine.Windows.File;

/// <summary>
/// Build script that exposes build method to command line.
/// </summary>
public class BuildScript
{
    public static void BuildWindows()
    {
        var path = "Builds/Windows";
        CreateDirectory(path);

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = GetEnabledScenes(),
            locationPathName = $"{path}/TestGame.exe",
            target = BuildTarget.StandaloneWindows,
            options = BuildOptions.None
        };

        BuildPipeline.BuildPlayer(buildPlayerOptions);
        CompressBuild(path);
    }

    public static void CreateDirectory(string path)
    {
        if (Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }

    private static string[] GetEnabledScenes()
    {
        return EditorBuildSettings.scenes.Where(scene => scene.enabled).Select(scene => scene.path).ToArray();
    }

    private static void CompressBuild(string buildPath)
    {
        var compressedFilePath = buildPath + ".zip";
        
        if (File.Exists(compressedFilePath))
        {
            File.Delete(compressedFilePath);
        }
        
        ZipFile.CreateFromDirectory(buildPath, compressedFilePath);
    }
}
