using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private CanvasGroup _logoCanvas;
    [SerializeField] private Image _logo;
    
    private InputMaster _inputMaster;
    private float _timer;
    private int _skipCounter;
    
    // Start is called before the first frame update
    private IEnumerator Start()
    {
        _inputMaster = new InputMaster();
        _inputMaster.Enable();
        
        yield return  _logo.transform.DOScale(Vector3.one * 1.5f, 5f).WaitForCompletion();
        yield return _logoCanvas.DOFade(0f, 1f).WaitForCompletion();

        while (_timer < 4f)
        {
            if (_skipCounter >= 2)
            {
                _timer = 10f;
            }
            
            _timer += Time.deltaTime;
            yield return null;
        }

        var data = new LevelData
        {
            CommutingType = CommutingType.None,
            LevelType = Level.MainMenu,
            LevelName = "Main_Menu"
        };

        SingletonManager.Instance.GameManager.LoadALevel(data);
    }
    
    private void Update()
    {
        if (!_inputMaster.UI.AnyKey.WasPressedThisFrame())
        {
            return;
        }
        
        switch (_skipCounter)
        {
            case 0:
                _logo.transform.DOComplete();
                _skipCounter++;
                break;
            case 1:
                _logoCanvas.DOComplete();
                _skipCounter++;
                break;
        }
    }

    private void OnDestroy()
    {
        _inputMaster.Disable();
    }
}
