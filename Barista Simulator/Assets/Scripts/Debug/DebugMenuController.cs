#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DebugMenuController : MonoBehaviour
{
    [Header("Money Generator")]
    [SerializeField] private TMP_InputField _customAmountInputField;
    
    [Header("Loading level")]
    [SerializeField] private TMP_Dropdown _levelsDropdown;

    [Header("Recipe Generator")]
    [SerializeField] private TMP_Dropdown _cupTypeDropdown;
    [SerializeField] private TMP_Dropdown _cupSizeDropdown;
    [SerializeField] private TMP_Dropdown _recipeDropdown;
    [SerializeField] private Button _generateRecipeButton;
    [SerializeField] private GameObject _cupsGameObject;

    [Header("Debug Menu")]
    [SerializeField] private GameObject _menuGameObject;
    
    private bool _menuActive;

#if UNITY_EDITOR

    private void Start()
    {
        AddDropdownOptions();
    }
    
#endif
    
    public void OnLoadLevelButton(LevelData data)
    {
        SingletonManager.Instance.GameManager.LoadALevel(data);
    }

    public void OnGenerateRecipe()
    {
        // Get player position
        var recipePosition = PlayerInteractionManager.Instance.SocketForInteraction;

        var recipeType = GameConfig.GetConfigData().Gameplay.RecipesOnMenu.Draw().RecipeType;
        var go = Instantiate(_cupsGameObject, recipePosition.position, recipePosition.rotation);

        var cup = go.GetComponent<Cup>();
        cup.InitializeCupPrefab((OrderType)_cupTypeDropdown.value, true);
        Container<Liquid> container = new LiquidContainer(new Liquid(new Temperature(90f), 1), 1, false);
        RecipeMaker<Liquid>.MakeRecipe(recipeType, container, cup.GetContainer(), 90f, new CoffeeData());
    }

    private void AddDropdownOptions()
    {
        List<string> recipes = GameConfig.GetConfigData().Gameplay.RecipesOnMenu.Items.Select(item => item.Value.RecipeName).ToList();

        _recipeDropdown.ClearOptions();
        _recipeDropdown.AddOptions(recipes);

        string[] cupTypeArr = Enum.GetNames(typeof(OrderType));
        List<string> cupTypes = new List<string>(cupTypeArr);

        _cupTypeDropdown.ClearOptions();
        _cupTypeDropdown.AddOptions(cupTypes);

        _cupSizeDropdown.ClearOptions();

        var optionDataList = new List<TMP_Dropdown.OptionData>();
        for (var i = 0; i < SceneManager.sceneCountInBuildSettings; ++i) 
        {
            var sceneName = System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
            optionDataList.Add(new TMP_Dropdown.OptionData(sceneName));
        }
 
        _levelsDropdown.ClearOptions();
        _levelsDropdown.AddOptions(optionDataList);
    }
}

#endif