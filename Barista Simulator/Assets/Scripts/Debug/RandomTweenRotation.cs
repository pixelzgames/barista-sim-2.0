#if UNITY_EDITOR

using DG.Tweening;
using UnityEngine;

public class RandomTweenRotation : MonoBehaviour
{
    private Vector3 _rotationBy;
    [SerializeField] private float _time;
    [SerializeField] private Ease _easing;
    
    // Start is called before the first frame update
    private void Start()
    {
        _rotationBy = new Vector3(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180));
        transform.DOBlendableRotateBy(_rotationBy, _time).SetEase(_easing).SetLoops(1,LoopType.Yoyo).OnComplete(Start);
    }
}

#endif