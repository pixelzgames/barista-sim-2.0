#if UNITY_EDITOR

using QFSW.QC;
using QFSW.QC.Suggestors.Tags;
using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class CheatManager : MonoBehaviour
{
    [Command("Cheat_TimeScale", "Change the time scale")]
    private static float TimeScale
    {
        set => Time.timeScale = value;
    }
    
    [Command("Cheat_SaveGame", "Saves game")]
    private static void SaveGame()
    {
        SingletonManager.Instance.DataPersistenceManager.SaveAllPlayerData();
    }
    
    [Command("Cheat_SaveGrid", "Saves grid")]
    private static Level SaveGrid
    {
        set => SingletonManager.Instance.DataPersistenceManager.SaveGrid(value);
    }
    
    [Command("Cheat_LoadGrid", "Loads grid")]
    private static Level LoadGrid
    {
        set
        {
            if (SceneManager.GetActiveScene().name == "Main_Menu")
            {
                Debug.LogError("Error: Loading world while in main menu.");
                return;
            }
            
            var loaderHandler = new LoaderHandler
            (
                new ILoaderWaitable[]
                { 
                    new GridLoader(value)
                }
            );
            
            loaderHandler.RunChainLoaders();
        }
    }
    
    [Command("Cheat_SaveWorld", "Saves World")]
    private static Level SaveWorld
    {
        set => SingletonManager.Instance.DataPersistenceManager.SaveWorld(value);
    }
    
    [Command("Cheat_LoadWorld", "Loads World")]
    private static Level LoadWorld
    {
        set
        {
            if (SceneManager.GetActiveScene().name == "Main_Menu")
            {
                Debug.LogError("Error: Loading world while in main menu.");
                return;
            }
            
            var loaderHandler = new LoaderHandler
            (
                new ILoaderWaitable[]
                { 
                    new WorldLoader(value)
                }
            );
            
            loaderHandler.RunChainLoaders();
        }
    }
    
    [Command("Cheat_ResetSaveFile", "Resets Save File to default first time user experience")]
    private static void ResetSaveFile()
    {
        SingletonManager.Instance.DataPersistenceManager.ResetAllPlayerData();
    }
    
    [Command("Cheat_AddMoney", "Add or remove money")]
    private static int AddMoney
    {
        set => SingletonManager.Instance.CurrenciesManager.ChangeCurrentMoneyBy(value);
    }
    
    [Command("Cheat_AddXp", "Add Xp")]
    private static int AddXP
    {
        set => SingletonManager.Instance.CurrenciesManager.ChangeCurrentXPBy(value);
    }
    
    [Command("Cheat_RandomOrder", "Generate a random order")]
    private static void RandomOrder()
    {
        var values = Enum.GetValues(typeof(OrderType));
        var random = new Random();
        var cupType = (OrderType)values.GetValue(random.Next(values.Length));
        SingletonManager.Instance.OrderManager.GenerateRandomOrderFromMenu(cupType);
    }

    [Command("Cheat_NextDay", "Move to the next day")]
    private static void NextDay()
    {
        SingletonManager.Instance.GameManager.ChangeDay();
    }
    
    [Command("Cheat_SpawnCharacter", "Spawn a character to play with")]
    private static void SpawnCharacter()
    {
        SingletonManager.Instance.GameManager.SpawnCharacter(null);
    }
    
    [Command("Cheat_MakeEspresso", "Spawn a cup with espresso inside, 0 for Espresso, 1 for Double Espresso")]
    private static int MakeEspresso
    {
        set
        {
            // Get player position
            var recipePosition = PlayerInteractionManager.Instance.SocketForInteraction;
            var recipeType = value == 0 ? RecipeType.Espresso : RecipeType.DoubleEspresso;
            var cup = Instantiate(SingletonManager.Instance.CafeManager.CupPrefab, recipePosition.position, recipePosition.rotation);
            cup.InitializeCupPrefab(OrderType.Stay, true);
            Container<Liquid> container = new LiquidContainer(new Liquid(new Temperature(90f), 1), 1, false);
            RecipeMaker<Liquid>.MakeRecipe(recipeType, container, cup.GetContainer(), 90f, new CoffeeData());
        }
    }

    [Command("Cheat_FillTrashBins", "Fill all trash bins")]
    private static void FillAllTrashBins()
    {
        var bins = FindObjectsByType<TrashBin>(FindObjectsSortMode.None);
        foreach (var bin in bins)
        {
            bin.Trash(100f);
        }
    }
    
    [Command("Cheat_Notifications_Minor", "Spawns X notification messages")]
    private static int SpawnMinorNotifications
    {
        set
        {
            for (var i = 0; i < value; i++)
            {
                SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Test notification"));
            }
        }
    }
    
    [Command("Cheat_Notifications_Main", "Spawns X notification messages")]
    private static int SpawnMainNotifications
    {
        set
        {
            for (var i = 0; i < value; i++)
            {
                var data = new NotificationData("Basic Title", "A basic subtitle", "no description to show here",
                    4f, NotificationSize.Main);
                SingletonManager.Instance.GameNotificationManager.QueueNotification(data);
            }
        }
    }
    
    [Command("Cheat_ReceiveBeanOSChat", "Send a generic message to BeanOS")]
    private static int NewBeanOSMessage
    {
        set
        {
            for (var i = 0; i < value; i++)
            {
                var contact = new ContactData
                {
                    ContactName = "Test Contact",
                    ContactID = "test"
                };

                var data = new List<MessageData>();
                var message = new MessageData
                {
                    Message = "Test Message",
                    TimeToType = 2f,
                    DelayBetweenMessages = 1f,
                    MessageContact = ScriptableObject.CreateInstance<BeanOSContactConfig>()
                };
                
                
                message.MessageContact.ContactData.ContactName = contact.ContactName;
                message.MessageContact.ContactData.ContactID = contact.ContactID;
                
                data.Add(message);
                
                SingletonManager.Instance.BeanOSManager.CreateNewMessage(data);
            }
        }
    }
    
    [Command("Cheat_LoadALevel", "Load a level with a string")]
    private static void LoadALevel([SceneName]string level)
    {
        {
            var levelData = new LevelData()
            {
                LevelName = level,
                CommutingType = CommutingType.None,
                LevelType = Level.None
            };
            
            SingletonManager.Instance.GameManager.LoadALevel(levelData);
        }
    }
    
    [Command("Cheat_TestDialogue", "Prompt a test dialogue")]
    private static void TestDialogue()
    {
        var data = ScriptableObject.CreateInstance<DialogueConfig>();
        var lines = new string[2];
        lines[0] = "Salut mon gars! Je suis en train de me <bounce>branler</bounce> la graine. " +
                   "Je me demandais si tu voulais me joindre...";
        lines[1] = "Ahhh.... Nevermind. Je joue a AOE all day...";
        
        data.ScriptableObjectConstructor(lines);
        data.Contact.ContactData.ContactName = "Paul Test";
        SingletonManager.Instance.DialogueManager.RequestDialogueSequence(data.Contact.ContactData);
    }
    
    [Command("Cheat_ForceReceiveShipments", "Force receive all pending shipments")]
    private static void ForceReceiveShipments()
    {
        if (!SingletonManager.Instance.CafeManager.TryForceReceiveShipments())
        {
            Debug.LogError( "Was not able to receive shipments");
        }
    }
}

#endif