using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;

public class BlurPostProcessComponentSpawner : MonoBehaviour
{
    [SerializeField] private VolumeProfile _volumeProfile;

    private GameObject _volumeObject;
    private Volume _volume;
    
    private void OnEnable()
    {
        _volumeObject = new GameObject("BlurVolume", typeof(Volume));
        _volume = _volumeObject.GetComponent<Volume>();
        _volume.profile = _volumeProfile;
        _volume.priority = 100;
    }

    private void OnDisable()
    {
        OnDestroy();
    }

    private void OnDestroy()
    {
        DOTween.To(() => _volume.weight, x => _volume.weight = x, 0, 0.5f).onComplete = () 
            => Destroy(_volumeObject);
    }
}