using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Warm Up", story: "[Agent] warms Up", category: "BRAG", id: "587a8d69fc9ad8d8ca9b57584815efe0")]
public class WarmUpAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    protected override Status OnStart()
    {
        return Status.Running;
    }

    protected override Status OnUpdate()
    {
        while (!Agent.Value.NPCWarmedUp)
        {
            return Status.Running;
        }
        
        return Status.Success;
    }
}

