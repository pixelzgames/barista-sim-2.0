using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;
using Object = UnityEngine.Object;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Sit and Drink", story: "[Agent] sits and drinks", category: "BRAG", id: "001eaef169f1a3e35011c02a8ef8cac2")]
public class SitAndDrinkAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    private Timer _timer;
    private bool _timeUp;
    
    protected override Status OnStart()
    {
        _timer = Agent.Value.gameObject.AddComponent<Timer>();
        _timer.TimerStateChanged += OnTimeStateChanged;
        _timer.StartTimer(Agent.Value.GetWaitTime(GameConfig.Data.AI.BaseWaitTime));
        
        return Status.Running;
    }

    protected override Status OnUpdate()
    {
        if (!_timeUp)
        {
            return Status.Running;
        }
        
        Agent.Value.DropOrder();
        Agent.Value.TargetSeat.Stand(Agent.Value);
        
        return Status.Success;
    }

    protected override void OnEnd()
    {
        OnTimerEnd();
    }
    
    private void OnTimeStateChanged(TimerState state)
    {
        switch (state)
        {
            case TimerState.Ended:
                OnTimerEnd();
                break;
        }
    }
    
    private void OnTimerEnd()
    {
        _timeUp = true;
        _timer.TimerStateChanged -= OnTimeStateChanged;
        Object.Destroy(_timer);
    }
}

