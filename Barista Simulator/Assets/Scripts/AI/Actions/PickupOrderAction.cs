using System;
using AI;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Pickup Order", story: "[Agent] picks up order", category: "BRAG", id: "761dab84cb9831156b36498ca2e96d85")]
public class PickupOrderAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    
    protected override Status OnStart()
    {
        Agent.Value.ReceiveOrder();
        return Status.Success;
    }
}

