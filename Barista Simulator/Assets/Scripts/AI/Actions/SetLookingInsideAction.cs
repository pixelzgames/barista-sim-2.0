using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;
using Random = UnityEngine.Random;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Set Looking Inside", story: "Set [Agent] looking inside properties", category: "BRAG", id: "cc162f0bdbbe770eb5d41708b3008455")]
public class LookingInsideAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<float> LookingTime;
    

    protected override Status OnStart()
    {
        SingletonManager.Instance.CafeManager.CustomerEntered(Agent.Value);
        LookingTime.Value = Random.Range(GameConfig.GetConfigData().AI.ConsiderMinMaxWaitTime.x,
            GameConfig.GetConfigData().AI.ConsiderMinMaxWaitTime.y);
        
        return Status.Success;
    }
}

