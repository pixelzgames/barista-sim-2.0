using AI;
using System;
using AI.Structure;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Get in line", story: "[Agent] Gets in Line", category: "BRAG", id: "375510af8a17dfef88cbb0f435fa59d1")]
public class GetInLineAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    private CustomerQueueNode _requestedNode;
    private bool _reachedLineNode;
    
    protected override Status OnStart()
    {
        _requestedNode = null;
        FindAndGoToNode();
        return Status.Running;
    }

    protected override Status OnUpdate()
    {
        return _reachedLineNode ? Status.Success : Status.Running;
    }
    
    private void FindAndGoToNode()
    {
        var node = Agent.Value.CustomerLineup.RequestAvailableLineUpNode();
        if (node == null)
        {
            return;
        }

        _requestedNode = node;
        _requestedNode.StateChange += OnNodeStateChanged;
        Agent.Value.MoveToThenCallback(_requestedNode.WorldPosition, DestinationReached);
    }

    private void DestinationReached()
    {
        _requestedNode.StateChange -= OnNodeStateChanged;
        _reachedLineNode = true;
        Agent.Value.GetInLine();
    }

    private void OnNodeStateChanged(bool taken)
    {
        if (!taken)
        {
            return;
        }

        _requestedNode.StateChange -= OnNodeStateChanged;
        _requestedNode = null;
        FindAndGoToNode();
    }
}

