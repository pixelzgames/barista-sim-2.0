using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Find seat", story: "[Agent] finds a [seat]", category: "BRAG", id: "4ed77dead6968dbc4aebf1c428ef39e2")]
public class FindSeatAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<Seat> Seat;

    protected override Status OnStart()
    {
        if (Agent.Value.FindAvailableSeat())
        {
            Seat.Value = Agent.Value.TargetSeat;
            return Status.Success;
        }

        Seat.Value = null;
        return Status.Failure;
    }
}

