using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Show Thoughts", story: "[Agent] shows [Thoughts] for [Time] seconds", category: "BRAG", id: "166e20d1e8bd6e8153a9f029f58e6eea")]
public class ShowThoughtsAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<ThoughtsDisplayConfig> Thoughts;
    [SerializeReference] public BlackboardVariable<float> Time;
    [SerializeReference] public BlackboardVariable<bool> IsThinkingAnimation;

    protected override Status OnStart()
    {
        Agent.Value.ThoughtsUIController.RequestDisplay(Thoughts.Value, Time.Value);

        if (IsThinkingAnimation.Value)
        {
            Agent.Value.StartCoroutine(Agent.Value.Think(Time.Value));
        }
        
        return Status.Success;
    }
}

