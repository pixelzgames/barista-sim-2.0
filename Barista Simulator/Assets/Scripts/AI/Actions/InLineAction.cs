using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "In line", story: "[Agent] is in line", category: "BRAG", id: "c57b60f67aa769bc5b17bf8b47697b68")]
public class InLineAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<OrderTypeEnum> CurrentOrderType;

    private bool _successfullyOrdered;
    
    protected override Status OnStart()
    {
        if (!SingletonManager.Instance.CafeManager.IsCafeOpen)
        {
            return Status.Failure;
        }
        
        Agent.Value.OrderTaken = OnOrderTaken;
        return Status.Running;
    }

    protected override Status OnUpdate()
    {
        if (!Agent.Value.Ordered)
        {
            return Status.Running;
        }

        if (!_successfullyOrdered)
        {
            return Status.Failure;
        }
        
        switch (Agent.Value.CurrentOrder.Type)
        {
            case OrderType.Stay:
                CurrentOrderType.Value = OrderTypeEnum.Stay;
                break;
            case OrderType.Togo:
                CurrentOrderType.Value = OrderTypeEnum.Togo;
                break;
            default:
                return Status.Failure;
        }

        SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().CustomerData.CustomersOrdered++;
        return Status.Success;
    }

    protected override void OnEnd()
    {
        _successfullyOrdered = false;
    }
    
    private void OnOrderTaken()
    {
        _successfullyOrdered = Agent.Value.TryProcessOrder();
    }
}

