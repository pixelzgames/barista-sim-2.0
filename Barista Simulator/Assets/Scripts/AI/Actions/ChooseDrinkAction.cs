using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Choose Drink", story: "[Agent] chooses drink to order", category: "BRAG", id: "8bd8b2a8f7aed888f404c38a1a10654c")]
public class ChooseDrinkAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    protected override Status OnStart()
    {
        Agent.Value.ChooseDrinkToOrder();
        return Status.Success;
    }
}

