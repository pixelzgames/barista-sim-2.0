using Unity.Behavior;

[BlackboardEnum]
public enum CurrentIntentionEnum
{
    Walking,
	Visiting,
	Looking
}
