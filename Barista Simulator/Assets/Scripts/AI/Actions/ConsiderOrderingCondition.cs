using AI;
using System;
using Unity.Behavior;
using UnityEngine;

[Serializable, Unity.Properties.GeneratePropertyBag]
[Condition(name: "ConsiderOrdering", story: "[Agent] Considers Ordering", category: "Conditions", id: "169d4df786755bb951d1b15d9419ba22")]
public class ConsiderOrderingCondition : Condition
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    public override bool IsTrue()
    {
        return Agent.Value.ConsiderOrder() && Agent.Value.CustomerLineup.RequestAvailableLineUpNode() != null;
    }
}
