using System;
using Unity.Behavior;
using UnityEngine;
using Unity.Properties;

#if UNITY_EDITOR
[CreateAssetMenu(menuName = "Behavior/Event Channels/NPC Start")]
#endif
[Serializable, GeneratePropertyBag]
[EventChannelDescription(name: "NPC Start", message: "NPC Start", category: "BRAG", id: "3c07efe7d310d61a083c5ff2cf7ef6c6")]
public class NpcStartEvent : EventChannelBase
{
    public delegate void NpcStartEventEventHandler();
    public event NpcStartEventEventHandler Event; 

    public void SendEventMessage()
    {
        Event?.Invoke();
    }

    public override void SendEventMessage(BlackboardVariable[] messageData)
    {
        Event?.Invoke();
    }

    public override Delegate CreateEventHandler(BlackboardVariable[] vars, System.Action callback)
    {
        NpcStartEventEventHandler del = () =>
        {
            callback();
        };
        return del;
    }

    public override void RegisterListener(Delegate del)
    {
        Event += del as NpcStartEventEventHandler;
    }

    public override void UnregisterListener(Delegate del)
    {
        Event -= del as NpcStartEventEventHandler;
    }
}

