using System;
using Unity.Behavior;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Wait For Order", story: "[Agent] waits for his order", category: "BRAG", id: "c98fc27158a1553d4c2f2c07c27f9a50")]
public class WaitForOrderAction : WaitAction
{
    private bool _timeUp;
    
    protected override Status OnStart()
    {
        var waitTime = GameConfig.GetConfigData().AI.BaseWaitTime + Agent.Value.CurrentOrder.Recipe.NPCWaitTimeMinimum;
        Agent.Value.GoToWaitingArea();
        Agent.Value.SetOrderTimer(SetupTimer(waitTime));
        _timeUp = false;
        SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().CustomerData.CustomersOrdered++;
        
        return Status.Running;
    }

    protected override Status OnUpdate()
    {
        if (_timeUp)
        {
            return Status.Failure;
        }
            
        while (Agent.Value.WaitingForOrder)
        {
            return Status.Running;
        }

        Agent.Value.SendWaitForOrderFeedback(_timer);
        return Status.Success;
    }
    
    protected override void OnTimeStateChanged(TimerState state)
    {
        switch (state)
        {
            case TimerState.Started:
                Agent.Value.ThoughtsUIController.RequestDisplay(Thought, _timer.TimeRemaining, Agent.Value.CurrentOrder);
                break;
            case TimerState.Warning:
                Agent.Value.ThoughtsUIController.CurrentWaitingThought.ChangeWaitingState(state);
                break;
            case TimerState.EndingImminent:
                Agent.Value.ThoughtsUIController.CurrentWaitingThought.ChangeWaitingState(state);
                Agent.Value.SetImpatient(true);
                break;
            case TimerState.Ended:
                Agent.Value.ThoughtsUIController.RequestWaitingDisplayRemoval();
                Agent.Value.SendWaitForOrderFeedback(_timer);
                _timeUp = true;
                break;
        }
    }

    protected override void OnEnd()
    {
        base.OnTimerEnd();
    }
}

