using System;
using AI;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Sets Cafe Properties", story: "Sets up cafe [Door] position and [PickupArea] position for [Agent]", category: "BRAG", id: "27735f4d0a28064176df7628e8128b77")]
public class SetsCafePropertiesAction : Action
{
    [SerializeReference] public BlackboardVariable<Transform> Door;
    [SerializeReference] public BlackboardVariable<Transform> PickupArea;
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    protected override Status OnStart()
    {
        Door.Value = Agent.Value.CafeDoor;
        PickupArea.Value = Agent.Value.PickupArea;
        return Status.Success;
    }
}

