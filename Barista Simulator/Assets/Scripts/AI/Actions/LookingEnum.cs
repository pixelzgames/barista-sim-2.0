using Unity.Behavior;

[BlackboardEnum]
public enum LookingEnum
{
    LookingInside,
	LookingOutside
}
