using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Sit and wait Action", story: "[Agent] sits and wait for [WaitTime] seconds order", category: "BRAG", id: "6dcdbe1a70b992c57a099c7c1450ee0a")]
public class SitAndWaitAction : WaitAction
{
    private bool _timeUp;
    
    protected override Status OnStart()
    {
        var waitTime = GameConfig.GetConfigData().AI.BaseWaitTime + Agent.Value.CurrentOrder.Recipe.NPCWaitTimeMinimum;
        
        Agent.Value.MoveToThenCallback(Agent.Value.TargetSeat.GetClosestInteractionPoint(Agent.Value.transform.position).position,
            MoveCompleted);
 
        Agent.Value.SetOrderTimer(SetupTimer(waitTime));
        
        return Status.Running;
    }

    private void MoveCompleted()
    {
        Agent.Value.TargetSeat.Sit(Agent.Value);
    }

    protected override Status OnUpdate()
    {
        if (_timeUp)
        {
            Agent.Value.TargetSeat.Stand(Agent.Value);
            return Status.Failure;
        }
        
        while (Agent.Value.WaitingForOrder)
        {
            return Status.Running;
        }
        
        KillTimer();
        return Status.Success;
    }
    
    protected override void OnTimerEnd()
    {
        _timeUp = true;
        base.OnTimerEnd();
    }

    protected override void OnEnd()
    {
        _timeUp = false;
    }
    
    protected override void OnTimeStateChanged(TimerState state)
    {
        switch (state)
        {
            case TimerState.Started:
                Agent.Value.ThoughtsUIController.RequestDisplay(Thought.Value, _timer.TimeRemaining, Agent.Value.CurrentOrder);
                break;
            case TimerState.Warning:
                Agent.Value.ThoughtsUIController.CurrentWaitingThought.ChangeWaitingState(state);
                break;
            case TimerState.EndingImminent:
                Agent.Value.ThoughtsUIController.CurrentWaitingThought.ChangeWaitingState(state);
                Agent.Value.SetImpatient(true);
                break;
            case TimerState.Ended:
                Agent.Value.ThoughtsUIController.RequestWaitingDisplayRemoval();
                OnTimerEnd();
                break;
        }
    }
}

public class WaitAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<ThoughtsDisplayConfig> Thought;
    
    protected Timer _timer;
    private bool _timerUIRemoved;

    protected override Status OnStart()
    {
        SetupTimer(GameConfig.GetConfigData().AI.BaseWaitTime);
        return Status.Running;
    }
    
    protected virtual void OnTimeStateChanged(TimerState state)
    {
        switch (state)
        {
            case TimerState.Started:
                Agent.Value.ThoughtsUIController.RequestDisplay(Thought.Value, _timer.TimeRemaining);
                break;
            case TimerState.Warning:
                Agent.Value.ThoughtsUIController.CurrentWaitingThought.ChangeWaitingState(state);
                break;
            case TimerState.EndingImminent:
                Agent.Value.ThoughtsUIController.CurrentWaitingThought.ChangeWaitingState(state);
                Agent.Value.SetImpatient(true);
                break;
            case TimerState.Ended:
                _timerUIRemoved = Agent.Value.ThoughtsUIController.RequestWaitingDisplayRemoval();
                OnTimerEnd();
                break;
        }
    }
    
    protected Timer SetupTimer(float timeToWait)
    {
        _timer = Agent.Value.gameObject.AddComponent<Timer>();
        _timer.TimerStateChanged += OnTimeStateChanged;
        _timer.StartTimer(timeToWait);
            
        return _timer;
    }
    
    protected virtual void OnTimerEnd()
    {
        KillTimer();
    }

    protected void KillTimer()
    {
        Agent.Value.SetImpatient(false);
        _timer.TimerStateChanged -= OnTimeStateChanged;
            
        if (!_timerUIRemoved)
        {
            Agent.Value.ThoughtsUIController.RequestWaitingDisplayRemoval();
        }
            
        _timer.KillTimer();
        _timer = null;
    }
}

