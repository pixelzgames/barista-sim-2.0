using System;
using Unity.Behavior;

[Serializable, Unity.Properties.GeneratePropertyBag]
[Condition(name: "Is Cafe Closed", story: "Is the Cafe currently closed", category: "Conditions", id: "00d6913559739f8becaed12f4844ca42")]
public class IsCafeClosedCondition : Condition
{

    public override bool IsTrue()
    {
        return !SingletonManager.Instance.CafeManager.IsCafeOpen;
    }
}
