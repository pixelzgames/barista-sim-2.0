using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;
using Random = UnityEngine.Random;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Looking Outside", story: "[Agent] decides to look outside for [WaitTime] seconds", category: "BRAG", id: "938efbb1d5517c33d95184d6d3dba59c")]
public class LookingOutsideAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<float> WaitTime;
    
    protected override Status OnStart()
    {
        WaitTime.Value = Random.Range(GameConfig.GetConfigData().AI.ConsiderMinMaxWaitTime.x,
            GameConfig.GetConfigData().AI.ConsiderMinMaxWaitTime.y);
        
        return Status.Success;
    }
}

