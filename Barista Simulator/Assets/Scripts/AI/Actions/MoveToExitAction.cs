using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Move To Exit", story: "[Agent] moves to exit position", category: "BRAG", id: "85abbb911574574f92ce7552601ffe05")]
public class MoveToExitAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;

    private bool _completed;

    protected override Status OnStart()
    {
        Agent.Value.MoveToThenCallback(Agent.Value.ExitPosition, OnCompleted);
        return Status.Running;
    }

    protected override Status OnUpdate()
    {
        if (!_completed)
        {
            if (Agent.Value.AIPath.remainingDistance < 0.2f && Agent.Value.AIPath.velocity.magnitude <= 0.1f)
            {
                Agent.Value.StopMovement();
                return Status.Failure;
            }
            
            return Status.Running;
        }
        
        return Status.Success;
    }

    private void OnCompleted()
    {
        _completed = true;
    }

    protected override void OnEnd()
    {
        _completed = false;
    }
}

