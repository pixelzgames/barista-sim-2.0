using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Move To", story: "[Agent] moves to [position]", category: "BRAG", id: "34c333b5074e3c8261f378ab48ab9a74")]
public class MoveToAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<Transform> Position;

    private bool _reached;
    
    protected override Status OnStart()
    {
        Agent.Value.MoveToThenCallback(Position.Value.position, MoveCompleted);
        
        return Status.Running;
    }

    private void MoveCompleted()
    {
        _reached = true;
    }

    protected override Status OnUpdate()
    {
        return _reached ? Status.Success : Status.Running;
    }

    protected override void OnEnd()
    {
        _reached = false;
    }
}

