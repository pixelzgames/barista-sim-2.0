using System;
using Unity.Behavior;
using Random = UnityEngine.Random;

[Serializable, Unity.Properties.GeneratePropertyBag]
[Condition(name: "ConsiderCafe", story: "Agent considers visiting", category: "Conditions", id: "d98ac2d74e2a56acbc4dcfeabd7413a5")]
public class ConsiderCafeCondition : Condition
{
    private bool _goingIn;
    
    public override bool IsTrue()
    {
        // TODO Check different cafe state to see if this Ai goes in
        if (SingletonManager.Instance.CafeManager.IsCafeOpen && !SingletonManager.Instance.CafeManager.IsCafeAtFullCapacity)
        {
            _goingIn = Random.Range(0f,100f) > 25f;
        }
        
        return _goingIn;
    }
}
