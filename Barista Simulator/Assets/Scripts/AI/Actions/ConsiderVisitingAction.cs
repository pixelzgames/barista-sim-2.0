using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;
using Random = UnityEngine.Random;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Consider Visiting Action", story: "Agent Sets Looking [State]", category: "BRAG", id: "9582c7ce3f3c7b5ebd580b46026b2445")]
public class ConsiderVisitingAction : Action
{
    [SerializeReference] public BlackboardVariable<LookingEnum> State;
    
    private bool _goingIn;

    protected override Status OnStart()
    {
        _goingIn = Random.Range(0f, 100f) < GameConfig.GetConfigData().AI.ConsiderInsideChanceVersusOutside;
        if ((_goingIn && !SingletonManager.Instance.CafeManager.IsCafeOpen) || SingletonManager.Instance.CafeManager.IsCafeAtFullCapacity)
        {
            _goingIn = false;
        }

        State.Value = _goingIn ? LookingEnum.LookingInside : LookingEnum.LookingOutside;
        return Status.Success;
    }
}

