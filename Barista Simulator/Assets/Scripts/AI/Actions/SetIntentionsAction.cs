using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "SetIntentions", story: "[Agent] Sets Intentions", category: "BRAG", id: "3c1828d5f5cae438b8319877917c322c")]
public class SetIntentionsAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<CurrentIntentionEnum> CurrentIntention;
    
    private CustomerIntention _currentIntention;

    protected override Status OnStart()
    {
        _currentIntention = Agent.Value.GetIntentionByWeightedChance();
        
        if (_currentIntention != CustomerIntention.WalkBy)
        {
            Agent.Value.SetExitPositionToBeTheSameAsEntry();
        }
        
        // Check if Cafe is full capacity before visiting, if so, look around
        if (_currentIntention == CustomerIntention.Visit &&
            SingletonManager.Instance.CafeManager.IsCafeAtFullCapacity)
        {
            _currentIntention = CustomerIntention.LookAround;
        }

        switch (_currentIntention)
        {
            case CustomerIntention.WalkBy:
                CurrentIntention.Value = CurrentIntentionEnum.Walking;
                break;
            case CustomerIntention.Visit:
                CurrentIntention.Value = CurrentIntentionEnum.Visiting;
                break;
            case CustomerIntention.LookAround:
                CurrentIntention.Value = CurrentIntentionEnum.Looking;
                break;
            default:
                return Status.Failure;
        }
        
        return Status.Success;
    }
}

