using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "LookAt", story: "[Agent] looks at [Target]", category: "BRAG", id: "7108adc369c6b0945a91fec0c5f5c423")]
public  class LookAtAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<Transform> Target;

    protected override Status OnStart()
    {
        Agent.Value.LookAt(Target.Value);
        return Status.Success;
    }
}

