using AI;
using System;
using Unity.Behavior;
using UnityEngine;
using Action = Unity.Behavior.Action;
using Unity.Properties;

[Serializable, GeneratePropertyBag]
[NodeDescription(name: "Agent moves to Looking Position", story: "[Agent] moves to looking position from [LookingType]", category: "BRAG", id: "3d60263780803658839412aebbac5c87")]
public class AgentMovesToLookingPositionAction : Action
{
    [SerializeReference] public BlackboardVariable<NPCCustomer> Agent;
    [SerializeReference] public BlackboardVariable<LookingEnum> LookingType;

    private bool _moveCompleted;
    
    protected override Status OnStart()
    {
        AIPositionSurfaceAreaType lookingType = AIPositionSurfaceAreaType.ConsiderOrder;
        switch (LookingType.Value)
        {
            case LookingEnum.LookingInside:
                lookingType = AIPositionSurfaceAreaType.ConsiderOrder;
                break;
            case LookingEnum.LookingOutside:
                lookingType = AIPositionSurfaceAreaType.ConsiderVisit;
                break;
        }

        Agent.Value.MoveToThenCallback(Agent.Value.WaitingPoint(lookingType), OnMoveCompleted);
        return Status.Running;
    }

    private void OnMoveCompleted()
    {
        _moveCompleted = true;
    }

    protected override Status OnUpdate()
    {
        return !_moveCompleted ? Status.Running : Status.Success;
    }

    protected override void OnEnd()
    {
        _moveCompleted = false;
    }
}

