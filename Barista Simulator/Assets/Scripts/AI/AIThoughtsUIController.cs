using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AIThoughtsUIController : MonoBehaviour
{
   public AIThoughtWaitingUIController CurrentWaitingThought { get; private set; }

   [SerializeField] private Image _thoughtBubbleImage;
   [SerializeField] private TextMeshProUGUI _thoughtText;
   [SerializeField] private LayoutElement _layoutElement;
   [SerializeField] private AIThoughtWaitingUIController _waitingThoughtPrefab;

   private List<DisplayProperties> _displayRequests = new();
   private DisplayProperties _currentlyDisplaying;
   private Image _imageBubble;

   private void Awake()
   {
      _imageBubble = GetComponentInChildren<Image>();

      _currentlyDisplaying = null;
      _displayRequests.Clear();
      _imageBubble.transform.localScale = Vector3.zero;
      _thoughtBubbleImage.sprite = null;
   }

   /// <summary>
   /// Request a display in order of priority in the queue system.
   /// Will be displayed instantly if the priority is higher or if nothing is being displayed
   /// </summary>
   public void RequestDisplay(ThoughtsDisplayConfig config, float displayTime = 2f, Order order = null)
   {
      var request = new DisplayProperties(config, displayTime, order);
      _displayRequests.Add(request);
      _displayRequests = _displayRequests.OrderByDescending(o => o.Data.DisplayPriority).ToList();
      
      DisplayNextInQueue();
   }

   public bool RequestWaitingDisplayRemoval()
   {
      if (_currentlyDisplaying == null || !_currentlyDisplaying.Data.IsWaitingThought)
      {
         return false;
      }
      
      StopAllCoroutines();
      RemoveFromQueue();
      Destroy(CurrentWaitingThought.gameObject);
      CurrentWaitingThought = null;
      
      return true;
   }
   
   private void DisplayNextInQueue()
   {
      if (_currentlyDisplaying != null || _displayRequests.Count <= 0)
      {
         return;
      }
      
      StartCoroutine(DisplayThought());
   }
   
   private IEnumerator DisplayThought()
   {
      _currentlyDisplaying = _displayRequests[0];

      if (_currentlyDisplaying.Data.IsWaitingThought)
      {
         CurrentWaitingThought = Instantiate(_waitingThoughtPrefab, _thoughtBubbleImage.transform);
         CurrentWaitingThought.InitializeData(_currentlyDisplaying);
      }
      else
      {
         _thoughtBubbleImage.sprite = _currentlyDisplaying.Data.ThoughtIcon;
      }
      
      _imageBubble.color = _currentlyDisplaying.Data.DisplayColor.PaletteColor.Color;
      _thoughtText.text = _currentlyDisplaying.Data.DisplayText;
      _thoughtText.color = GetTextColor(_currentlyDisplaying.Data);
      _layoutElement.ignoreLayout = _thoughtText.text.Length <= 0;

      yield return _imageBubble.transform.DOScale(Vector3.one, 0.2f).WaitForCompletion();
      
      yield return new WaitForSeconds(_currentlyDisplaying.DisplayTime);
      
      yield return _imageBubble.transform.DOScale(Vector3.zero, 0.2f).WaitForCompletion();

      RemoveFromQueue();
   }
   
   private void RemoveFromQueue()
   {
      _displayRequests.Remove(_currentlyDisplaying);
      _currentlyDisplaying = null;

      DisplayNextInQueue();
   }

   private Color32 GetTextColor(ThoughtsDisplayConfig config)
   {
      return config.DisplayColor.PaletteColor.Color != Color.white ? Color.white : Color.black;
   }
}

public class DisplayProperties
{
   public DisplayProperties(ThoughtsDisplayConfig data, float time, Order order)
   {
      Data = data;
      DisplayTime = time;
      Order = order;
   }
   
   public ThoughtsDisplayConfig Data { get; private set; }
   public float DisplayTime { get; private set; }
   public Order Order { get; private set; }
}
