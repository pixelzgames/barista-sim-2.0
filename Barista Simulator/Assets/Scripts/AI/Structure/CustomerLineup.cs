using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Splines;

namespace AI.Structure
{
    public class CustomerLineup : MonoBehaviour
    {
        [SerializeField] 
        private SplineContainer _lineUpSpline;
        
        [SerializeField, Tooltip("Minimum distance between customers in the lineup.")]
        private float _minDistanceBetweenCustomers = 1f;

        private readonly List<NPCCustomer> _customerQueue = new();
        private readonly List<CustomerQueueNode> _lineupNodes = new();

        private void Awake()
        {
            GenerateLineupStructure();
        }

        private void Start()
        {
            SingletonManager.Instance.CafeManager.TakingOrder += OnOrderTaken;
        }
        
        private void OnDestroy()
        {
            SingletonManager.Instance.CafeManager.TakingOrder -= OnOrderTaken;
        }
        
        private void UpdateQueuePositions()
        {
            if (!SingletonManager.Instance.CafeManager.IsCafeOpen)
            {
                return;
            }
            
            for (var i = 0; i < _customerQueue.Count; i++)
            {
                _customerQueue[i].MoveToThenLookAt(_lineupNodes[i].WorldPosition, i == 0 ? _customerQueue[i].PickupArea : 
                    _customerQueue[i-1].transform);
                _customerQueue[i].CurrentQueueNode = _lineupNodes[i];
            }
        }

        public CustomerQueueNode RequestAvailableLineUpNode()
        {
            return _lineupNodes.FirstOrDefault(node => !node.Taken);
        }

        private void OnOrderTaken()
        {
            // Make sure the customer at front of the line has reached the counter (Inline == true) and the lineup isn't full.
            if (_customerQueue.Count == 0 || !_customerQueue[0].InLine)
            {
                SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("No order to take!"));
                return;
            }
            
            var npc = _customerQueue[0];
            npc.OrderTaken();
        }

        private void GenerateLineupStructure()
        {
            for (var i = 0; i < _lineUpSpline.Spline.Count - 1; i++)
            {
                // Calculate how many spots can exist from node i to node i+1.
                var dist = Vector3.Distance(_lineUpSpline.Spline[i].Position, _lineUpSpline.Spline[i+1].Position);
                var lineAmount = (int) Mathf.Floor(dist / _minDistanceBetweenCustomers);

                Vector3 lineVector = _lineUpSpline.Spline[i+1].Position - _lineUpSpline.Spline[i].Position;
                lineVector.Normalize();

                for (var j = 0; j < lineAmount; j++)
                {
                    var position = (Vector3)_lineUpSpline.Spline[i].Position + (j * dist/lineAmount * lineVector);
                    var worldPos = _lineUpSpline.transform.TransformPoint(position);
                    var node = new CustomerQueueNode(worldPos);
                    _lineupNodes.Add(node);
                }
            }
        }

        public void AddCustomerToQueue(NPCCustomer customer)
        {
            if (_customerQueue.Contains(customer))
            {
                return;
            }
            
            _customerQueue.Add(customer);
            UpdateQueuePositions();
        }

        public void RemoveCustomerFromQueue(NPCCustomer customer)
        {
            if (!_customerQueue.Contains(customer))
            {
                return;
            }
            
            _customerQueue.Remove(customer);
            UpdateQueuePositions();
        }

        public bool IsFirstInLine(NPCCustomer customer)
        {
            return _customerQueue.Count > 0 && _customerQueue[0].Equals(customer);
        }
    }
    
    public class CustomerQueueNode
    {
        public Action<bool> StateChange = delegate { };
        public bool Taken { get; private set; }
        public Vector3 WorldPosition { get; private set; }

        public CustomerQueueNode(Vector3 position)
        {
            WorldPosition = position;
            Taken = false;
        }

        public void UpdateNodeState(bool isTaken)
        {
            Taken = isTaken;
            StateChange.Invoke(isTaken);
        }
    }
}