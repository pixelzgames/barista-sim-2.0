using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AI.Feedback
{
    /// <summary>
    /// Container of all FeedbackDays 
    /// </summary>
    [Serializable]
    public class FeedbackCatalogData
    {
        [JsonProperty]
        public List<FeedbackDay> FeedbackDays { get; private set; } = new();

        [JsonIgnore]
        public int CurrentDay { get; private set; }
    
        public void AddCustomerFeedbackToCatalog(CustomerFeedback feedback)
        {
            if (FeedbackDays.Count == 0)
            {
                FeedbackDays.Add(new FeedbackDay(CurrentDay, new List<CustomerFeedback>()));
            }

            FeedbackDays[CurrentDay].CustomerFeedbackList.Add(feedback);
        }

        public void AddNewFeedbackDay(int day)
        {
            CurrentDay = day;
            FeedbackDays.Add(new FeedbackDay(day, new List<CustomerFeedback>()));
        }
    }
}
