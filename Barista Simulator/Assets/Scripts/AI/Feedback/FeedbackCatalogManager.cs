namespace AI.Feedback
{
    /// <summary>
    /// Manages the FeedbackCatalog state and handles saving and loading 
    /// </summary>
    public class FeedbackCatalogManager : PersistentBaseData<FeedbackCatalogData>
    {
        private void Start()
        {
            SingletonManager.Instance.GameManager.ChangedDay += OnChangedDay;
            SingletonManager.Instance.CafeManager.CafeStateChange += OnCafeStateChanged;
        }

        private void OnDestroy()
        {
            SingletonManager.Instance.GameManager.ChangedDay -= OnChangedDay;
            SingletonManager.Instance.CafeManager.CafeStateChange -= OnCafeStateChanged;
        }

        private void OnChangedDay(int day)
        {
            Data.AddNewFeedbackDay(day);
        }

        private void OnCafeStateChanged(bool open)
        {
            if (open)
            {
                return;
            }
            
            // Save feedback catalog when cafe is closed.
            SingletonManager.Instance.DataPersistenceManager.SaveAllPlayerData();
        }

        protected override void LoadData(PlayerData data)
        {
            Data = data.FeedbackCatalogData;
        }
    }
}
