using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AI.Feedback
{
    [Serializable]
    public class FeedbackDay
    {
        [JsonProperty]
        public int Day { get; private set; }
        [JsonProperty]
        public List<CustomerFeedback> CustomerFeedbackList { get; private set; }

        public FeedbackDay(int day, List<CustomerFeedback> customerFeedbacks)
        {
            Day = day;
            CustomerFeedbackList = customerFeedbacks;
        }
    }
}
