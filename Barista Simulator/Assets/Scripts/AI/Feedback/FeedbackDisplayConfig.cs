using UnityEngine;

[CreateAssetMenu(fileName = "NewFeedbackEntryConfig", menuName = "BRAG/AI/FeedbackEntryConfig")]
public class FeedbackDisplayConfig : ScriptableObject
{
    [field: SerializeField]
    public FeedbackDisplayType FeedbackDisplayType { get; private set; }

    [field: SerializeField]
    public FeedbackResultThoughtsDisplayConfigDictionary ThoughtsResultsMapping { get; private set; }

    [field: SerializeField]
    public FeedbackResultSocialDisplayConfigDictionary SocialResultsMapping { get; private set; }
}
