using UnityEngine;

/// <summary>
/// Holds the mapping between all FeedbackTypes and their display config
/// </summary>
[CreateAssetMenu(fileName = "NewFeedbackEntryTypeConfig", menuName = "BRAG/AI/FeedbackEntryTypeConfig")]
public class FeedbackEntryTypeDisplayMapping : ScriptableObject
{
    public FeedbackEntryTypeDisplayConfigDictionary FeedbackEntryTypes;
}