public enum FeedbackEntryResult
{
    Fail,
    BelowExpected,
    Expected,
    AboveExpected
}
