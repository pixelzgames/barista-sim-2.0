using System;

public enum FeedbackEntryType
{
    OrderTemperature,
    InLineWaitTime,
    OrderWaitTime,
    Generics,
}

[Flags]
public enum FeedbackDisplayType
{
    None = 0,
    MomentToMoment = 1,
    Social = 2,
    Summary = 4
}
