using AI.Feedback;
using UnityEngine;

/// <summary>
/// Processor to calculate scores, results and create FeedbackEntries from raw data passed from customers.
/// </summary>
public static class FeedbackProcessor
{
    public static FeedbackEntry CreateOrderTemperatureFeedback(Recipe recipe)
    {
        FeedbackEntryResult result;

        float score;
        const int constant = 4;

        if (recipe.Temperature.Current < recipe.Config.IdealTemperature.x)
        {
            // Too low
            result = FeedbackEntryResult.BelowExpected;
            score = Mathf.Clamp(100 - (recipe.Temperature.Current - recipe.Config.IdealTemperature.x) * constant, 0, 100);
        }
        else if (recipe.Temperature.Current > recipe.Config.IdealTemperature.y)
        {
            // Too high
            result = FeedbackEntryResult.AboveExpected;
            score = Mathf.Clamp(100 - (recipe.Temperature.Current - recipe.Config.IdealTemperature.x) * constant, 0, 100);
        }
        else
        {
            // Perfect
            result = FeedbackEntryResult.Expected;
            score = 100;
        }

        return new FeedbackEntry(FeedbackEntryType.OrderTemperature, result, score);
    }

    public static FeedbackEntry CreateWaitTimeFeedback(Timer timer, FeedbackEntryType type)
    {
        FeedbackEntryResult result;
        float score;

        if (timer.TimerFinished)
        {
            result = FeedbackEntryResult.Fail;
            score = -100f;
        }
        else if (timer.ImminentReached)
        {
            result = FeedbackEntryResult.BelowExpected;
            score = 50f;
        }
        else if (timer.ModerateReached)
        {
            result = FeedbackEntryResult.Expected;
            score = 80f;
        }
        else
        {
            result = FeedbackEntryResult.AboveExpected;
            score = 100f;
        }

        return new FeedbackEntry(type, result, score);
    }
}
