using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AI.Feedback
{
    /// <summary>
    /// Holds list of all feedback associated to a specific customer
    /// </summary>
    [Serializable]
    public class CustomerFeedback
    {
        [JsonProperty]
        public string CustomerID { get; private set; }
        [JsonProperty]
        public List<FeedbackEntry> FeedbackList { get; private set; }

        [JsonIgnore]
        public bool IsEmpty => FeedbackList.Count == 0;
        [JsonIgnore]
        public Action<FeedbackEntry> FeedbackLogged = delegate { };

        public CustomerFeedback(string customerID)
        {
            CustomerID = customerID;
            FeedbackList = new List<FeedbackEntry>();
        }

        public void LogFeedback(FeedbackEntry feedback)
        {
            FeedbackList.Add(feedback);
            FeedbackLogged(feedback);
        }

        public void AddFeedbackToCatalog()
        {
            SingletonManager.Instance.FeedbackCatalogManager.Data.AddCustomerFeedbackToCatalog(this);
        }
    }
}
