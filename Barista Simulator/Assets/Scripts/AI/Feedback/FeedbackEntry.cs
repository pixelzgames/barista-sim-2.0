using Newtonsoft.Json;
using System;

namespace AI.Feedback
{
    /// <summary>
    /// Feedback data object
    /// </summary>
    [Serializable]
    public class FeedbackEntry
    {
        [JsonProperty]
        public FeedbackEntryType Type { get; private set; }
        [JsonProperty]
        public FeedbackEntryResult Result { get; private set; }
        [JsonProperty]
        public float Score { get; private set; } = 100;

        public FeedbackEntry(FeedbackEntryType type, FeedbackEntryResult result, float score)
        {
            Type = type;
            Result = result;
            Score = score;
        }
    }
}
