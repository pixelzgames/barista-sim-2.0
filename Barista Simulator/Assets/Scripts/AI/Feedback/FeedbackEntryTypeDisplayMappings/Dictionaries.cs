using System;
public class Dictionaries
{
    
}

[Serializable]
public class FeedbackEntryTypeDisplayConfigDictionary : SerializableDictionary<FeedbackEntryType, FeedbackDisplayConfig>
{

}

[Serializable]
public class FeedbackResultSocialDisplayConfigDictionary : SerializableDictionary<FeedbackEntryResult, SocialPostDisplayConfig>
{

}

[Serializable]
public class FeedbackResultThoughtsDisplayConfigDictionary : SerializableDictionary<FeedbackEntryResult, ThoughtsDisplayConfig>
{

}

[Serializable]
public class GameplayStateInteractionModeDictionary : SerializableDictionary<GameplayState, InteractionMode>
{

}

[Serializable]
public class ObjectStatesDictionary : SerializableDictionary<ObjectState, ObjectStateConfig>
{

}

[Serializable]
public class CoffeeDescriptorsDictionary : SerializableDictionary<TastingDescriptor, CoffeeDescriptorScheme>
{

}

[Serializable]
public class AnimationSocketDictionary : SerializableDictionary<InteractableAnimationType, Socket>
{
    
}

[Serializable]
public class PrefabDictionary : SerializableDictionary<byte, BRAGPrefabHandler>
{
    
}
