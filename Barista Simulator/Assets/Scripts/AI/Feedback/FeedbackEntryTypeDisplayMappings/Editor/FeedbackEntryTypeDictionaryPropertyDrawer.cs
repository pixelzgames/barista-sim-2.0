using UnityEditor;

[CustomPropertyDrawer(typeof(FeedbackEntryTypeDisplayConfigDictionary))]
public class FeedbackEntryTypeDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(FeedbackResultSocialDisplayConfigDictionary))]
public class FeedbackResultSocialDisplayConfigDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(FeedbackResultThoughtsDisplayConfigDictionary))]
public class FeedbackResultThoughtsDisplayConfigDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(GameplayStateInteractionModeDictionary))]
public class GameplayStateInteractionModeDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(ObjectStatesDictionary))]
public class ObjectStatesDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }

[CustomPropertyDrawer(typeof(AnimationSocketDictionary))]
public class AnimationSocketDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer { }