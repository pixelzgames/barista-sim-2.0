using System;
using UnityEngine;
using UnityEngine.UI;

public class AIThoughtWaitingUIController : MonoBehaviour
{
    [SerializeField] private Sprite _normalIcon, _warningIcon, _imminentIcon;
    [SerializeField] private Image _fillingImage;
    [SerializeField] private Image _icon;
    [SerializeField] private FillingUIController _fillingController;

    private DisplayProperties _data;
    public void InitializeData(DisplayProperties data)
    {
        _data = data;
        _icon.sprite = data.Order ? data.Order.Recipe.Icon : data.Data.ThoughtIcon;
        _fillingController.StartFilling(data.DisplayTime);
        _fillingImage.color = DTT.GUI_Elements.Thoughts_Overlay_Waiting_Good;
    }

    public void ChangeWaitingState(TimerState state)
    {
        switch (state)
        {
            case TimerState.Started:
                _fillingImage.color = DTT.GUI_Elements.Thoughts_Overlay_Waiting_Good;
                break;
            case TimerState.Warning:
                _fillingImage.color = DTT.GUI_Elements.Thoughts_Overlay_Waiting_Moderate;
                if (!_data.Order)
                {
                    _icon.sprite = _warningIcon;
                }
                
                break;
            case TimerState.EndingImminent:
                _fillingImage.color = DTT.GUI_Elements.Thoughts_Overlay_Waiting_Imminent;
                if (!_data.Order)
                {
                    _icon.sprite = _imminentIcon;
                }

                break;
            case TimerState.Ended:
                _fillingController.StopFilling();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    private void OnDisable()
    {
        _fillingController.StopFilling();
    }
}
