using System;
using System.Collections;
using DG.Tweening;
using Pathfinding;
using UnityEngine;

namespace AI
{
    public abstract class NPCBase : MonoBehaviour
    {
        public event Action DestinationReached;
        public event Action Stopped;
        
        [SerializeField] protected AIPath _aiPath;
        [SerializeField] protected Transform _handSocket;

        public AIPath AIPath => _aiPath;

        private Coroutine _currentMoveTo;
        
        protected virtual void Start()
        {
            if (!_aiPath)
            {
                Debug.LogError($"{gameObject.name} does not have a NavMesh Agent component, deleting...");
                Destroy(gameObject);
            }
        }
        
        protected virtual void OnDisable()
        {
            DestinationReached = null;
        }

        public void LookAt(Transform target)
        {
            _aiPath.updateRotation = false;
            transform.DOLookAt(target.position, 0.4f);
        }

        public void MoveTo(Vector3 positionToGo)
        {
            _currentMoveTo = StartCoroutine(MoveToDestination(positionToGo));
        }

        private IEnumerator MoveToDestination(Vector3 positionToGo)
        {
            _aiPath.isStopped = false;
            _aiPath.updateRotation = true;
            _aiPath.destination = positionToGo;
            _aiPath.SearchPath();
            
            while (!_aiPath.reachedDestination) 
            {
                yield return null;
            }
            
            DestinationReached?.Invoke();
        }

        public void StopMovement()
        {
            _aiPath.isStopped = true;
            StopCoroutine(_currentMoveTo);
            Stopped?.Invoke();
        }

        public void MoveToThenLookAt(Vector3 positionToGo, Transform target)
        {
            DestinationReached = () => LookAt(target);
            MoveTo(positionToGo);
        }

        public void MoveToThenCallback(Vector3 positionToGo, Action completed)
        {
            DestinationReached = completed;
            MoveTo(positionToGo);
        }

        protected void TeleportTo(Vector3 positionToTeleportTo)
        {
            transform.position = positionToTeleportTo;
            //_aiPath.Teleport(positionToTeleportTo);
        }
    }
}
