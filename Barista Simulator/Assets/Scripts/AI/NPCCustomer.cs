using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AI.Feedback;
using AI.Structure;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AI
{
    public class NPCCustomer : NPCBase
    {
        public Action OrderTaken = delegate {  };
        
        private const string TAG_TO_CHECK = "TriggerNPC";
        private static readonly int WalkSpeed = Animator.StringToHash("walk_speed");
        private static readonly int Sitting = Animator.StringToHash("Sitting");
        private static readonly int Impatient = Animator.StringToHash("Impatient");
        private static readonly int Thinking = Animator.StringToHash("Thinking");

        [Header("Wait Parameters")]

        // TODO: Maybe add impatience/patience value to TraitType/Trait class
        [SerializeField] private float _impatienceValue;
        private float _impatienceConstant => _currentTraits.Contains(TraitType.Impatient) ? _impatienceValue : 0f; 

        [SerializeField] private float _patienceValue;
        private float _patienceConstant => _currentTraits.Contains(TraitType.Patient) ? _patienceValue : 0f;

        private RangeInt _randomWaitTimeInterval = new RangeInt(0,20);

        public AIThoughtsUIController ThoughtsUIController => _thoughtsUIController;
        public CustomerLineup CustomerLineup => _allocator.Lineup;
        public CustomerQueueNode CurrentQueueNode
        {
            get => _currentQueueNode;
            set
            {
                if (_currentQueueNode == value)
                {
                    return;
                }
                
                _currentQueueNode?.UpdateNodeState(false);

                _currentQueueNode = value;
                _currentQueueNode?.UpdateNodeState(true);
            }
        }

        public bool NPCWarmedUp { get; private set; }

        [Header("AI Movement")]
        [SerializeField] private Animator _animator;
        [SerializeField] private Rigidbody _rb;
        
        private CustomersController _allocator;
        private CustomerQueueNode _currentQueueNode;
        
        private bool _leftInitialTrigger;
        private float _timeStampLeftTrigger;
        private const float TIME_THRESHOLD_FOR_TRIGGER = 3f;

        public Transform PickupArea => _allocator.SaleCounter.transform;
        public Vector3 WaitingPoint(AIPositionSurfaceAreaType type) => _allocator.GenerateRandomPointOnSurface(_allocator.GetTransformsForSurfaceType(type));
        public Transform CafeDoor => _allocator.RandomDoor.GetClosestInteractionPoint(transform.position);
        
        // AI traits, expectations and needs
        private readonly List<TraitType> _currentTraits = new();
        private readonly List<Expectation> _currentExpectations = new();
        private readonly List<Expectation> _currentNeeds = new();

        // Feedback 
        private CustomerFeedback _customerFeedback;

        [Header("UI")]
        // Display thoughts UI
        [SerializeField]
        private AIThoughtsUIController _thoughtsUIController;
        
        public Seat TargetSeat { get; private set; }
        public Order CurrentOrder { get; private set; }
        public bool InLine { get; private set; }
        public bool Ordered { get; private set; }
        public bool ReceivedOrder { get; private set; }
        public bool WaitingForOrder { get; private set; }
        public GameObject OrderObject { get; private set; }
        public Vector3 EntryPosition { get; private set; }
        public Vector3 ExitPosition { get; private set; }
        public RecipeConfig DesiredDrinkToOrder { get; private set; }
        public OrderType DesiredOrderType { get; private set; }
        public TastingDescriptor DesiredTastingNote { get; private set; }

        public string DesiredDrinkString { get; private set; }
        public string ExpectationsString { get; private set; }
        public string TraitsString { get; private set; }
        
        private float _orderTime;
        private static readonly int Pickup = Animator.StringToHash("Pickup");
        private static readonly int Holding = Animator.StringToHash("Holding");

        private void OnEnable()
        {
            _customerFeedback = new CustomerFeedback(Guid.NewGuid().ToString());
            _customerFeedback.FeedbackLogged += OnFeedbackLogged;
            SingletonManager.Instance.CafeManager.CafeStateChange += OnCafeStateChanged;
        }

        private void Update()
        {
            _animator.SetFloat(WalkSpeed, _aiPath.velocity.magnitude / 2f);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            SingletonManager.Instance.CafeManager.CafeStateChange -= OnCafeStateChanged;
            _customerFeedback.FeedbackLogged -= OnFeedbackLogged;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(TAG_TO_CHECK) || !_leftInitialTrigger || Time.time - _timeStampLeftTrigger < TIME_THRESHOLD_FOR_TRIGGER)
            {
                return;
            }

            UnAllocate();
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag(TAG_TO_CHECK) || _leftInitialTrigger)
            {
                return;
            }

            _timeStampLeftTrigger = Time.time;
            _leftInitialTrigger = true;
        }

        #region Callbacks
        private void OnCafeStateChanged(bool isCafeOpen)
        {
            if (!isCafeOpen && InLine)
            {
                //_stateMachine.ForceLeaveCafeState();
            }
        }

        private void OnFeedbackLogged(FeedbackEntry entry)
        {
            var displayConfig = _allocator.CustomerConfigData.FeedbackEntryTypeConfig.FeedbackEntryTypes[entry.Type];
            if (displayConfig.FeedbackDisplayType.HasFlag(FeedbackDisplayType.MomentToMoment) &&
                displayConfig.ThoughtsResultsMapping.TryGetValue(entry.Result, out var thoughtDisplayConfig))
            {
                _thoughtsUIController.RequestDisplay(thoughtDisplayConfig);
            }
        }
        #endregion

        #region Pool methods
        public void InitAllocation(CustomersController allocator)
        {
            _allocator = allocator;
            
            AssignTraits();
            AssignExpectations();

            EntryPosition = allocator.GetRandomEntryExitPosition(out var exit);
            ExitPosition = exit;
            TeleportTo(EntryPosition);
            _aiPath.maxSpeed = Random.Range(GameConfig.Data.AI.MinMaxNPCSpeed.x, GameConfig.Data.AI.MinMaxNPCSpeed.y);
            transform.rotation = ExtensionMethods.GetLookAtRotation(transform.position, exit);
            NPCWarmedUp = true;
        }

        private void UnAllocate()
        {
            // Social Post
            if (!_customerFeedback.IsEmpty)
            {
                SingletonManager.Instance.BeanOSManager.AddSocialPost(new SocialPostStubber
                {
                    ContentText = SocialPostBuilder.GenerateSocialFeedbackString(_customerFeedback.FeedbackList, _allocator.CustomerConfigData.FeedbackEntryTypeConfig.FeedbackEntryTypes),
                    LikesAmount = Random.Range(10, 4230),
                    UserTag = "@CoffeeLover69"
                });
            }
            
            CurrentOrder = null;
            _allocator.UnAllocateCustomer(this);
            _leftInitialTrigger = false;
            NPCWarmedUp = false;

            // Send customer feedback to catalog to be saved
            if (!_customerFeedback.IsEmpty)
            {
                _customerFeedback.AddFeedbackToCatalog();
            }
            
            gameObject.SetActive(false);
        }

        public void ResetNPC()
        {
            NPCWarmedUp = false;
            InLine = false;
            Ordered = false;
            ReceivedOrder = false;
            WaitingForOrder = false;

            _animator.ResetTrigger(Pickup);
            _animator.SetBool(Holding, false);

            if (OrderObject != null)
            {
                Destroy(OrderObject);
            }
        }
        #endregion

        #region Expectations/Needs/Traits methods
        /// <summary>
        /// Check and updates the current status of the expectations
        /// Eg; When ordering, when entering cafe, when leaving...
        /// </summary>
        public void UpdateCurrentExpectationsAndNeeds(ExpectationType[] types)
        {
            foreach (var expectation in _currentExpectations.Where(expectation => types.Contains(expectation.ExpectationConfig.Type)))
            {
                expectation.UpdateAndCalculateSatisfaction(this);
            }

            foreach (var need in _currentNeeds.Where(need => types.Contains(need.ExpectationConfig.Type)))
            {
                need.UpdateAndCalculateSatisfaction(this);
            }
        }

        /// <summary>
        /// Get a random intention based on weighted probabilities config
        /// </summary>
        /// <returns>Random intention</returns>
        public CustomerIntention GetIntentionByWeightedChance()
        {
            // Draw a random weighted intention but do not remote or alter the config
            var randomIntention = _allocator.CustomerConfigData.IntentionsConfig.Draw();
            return randomIntention;
        }

        private void AssignTraits()
        {
            // Clear previous list
            _currentTraits.Clear();

            var tempConfig = Instantiate(_allocator.CustomerConfigData.TraitConfig);

            var randomIterationNumber =
                Random.Range(tempConfig.MinTraitAmount, tempConfig.MaxTraitAmount);

            TraitsString = string.Empty;
            for (var i = 0; i < randomIterationNumber; i++)
            {
                var randomTraitByWeight = tempConfig.Draw(true);
                _currentTraits.Add(randomTraitByWeight);
                TraitsString += randomTraitByWeight + (i == randomIterationNumber-1 ? string.Empty : ", ");
            }

            // Wipe the temporary config created
            Destroy(tempConfig);
        }

        private void AssignExpectations()
        {
            // Clear previous lists
            _currentExpectations.Clear();
            _currentExpectations.Clear();
            _currentNeeds.Clear();

            ExpectationsString = string.Empty;
            // Assign expectations with current traits
            foreach (var expectation in _allocator.CustomerConfigData.ExpectationsTraitsList.ExpectationsByTraits)
            {
                if (!expectation.RequiredTraitsForExpectation.All(
                        requiredTrait => _currentTraits.Contains(requiredTrait)))
                {
                    continue;
                }

                _currentExpectations.Add(new Expectation(expectation));

                if (_currentExpectations.Count == _allocator.CustomerConfigData.ExpectationsConfig.MaxExpectationTraitAmount)
                {
                    break;
                }
            }

            // Assign binaries expectations
            var tempConfigExpectations = Instantiate(_allocator.CustomerConfigData.ExpectationsConfig);
            var copyExpectation = Instantiate(tempConfigExpectations.Binaries);

            var randomIterationNumber =
                Random.Range(tempConfigExpectations.MinExpectationBinaryAmount, tempConfigExpectations.MaxExpectationBinaryAmount);

            for (var i = 0; i < randomIterationNumber; i++)
            {
                if (Random.Range(0f, 100f) > tempConfigExpectations.ChanceOfGettingBinary)
                {
                    continue;
                }

                var randomExpectationByWeight = copyExpectation.Draw(true);
                _currentExpectations.Add(new Expectation(randomExpectationByWeight));
            }

            // Wipe the temporary config created
            Destroy(tempConfigExpectations);

            // Assign needs
            var tempNeedsConfig = Instantiate(_allocator.CustomerConfigData.ExpectationsConfig);
            var copyNeed = Instantiate(tempNeedsConfig.Needs);

            randomIterationNumber =
                Random.Range(tempNeedsConfig.MinNeedAmount, tempNeedsConfig.MaxNeedAmount);

            for (var i = 0; i < randomIterationNumber; i++)
            {
                if (Random.Range(0f, 100f) > tempNeedsConfig.ChanceOfGettingNeed)
                {
                    continue;
                }

                var randomNeedsByWeight = copyNeed.Draw(true);
                foreach (var currentExpectation in _currentExpectations.Where(
                             currentExpectation => currentExpectation.ExpectationConfig.Type == randomNeedsByWeight.Type))
                {
                    _currentExpectations.Remove(currentExpectation);
                    break;
                }

                var newNeed = new Expectation(randomNeedsByWeight)
                {
                    IsANeed = true
                };

                _currentNeeds.Add(newNeed);
            }

            // Wipe the temporary config created
            DestroyImmediate(tempNeedsConfig);

            // Set proper thresholds on expectations
            for (var i = 0; i < _currentExpectations.Count; i++)
            {
                _currentExpectations[i].InfluenceThresholdWithTraits(_currentTraits.ToArray());
                ExpectationsString += _currentExpectations[i].ExpectationConfig.Type + (i == _currentExpectations.Count - 1 ? string.Empty : ", ");
            }

            foreach (var need in _currentNeeds)
            {
                need.InfluenceThresholdWithTraits(_currentTraits.ToArray());
            }
        }
        #endregion

        #region NPC Action methods

        public void SetExitPositionToBeTheSameAsEntry()
        {
            ExitPosition = EntryPosition;
        }
        
        public bool TryProcessOrder()
        {
            if (!CustomerLineup.IsFirstInLine(this))
            {
                return false;
            }

            // Dequeue this NPC from customer queue
            LeaveLineup();

            if (!FindAvailableSeat())
            {
                return false;
            }

            var validRecipe = GameConfig.GetConfigData().Gameplay.RecipesOnMenu.Items.Any(item => item.Value == DesiredDrinkToOrder);

            if (validRecipe)
            {
                CurrentOrder = SingletonManager.Instance.OrderManager.GenerateSpecificOrder(DesiredOrderType,
                    DesiredDrinkToOrder, this, DesiredTastingNote);
            }
            else
            {
                // TODO log disappointment
                CurrentOrder =
                    SingletonManager.Instance.OrderManager.GenerateRandomOrderFromMenu(DesiredOrderType, this,
                        DesiredTastingNote);
            }

            WaitingForOrder = true;
            CurrentOrder.OnFailure += AbandonWaiting;
            
            // Debug AI
            Ordered = true;
            return true;
        }

        public bool FindAvailableSeat()
        {
            // Check requested order and order the desired order if it is available
            var seat = SingletonManager.Instance.CafeManager.Seats.Find(x => !x.Occupied);
            var isSeatAvailable = seat != null;
            
            // Check if staying and sitting was an expectation
            var expectationToSit = _currentExpectations.FirstOrDefault(o => o.ExpectationConfig.Type == ExpectationType.Seat);
            var needsToSit = _currentNeeds.FirstOrDefault(o => o.ExpectationConfig.Type == ExpectationType.Seat);
            
            if (needsToSit != null && !isSeatAvailable)
            {
                // TODO Log disappointment
                return false;
            }
            
            if (!isSeatAvailable)
            {
                if (expectationToSit != null)
                {
                    // TODO log disappointment
                }
                
                DesiredOrderType = OrderType.Togo;
            }
            else if (DesiredOrderType == OrderType.Stay)
            {
                TargetSeat = seat;
                TargetSeat.SetOccupied();
            }

            return true;
        }

        public void SendWaitInLineFeedback(Timer timer)
        {
            // Send WaitInLineTime feedback
            _customerFeedback.LogFeedback(FeedbackProcessor.CreateWaitTimeFeedback(timer, FeedbackEntryType.InLineWaitTime));
        }

        public void SendWaitForOrderFeedback(Timer timer)
        {
            // Send WaitInLineTime feedback
            _customerFeedback.LogFeedback(FeedbackProcessor.CreateWaitTimeFeedback(timer, FeedbackEntryType.OrderWaitTime));
        }

        /// <summary>
        /// Called from WaitForOrder states to give a reference to their timers to the Order object.
        /// </summary>
        /// <param name="timer"></param>
        public void SetOrderTimer(Timer timer)
        {
            CurrentOrder.SetOrderTimer(timer);
        }

        public void GetInLine()
        {
            StopMovement();
            CustomerLineup.AddCustomerToQueue(this);
            InLine = true;
        }

        private void LeaveLineup()
        {
            CurrentQueueNode = null;
            CustomerLineup.RemoveCustomerFromQueue(this);
            InLine = false;
        }

        public void GoToWaitingArea()
        {
            var waitingPoint = _allocator.GenerateRandomPointOnSurface(_allocator.GetTransformsForSurfaceType(AIPositionSurfaceAreaType.Order));
            MoveToThenLookAt(waitingPoint, PickupArea);
        }

        public void GoToPickupArea()
        {
            MoveTo(PickupArea.transform.position);
        }

        public Order ReceiveOrder()
        {
            var order = OrderObject.GetComponentInChildren<Pickable>();
            //order.OnPickUp(null, null);

            // TODO: use same behaviour as PlayerInteractionManager.PlaceOnAPoint
            var trans = _handSocket.transform;
            OrderObject.transform.SetParent(trans);
            OrderObject.transform.DOLocalMove(Vector3.zero + order.IKHandle.localPosition, 0.1f);
            OrderObject.transform.DOLocalRotateQuaternion(Quaternion.identity, 0.1f);

            ReceivedOrder = true;
            CurrentOrder.OnSuccess?.Invoke();
            SingletonManager.Instance.OrderManager.SellOrder(CurrentOrder, CurrentOrder.Price);

            // Animator settings
            _animator.SetLayerWeight(1, 1f);
            _animator.SetTrigger(Pickup);
            _animator.SetBool(Holding, true);

            // Send Order Feedback to Catalog
            _customerFeedback.LogFeedback(FeedbackProcessor.CreateOrderTemperatureFeedback(order.CurrentRecipe));

            return CurrentOrder;
        }

        public void DropOrder()
        {
            if (!OrderObject)
            {
                return;
            }
            
            _animator.SetBool(Holding, false);
            _animator.SetLayerWeight(1, 0f);
            OrderObject.GetComponent<Pickable>().Drop(unEquip:false);
            OrderObject = null;
        }

        public void SetOrderObject(GameObject order)
        {
            OrderObject = order;
        }

        public void SetWaitingForOrder(bool on)
        {
            WaitingForOrder = on;
        }

        public void Sit(bool sit)
        {
            _aiPath.enableRotation = !sit;
            _aiPath.canMove = !sit;
            _rb.isKinematic = sit;
            _animator.SetBool(Sitting, sit);
        }

        /// <summary>
        /// Gets wait time based on the NPC current traits and some randomness and adds it to a base time given by the state calling the method.
        /// </summary>
        /// <param name="baseWaitTime">A base time given by the state calling the method.</param>
        /// <returns></returns>
        public float GetWaitTime(float baseWaitTime)
        {
            return baseWaitTime + _patienceConstant - _impatienceConstant + Random.Range(_randomWaitTimeInterval.start, _randomWaitTimeInterval.end) + SingletonManager.Instance.CafeManager.GetExternalTimeModifiers();
        }

        /// <summary>
        /// Choose a drink and drink type to order without actually ordering it
        /// </summary>
        public void ChooseDrinkToOrder()
        {
            var orderType = _allocator.CustomerConfigData.ChooseDrinkConfig.Draw();
            DesiredDrinkToOrder = SingletonManager.Instance.OrderManager.GenerateRandomRecipeForOrder(orderType);
            
            DesiredOrderType = _currentExpectations.Any(o => o.ExpectationConfig.Type == ExpectationType.Seat) ? OrderType.Stay : 
                SingletonManager.Instance.OrderManager.GenerateRandomCupType();

            DesiredTastingNote = _currentExpectations.Any(o => o.ExpectationConfig.Type == ExpectationType.SpecificFlavour) ? 
                _allocator.CustomerConfigData.TastingNotesToChooseFromConfig.DrawRandomTastingNote() : TastingDescriptor.OPENED;
         
            DesiredDrinkString = "I want: " + DesiredDrinkToOrder.RecipeName +
                  " " + DesiredOrderType + (DesiredTastingNote == TastingDescriptor.OPENED ? " " + DesiredTastingNote : "");
        }

        public IEnumerator Think(float thinkTime)
        {
            _animator.SetBool(Thinking, true);

            yield return new WaitForSeconds(thinkTime);
            _animator.SetBool(Thinking, false);
        }

        public bool ConsiderOrder()
        {
            // [Base%] + [Attraction] + [MenuSize/Rating - [Picky] + [Curious]
            var baseChance = _allocator.CustomerConfigData.ConsiderVisitConfig.BaseChanceToVisit;
            var menuSize = GameConfig.GetConfigData().Gameplay.RecipesOnMenu.Items.Count;
            var attraction = SingletonManager.Instance.CafeManager.Attractiveness;
            var isPicky = _currentTraits.Contains(TraitType.Picky) ? 10f : 0f;
            var isCurious = _currentTraits.Contains(TraitType.Curious) ? 10f : 0f;

            var chance = baseChance + attraction + menuSize - isPicky + isCurious;

            return Random.Range(0, 100f) <= chance;
        }

        private void AbandonWaiting()
        {
            WaitingForOrder = false;
            CurrentOrder.OnFailure -= AbandonWaiting;
        }

        public void SetImpatient(bool isImpatient)
        {
            _animator.SetBool(Impatient, isImpatient);
        }
        #endregion
    }
}

public enum CustomerIntention
{
    WalkBy,
    Visit,
    LookAround
}