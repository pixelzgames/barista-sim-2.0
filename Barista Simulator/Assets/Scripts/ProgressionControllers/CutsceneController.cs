using UnityEngine;

public class CutsceneController : ProgressionControllerBase
{
    private CutscenePlayer _cutscenePlayer;
    
    public CutsceneController(ProgressionEvent progressionEvent) : base(progressionEvent)
    {
        
    }
    
    protected override void ExecuteEvent()
    {
        base.ExecuteEvent();
        
        _cutscenePlayer = Object.Instantiate(_progressionEvent._cutsceneConfig.CutscenePlayer);
        _cutscenePlayer.CutsceneEnded = Finish;
    }
}
