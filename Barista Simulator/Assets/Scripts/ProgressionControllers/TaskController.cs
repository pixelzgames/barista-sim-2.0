public class TaskController : ProgressionControllerBase
{
    public TaskController(ProgressionEvent progressionEvent) : base(progressionEvent) { }
    
    protected override void ExecuteEvent()
    {
        base.ExecuteEvent();
        
        SingletonManager.Instance.TaskManager.AddTask(_progressionEvent.TaskConfig.TaskData);
        Finish();
    }
}
