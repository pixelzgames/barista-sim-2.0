public class BeanChatController : ProgressionControllerBase
{
    public BeanChatController(ProgressionEvent progressionEvent) : base(progressionEvent)
    {
        
    }
    
    protected override void ExecuteEvent()
    {
        base.ExecuteEvent();
        
        SingletonManager.Instance.BeanOSManager.CreateNewMessage(_progressionEvent.TextMessage.MessagesData);
        Finish();
    }
}
