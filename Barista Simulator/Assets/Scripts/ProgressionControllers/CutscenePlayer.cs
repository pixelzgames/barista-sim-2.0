using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CutscenePlayer : MonoBehaviour
{
    public Action CutsceneEnded;
    
    [SerializeField] private PlayableDirector _playableDirector;

    private void Start()
    {
        if (SingletonManager.Instance.CameraManager.CinemachineBrain == null)
        {
            return;
        }
        
        RebindCameras();
        
        PlayCutscene();
    }
    
    private void PlayCutscene()
    {
        SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.performed += SkipCutscene;
        _playableDirector.Play();
        SingletonManager.Instance.GameManager.ToggleCinematicMode(true);
        _playableDirector.stopped += TimelineEnded;
    }

    private void RebindCameras()
    {
        var timeline = _playableDirector.playableAsset as TimelineAsset;
        foreach (var track in timeline.GetOutputTracks())
        {
            if (track.name.Contains("Cinemachine"))
            {
                _playableDirector.SetGenericBinding(track, SingletonManager.Instance.CameraManager.CinemachineBrain);
            }
        }
    }

    private void TimelineEnded(PlayableDirector obj)
    {
        CutsceneEnded?.Invoke();
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.CutscenePlayed));
        SingletonManager.Instance.GameManager.ToggleCinematicMode(false);
        SingletonManager.Instance.InputManager.InputMaster.Cinematic.Skip.performed -= SkipCutscene;
    }

    private void SkipCutscene(InputAction.CallbackContext callbackContext)
    {
        _playableDirector.Stop();
    }
}
