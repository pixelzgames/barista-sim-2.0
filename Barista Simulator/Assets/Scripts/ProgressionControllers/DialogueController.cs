public class DialogueController : ProgressionControllerBase
{
    private DialogueCanvasController _dialogue;
    public DialogueController(ProgressionEvent progressionEvent) : base(progressionEvent)
    {
        
    }
    
    protected override void ExecuteEvent()
    {
        base.ExecuteEvent();

        SingletonManager.Instance.DialogueManager.AddDialogueToDictionary(_progressionEvent._dialogueConfig);

        if (_progressionEvent._dialogueConfig.Instant)
        {
            SingletonManager.Instance.DialogueManager.RequestDialogueSequence(_progressionEvent._dialogueConfig.Contact.ContactData);
        }

        Finish();
    }
}
