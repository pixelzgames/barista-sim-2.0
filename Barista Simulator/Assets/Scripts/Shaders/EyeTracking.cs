using UnityEngine;

public class EyeTracking : MonoBehaviour
{
    private static readonly int PupilOffset = Shader.PropertyToID("_PupilOffset");

    [Header("Material for eye Tracking")]
    [SerializeField]
    private Renderer  _EyeRend;
    [SerializeField] private float _eyeMaxOffset = 0.3f;
    
    private Material _eyeMat;
    private Vector2 _eyeOffset;
    private Vector2 _eyelidOffset;

    private void Start()
    {
        _eyeMat = _EyeRend.materials[1];
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceDataChanged += OnAppearanceChange;
    }

    private void OnDisable()
    {
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceDataChanged -= OnAppearanceChange;
    }

    private void OnAppearanceChange(Appearance obj)
    {
        _eyeMat = _EyeRend.materials[1];
    }

    // Update is called once per frame
    private void Update()
    {
        var localPosition = transform.localPosition;
        _eyeOffset = new Vector2(localPosition.y, localPosition.x);
        
        // clamp so the eye doesnt disappear  
        if (_eyeOffset.x < -_eyeMaxOffset || _eyeOffset.x > _eyeMaxOffset)
        {
            _eyeOffset.x = Mathf.Clamp(_eyeOffset.x, -_eyeMaxOffset, _eyeMaxOffset);
        }

        if (_eyeOffset.y < -_eyeMaxOffset || _eyeOffset.y > _eyeMaxOffset)
        {
            _eyeOffset.y = Mathf.Clamp(_eyeOffset.y, -_eyeMaxOffset, _eyeMaxOffset);
        }

        // send offset to shader
        _eyeMat.SetVector(PupilOffset, _eyeOffset);
    }
}
