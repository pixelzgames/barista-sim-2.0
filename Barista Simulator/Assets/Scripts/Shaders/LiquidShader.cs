using UnityEngine;

public class LiquidShader : MonoBehaviour
{
    private Renderer _rend;
    private static readonly int ObjectBottomPos = Shader.PropertyToID("_objectMinPos");
    private static readonly int Height = Shader.PropertyToID("_height");
    
    private void Start()
    {
        _rend = GetComponent<Renderer>();
    }
    
    private void Update()
    {
        var bounds = _rend.bounds;
        _rend.material.SetVector(ObjectBottomPos, new Vector3 (bounds.center.x, bounds.center.y - bounds.extents.y, bounds.center.z));
        _rend.material.SetFloat(Height, bounds.size.y);
    }
}
