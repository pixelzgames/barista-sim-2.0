using System;

[Serializable]
public class GameEvent
{
    public GameEventKey Key;
    public int Value;
    public string ID;
    
    public GameEvent(GameEventKey key, int value = 1, string id = "")
    {
        Key = key;
        Value = value;
        ID = id;
    }
}
