using UnityEngine;

public class UnlockableItemManager : PersistentBaseData<UnlockableItemData>
{
    [SerializeField] 
    private AppearanceCatalog _catalog;

    public UnlockableItem GetUnlockable(string id)
    {
        Data.UnlockableItems.TryGetValue(id, out var unlockable);
        return unlockable;
    }
    
    public void UpdateUnlockable(string id, bool purchased, bool visible, bool isNew)
    {
        Data.UnlockableItems.TryGetValue(id, out var unlockable);
        if (unlockable == null)
        {
            return;
        }

        unlockable.Purchased = purchased;
        unlockable.Visible = visible;
        unlockable.New = isNew;
    }
    
    protected override void LoadData(PlayerData data)
    {
        if (data.UnlockableItemData.UnlockableItems == null)
        {
            data.UnlockableItemData = new UnlockableItemData();
            Data = data.UnlockableItemData;
        }
        else
        {
            Data = data.UnlockableItemData;
        }
       
        InitializeUnlockableSaveData();
    }
    
    private void InitializeUnlockableSaveData()
    {
        foreach (var catalog in _catalog.CustomizableCatalogs)
        {
            foreach (var customizable in catalog.Customizables)
            {
                Data.UnlockableItems.TryGetValue(customizable.Id, out var item);

                if (item == null)
                {
                    Data.UnlockableItems.TryAdd(customizable.Id, new UnlockableItem(customizable.Id, customizable.InitiallyPurchased, customizable.InitiallyVisible));
                }

                foreach (var objectTheme in customizable.AvailableThemes)
                {
                    Data.UnlockableItems.TryGetValue(objectTheme.Id, out var objectThemeItem);

                    if (objectThemeItem == null)
                    {
                        Data.UnlockableItems.TryAdd(objectTheme.Id, new UnlockableItem(objectTheme.Id, true, true));
                    }
                }
            }
        }
    }
}
