using System;
using System.Linq;
using AI;
using UnityEngine;
using GD.MinMaxSlider;
using Object = UnityEngine.Object;

[Serializable]
[CreateAssetMenu(menuName = "BRAG/AI/Expectation Data", fileName = "NewExpectationData")]
public class ExpectationConfig : ScriptableObject
{
    [field: SerializeField]
    public ExpectationType Type { get; private set; }
    [field: SerializeField]
    public TraitType[] RequiredTraitsForExpectation { get; private set; }
    
    [Header("Expectations algorithm config")]
    [MinMaxSlider(0, 100)] 
    public Vector2Int MinMaxExpectationThreshold = new Vector2Int(40, 60);

    [field: SerializeField, Range(0,100)] 
    public float StartingValue { get; private set; }
    
    [field: SerializeField]
    public ExpectationThresholdModifier[] TraitsInfluencers { get; private set; }
}

[Serializable]
public class ExpectationThresholdModifier
{
    [field: SerializeField]
    public TraitType Trait { get; private set; }
    
    [field: SerializeField, Range(-30,30)]
    public int ValueToInfluenceTheThresholdBy { get; private set; }
}

[Serializable]
public class Expectation
{
    public ExpectationConfig ExpectationConfig { get; private set; }
    public float CurrentValue { get; private set; }
    public ExpectationState ExpectationState { get; private set; }

    public bool IsANeed;
    
    // Constructor copying the data so it does not reference it.
    public Expectation (ExpectationConfig config)
    {
        ExpectationConfig = Object.Instantiate(config);
        CurrentValue = ExpectationConfig.StartingValue;
    }
    
    /// <summary>
    /// Influence the default config threshold with traits (influencers)
    /// </summary>
    /// <param name="traits">List of traits the AI has</param>
    public void InfluenceThresholdWithTraits(TraitType[] traits)
    {
        foreach (var traitInfluencer in ExpectationConfig.TraitsInfluencers)
        {
            if (!traits.Contains(traitInfluencer.Trait))
            {
                continue;
            }

            ExpectationConfig.MinMaxExpectationThreshold.x += traitInfluencer.ValueToInfluenceTheThresholdBy;
            ExpectationConfig.MinMaxExpectationThreshold.y += traitInfluencer.ValueToInfluenceTheThresholdBy;
        }
    }

    /// <summary>
    /// Update the Expectation State
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public void UpdateAndCalculateSatisfaction(NPCCustomer npc)
    {
        // Update the Current Value
        switch (ExpectationConfig.Type)
        {
            case ExpectationType.Cleanliness:
                CurrentValue = SingletonManager.Instance.CafeManager.NormalizedCleanliness() * 100f;
                break;
            case ExpectationType.Decor:
                CurrentValue = SingletonManager.Instance.CafeManager.NormalizedComfort() * 100f;
                break;
            case ExpectationType.Music:
                CurrentValue = SingletonManager.Instance.CafeManager.Music ? 100f: 0f;
                break;
            case ExpectationType.QualityIngredient:
                // TODO
                break;
            case ExpectationType.QualityRecipe:
                // TODO 
                break;
            case ExpectationType.SpecificCoffee: 
                CurrentValue = npc.DesiredDrinkToOrder == npc.CurrentOrder.Recipe ? 100f : 0f;
                break;
            case ExpectationType.SpecificFlavour:
                // TODO
                break;
            case ExpectationType.Wifi:
                CurrentValue = SingletonManager.Instance.CafeManager.Wifi ? 100f: 0f;
                break;
            case ExpectationType.Seat:
                // TODO 
                break;
            case ExpectationType.Eat:
                // TODO
                break;
            case ExpectationType.Decaf:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        // Update the current State based on its value and threshold 
        if (CurrentValue < ExpectationConfig.MinMaxExpectationThreshold.x)
        {
            ExpectationState = ExpectationState.Disappointed;
        }
        else if (CurrentValue >= ExpectationConfig.MinMaxExpectationThreshold.x &&
                 CurrentValue <= ExpectationConfig.MinMaxExpectationThreshold.y)
        {
            ExpectationState = ExpectationState.Satisfied;
        }
        else
        {
            ExpectationState = ExpectationState.Exceeded;
        }

        if (IsANeed && ExpectationState == ExpectationState.Disappointed)
        {
            // TODO Leave the place
        }
    }
}

public enum ExpectationState
{
    Disappointed,
    Satisfied,
    Exceeded
}

public enum ExpectationType
{
    Cleanliness,
    Decor,
    Music,
    QualityIngredient,
    QualityRecipe,
    SpecificCoffee,
    SpecificFlavour,
    Wifi,
    Seat,
    Eat,
    Decaf
}