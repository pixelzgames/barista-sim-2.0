using UnityEngine;
// ReSharper disable InconsistentNaming

[CreateAssetMenu(fileName = "NewActionInput", menuName = "BRAG/Action Input")]
public class ActionInputConfig : ScriptableObject
{
    [Tooltip("Name to display on the UI for the player")]
    public string ActionDisplayName;
    [Tooltip("Button number referring to the InputMaster, i.e; 1 = E")]
    public  ActionButton Button;
    [Tooltip("If HOLD, how long should we hold for?")]
    public float HoldTime;
    [Tooltip("Is this a category empty action for duplicates"), HideInInspector]
    public bool IsDuplicateCategory;
    public string Category;

    [field: SerializeField] public InteractionMode InteractionMode { get; private set; } = InteractionMode.Barista;
    public InteractableAnimationConfig AnimationConfig;

    public enum ActionButton
    {
        MainAction = 1,
        SecondaryAction = 2,
        ThirdAction = 3,
        FourthAction = 4,
    }
}
