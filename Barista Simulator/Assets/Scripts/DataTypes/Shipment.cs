public class Shipment
{
    public Product ProductConfig { get; private set; }
    public int Days;

    public Shipment(Product productConfig, int days)
    {
        ProductConfig = productConfig;
        Days = days;
    }
}
