using System;
using System.Collections.Generic;

[Serializable]
public class UnlockableItemData
{
    public Dictionary<string, UnlockableItem> UnlockableItems;

    public UnlockableItemData()
    {
        UnlockableItems = new Dictionary<string, UnlockableItem>();
    }
}

public class UnlockableItem
{
    public string Id;
    public bool Purchased;
    public bool Visible;
    public bool New;

    public UnlockableItem()
    {
        Id = "INVALID";
    }

    public UnlockableItem(string id, bool purchased = false, bool visible = false)
    {
        Id = id;
        Purchased = purchased;
        Visible = visible;
    }
}
