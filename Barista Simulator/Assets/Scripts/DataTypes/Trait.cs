public enum TraitType
{
    Impatient,
    Patient,
    Polite,
    Clean,
    Impolite,
    Generous,
    Hungry,
    Picky,
    Snob,
    Artsy,
    Connoisseur,
    Curious
}