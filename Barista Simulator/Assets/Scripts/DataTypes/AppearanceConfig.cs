﻿using UnityEngine;
// ReSharper disable InconsistentNaming

[System.Serializable]
[CreateAssetMenu(fileName = "NewAppearanceData", menuName = "BRAG/New Appearance Data")]
public class AppearanceConfig : ScriptableObject
{
    public Appearance Appearance; 
}
