using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/CustomizableCatalog", fileName = "NewCustomizableCatalog")]
public class CustomizableCatalog : ScriptableObject
{
    public string CatalogName;
    public Sprite Icon;
    public List<Customizable> Customizables;
    public bool FaceCam;
    public bool CanBeSetToNone;
}
