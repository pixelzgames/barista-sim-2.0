using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Pants", fileName = "NewCustomizablePants")]
public class Pants : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Pants;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetPants(index);
    }
}