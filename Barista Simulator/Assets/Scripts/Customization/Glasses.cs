using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Glasses", fileName = "NewCustomizableGlasses")]
public class Glasses : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Glasses;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetGlasses(index);
    }
}