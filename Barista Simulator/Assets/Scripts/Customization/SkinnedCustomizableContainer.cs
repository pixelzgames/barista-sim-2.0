using System.Collections.Generic;
using UnityEngine;

public class SkinnedCustomizableContainer : MonoBehaviour
{
    [Tooltip("Coincides with order in Customizable scriptable object")]
    [field:SerializeField] public List<CustomizationReceiver> CustomizationReceivers { get; private set; }
}
