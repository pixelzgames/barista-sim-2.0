using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Shirt", fileName = "NewCustomizableShirt")]
public class Shirt : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Shirt;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetShirt(index);
    }
}
