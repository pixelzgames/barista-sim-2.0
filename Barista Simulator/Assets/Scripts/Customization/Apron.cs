using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Apron", fileName = "NewCustomizableApron")]
public class Apron : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Apron;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetApron(index);
    }
}
