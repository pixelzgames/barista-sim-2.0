using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class NPCAppearanceController : CharacterAppearanceController
{
    [SerializeField] private AppearanceCatalog _appearanceCatalog;
    [FormerlySerializedAs("_appearanceData")] [SerializeField] private AppearanceConfig _appearanceConfig;

    private void OnEnable()
    {
        ApplyAppearance(_appearanceConfig ? _appearanceConfig.Appearance : _appearanceCatalog.GenerateRandomAppearance());
    }

    protected override void SetLayer(GameObject go)
    {
        foreach (var childTransform in go.transform.GetComponentsInChildren<Transform>())
        {
            childTransform.gameObject.layer = LayerMask.NameToLayer("NPC");
        }
    }

    protected override List<CustomizableCatalog> GetCustomizableCatalogs()
    {
        return _appearanceCatalog.CustomizableCatalogs;
    }
}
