using System.Collections.Generic;
using UnityEngine;

public class UtilityAppearanceController : CharacterAppearanceController
{
    [SerializeField] private AppearanceCatalog _appearanceCatalog;
    
    protected override List<CustomizableCatalog> GetCustomizableCatalogs()
    {
        return _appearanceCatalog.CustomizableCatalogs;
    }

    public Appearance RandomizeAppearance()
    {
        var appearance = _appearanceCatalog.GenerateRandomAppearance();
        ApplyAppearance(appearance);
        return appearance;
    }

    public void ApplyAppearanceFromExternal(Appearance appearance)
    {
        ApplyAppearance(appearance);
    }
}