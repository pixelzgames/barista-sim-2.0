public class PlayerAppearanceController : CharacterAppearanceController
{
    private void Start()
    {
        // Apply saved appearance to character;
        ApplyAppearance(SingletonManager.Instance.CharacterAppearanceManager.Data);
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceChangeRequested += ApplyAppearance;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceChangeRequested -= ApplyAppearance;
    }

    protected override void ApplyAppearance(Appearance appearance)
    {
        base.ApplyAppearance(appearance);
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceDataChanged.Invoke(appearance);
    }
}
