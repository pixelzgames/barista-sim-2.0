using System.Collections.Generic;
using UnityEngine;

public class CharacterAppearanceController : MonoBehaviour
{
    [Header("Eyes")] [SerializeField] private CustomizationReceiver _eyes;

    [Header("Hat")] [SerializeField] private CustomizationReceiver _hat;

    [Header("Facial Hair")] [SerializeField]
    private CustomizationReceiver _facialHair;

    [Header("Glasses")] [SerializeField] private CustomizationReceiver _glasses;

    [Header("Apron")] [SerializeField] private CustomizationReceiver _apron;

    [Header("Shirt")] [SerializeField] private SkinnedCustomizableContainer _shirts;

    [Header("Pants")] [SerializeField] private SkinnedCustomizableContainer _pants;

    [Header("Skin")] [SerializeField] private CustomizationReceiver _skin; 
    
    [Header("Shoes")] [SerializeField] private SkinnedCustomizableContainer _shoes;

    protected virtual List<CustomizableCatalog> GetCustomizableCatalogs()
    {
        return SingletonManager.Instance.CharacterAppearanceManager.GetCustomizableCatalogs();
    }

    protected virtual void SetLayer(GameObject go)
    {
        foreach (var trans in go.transform.GetComponentsInChildren<Transform>())
        {
            trans.gameObject.layer = LayerMask.NameToLayer("Player");
        }
    }

    private void SetGlasses(CustomizationReceiver glassesPrefab)
    {
        if (!glassesPrefab)
        {
            _glasses.gameObject.SetActive(false);
            return;
        }

     
        _glasses = SwapPrefab(glassesPrefab, _glasses.gameObject);
        var glassesGameObject = _glasses.gameObject;
        glassesGameObject.SetActive(true);
        SetLayer(glassesGameObject);
    }

    private void SetShirt(int index)
    {
        for (var i = 0; i < _shirts.CustomizationReceivers.Count; i++)
        {
            if (_shirts.CustomizationReceivers[i] == null)
            {
                continue;
            }
            
            _shirts.CustomizationReceivers[i].gameObject.SetActive(i == index);
            SetLayer(_shirts.CustomizationReceivers[i].gameObject);
        }
    }
    
    private void SetShoes(int index)
    {
        for (var i = 0; i < _shoes.CustomizationReceivers.Count; i++)
        {
            if (_shoes.CustomizationReceivers[i] == null)
            {
                continue;
            }

            foreach (var showRenderer in _shoes.CustomizationReceivers[i].Renderers)
            {
                var showGO = showRenderer.gameObject;
                showGO.SetActive(i == index);
                SetLayer(showGO);
            }
        }
    }

    private void SetPants(int index)
    {
        for (var i = 0; i < _pants.CustomizationReceivers.Count; i++)
        {
            if (_pants.CustomizationReceivers[i] == null)
            {
                continue;
            }

            foreach (var pantsRenderer in _pants.CustomizationReceivers[i].Renderers)
            {
                var pantsGO = pantsRenderer.gameObject;
                pantsGO.SetActive(i == index);
                SetLayer(pantsGO);
            }
        }
    }

    private void SetApron(CustomizationReceiver apronPrefab)
    {
        var apronGameObject = _apron.gameObject;
        apronGameObject.SetActive(apronPrefab);
        SetLayer(apronGameObject);
    }

    private void SetFacialHair(CustomizationReceiver facialHairPrefab)
    {
        if (!facialHairPrefab)
        {
            _facialHair.gameObject.SetActive(false);
            return;
        }
        
        _facialHair = SwapPrefab(facialHairPrefab, _facialHair.gameObject);
        var facialHairGO = _facialHair.gameObject;
        facialHairGO.SetActive(true);
        SetLayer(facialHairGO);
    }

    private void SetHat(CustomizationReceiver hatPrefab)
    {
        if (!hatPrefab)
        {
            _hat.gameObject.SetActive(false);
            return;
        }

        _hat = SwapPrefab(hatPrefab, _hat.gameObject);
        var hatGO = _hat.gameObject;
        hatGO.SetActive(true);
        SetLayer(hatGO);
    }

    private CustomizationReceiver SwapPrefab(CustomizationReceiver newPrefab, GameObject oldPrefab)
    {
        var newObject = Instantiate(newPrefab, oldPrefab.transform.parent);

        foreach (Transform child in newObject.transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Player");
        }

        newObject.gameObject.layer = LayerMask.NameToLayer("Player");
        Destroy(oldPrefab);
        return newObject;
    }

    private void SetCustomizableSkin(List<Renderer> renderers, List<MaterialList> mats)
    {
        for (var i = 0; i < renderers.Count; i++)
        {
            var materialsToApply = new Material[renderers[i].materials.Length];
            
            for (var j = 0; j < renderers[i].materials.Length; j++)
            {
                if (mats[i].Materials[j] != null)
                {
                    materialsToApply[j] = mats[i].Materials[j];
                }
                else
                {
                    materialsToApply[j] = renderers[i].materials[j];
                }
            }
            renderers[i].materials = materialsToApply;
        }
    }

    protected virtual void ApplyAppearance(Appearance appearance)
    {
        var catalogues = GetCustomizableCatalogs();

        // Glasses
        if (appearance.GlassesStyle < catalogues[0].Customizables.Count && appearance.GlassesStyle >= 0)
        {
            var glasses = catalogues[0].Customizables[appearance.GlassesStyle];
            SetGlasses(glasses.CustomizationReceiver);
        
            if (glasses.AvailableThemes.Count > 0 && appearance.GlassesColor < glasses.AvailableThemes.Count && appearance.GlassesColor >= 0)
            {
                SetCustomizableSkin(_glasses.Renderers, glasses.AvailableThemes[appearance.GlassesColor].MaterialsPerRenderer);
            }
        }

        // Eyes
        if (appearance.EyeStyle < catalogues[1].Customizables.Count && appearance.EyeStyle >= 0)
        {
            var eyes = catalogues[1].Customizables[appearance.EyeStyle];

            if (eyes.AvailableThemes.Count > 0 && appearance.EyeColor < eyes.AvailableThemes.Count && appearance.EyeColor >= 0)
            {
                SetCustomizableSkin(_eyes.Renderers, eyes.AvailableThemes[appearance.EyeColor].MaterialsPerRenderer);
            }
        }
        
        // Facial Hair
        if (appearance.FacialHairStyle < catalogues[2].Customizables.Count && appearance.FacialHairStyle >= 0)
        {
            var facialHair = catalogues[2].Customizables[appearance.FacialHairStyle];
            SetFacialHair(facialHair.CustomizationReceiver);
        
            if (facialHair.AvailableThemes.Count > 0 && appearance.FacialHairColor < facialHair.AvailableThemes.Count && appearance.FacialHairColor >= 0)
            {
                SetCustomizableSkin(_facialHair.Renderers,
                    facialHair.AvailableThemes[appearance.FacialHairColor].MaterialsPerRenderer);
            }
        }

        // Apron
        if (appearance.ApronStyle < catalogues[3].Customizables.Count && appearance.ApronStyle >= 0)
        {
            var apron = catalogues[3].Customizables[appearance.ApronStyle];
            SetApron(apron.CustomizationReceiver);
        
            if (apron.AvailableThemes.Count > 0 && appearance.ApronColor < apron.AvailableThemes.Count && appearance.ApronColor >= 0)
            {
                SetCustomizableSkin(_apron.Renderers, apron.AvailableThemes[appearance.ApronColor].MaterialsPerRenderer);
            }
        }

        // Shirt
        if (appearance.ShirtStyle < catalogues[4].Customizables.Count && appearance.ShirtStyle >= 0)
        {
            var shirt = catalogues[4].Customizables[appearance.ShirtStyle];
            SetShirt(appearance.ShirtStyle);
        
            if (shirt.AvailableThemes.Count > 0 && appearance.ShirtColor < shirt.AvailableThemes.Count && appearance.ShirtColor >= 0 && _shirts.CustomizationReceivers[appearance.ShirtStyle] != null)
            {
                SetCustomizableSkin(_shirts.CustomizationReceivers[appearance.ShirtStyle].Renderers, shirt.AvailableThemes[appearance.ShirtColor].MaterialsPerRenderer);
            }
        }

        // Pants
        if (appearance.PantsStyle < catalogues[5].Customizables.Count && appearance.PantsStyle >= 0)
        {
            var pants = catalogues[5].Customizables[appearance.PantsStyle];
            SetPants(appearance.PantsStyle);
        
            if (pants.AvailableThemes.Count > 0 && appearance.PantsColor < pants.AvailableThemes.Count && appearance.PantsColor >= 0 && _pants.CustomizationReceivers[appearance.PantsStyle] != null)
            {
                SetCustomizableSkin(_pants.CustomizationReceivers[appearance.PantsStyle].Renderers, pants.AvailableThemes[appearance.PantsColor].MaterialsPerRenderer);
            }
        }

        // Hat/Hair
        if (appearance.HatStyle < catalogues[6].Customizables.Count && appearance.HatStyle >= 0)
        {
            var headwear = catalogues[6].Customizables[appearance.HatStyle];
            SetHat(headwear.CustomizationReceiver);

            if (headwear.AvailableThemes.Count > 0 && appearance.HatColor < headwear.AvailableThemes.Count && appearance.HatColor >= 0)
            {
                SetCustomizableSkin(_hat.Renderers, headwear.AvailableThemes[appearance.HatColor].MaterialsPerRenderer);
            }
        }


        // Skin
        if (appearance.SkinStyle < catalogues[7].Customizables.Count && appearance.SkinStyle >= 0)
        {
            Customizable skin = catalogues[7].Customizables[appearance.SkinStyle];
            if (skin.AvailableThemes.Count > 0 && appearance.SkinColor < skin.AvailableThemes.Count && appearance.SkinColor >= 0)
            {
                SetCustomizableSkin(_skin.Renderers, skin.AvailableThemes[appearance.SkinColor].MaterialsPerRenderer);
            }
        }
        
        // Shoes
        if (appearance.ShoeStyle < catalogues[8].Customizables.Count && appearance.ShoeStyle >= 0)
        {
            var shoes = catalogues[8].Customizables[appearance.ShoeStyle];
            SetShoes(appearance.ShoeStyle);
        
            if (shoes.AvailableThemes.Count > 0 && appearance.ShoeColor < shoes.AvailableThemes.Count && appearance.ShoeColor >= 0 && _shoes.CustomizationReceivers[appearance.ShoeStyle] != null)
            {
                SetCustomizableSkin(_shoes.CustomizationReceivers[appearance.ShoeStyle].Renderers, shoes.AvailableThemes[appearance.ShoeColor].MaterialsPerRenderer);
            }
        }
    }
}

