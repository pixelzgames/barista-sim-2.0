using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Shoes", fileName = "NewCustomizableShoes")]
public class Shoes : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Shoes;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetShoes(index);
    }
}
