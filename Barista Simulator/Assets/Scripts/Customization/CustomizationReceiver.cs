using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that lives on gameobjects that are customizable 
/// </summary>
public class CustomizationReceiver : MonoBehaviour
{
    [field:SerializeField] public List<Renderer> Renderers { get; private set; }
}
