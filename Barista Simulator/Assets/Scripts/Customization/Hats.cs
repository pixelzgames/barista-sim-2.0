using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Hat", fileName = "NewCustomizableHat")]
public class Hats : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Hat;
    }

    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetHat(index);
    }
}
