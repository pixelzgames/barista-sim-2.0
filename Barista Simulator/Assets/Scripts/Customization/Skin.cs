using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Skin", fileName = "NewCustomizableSkin")]
public class Skin : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Skin;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetSkin(index);
    }
}
