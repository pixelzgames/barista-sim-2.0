using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/Eyes", fileName = "NewCustomizableEyes")]
public class Eyes : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.Eyes;
    }

    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetEyes(index);
    }
}
