using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable/FacialHair", fileName = "NewCustomizableFacialHair")]
public class FacialHair : Customizable
{
    private void OnEnable()
    {
        CustomizableType = CustomizableType.FacialHair;
    }
    
    public override void SetAppearance(int index)
    {
        SingletonManager.Instance.CharacterAppearanceManager.SetFacialHair(CustomizationReceiver, index);
    }
}
