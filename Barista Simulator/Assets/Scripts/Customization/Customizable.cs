using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/Customizable", fileName = "NewCustomizable")]
public class Customizable : ScriptableObject
{
    [field:SerializeField] public CustomizationReceiver CustomizationReceiver { get; private set; }
    [field:SerializeField] public Sprite Icon { get; private set; }
    [field:SerializeField] public string ItemName { get; private set; }
    [field:SerializeField] public int Price { get; private set; }
    [field:SerializeField] public List<ObjectTheme> AvailableThemes { get; private set; }
    [field:SerializeField] public CustomizableType CustomizableType { get; protected set; }
    [field:SerializeField] public bool InitiallyPurchased { get; protected set; }
    [field:SerializeField] public bool InitiallyVisible { get; protected set; }
    
    public string Id => CustomizableType + "_" + name;
    
    /// <summary>
    /// Makes call to CharacterAppearanceManager to change the customizable on the character.
    /// </summary>
    public virtual void SetAppearance(int index) { }
}

public enum CustomizableType
{
    Glasses = 0,
    Eyes = 1,
    FacialHair = 2,
    Apron = 3,
    Shirt = 4,
    Pants = 5,
    Hat = 6,
    Skin = 7,
    Shoes = 8
}
