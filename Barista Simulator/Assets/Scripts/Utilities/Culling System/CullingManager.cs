using System.Collections.Generic;
using UnityEngine;

public class CullingManager : MonoBehaviour
{
    [SerializeField] private float _occlusionCapsuleHeight;
    [SerializeField] private float _occlusionCapsuleRadius = 1f;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private Transform _cameraTransform;
    [SerializeField] private float _viewAngle;
    [SerializeField] private bool _drawDebug;

    // list of objects that will trigger the culling effect
    private GameObject _player;

    // List of all the objects that we've set to occluding state
    private List<Cullable> _occludingObjects = new();

    private bool _cullingEnabled = true;

    private void Start()
    {
        SingletonManager.Instance.GameManager.SpawnedCharacter += spawnedCharacter =>
        {
            _player = spawnedCharacter;
        };
    }

    public void Update()
    {
        if (_player == null)
        {
            return;
        }

        if (!_cullingEnabled && _occludingObjects.Count == 0)
        {
            return;
        }

        SetOccludingObjects(_cullingEnabled ? FindOccludingObjects(_player.transform.position) : new List<Cullable>());
    }

    // Update the stored list of occluding objects
    private void SetOccludingObjects(List<Cullable> newList)
    {
        foreach (var cullable in newList)
        {
            var foundIndex = _occludingObjects.IndexOf(cullable);

            if (foundIndex < 0 && cullable)
            {
                // This object isnt in the old list, so we need to mark it as occluding
                cullable.Occluding = true;
            }
            else
            {
                // This object was already in the list, so remove it from the old list
                _occludingObjects.RemoveAt(foundIndex);
            }
        }

        // Any object left in the old list, was not in the new list, so it's no longer occludding
        foreach (var cullable in _occludingObjects)
        {
            cullable.Occluding = false;
        }

        _occludingObjects = newList;
    }

    private List<Cullable> FindOccludingObjects(Vector3 importantPos)
    {
        var occludingObjects = new List<Cullable>();
        var cameraMain = Camera.main;
        
        if (!cameraMain)
        {
            Debug.LogError("Error: No main Camera found for the culling system");
            return null;
        }

        importantPos.y += _occlusionCapsuleHeight;

        var colliders = new Collider[30];
        var collidersCount = Physics.OverlapCapsuleNonAlloc(importantPos,cameraMain.transform.position, _occlusionCapsuleRadius, colliders, _layerMask, QueryTriggerInteraction.Ignore);

        // Add cull-able objects we found to the list
        for (var i = 0; i < collidersCount; i++)
        {
            var cullable = colliders[i].GetComponent<Cullable>();
            
            Debug.Assert(cullable != null, "Found an object on the occlusion layer without the occlusion component!");

            if (!occludingObjects.Contains(cullable) && ColliderAngleFromPlayer(colliders[i]) < _viewAngle)
            {
                occludingObjects.Add(cullable);
            }
        }

        return occludingObjects;
    }

    private float ColliderAngleFromPlayer(Collider col)
    {
        var colliderPos = new Vector3(col.bounds.center.x, 0f, col.bounds.center.z);
        var cameraPos = new Vector3(_cameraTransform.position.x, 0f, _cameraTransform.position.z);
        var playerToColVector = _player.transform.position - colliderPos;
        var angle = Vector3.Angle(playerToColVector, _player.transform.position - cameraPos);

        if (_drawDebug)
        {
            Debug.DrawLine(_player.transform.position, colliderPos, angle > _viewAngle ? Color.green : Color.red, Time.deltaTime);
            Debug.DrawLine(cameraPos, _player.transform.position, Color.yellow, Time.deltaTime); 
        }

        return angle;
    }
    
    public void ToggleCulling(bool enableCulling)
    {
        _cullingEnabled = enableCulling;
    }
}