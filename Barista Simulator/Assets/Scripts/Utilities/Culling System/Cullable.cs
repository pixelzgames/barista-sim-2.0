using System.Collections;
using UnityEngine;

public class Cullable : MonoBehaviour
{
    //speed of the transition
    private float m_alphaChangeSpeed = 10f;

    // name of the variable in the shader that will be adjusted
    public string m_shaderVariableName = "_Dither";

    // end point of culling
    [SerializeField] private float m_fadeTo;
    
    // start point of culling
    private float m_fadeFrom = 2f;

    // the float for the transition
    private float m_currentAlpha = 1.0f;

    // The material that will be affected by the transition 
    private Material[] m_mats;
    
    // Set to true if we want to fade the object out because it's in the way
    private bool m_occluding;

    // Set to true when the fade control co-routine is active
    private bool m_inCoroutine;


    public bool Occluding
    {
        get => m_occluding;
        set
        {
            if (m_occluding == value)
            {
                return;
            }
            
            m_occluding = value;
            OnOccludingChanged();
        }
    }

    // Called when the Occluding value is changed
    private void OnOccludingChanged()
    {
        if (m_inCoroutine)
        {
            return;
        }
        
        StartCoroutine(nameof(FadeAlphaRoutine));
        m_inCoroutine = true;
    }
    
    // Start is called before the first frame update
    private void Start()
    {
        // grab the renderer's material and set the current alpha 
        m_mats = GetComponent<Renderer>().materials;
        m_currentAlpha = m_fadeFrom;
    }

    // Return the alpha value we want on all of our models
    private float GetTargetAlpha()
    {
        return m_occluding ? m_fadeTo : m_fadeFrom;
    }

    private IEnumerator FadeAlphaRoutine()
    {
        while (!Mathf.Approximately(m_currentAlpha, GetTargetAlpha()))
        {
            var alphaShift = m_alphaChangeSpeed * Time.deltaTime;

            var targetAlpha = GetTargetAlpha();
            if (m_currentAlpha < targetAlpha)
            {
                m_currentAlpha += alphaShift;
                if (m_currentAlpha > targetAlpha)
                {
                    m_currentAlpha = targetAlpha;
                }
            }
            else
            {
                m_currentAlpha -= alphaShift;
                if (m_currentAlpha < targetAlpha)
                {
                    m_currentAlpha = targetAlpha;
                }
            }

            foreach (var mat in m_mats)
            {
                mat.SetFloat(m_shaderVariableName, m_currentAlpha);
            }
            
            yield return null;
        }
        
        m_inCoroutine = false;
    }

    private void OnDestroy()
    {
        // Setting to true to avoid going through when being destroyed
        m_inCoroutine = true;
        StopAllCoroutines();
    }
}