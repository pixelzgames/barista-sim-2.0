using UnityEngine;

public static class PrefabsUtility
{
    private const string PATH = "Prefabs List/AllPrefabs";
    
    private static PrefabListConfig _prefabListConfig;

    [RuntimeInitializeOnLoadMethod]
    private static void LoadDataBase()
    {
        _prefabListConfig = Resources.Load<PrefabListConfig>(PATH);
        if (_prefabListConfig == null)
        {
            Debug.LogError(
                $"All prefabs data base file were not found in {PATH}." +
                " Make sure to have the list config in that folder.");
        }
    }

    /// <summary>
    /// Get the prefab mapped to the key provided
    /// </summary>
    /// <param name="key">Unique key to find the prefab</param>
    /// <returns>Prefab in the list mapped to the key provided</returns>
    public static BRAGPrefabHandler GetPrefabFromList(byte? key)
    {
        if (key == null)
        {
            Debug.LogWarning($"Trying to get prefab from list with key that is null");
            return null;
        }
        
        if (_prefabListConfig.PrefabData.TryGetValue(key.Value, out var outValue))
        {
            return outValue;
        }
     
        Debug.LogWarning($"No prefab found with key {key}. Make sure it is part of the" +
                         $" {PATH} prefab list.");
        return null;
    }

    public static byte? GetKeyFromPrefab(GameObject interactable)
    {
        foreach (var prefab in _prefabListConfig.PrefabData)
        {
            if (prefab.Value == null)
            {
                Debug.LogError($"No prefab found with key {prefab.Key} in AllPrefabs list.");
                continue;
            }
            
            if (prefab.Value.DataObject.gameObject.name == RemoveAfterParenthesis(interactable.name).Trim())
            {
                return prefab.Key;
            }
        }
        
        Debug.LogError($"Error: Prefab with value {interactable} was not found in {PATH} prefab list.");
        return null;
    }
    
    private static string RemoveAfterParenthesis(string input)
    {
        var index = input.IndexOf('(');
        if (index >= 0)
        {
            return input.Substring(0, index);
        }
        
        return input;
    }
}