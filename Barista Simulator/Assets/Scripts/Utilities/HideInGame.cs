using UnityEngine;

/// <summary>
/// Class utility to hide any GameObject on Play
/// </summary>
public class HideInGame : MonoBehaviour
{
    private void Awake()
    {
        gameObject.SetActive(false);
    }
}
