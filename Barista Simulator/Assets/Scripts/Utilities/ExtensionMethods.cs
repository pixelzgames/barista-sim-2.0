﻿using UnityEngine;

public static class ExtensionMethods
{
    public static float Remap(this float x, float x1, float x2, float y1, float y2)
    {
        var m = (y2 - y1) / (x2 - x1);
        var c = y1 - m * x1; // point of interest: c is also equal to y2 - m * x2, though float math might lead to slightly different results.

        return m * x + c;
    }
    
    public static void SetLayerRecursively(this GameObject obj, int layer) 
    {
        obj.layer = layer;
 
        foreach (Transform child in obj.transform) 
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }
    
    /// <summary>
    /// Get the LookAt quaternion from 2 positions
    /// </summary>
    /// <param name="self">Position of first object</param>
    /// <param name="target">Position ob second object</param>
    /// <returns></returns>
    public static Quaternion GetLookAtRotation(Vector3 self, Vector3 target)
    {
        var direction = (target - self).normalized;
        return Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
    }
    
    public static float NormalizeAngle(float a) => (a + 360f) % 360f;
    
    public static bool IsInRange(this float value, float minValue, float maxValue)
    {
        return value > minValue && value < maxValue;
    }

    public static Vector3 ToVector3(this SerializedVector3 serializedVector3)
    {
        return new Vector3(serializedVector3.X, serializedVector3.Y, serializedVector3.Z);
    }

    public static SerializedVector3 ToSerializedVector3(this Vector3 vector3)
    {
        return new SerializedVector3 {X = vector3.x, Y = vector3.y, Z = vector3.z};
    }
}
