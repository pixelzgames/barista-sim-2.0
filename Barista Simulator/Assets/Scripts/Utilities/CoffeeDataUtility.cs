using System;
using System.Text.RegularExpressions;

internal static class CoffeeDataUtility
{
    public static string GetOriginDisplay(CoffeeData data)
    {
        return "Origin: " + "<b>" + Regex.Replace(data.CoffeeOrigin.Origin.ToString(), "(\\B[A-Z])", " $1") + "</b>" + Environment.NewLine;
    }
    
    public static string GetRoastDisplay(RoastType roast)
    {
        return $"{"Roast level: " + "<b>" + Regex.Replace(roast.ToString(), "(\\B[A-Z])", " $1") + "</b>"}"  + Environment.NewLine;
    }
    
    public static string GetCoffeeDataDisplay(GreenBeans beans)
    {
        var origin = $"{(beans != null ? GetOriginDisplay(beans.CoffeeData) : "")}";
        return origin;
    }

    public static string GetCoffeeDataDisplay(WholeBeans beans)
    {
        var greenData = GetCoffeeDataDisplay((GreenBeans)beans);
        var roastLevel = $"{(beans != null ? GetRoastDisplay(beans.CoffeeData.Roast) : "")}";
        var idealGrindSize = $"Brewing grind size is {beans.CoffeeData.IdealBrewingGrindSize}" + Environment.NewLine;
        var espresso = $"Espresso grind size {beans.CoffeeData.IdealEspressoGrindSize}" + Environment.NewLine;
        return greenData + roastLevel + idealGrindSize + (beans.CoffeeData.IsEspressoDialedIn ? espresso : "");
    }
}
