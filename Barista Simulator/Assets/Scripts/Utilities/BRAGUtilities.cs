using System;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public static class BRAGUtilities
{
#if UNITY_EDITOR
    [MenuItem("BRAG/Edit/Play-Stop, But From PreLaunch Scene %P")]
    public static void PlayFromPreLaunchScene()
    {
        if (EditorApplication.isPlaying)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Boot.unity");
        EditorApplication.isPlaying = true;
    }

    [MenuItem("BRAG/Delete Save File")]
    public static void DeleteSaveFile()
    {
        FileGameDataHandler<PlayerData>.DeleteSaveFile();
    }
#endif
    
    public static void TransferContentTo(SolidContainer from, SolidContainer to)
    {
        if (from == null || to == null || from.IsEmpty())
        {
            return;
        }

        var servingsFrom = from.Content.Servings;
        var maxServingsTo = to.ServingsCapacity - to.Content.Servings;

        if (servingsFrom > maxServingsTo)
        {
            servingsFrom = maxServingsTo;
        }

        if (to.IsEmpty())
        {
            if (from.Content is Grounds fromGrounds && to.Content is Grounds toGrounds)
            {
                toGrounds.SetData(fromGrounds.CoffeeData, fromGrounds.Size);
            }
            if (from.Content is WholeBeans fromBeans && to.Content is WholeBeans toBeans)
            {
                toBeans.SetData(fromBeans.CoffeeData);
            }
            if (from.Content is GreenBeans fromGreenBeans && to.Content is GreenBeans toGreenBeans)
            {
                toGreenBeans.SetData(fromGreenBeans.CoffeeData);
            }
        }
        
        from.RemoveFromContainer(servingsFrom);
        to.AddToContainer(servingsFrom);
    }
    
    public static void TransferContentTo(LiquidContainer from, LiquidContainer to, int serving = -1)
    {
        if (from == null || to == null || from.IsEmpty() || serving == 0)
        {
            return;
        }

        var servingsFrom = serving == -1 ? from.Content.Servings : serving;
        var maxServingsTo = to.ServingsCapacity - to.Content.Servings;

        if (servingsFrom > maxServingsTo)
        {
            servingsFrom = maxServingsTo;
        }

        if (to.IsEmpty())
        {
            if (from.Content is Water fromWater && to.Content is Water toWater)
            {
                toWater.SetData(fromWater.Temperature);
            }
            if (from.Content is Milk fromMilk && to.Content is Milk toMilk)
            {
                toMilk.SetData(fromMilk.Temperature, fromMilk.IsFrothed, fromMilk.FoamType);
            }
        }
        
        from.RemoveFromContainer(servingsFrom);
        to.AddToContainer(servingsFrom);
    }
    
    /// <summary>
    /// Get the joystick axis radial angle
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static float RadialJoystickPosition(float stepLength, Vector2 input)
    {
        var angle = ExtensionMethods.NormalizeAngle(Mathf.Atan2(-input.x, input.y) * Mathf.Rad2Deg + stepLength / 2f);
        if (input.magnitude < 0.1f)
        {
            return -1f;
        }

        return angle;
    }
    
    /// <summary>
    /// Get the mouse position axis radial angle relative to its center
    /// </summary>
    /// <param name="stepLength"></param>
    /// <param name="input"></param>
    /// <param name="relativeTransform"></param>
    /// <returns></returns>
    public static float RadialMousePosition(float stepLength, Vector3 input, Transform relativeTransform)
    {
        if (Mathf.Abs(input.x - relativeTransform.position.x) / Screen.width * 100f < 10f && 
            Mathf.Abs(input.y - relativeTransform.position.y) / Screen.height * 100f < 10f)
        {
            return -1f;
        }
        
        var angle = ExtensionMethods.NormalizeAngle(Vector3.SignedAngle(Vector3.up, input - relativeTransform.position, Vector3.forward) +
                                                    stepLength / 2f);
        return angle;
    }
}

public static class RecipeMaker<T>
{
    public static void MakeRecipe(RecipeType recipeType, Container<T> source, Container<Liquid> destination, float temperature,
        CoffeeData coffeeData, bool badQuality = false, int serving = 1)
    {
        var config = GameConfig.GetConfigData().Gameplay.AllRecipes.GetRecipeConfigByType(recipeType);
        switch (config.RecipeType)
        {
            case RecipeType.Americano:
            case RecipeType.InstantCoffee:
            case RecipeType.BlackCoffee:
            case RecipeType.Espresso:
            case RecipeType.Lungo:
            case RecipeType.Latte:
            case RecipeType.FlatWhite:
            case RecipeType.LongBlack:
            case RecipeType.Cappuccino:
            case RecipeType.Cupping:
                source.RemoveFromContainer(serving);
                destination.Content = new Recipe(config, temperature, serving, coffeeData, isBadQuality: badQuality);
                destination.Content.SetTemperatureGoal(Temperature.ROOM_TEMP, Liquid.DEFAULT_ROOMTEMP_CHANGE_RATE);
                break;
            case RecipeType.DoubleEspresso:
                source.RemoveFromContainer(serving);
                destination.Content = new Recipe(config, temperature, 1, coffeeData, isBadQuality: badQuality);
                destination.Content.SetTemperatureGoal(Temperature.ROOM_TEMP, Liquid.DEFAULT_ROOMTEMP_CHANGE_RATE);
                break;
            case RecipeType.Macchiato:
            case RecipeType.Cortado:
            case RecipeType.Ristretto:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        SingletonManager.Instance.GameNotificationManager.QueueNotification(
            new NotificationData($"Made {config.RecipeName}"));
    }

    public static bool CanMakeInstantCoffeeRecipe(Container<T> source, Container<T> destination)
    {
        if (destination == null || source == null || source.IsEmpty() || !destination.IsEmpty() || destination.Ingredients.Count <= 0)
        {
            return false;
        }

        if ((source.Content is Water waterSource && destination.Ingredients.OfType<InstantGrounds>().Any() &&
             waterSource.Servings > 0) ||
            (source.Ingredients.OfType<InstantGrounds>().Any() && destination.Content is Water waterDestination &&
             waterDestination.Servings > 0))
        {
            return true;
        }
        
        return false;
    }
}