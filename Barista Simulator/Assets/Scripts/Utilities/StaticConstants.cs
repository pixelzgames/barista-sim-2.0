using UnityEngine;

public class StaticConstants : MonoBehaviour
{
    //Just for the file :shrug emoji here:
}

namespace Barista
{
    public static class Colors
    {
        public static Color32 On = new Color32(166,207,84,255);
        public static string OnHex => HexConverter(On);
        public static Color32 Off = new Color32(239,64,57,255);
        public static string OffHex => HexConverter(Off);

        private static string HexConverter(Color32 c)
        {
            return "#" + c.r.ToString("X2") + c.g.ToString("X2") + c.b.ToString("X2");
        }
    }
}

