using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

internal static class CoffeeConfigUtility
{
    private static CoffeeDatabaseConfig _coffeeDatabaseConfig;
    public static CoffeeDescriptorsDatabaseConfig CoffeeDescriptorsDatabaseConfig { get; private set; }

    [RuntimeInitializeOnLoadMethod]
    private static void LoadDataBase()
    {
        _coffeeDatabaseConfig = Resources.Load<CoffeeDatabaseConfig>("AllCoffeesDatabase");
        CoffeeDescriptorsDatabaseConfig = Resources.Load<CoffeeDescriptorsDatabaseConfig>("AllDescriptorsDatabase");
    }

    /// <summary>
    /// Get a random coffee data completely using the scriptable object database.
    /// </summary>
    private static GreenCoffeeConfig GetRandomCoffeeConfig()
    {
        return _coffeeDatabaseConfig.AllCoffees[Random.Range(0, _coffeeDatabaseConfig.AllCoffees.Length)];
    }

    public static CoffeeData ConvertConfigToData(GreenCoffeeConfig config)
    {
        var possibleQualities = GetPossibleQualitiesFromConfig(config);

        return new CoffeeData(config.CoffeeOrigin.CoffeeOrigin, GetRandomEspressoGrindSize(), config.PossibleFlavourNotes, 
            possibleQualities.ToArray(), false);
    }

    private static List<CoffeeQuality> GetPossibleQualitiesFromConfig(GreenCoffeeConfig config)
    {
        List<CoffeeQuality> possibleQualities = new();
        foreach (var qualityConfig in config.PossibleCoffeeQualityConfigs)
        {
            List<CoffeeDescriptorSlot> descriptorSlots = new();
            for (var i = 0; i < qualityConfig.Slots; i++)
            {
                var slot = new CoffeeDescriptorSlot
                {
                    CoffeeDescriptor = new CoffeeDescriptor()
                };
                descriptorSlots.Add(slot);
            }

            var coffeeQuality = new CoffeeQuality
            {
                Quality = qualityConfig.Quality,
                TastingDecriptorSlots = descriptorSlots.ToArray()
            };

            possibleQualities.Add(coffeeQuality);
        }

        return possibleQualities;
    }

    /// <summary>
    /// Get a coffee data based on the origin using the scriptable object database.
    /// </summary>
    /// <param name="origin"></param>
    /// <returns></returns>
    public static GreenCoffeeConfig GetCoffeeConfigByOrigin(Origin origin)
    {
        return _coffeeDatabaseConfig.AllCoffees.FirstOrDefault(coffee => coffee.CoffeeOrigin.CoffeeOrigin.Origin.Equals(origin));
    }

    private static GrindSize GetRandomEspressoGrindSize()
    {
        return (GrindSize)Random.Range(3, Enum.GetValues(typeof(GrindSize)).Length);
    }
    
    public static CoffeeData GetRandomCoffeeData(bool isEspressoDialedIn = true)
    {
        var coffeeConfig = GetRandomCoffeeConfig();
        var espressoGrindSize = GetRandomEspressoGrindSize();
        var possibleQualities = GetPossibleQualitiesFromConfig(coffeeConfig);

        return new CoffeeData(coffeeConfig.CoffeeOrigin.CoffeeOrigin, espressoGrindSize, coffeeConfig.PossibleFlavourNotes,
            possibleQualities.ToArray(), isEspressoDialedIn);
    }
    
    public static WholeBeans GenerateRandomCoffeeBag(int servings, bool isEspressoDialedIn = true)
    {
        var coffeeData = GetRandomCoffeeData(isEspressoDialedIn);
        coffeeData.SetFlavorNote();
        var values = Enum.GetValues(typeof(RoastType));
        var randomRoastedType = (RoastType)values.GetValue(Random.Range(1,values.Length - 1));
        coffeeData.SetRoast(randomRoastedType);
        coffeeData.SetQualityFromRoastScore(0);
        coffeeData.SetRandomFlavourNotes();
        return new WholeBeans(servings, coffeeData);
    }
}
