using UnityEngine;

public class FTUE_Home : MonoBehaviour
{
    [SerializeField] private Bed _bed;
    [SerializeField] private Customizable[] _uniformCustomizables;
    [SerializeField] private ObjectTheme[] _uniformThemes;

    private void OnEnable()
    {
        SingletonManager.Instance.GameManager.SpawnedCharacter += CheckForFTUE;
    }

    private void OnDisable()
    {
        SingletonManager.Instance.GameManager.SpawnedCharacter -= CheckForFTUE;
    }

    private void CheckForFTUE(GameObject character)
    {
        if (FileGameDataHandler<PlayerData>.HasSaveFile())
        {
            return;
        }

        PlayerInteractionManager.Instance.PlayerController.Sleep(true);
        PlayerInteractionManager.Instance.PlayerController.SetPlayerTransform(_bed.CharacterSocket);
        
        // Set character appearance to NAKED
        SingletonManager.Instance.CharacterAppearanceManager.AppearanceDataChanged.Invoke(
            new Appearance(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

        foreach (var customizable in _uniformCustomizables)
        {
            SingletonManager.Instance.UnlockableItemManager.UpdateUnlockable(customizable.Id, true, true, true);
        }
        
        foreach (var customizable in _uniformThemes)
        {
            SingletonManager.Instance.UnlockableItemManager.UpdateUnlockable(customizable.Id, true, true, true);
        }
    }
}
