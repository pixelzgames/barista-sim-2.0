using UnityEngine;

public class FTUE_CharacterCustomization : MonoBehaviour
{
    [SerializeField] private Wardrobe _wardrobe;

    private void Awake()
    {
        SingletonManager.Instance.GameManager.SpawnedCharacter += CheckForFTUE;
    }

    private void OnDisable()
    {
        SingletonManager.Instance.GameManager.SpawnedCharacter -= CheckForFTUE;
    }

    private void CheckForFTUE(GameObject character)
    {
        if (FileGameDataHandler<PlayerData>.HasSaveFile())
        {
            return;
        }
        
        _wardrobe.OpenCharacterCreator();
    }
}
