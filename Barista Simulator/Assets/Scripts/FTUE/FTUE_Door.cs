using UnityEngine;

public class FTUE_Door : MonoBehaviour
{
   [SerializeField] private Door _door;

   private int _uniformCount;

   private void OnEnable()
   {
      if (FileGameDataHandler<PlayerData>.HasSaveFile())
      {
         Destroy(this);
         return;
      }

      _door.enabled = false;
      SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted += EquipUniformPiece;
   }

   private void OnDestroy()
   {
      SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted -= EquipUniformPiece;
   }

   private void EquipUniformPiece(GameEvent gameEvent)
   {
      if (gameEvent.Key is not (GameEventKey.ApronUniformEquipped or GameEventKey.CapUniformEquipped))
      {
         return;
      }

      _uniformCount++;

      if (_uniformCount != 2)
      {
         return;
      }
      
      _door.enabled = true;
      Destroy(this);
   }
}
