using Steamworks;
using UnityEngine;

public class BaristaSteamManager : MonoBehaviour
{
    private void Start()
    {
        if (!SteamManager.Initialized)
        {
            return;
        }
        
        Debug.Log($"Salut {SteamFriends.GetPersonaName()}! Tu as seulement que {SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagAll)} amis sur Steam...");
    }
}
