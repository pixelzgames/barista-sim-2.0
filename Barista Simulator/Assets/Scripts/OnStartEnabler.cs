using UnityEngine;

public class OnStartEnabler : BRAGBehaviour
{
    [SerializeField] private MonoBehaviour[] _components;
    
    protected override void OnLevelLoaded(LevelData levelData)
    {
        base.OnLevelLoaded(levelData);

        foreach (var component in _components)
        {
            component.enabled = true;
        }
    }
}
