using TMPro;
using UnityEngine;

public class BeanOSDayController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _dayText;
    private const string DAYTEXT = "DAY ";

    private void Start()
    {
        UpdateDayText(SingletonManager.Instance.ProgressionManager.Data.CurrentDay); 
        
        SingletonManager.Instance.GameManager.ChangedDay += UpdateDayText;
    }
    
    private void OnDestroy()
    {
        SingletonManager.Instance.GameManager.ChangedDay -= UpdateDayText;
    }
    
    private void UpdateDayText(int day)
    {
        _dayText.text = DAYTEXT + day;
    }
}
