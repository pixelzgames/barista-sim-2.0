using UnityEngine;
using UnityEngine.InputSystem;

public class BeanOSMessagesApp : BeanOSApp
{
    [SerializeField] private BeanOSContactPage _contactPage;
    [SerializeField] private BeanOSChatPage _chatPage;
    
    private BRAGCanvasController _currentPage;
    
    protected override void OnEnable()
    {
        base.OnEnable();
        _currentPage = _contactPage;
        SingletonManager.Instance.BeanOSManager.NewMessage += OnNewMessage;
        UpdateNotification();
    }
    
    protected override void OnDisable()
    {
        base.OnDisable();
        SingletonManager.Instance.BeanOSManager.NewMessage -= OnNewMessage;
    }
    
    private void OnNewMessage(ProcessedMessage message)
    {
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.MessageReceived));
        UpdateNotification();
    }

    protected override void UpdateNotification()
    {
        _notificationCount = _contactPage.GetNotificationsCount();
        base.UpdateNotification();
    }

    public override void Open()
    {
        base.Open();
        OpenContactPage();
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.ChatOSOpened));
    }
    
    protected override void OnBackButtonPressed(InputAction.CallbackContext obj)
    {
        switch (_currentPage)
        {
            case BeanOSChatPage:
                OpenContactPage();
                break;
            case BeanOSContactPage:
                UpdateNotification();
                Close();
                break;
        }
    }
        
    public void OpenChatPage(ContactData contactChat)
    {
        _contactPage.DisableCanvas();
        _contactPage.HasOpenChatPageWithContact(contactChat);
        _chatPage.EnableCanvasWithContact(contactChat);
        _currentPage = _chatPage;
    }

    private void OpenContactPage()
    {
        _contactPage.EnableCanvas();
        _chatPage.DisableCanvas();
        _currentPage = _contactPage;
    }
}
