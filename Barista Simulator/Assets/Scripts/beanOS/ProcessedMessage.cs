﻿using System;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class ProcessedMessage
{
    [JsonIgnore]
    public Action<MessageState> StateChanged = delegate {  };
    [JsonIgnore]
    public Action<ProcessedMessage> SendMessage = delegate {  };
    
    public MessageState CurrentMessageState { get; private set; }
    public string Message { get; private set; }
    public string SenderID { get; private set; } 
    public int DateSent { get; private set; }

    private float _timeBeforeTyping;
    private float _timeBeforeSending;
    private float _timer;

    [JsonConstructor]
    public ProcessedMessage(MessageState state, string message, string senderID, int date)
    {
        CurrentMessageState = MessageState.Sent;
        Message = message;
        SenderID = senderID;
        DateSent = date;
    }

    public ProcessedMessage(MessageData message, MessageState state)
    {
        CurrentMessageState = state;
        Message = message.Message;
        _timeBeforeSending = message.TimeToType;
        _timeBeforeTyping = message.DelayBetweenMessages;
        SenderID = message.MessageContact.ContactData.ContactID;
        DateSent = SingletonManager.Instance.ProgressionManager.Data.CurrentDay;
    }

    public void TimerTick()
    {
        _timer += Time.deltaTime;
        if (_timer < _timeBeforeTyping)
        {
            return;
        }

        if (CurrentMessageState != MessageState.Typing)
        {
            _timer = 0;
            SendMessage?.Invoke(this);
            ChangeState(MessageState.Typing);
        }

        if (_timer < _timeBeforeSending)
        {
            return;
        }
        
        if (CurrentMessageState != MessageState.Sent)
        {
            ChangeState(MessageState.Sent);
        }
    }
    
    public void ChangeState(MessageState newState)
    {
        CurrentMessageState = newState;
        StateChanged?.Invoke(CurrentMessageState);
    }
    
    public enum MessageState
    {
        Waiting = 0,
        Typing = 1,
        Sent = 2
    }
}