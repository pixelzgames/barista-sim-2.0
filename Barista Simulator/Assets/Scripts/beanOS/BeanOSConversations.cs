﻿using System;
using System.Collections.Generic;

[Serializable]
public class BeanOSConversations
{
    public Dictionary<string, List<ProcessedMessage>> Conversations { get; private set; } = new();
    
    public void AddMessage(ProcessedMessage message, ContactData replyTo = null)
    {
        var conversationKey = replyTo != null ? replyTo.ContactID : message.SenderID;
        
        if (Conversations.TryGetValue(conversationKey, out var value))
        {
            value.Add(message);
            return;
        }

        var newMessageList = new List<ProcessedMessage> { message };
        Conversations.Add(conversationKey, newMessageList);
    }

    public List<ProcessedMessage> GetMessagesInConversation(string key)
    {
        return Conversations.TryGetValue(key, out var conversation) ? conversation : null;
    }
}