using System;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public abstract class BeanOSApp : MonoBehaviour
{
    public Action<BeanOSApp> AppOpened = delegate {};
    [field:SerializeField] public bool IsUnlocked { get; private set; }
    
    [SerializeField] private BRAGCanvasController _canvasController;
    [SerializeField] protected Button _button;
    [SerializeField] protected Image _notificationImage;
    [SerializeField] protected TMP_Text _notificationCountText;

    protected int _notificationCount;
    
    public void BootUp(bool active)
    {
        if (active)
        {
            _button.onClick.AddListener(Open);
        }
        else
        {
            _button.onClick.RemoveListener(Open);
        }
    }

    protected virtual void OnEnable()
    {
        _button.interactable = IsUnlocked;
    }

    protected virtual void OnDisable()
    {
        
    }
    
    protected virtual void UpdateNotification()
    {
        _notificationImage.gameObject.SetActive(_notificationCount > 0);
        _notificationCountText.text = _notificationCount.ToString();
    }
    
    public virtual void Close()
    {
        _canvasController.DisableCanvas();
        AppOpened?.Invoke(null);
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.Back.performed -= OnBackButtonPressed;
    }
    
    public virtual void Open()
    {
        if (!IsUnlocked)
        {
            return;
        }

        SingletonManager.Instance.InputManager.InputMaster.BeanOS.Back.performed += OnBackButtonPressed;
        _notificationCount = 0;
        _canvasController.EnableCanvas();
        AppOpened?.Invoke(this);
    }

    protected virtual void OnBackButtonPressed(InputAction.CallbackContext obj)
    {
        Close();
    }

    public void ToggleUnlock(bool unlocked)
    {
        IsUnlocked = unlocked;
    }
}
