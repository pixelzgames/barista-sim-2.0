using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BeanOSMainUIController : MonoBehaviour
{
    [SerializeField] private List<BeanOSApp> _appPages;
    [SerializeField] private List<BeanOSAppMapping> _apps;
    [SerializeField] private PhoneController _phoneController;
    [SerializeField] private GameObject _initialSelectedGameObject;
    [SerializeField] private BRAGCanvasController _bragCanvasController;
    [SerializeField] private Animator _animator;
    
    private static readonly int _phoneBootupAnimID = Animator.StringToHash("Opened");
    private BeanOSApp _currentApp;

    private void OnEnable()
    {
        _phoneController.PhoneStatusUpdated += OnPhoneStatusUpdated;
        SubscribeToAppPagesOpened();
    }
    
    private void OnDestroy()
    {
        _phoneController.PhoneStatusUpdated -= OnPhoneStatusUpdated;
        
        foreach (var app in _appPages)
        {
            app.AppOpened -= OnPageOpened;
        }
    }

    private void SubscribeToAppPagesOpened()
    {
        foreach (var app in _appPages)
        {
            app.AppOpened += OnPageOpened;
        }
    }

    private void OnPageOpened(BeanOSApp app)
    {
        _currentApp = app;
        
        if (app == null)
        {
            // app is null when no app is opened
            EventSystem.current.SetSelectedGameObject(_initialSelectedGameObject);
            _bragCanvasController.EnableCanvas();
            return;
        }

        _bragCanvasController.DisableCanvas();
    }

    public void OpenApp(BeanOSAppName appName)
    {
        _apps.Find(x => x.App == appName).AppInstance.Open();
    }

    private void OnPhoneStatusUpdated(bool active)
    {
        _animator.SetBool(_phoneBootupAnimID, active);
        
        foreach (var app in _appPages)
        {
            app.BootUp(active);
        }

        // Close current app
        _currentApp?.Close();

        if (active)
        {
            EventSystem.current.SetSelectedGameObject(_initialSelectedGameObject);
        }
    }
}
