using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BeanOSNetworkPanelController : BaseBeanOSCanvasGroup
{
    [SerializeField] private Image[] _networkBars;

    private static readonly Color32 _normalColor = Color.black;
    private static readonly Color32 _fadedColor = new (0,0,0,150);

    private float _timer;
    private float _timeToWait;
    private Coroutine _coroutine;

    private void OnEnable()
    {
        Random.InitState(DateTime.Now.Millisecond);
        ChooseAnotherTimer();
    }

    private void OnDisable()
    {
        if (_coroutine == null)
        {
            return;
        }
        
        StopCoroutine(_coroutine);
        _coroutine = null;
    }
    
    private IEnumerator ChangeNetworkConnectivity()
    {
        var networkSignal = Random.Range(1, _networkBars.Length);
        for (var i = 1; i < _networkBars.Length; i++)
        {
            _networkBars[i].color = i <= networkSignal ? _normalColor : _fadedColor;
        }

        while (_timer < _timeToWait)
        {
            _timer += Time.deltaTime;
            yield return null;
        }
        
        ChooseAnotherTimer();
    }

    private void ChooseAnotherTimer()
    {
        _timeToWait = Random.Range(4f, 20f);
        _timer = 0f;
        _coroutine = StartCoroutine(ChangeNetworkConnectivity());
    }
}
