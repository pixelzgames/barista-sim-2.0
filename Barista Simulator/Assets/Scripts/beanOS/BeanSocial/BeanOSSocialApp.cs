using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BeanOSSocialApp : BeanOSApp
{
    [SerializeField] private SocialPostDisplay _socialPostDisplayPrefab;
    [SerializeField] private Transform _postGrid;
    [SerializeField] private int _maxPostsToDisplay = 10;
    
    private readonly List<SocialPostStubber> _socialPosts = new();
    private readonly List<SocialPostDisplay> _spawnedPosts = new();
    
    protected override void OnEnable()
    {
        base.OnEnable();
        SingletonManager.Instance.BeanOSManager.NewSocialPost += AddSocialNotification;
        UpdateNotification();
    }
    
    protected override void OnDisable()
    {
        base.OnDisable();
        SingletonManager.Instance.BeanOSManager.NewSocialPost -= AddSocialNotification;
    }

    private void AddSocialNotification(SocialPostStubber post)
    {
        if (_socialPosts.Count > _maxPostsToDisplay)
        {
            Destroy(_spawnedPosts.Last());
            _socialPosts.Remove(_socialPosts.Last());
            _spawnedPosts.Remove(_spawnedPosts.Last());
        }
        
        var postDisplay = Instantiate(_socialPostDisplayPrefab, _postGrid);
        postDisplay.PopulateDataUI(post);
        _spawnedPosts.Add(postDisplay);
        
        _socialPosts.Add(post);
        SingletonManager.Instance.BeanOSManager.Data.CurrentSocialPosts = _socialPosts.ToArray();
        _notificationCount++;
    }

    public void GetSocialNotificationFromConfig(SocialPostDisplayConfig config)
    {
        var rand = Random.Range(0, config.ContentPool.Count);

        AddSocialNotification(new SocialPostStubber
        {
            ContentText = config.ContentPool[rand].ContentText,
            LikesAmount = 30,
            UserTag = "@loser32"
        });
    }
}
