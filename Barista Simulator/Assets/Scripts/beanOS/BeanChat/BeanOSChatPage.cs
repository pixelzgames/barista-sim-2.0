using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BeanOSChatPage : BRAGCanvasController
{
    [TitleGroup("Chat Properties")] [SerializeField]
    private RectTransform _rectTransform;
    [SerializeField] private TMP_Text _contactNameText;
    [SerializeField] private Image _contactIcon;

    [SerializeField] private RectTransform _avatarChatIcon;
    [SerializeField] private Image _messageIcon;
    [SerializeField] private BeanOSChatBubble _chatBubblePrefab;

    private ContactData _myContact;
    private readonly List<BeanOSChatBubble> _chatBubbles = new();
    
    public void EnableCanvasWithContact(ContactData data)
    {
        _contactNameText.text = data.ContactName;
        _contactIcon.sprite = data.ContactImage;
        _messageIcon.sprite = data.ContactImage;
        _myContact = data;
        
        PopulateConversation();
        
        base.EnableCanvas();
        SingletonManager.Instance.BeanOSManager.NewMessage += UpdateConversation;
    }

    public override void DisableCanvas(float fadeOutTime = 0f)
    {
        base.DisableCanvas(fadeOutTime);
        SingletonManager.Instance.BeanOSManager.NewMessage -= UpdateConversation;
        ResetPage();
    }

    private void PopulateConversation()
    {
        var conversation = 
            SingletonManager.Instance.BeanOSManager.Data.Conversations.GetMessagesInConversation(_myContact.ContactID);

        foreach (var message in conversation)
        {
            var chatBubble = Instantiate(_chatBubblePrefab, _chatBubblePrefab.transform.parent);
            chatBubble.gameObject.SetActive(true);
            chatBubble.InitWithData(message);
            _chatBubbles.Add(chatBubble);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
    }

    private void UpdateConversation(ProcessedMessage processedMessage)
    {
        if (processedMessage.SenderID != _myContact.ContactID)
        {
            return;
        }
        
        var chatBubble = Instantiate(_chatBubblePrefab, _chatBubblePrefab.transform.parent);
        chatBubble.InitWithData(processedMessage);
        chatBubble.gameObject.SetActive(true);
        _chatBubbles.Add(chatBubble);
    }

    private void ResetPage()
    {
        _myContact = null;
        foreach (var message in _chatBubbles)
        {
            Destroy(message.gameObject);
        }
        
        _chatBubbles.Clear();
    }
}
