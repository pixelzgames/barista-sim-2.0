using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BeanOSChatBubble : MonoBehaviour
{
   [SerializeField] private RectTransform _rectTransform;
   [SerializeField] private TMP_Text _messageText;
   [SerializeField] private TMP_Text _dateText;
   [SerializeField] private TextMaximizer _textMaximizer;

   private ProcessedMessage _message;
   
   public void InitWithData(ProcessedMessage data)
   {
      _message = data;

      if (data.CurrentMessageState != ProcessedMessage.MessageState.Sent)
      {
         _messageText.text = "...";
         _dateText.text = "";
         _message.StateChanged += OnStateChanged;
         return;
      }
      
      ShowTextChat();
   }

   private void OnStateChanged(ProcessedMessage.MessageState messageState)
   {
      if (_message.CurrentMessageState != ProcessedMessage.MessageState.Sent)
      {
         return;
      }

      ShowTextChat();
   }

   private void ShowTextChat()
   {
      _messageText.text = _message.Message;
      _dateText.text = "DAY " + _message.DateSent;
      
      LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
      _textMaximizer.UpdateTextBubble();
   }

   private void OnDestroy()
   {
      _message.StateChanged -= OnStateChanged;
   }
}
