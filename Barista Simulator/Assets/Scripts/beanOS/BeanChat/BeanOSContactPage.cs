using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class BeanOSContactPage : BRAGCanvasController
{
    [SerializeField] private BeanOSConversationButton _conversationPrefab;
    
    private readonly List<BeanOSConversationButton> _conversations = new();
    
    private ContactData _inChatWithThisContact;
    
    protected override void Awake()
    {
        InitContactList();
        SingletonManager.Instance.BeanOSManager.NewMessage += NewMessage;
        base.Awake();
    }

    protected override void OnDestroy()
    {
        SingletonManager.Instance.BeanOSManager.NewMessage -= NewMessage;
        base.OnDestroy();
    }

    private void InitContactList()
    {
        if (SingletonManager.Instance.BeanOSManager.Data.Conversations == null)
        {
            return;
        }
        
        foreach (var contact in SingletonManager.Instance.BeanOSManager.Data.Conversations.Conversations)
        {
            CreateNewConversationButton(contact.Key);
        }
    }

    public override void EnableCanvas(float fadeInTime = 0f)
    {
        base.EnableCanvas(fadeInTime);
        if (_conversations.Count > 0)
        {
            EventSystem.current.SetSelectedGameObject(_conversations[0].gameObject);
        }

        _inChatWithThisContact = null;
    }

    private void NewMessage(ProcessedMessage message)
    {
        // Adds notification to current conversation and pop it on top of the list
        var conversation = FindExistingConversation(message);
        if (conversation == null)
        {
            CreateNewConversationButton(message.SenderID, true);
            return;
        }

        if (_inChatWithThisContact == conversation.Contact)
        {
            return;
        }
        
        conversation.AddNotification();
        PopConversationOnTopOfList(conversation);
    }

    private void CreateNewConversationButton(string contactKey, bool addNotification = false)
    {
        if (!SingletonManager.Instance.BeanOSManager.AllContacts.AllContactData.TryGetValue(contactKey,
                out var contactData))
        {
            return;
        }
            
        var conversation = Instantiate(_conversationPrefab, _conversationPrefab.transform.parent);
        conversation.InitWithData(contactData.ContactData);
        _conversations.Add(conversation);
        var go = conversation.gameObject;
        go.SetActive(true);
        
        if (addNotification)
        {
            conversation.AddNotification();
            PopConversationOnTopOfList(conversation);
        }
    }

    private void PopConversationOnTopOfList(BeanOSConversationButton button)
    {
        _conversations.Remove(button);
        _conversations.Insert(0, button);
        
        for (var i = 0; i < _conversations.Count; i++)
        {
            _conversations[i].transform.SetSiblingIndex(i);
        }
    }
    
    private BeanOSConversationButton FindExistingConversation(ProcessedMessage message)
    {
        return _conversations.FirstOrDefault(conversation => conversation.Contact.ContactID == message.SenderID);
    }

    public void HasOpenChatPageWithContact(ContactData contact)
    {
        _inChatWithThisContact = contact;
    }
    
    public int GetNotificationsCount()
    {
        return _conversations.Sum(conversation => conversation.NotificationCount);
    }
}
