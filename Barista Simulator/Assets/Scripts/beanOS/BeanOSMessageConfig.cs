using System;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/BeanOS/Message", fileName = "NewMessageData")]
public class BeanOSMessageConfig : ScriptableObject
{
    [field: SerializeField] public MessageData[] MessagesData { get; private set; }
}

[Serializable]
public class MessageData
{
    public BeanOSContactConfig MessageContact;
    [TextArea]
    public string Message;
    public float TimeToType;
    public float DelayBetweenMessages;
}
