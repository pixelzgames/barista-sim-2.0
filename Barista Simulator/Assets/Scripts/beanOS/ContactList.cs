using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Contact List", fileName = "ALlContacts")]
public class ContactList : ScriptableObject
{
    public ContactsDictionary AllContactData;
}

[System.Serializable]
public class ContactsDictionary : SerializableDictionary<string, BeanOSContactConfig>
{

}
