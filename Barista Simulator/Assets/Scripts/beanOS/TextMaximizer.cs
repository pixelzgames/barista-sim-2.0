using UnityEngine;
using UnityEngine.UI;

public class TextMaximizer : MonoBehaviour
{
    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private float _maxWidthThreshold;
    [SerializeField] private LayoutElement _layoutElement;
    
    public void UpdateTextBubble()
    {
        if (_rectTransform.rect.width >= _maxWidthThreshold)
        {
            _layoutElement.preferredWidth = _maxWidthThreshold;
        }
        
        LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
    }
}
