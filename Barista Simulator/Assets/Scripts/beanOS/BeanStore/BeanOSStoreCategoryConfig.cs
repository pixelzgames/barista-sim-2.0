using UnityEngine;

[CreateAssetMenu(fileName = "NewStoreCategory", menuName = "BRAG/Store/Category ")]
public class BeanOSStoreCategoryConfig : ScriptableObject
{
    public string CategoryName;
    public ProductConfig[] Products;
    public CoffeeMarketStoreConfig CoffeeMarketStore;
}
