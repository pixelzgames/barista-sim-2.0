using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class BeanOsStorePageBase<T> : BRAGCanvasController, IBeanOSPage
{
    [SerializeField] protected TMP_Text _pageTitleText;
    
    protected BeanOSShopApp _myApp;
    protected GameObject _lastButtonSelected;
    protected T _currentData;

    public void InitWithData(BeanOSShopApp app, T data)
    {
        _myApp = app;
        _currentData = data;
        gameObject.SetActive(true);
        InitPage();
    }

    protected virtual void InitPage()
    {
        MakePageActive();
    }

    public virtual void MakePageActive()
    {
        EnableCanvas();
        EventSystem.current.SetSelectedGameObject(_lastButtonSelected);
    }

    public virtual void ClosePage()
    {
        Destroy(gameObject);
    }
}
