using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class BeanOSStoreCatalogPage : BeanOsStorePageBase<BeanOSStoreCategoryCatalogConfig>
{
    [SerializeField] private BeanOSStoreProductButton _productButtonPrefab;
    [SerializeField] private Transform _contentTransform;
    [SerializeField] private Transform _dottedTabGridTransform;
    [SerializeField] private DottedTabButton _dottedTabButtonPrefab;
    
    [Header("Bottom Selected Product Info")] 
    [SerializeField] private TMP_Text _productNameText;
    [SerializeField] private TMP_Text _priceText;
    [SerializeField] private TMP_Text _productDescriptionText;

    private readonly List<BeanOSStoreProductButton> _spawnedProductButtons = new();
    private readonly List<DottedTabButton> _tabs = new();
    private int _currentTabIndex;
    
    protected override void InitPage()
    {
        for (var i = 0; i < _currentData.CategoriesInCatalog.Length; i++)
        {
            var dottedTab = Instantiate(_dottedTabButtonPrefab, _dottedTabGridTransform);
            dottedTab.gameObject.SetActive(true);
            _tabs.Add(dottedTab);
        }

        ChangeTab();
        base.InitPage();
    }

    private void PageRightOnPerformed(InputAction.CallbackContext obj)
    {
        _currentTabIndex++;
        if (_currentTabIndex >= _currentData.CategoriesInCatalog.Length)
        {
            _currentTabIndex = 0;
        }
        
        ChangeTab();
    }

    private void PageLeftOnPerformed(InputAction.CallbackContext obj)
    {
        _currentTabIndex--;
        if (_currentTabIndex < 0)
        {
            _currentTabIndex = _currentData.CategoriesInCatalog.Length - 1;
        }
        
        ChangeTab();
    }
    
    /// <summary>
    /// Show the current products of the category on this page
    /// </summary>
    private void ChangeTab()
    {
        // Clearing previous products
        foreach (var productButton in _spawnedProductButtons)
        {
            productButton.ButtonSelected -= DisplayProductSelectionInfo;
            productButton.ButtonPressed -= CategoryButtonOnButtonPressed;
            Destroy(productButton.gameObject);
        }
        
        _spawnedProductButtons.Clear();
        
        // Selecting the proper tab UI
        for (var i = 0; i < _tabs.Count; i++)
        {
            if (_currentTabIndex == i)
            {
                _tabs[i].SelectTab();
            }
            else
            {
                _tabs[i].DeselectTab();
            }
        }

        var productsInCategoryArray =
            // Populate the coffees if it has a market store
            _currentData.CategoriesInCatalog[_currentTabIndex].CoffeeMarketStore != null
            ? _myApp.CoffeeProducts
            :
            // Populating the products from normal category data
            _currentData.CategoriesInCatalog[_currentTabIndex].Products.Select(product => product.Product).ToArray();

        foreach (var product in productsInCategoryArray)
        {
            var productButton = Instantiate(_productButtonPrefab, _contentTransform);
            productButton.InitButtonWithData(product);
            productButton.ButtonSelected += DisplayProductSelectionInfo;
            productButton.ButtonPressed += CategoryButtonOnButtonPressed;
            _spawnedProductButtons.Add(productButton);
        }
        
        // Changing the store title
        _pageTitleText.text = _currentData.CategoriesInCatalog[_currentTabIndex].CategoryName;
        _lastButtonSelected = _spawnedProductButtons[0].gameObject;
        EventSystem.current.SetSelectedGameObject(_lastButtonSelected);
    }

    private void DisplayProductSelectionInfo(Product productConfigData)
    {
        _productNameText.text = productConfigData.ProductName;
        _productDescriptionText.text = productConfigData.DeliveryTimeInDays + " days to ship";
        _priceText.text = productConfigData.Price + " $";
    }
    
    private void CategoryButtonOnButtonPressed(Product buttonDataToOpen)
    {
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageLeft.performed -= PageLeftOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageRight.performed -= PageRightOnPerformed;
        DisableCanvas();
        _myApp.OpenShopPage(buttonDataToOpen);
    }

    public override void ClosePage()
    {
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageLeft.performed -= PageLeftOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageRight.performed -= PageRightOnPerformed;
        base.ClosePage();
    }

    public override void MakePageActive()
    {
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageLeft.performed += PageLeftOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageRight.performed += PageRightOnPerformed;
        base.MakePageActive();
    }
}
