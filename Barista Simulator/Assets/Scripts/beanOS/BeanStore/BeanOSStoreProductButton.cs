using System;
using UnityEngine.EventSystems;

public class BeanOSStoreProductButton : BeanOSStoreButtonBase<Product>
{
    public event Action<Product> ButtonSelected = delegate { };
    
    public override void InitButtonWithData(Product data)
    {
        base.InitButtonWithData(data);
        _icon.sprite = _myData.Icon;
    }

    protected override void Selected(BaseEventData arg)
    {
        base.Selected(arg);
        ButtonSelected?.Invoke(_myData);
    }
}
