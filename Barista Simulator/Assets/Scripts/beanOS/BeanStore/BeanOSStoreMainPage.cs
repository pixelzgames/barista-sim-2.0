using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BeanOSStoreMainPage : BeanOsStorePageBase<BeanOSStoreMainPageConfig>
{
   [SerializeField] private BeanOSStoreCatalogButton _catalogButtonPrefab;
   [SerializeField] private Transform _contentTransform;
   
   private readonly List<BeanOSStoreCatalogButton> _categoryButtons = new();
   
   protected override void InitPage()
   {
      foreach (var category in _currentData.CatalogsInStore)
      {
         var categoryButton = Instantiate(_catalogButtonPrefab, _contentTransform);
         categoryButton.InitButtonWithData(category);
         categoryButton.ButtonPressed += CategoryButtonOnButtonPressed;
         _categoryButtons.Add(categoryButton);
      }

      if (_categoryButtons.Count > 0)
      {
         _lastButtonSelected = _categoryButtons[0].gameObject;
      }

      _pageTitleText.text = _currentData.StoreName;
      _myApp.AppOpened += AppOpened;
      base.InitPage();
   }

   private void AppOpened(BeanOSApp app)
   {
      if (app != null)
      {
         EventSystem.current.SetSelectedGameObject(_lastButtonSelected);
      }
   }

   private void CategoryButtonOnButtonPressed(BeanOSStoreCategoryCatalogConfig buttonConfigToOpen)
   {
      DisableCanvas();
      _myApp.OpenShopPage(buttonConfigToOpen);
   }
}
