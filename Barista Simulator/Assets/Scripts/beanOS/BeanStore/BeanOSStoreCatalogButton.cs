public class BeanOSStoreCatalogButton : BeanOSStoreButtonBase<BeanOSStoreCategoryCatalogConfig>
{
    public override void InitButtonWithData(BeanOSStoreCategoryCatalogConfig config)
    {
        base.InitButtonWithData(config);
        _text.text = _myData.CatalogName;
        _icon.sprite = _myData.Icon;
    }
}
