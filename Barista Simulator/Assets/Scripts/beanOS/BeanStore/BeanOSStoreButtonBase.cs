using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BeanOSStoreButtonBase<T> : MonoBehaviour
{
    public event Action<T> ButtonPressed = delegate { };
    
    [SerializeField] protected TMP_Text _text;
    [SerializeField] protected Image _icon;
    [SerializeField] private Button _button;
    [SerializeField] private Image _background;
    [SerializeField] private EventTrigger _eventTrigger;
    
    [TitleGroup("Normal Colors")]
    [SerializeField] private Color32 _backgroundNormalColor;
    [SerializeField] private Color32 _textNormalColor = Color.black;
    [TitleGroup("Selected Colors")]
    [SerializeField] private Color32 _backgroundSelectedColor;
    [SerializeField] private Color32 _textSelectedColor = Color.white;
    
    protected T _myData;
    
    private void Awake()
    {
        var entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Select
        };
        entry.callback.AddListener(Selected);
        _eventTrigger.triggers.Add(entry);
        
        entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Deselect
        };
        entry.callback.AddListener(Deselected);
        _eventTrigger.triggers.Add(entry);
        
        _button.onClick.AddListener(OnButtonPressed);
    }
    
    public virtual void InitButtonWithData(T data)
    {
        _myData = data;
        gameObject.SetActive(true);
    }
    
    protected virtual void Selected(BaseEventData arg)
    {
        _background.color = _backgroundSelectedColor;
        if (_text)
        {
            _text.color = _textSelectedColor;
        }
    }
    
    protected virtual void Deselected(BaseEventData arg)
    {
        _background.color = _backgroundNormalColor;
        if (_text)
        {
            _text.color = _textNormalColor;
        }
    }
    
    protected virtual void OnButtonPressed()
    {
        ButtonPressed?.Invoke(_myData);
    }
}
