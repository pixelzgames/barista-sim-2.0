using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class BeanOSStoreProductPage : BeanOsStorePageBase<Product>
{
    [SerializeField] private RectTransform _pageRect;
    [SerializeField] private TMP_Text _productNameText;
    [SerializeField] private TMP_Text _priceText;
    [SerializeField] private TMP_Text _productDescriptionText;
    [SerializeField] private TMP_Text _infoText;
    [SerializeField] private Image _image;
    
    protected override void InitPage()
    {
        base.InitPage();
        _pageTitleText.text = _currentData.ProductName;
        _productNameText.text = _currentData.ProductName;
        _priceText.text = _currentData.Price + " $";
        _productDescriptionText.text = _currentData.DeliveryTimeInDays + " days for shipping";
        _infoText.text = "Hard coded infos for now please add data!";
        _image.sprite = _currentData.Icon;
        LayoutRebuilder.ForceRebuildLayoutImmediate(_pageRect);
        
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.Alternate.performed += OnBuyButtonPressed;
    }

    private void OnBuyButtonPressed(InputAction.CallbackContext obj)
    {
        var modalWindow = new ModalWindowData("Purchase Product",
            $"Are you sure you want to buy {_currentData.ProductName} for {_currentData.Price}$ ?", null, false,
            () => Purchase(true), () => Purchase(false));
        SingletonManager.Instance.GameManager.ShowModalWindow(modalWindow);
    }

    private void Purchase(bool purchase)
    {
        if (!purchase)
        {
            return;
        }

        SingletonManager.Instance.CurrenciesManager.TryOrderingProduct(_currentData);
    }

    public override void ClosePage()
    {
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.Alternate.performed -= OnBuyButtonPressed;
        base.ClosePage();
    }
}
