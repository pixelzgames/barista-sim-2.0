using UnityEngine;

[CreateAssetMenu(fileName = "NewStoreCategoryCatalog", menuName = "BRAG/Store/Category Catalog ")]
public class BeanOSStoreCategoryCatalogConfig : ScriptableObject
{
    public Sprite Icon;
    public string CatalogName;
    public BeanOSStoreCategoryConfig[] CategoriesInCatalog;
}
