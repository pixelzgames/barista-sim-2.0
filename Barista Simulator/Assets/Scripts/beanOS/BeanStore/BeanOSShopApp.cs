using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class BeanOSShopApp : BeanOSApp
{
    public Product[] CoffeeProducts => _dailyProducts.ToArray();
    
    [FormerlySerializedAs("_storeData")] [SerializeField] private BeanOSStoreMainPageConfig _storeConfig;
    [SerializeField] private BeanOSStoreMainPage _mainPageCategoryPrefab;
    [SerializeField] private BeanOSStoreCatalogPage _catalogPagePrefab;
    [SerializeField] private BeanOSStoreProductPage _productPagePrefab;
    [Required, SerializeField] private GameObject _smallCoffeeBagPrefab;
    
    private readonly Stack<IBeanOSPage> _activePages = new();
    private readonly List<Product> _dailyProducts = new();
    
    protected override void OnEnable()
    {
        base.OnEnable();
        
        OpenShopPage(_storeConfig);
        GenerateMarketProducts();
    }

    private void GenerateMarketProducts()
    {
        foreach (var catalog in _storeConfig.CatalogsInStore)
        {
            foreach (var category in catalog.CategoriesInCatalog)
            {
                if (category.CoffeeMarketStore == null)
                {
                    continue;
                }
                
                foreach (var offering in category.CoffeeMarketStore.CoffeeProductsMarket.PossibleCoffeesOffering)
                {
                    var productData = new Product
                    {
                        ProductPrefab = _smallCoffeeBagPrefab,
                        Icon = category.Products[0].Product.Icon,
                        ProductName = category.Products[0].Product.ProductName,
                        DeliveryTimeInDays = category.Products[0].Product.DeliveryTimeInDays,
                        Price = Mathf.RoundToInt(Random.Range(category.CoffeeMarketStore.CoffeeProductsMarket.MinMaxPrice.x,
                            category.CoffeeMarketStore.CoffeeProductsMarket.MinMaxPrice.y))
                    };
                    productData.SetCoffeeData(offering);
                    _dailyProducts.Add(productData);
                }
            }
        }
    }

    protected override void OnBackButtonPressed(InputAction.CallbackContext ctx)
    {
        if (_activePages.Count > 1)
        {
            var pageToKill = _activePages.Pop();
            pageToKill.ClosePage();

            _activePages.Peek().MakePageActive();
            return;
        }
        
        base.OnBackButtonPressed(ctx);
    }

    /// <summary>
    /// Opens the main app page
    /// </summary>
    /// <param name="pageConfig"></param>
    private void OpenShopPage(BeanOSStoreMainPageConfig pageConfig)
    {
        var newPage = Instantiate(_mainPageCategoryPrefab, transform);
        newPage.InitWithData(this, pageConfig);
        _activePages.Push(newPage);
    }
    
    /// <summary>
    /// Opens the catalog page
    /// </summary>
    /// <param name="pageConfig"></param>
    public void OpenShopPage(BeanOSStoreCategoryCatalogConfig pageConfig)
    {
        var newPage = Instantiate(_catalogPagePrefab, transform);
        newPage.InitWithData(this, pageConfig);
        _activePages.Push(newPage);
    }
    
    /// <summary>
    /// Opens a product page
    /// </summary>
    /// <param name="pageData"></param>
    public void OpenShopPage(Product pageData)
    {
        var newPage = Instantiate(_productPagePrefab, transform);
        newPage.InitWithData(this, pageData);
        _activePages.Push(newPage);
    }
}
