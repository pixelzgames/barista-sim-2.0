using UnityEngine;

[CreateAssetMenu(fileName = "NewMainPageData", menuName = "BRAG/Store/MainPageData ")]
public class BeanOSStoreMainPageConfig : ScriptableObject
{
    public string StoreName;
    public BeanOSStoreCategoryCatalogConfig[] CatalogsInStore;
}