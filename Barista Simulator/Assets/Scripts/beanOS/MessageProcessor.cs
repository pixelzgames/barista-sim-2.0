﻿using System.Collections.Generic;
using UnityEngine;

public class MessageProcessor : MonoBehaviour
{
    private List<ProcessedMessage> MessagesList = new();

    private void Update()
    {
        if (MessagesList.Count <= 0)
        {
            return;
        }

        if (MessagesList[0].CurrentMessageState == ProcessedMessage.MessageState.Sent)
        {
            MessagesList.Remove(MessagesList[0]);
            return;
        }

        MessagesList[0].TimerTick();
    }

    public ProcessedMessage[] ProcessMessages(IEnumerable<MessageData> messages)
    {
        var processedMessages = new List<ProcessedMessage>();
        foreach (var msg in messages)
        {
            var processedMsg = new ProcessedMessage(msg, ProcessedMessage.MessageState.Waiting);
            processedMessages.Add(processedMsg);
            MessagesList.Add(processedMsg);
        }
        
        return processedMessages.ToArray();
    }
}