using System;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "BRAG/BeanOS/Contact", fileName = "NewContactData")]
public class BeanOSContactConfig : ScriptableObject
{
    [SerializeField] public ContactData ContactData;
    
    public BeanOSContactConfig()
    {
        ContactData = new ContactData();
    }
}

[Serializable]
public class ContactData
{
    public string ContactID;
    public string ContactName;
    public string ContactNote;
    [FormerlySerializedAs("AppearanceData")] public AppearanceConfig _appearanceConfig;
    
    [PreviewField(100, ObjectFieldAlignment.Left)]
    [JsonIgnore]
    public Sprite ContactImage;
}
