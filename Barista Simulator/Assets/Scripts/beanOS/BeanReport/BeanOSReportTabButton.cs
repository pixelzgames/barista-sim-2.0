using UnityEngine;
using UnityEngine.UI;

public class BeanOSReportTabButton : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Image _icon;
    
    public void SetSelected(bool selected)
    {
        _image.color = selected ? DTT.BeanOS_Report_App.Report_Accent : DTT.BeanOS_Report_App.Report_Button_Normal;
        _icon.color = selected ? DTT.BeanOS_Report_App.Report_Button_Imahe_Selected : DTT.BeanOS_Report_App.Report_Button_Image_Normal;
    }
}
