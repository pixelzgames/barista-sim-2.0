using TMPro;
using UnityEngine;

public class BeanOSReportContentPageCustomers : BeanOSReportContentPageBase
{
    [SerializeField] private TMP_Text _totalPassersby;
    [SerializeField] private TMP_Text _customersVisited;
    [SerializeField] private TMP_Text _customersOrdered;
    [SerializeField] private TMP_Text _customerVisitRate;
    
    public override void InitWithData(DailyGameData gameData)
    {
        var customersVisited = gameData.CustomerData.CustomersVisited;
        var passersby = gameData.CustomerData.TotalPassersby;
        _totalPassersby.text = passersby.ToString();
        _customersVisited.text = customersVisited.ToString();
        _customersOrdered.text = gameData.CustomerData.CustomersOrdered.ToString();
        _customerVisitRate.text = ReportDataProcessor.CalculateVisitRate(customersVisited, passersby).ToString("P0");
    }
}
