using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BeanOSReportButton : MonoBehaviour
{
    [SerializeField] private Image _buttonImage;
    [SerializeField] private TMP_Text _dayText;
    [SerializeField] private TMP_Text _revenuesText;
    [SerializeField] private Image[] _stars;
    
    private BeanOSReportApp _app;
    private DailyGameData _data;
    
    public void InitButtonWithData(DailyGameData data, BeanOSReportApp app)
    {
        _data = data;
        
        _dayText.text = "DAY " + data.Day;
        _revenuesText.text = "+" + data.FinanceData.Revenue + "$";
        _app = app;
        
        // Loop up to the max index that the data has stars
        var starRating = ReportDataProcessor.CalculateStarRating(data);
        for (var i = 0; i < starRating; i++)
        {
            _stars[i].color = DTT.BeanOS_Report_App.Report_Accent;
        }
    }

    public void OnButtonSelected()
    {
        _buttonImage.color = DTT.BeanOS_Report_App.Report_Button_Selected;
        _dayText.color = DTT.BeanOS_Report_App.Report_Text_Selected;
    }

    public void OnButtonDeselected()
    {
        _buttonImage.color = DTT.BeanOS_Report_App.Report_Button_Normal;
        _dayText.color = DTT.BeanOS_Report_App.Report_Text_Normal;
    }

    public void OnButtonClick()
    {
        _app.OpenReportPage(_data);
    }
}
