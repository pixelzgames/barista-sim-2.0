using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class BeanOSReportPage : BRAGCanvasController, IBeanOSPage
{
    [SerializeField] private TMP_Text _titleText;
    [SerializeField] private BeanOSReportTabButton[] _beanOSReportTabButtons;
    [SerializeReference] private BeanOSReportContentPageBase[] _contentPages;
    
    private int _currentTabIndex;
    private DailyGameData _data;
    
    public void InitReportPage(DailyGameData data)
    {
        _titleText.text = "DAY " + data.Day +  " REPORT";
        _data = data;
    }
    
    public void MakePageActive()
    {
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageLeft.performed += PageLeftOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageRight.performed += PageRightOnPerformed;
        EnableCanvas();
        ChangeTab();
    }

    public void ClosePage()
    {
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageLeft.performed -= PageLeftOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.BeanOS.PageRight.performed -= PageRightOnPerformed;
        DisableCanvas();
    }
    
    private void PageRightOnPerformed(InputAction.CallbackContext obj)
    {
        _currentTabIndex++;
        if (_currentTabIndex >= _beanOSReportTabButtons.Length)
        {
            _currentTabIndex = 0;
        }
        
        ChangeTab();
    }

    private void PageLeftOnPerformed(InputAction.CallbackContext obj)
    {
        _currentTabIndex--;
        if (_currentTabIndex < 0)
        {
            _currentTabIndex = _beanOSReportTabButtons.Length - 1;
        }
        
        ChangeTab();
    }
    
    private void ChangeTab()
    {
        for (var i = 0; i < _contentPages.Length; i++)
        {
            if (_currentTabIndex == i)
            {
                _contentPages[i].MakePageActive();
                _contentPages[i].InitWithData(_data);
                _beanOSReportTabButtons[i].SetSelected(true);
                continue;
            }
            
            _contentPages[i].ClosePage();
            _beanOSReportTabButtons[i].SetSelected(false);
        }
    }
}
