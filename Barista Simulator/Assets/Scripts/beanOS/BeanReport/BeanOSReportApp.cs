using UnityEngine;
using UnityEngine.InputSystem;

public class BeanOSReportApp : BeanOSApp
{
    [SerializeField] private BeanOSReportPage _reportPage;
    [SerializeField] private BeanOSReportMainPage _reportMainPage;

    private bool _isInReportPage;

    public override void Open()
    {
        base.Open();
        _reportMainPage.MakePageActive();
        _reportPage.ClosePage();
    }

    protected override void OnBackButtonPressed(InputAction.CallbackContext obj)
    {
        if (_isInReportPage)
        {
            _reportMainPage.MakePageActive();
            _reportPage.ClosePage();
            _isInReportPage = false;
            return;
        }
        
        _reportMainPage.ClosePage();
        base.OnBackButtonPressed(obj);
    }

    public void OpenReportPage(DailyGameData data)
    {
        _reportMainPage.ClosePage();
        _reportPage.InitReportPage(data);
        _reportPage.MakePageActive();
        _isInReportPage = true;
    }
}
