using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class BeanOSReportContentPageFinances : BeanOSReportContentPageBase
{
    private const int DRINKS_PRICE_MULTIPLIER = 30; // TODO: replace with real price multiplier
    private const int BAD_DRINKS_PRICE_MULTIPLIER = -20; // TODO: replace with real price multiplier
    
    [SerializeField] private TMP_Text _drinksText;
    [SerializeField] private TMP_Text _badDrinksText;
    [SerializeField] private TMP_Text _tipsText;
    [SerializeField] private TMP_Text _revenuesText;
    [SerializeField] private TMP_Text _rentText;
    [SerializeField] private TMP_Text _profitsText;
    [SerializeField] private TMP_Text _dailyBalanceText;
    [SerializeField] private TMP_Text _noPreviousDayDatText;
    [SerializeField] private UILineRenderer _graph;

    public override void InitWithData(DailyGameData gameData)
    {
        base.InitWithData(gameData);
        
        MoneyTextBuilder(_revenuesText, gameData.FinanceData.Revenue);
        MoneyTextBuilder(_tipsText, gameData.FinanceData.Tips);
        MoneyTextBuilder(_rentText, gameData.FinanceData.RentPaid, -1);
        MoneyTextBuilder(_profitsText, ReportDataProcessor.CalculateProfit(gameData.FinanceData));
        DrinksStyle(_drinksText, gameData.FinanceData.DrinksSold, DRINKS_PRICE_MULTIPLIER);
        DrinksStyle(_badDrinksText, gameData.FinanceData.DrinksSold, BAD_DRINKS_PRICE_MULTIPLIER);
        _dailyBalanceText.text = gameData.FinanceData.EndOfDayBalance + "$";
        
        SetUpFinanceGraph(gameData);
    }

    private void MoneyTextBuilder(TMP_Text text, float amount, int multiplier = 1)
    {
        if (amount * multiplier < 0)
        {
            text.color = DTT.BeanOS_Report_App.Report_Negative;
            text.text = amount * multiplier + "$";
        }
        else
        {
            text.color = DTT.BeanOS_Report_App.Report_Positive;
            text.text = "+" + amount * multiplier + "$";
        }
    }

    /// <summary>
    /// The first point on the graph should always be at (0, 0.5) coordinate.
    /// The first point on the graph should be the latest up to five days prior today.
    /// All other points (potential 4 other points) should be offset relative to the first one clamped (0,1) on the Y axis only.
    /// All other points should be spread evenly fro 0-1 on the X axis
    /// </summary>
    private void SetUpFinanceGraph(DailyGameData gameData)
    {
        List<DailyGameData> _previousDailyGameDatas = new List<DailyGameData>();
        int biggestDelta = 0;
        int firstPreviousDayAmount = 0;
        for (int i = 4; i >= 0; i--)
        {
            SingletonManager.Instance.GameStatsTracker.Data.DailyGameDataDictionary.TryGetValue(
                gameData.Day - i, out DailyGameData dailyGameData);

            if (dailyGameData != null)
            {
                if (_previousDailyGameDatas.Count == 0)
                {
                    firstPreviousDayAmount = dailyGameData.FinanceData.EndOfDayBalance;
                    _previousDailyGameDatas.Add(dailyGameData);
                    continue;
                }
                
                var delta = Mathf.Abs(dailyGameData.FinanceData.EndOfDayBalance - firstPreviousDayAmount);

                if (biggestDelta < delta)
                {
                    biggestDelta = delta;
                }
                
                _previousDailyGameDatas.Add(dailyGameData);
            }
        }

        float NormalizeBalance(float balance, float upperRange, float lowerRange)
        {
            return (balance - lowerRange) / (upperRange - lowerRange);
        }

        Vector2[] points = new Vector2[_previousDailyGameDatas.Count];
        
        for (int i = 0; i < _previousDailyGameDatas.Count; i++)
        {
            if (i == 0)
            {
                points[i] = new Vector2(0f, 0.5f);
                continue;
            }
            
            points[i] = new Vector2((float) i / (_previousDailyGameDatas.Count - 1), NormalizeBalance(_previousDailyGameDatas[i].FinanceData.EndOfDayBalance, firstPreviousDayAmount + biggestDelta, firstPreviousDayAmount - biggestDelta));
        }

        _graph.Points = points;

        if (points.Length > 1)
        {
            _graph.color = points[^1].y > points[^2].y ? DTT.BeanOS_Report_App.Report_Positive : DTT.BeanOS_Report_App.Report_Negative;
            _noPreviousDayDatText.gameObject.SetActive(false);
        }
        else
        {
            _noPreviousDayDatText.gameObject.SetActive(true);
        }
    }

    private void DrinksStyle(TMP_Text text, int amount, int priceMultiplier)
    {
        // TODO calculate this based on some real price modifier 
        if (priceMultiplier < 0)
        {
            text.text = priceMultiplier * amount + "$ (" + amount + ")";
            text.color = DTT.BeanOS_Report_App.Report_Negative;
        }
        else
        {
            text.text = "+" + priceMultiplier * amount + "$ (" + amount + ")";
            text.color = DTT.BeanOS_Report_App.Report_Positive;
        }
    }
}
