using UnityEngine;

public class BeanOSReportContentPageBase : MonoBehaviour, IBeanOSPage
{
    public virtual void InitWithData(DailyGameData gameData)
    {
        
    }
    
    public void MakePageActive()
    {
        gameObject.SetActive(true);
    }

    public void ClosePage()
    {
        gameObject.SetActive(false);
    }
}
