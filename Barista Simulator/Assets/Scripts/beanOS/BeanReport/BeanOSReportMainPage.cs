using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BeanOSReportMainPage : BRAGCanvasController, IBeanOSPage
{
    [SerializeField] private BeanOSReportButton _reportButtonPrefab;
    [SerializeField] private Transform _buttonContainer;
    [SerializeField] private BeanOSReportApp _app;
    
    private List<BeanOSReportButton> _spawnedButtons = new ();
    
    public void MakePageActive()
    {
        EnableCanvas();
        InitAllReportButtons();
    }

    private void ClearButtons()
    {
        foreach (var button in _spawnedButtons)
        {
            Destroy(button.gameObject);
        }
        
        _spawnedButtons.Clear();
    }
    
    private void InitAllReportButtons()
    {
        var first = true;
        foreach (var data in SingletonManager.Instance.GameStatsTracker.Data.DailyGameDataDictionary)
        {
            BeanOSReportButton button = Instantiate(_reportButtonPrefab, _buttonContainer);
            button.InitButtonWithData(data.Value, _app);
            _spawnedButtons.Add(button);
            
            if (first)
            {
                first = false;
                EventSystem.current.SetSelectedGameObject(button.gameObject);
            }
        }
    }

    public void ClosePage()
    {
        ClearButtons();
        DisableCanvas();
    }
}
