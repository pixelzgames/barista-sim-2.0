using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BeanOSReportContentPageSummary : BeanOSReportContentPageBase
{
    [SerializeField] private TMP_Text _revenuesText;
    [SerializeField] private TMP_Text _customersSatisfactionText;
    [SerializeField] private TMP_Text _goodDrinksText;
    [SerializeField] private TMP_Text _badDrinksText;
    [SerializeField] private Image[] _stars;
    
    public override void InitWithData(DailyGameData gameData)
    {
        _revenuesText.text = gameData.FinanceData.Revenue.ToString();
        _customersSatisfactionText.text = ReportDataProcessor.CalculateCustomerSatisfaction(gameData) + "%";
        _goodDrinksText.text = (gameData.FinanceData.DrinksSold - gameData.FinanceData.BadDrinks).ToString();
        _badDrinksText.text = gameData.FinanceData.BadDrinks.ToString();

        // Loop up to the max index that the data has stars
        var starRating = ReportDataProcessor.CalculateStarRating(gameData);
        for (var i = 0; i < starRating; i++)
        {
            _stars[i].color = DTT.BeanOS_Report_App.Report_Accent;
        }
    }
}
