using DG.Tweening;
using UnityEngine;

public class BaseBeanOSCanvasGroup : MonoBehaviour
{
   [SerializeField] private CanvasGroup _canvasGroup;

   public virtual void ShowElement(bool show)
   {
      _canvasGroup.DOFade(show ? 1f : 0f, 0.2f);
   }
}
