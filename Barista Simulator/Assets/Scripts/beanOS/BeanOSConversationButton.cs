using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BeanOSConversationButton : MonoBehaviour
{
    public ContactData Contact => _contact;
    public int NotificationCount => _notificationCount;
    
    [SerializeField] private BeanOSMessagesApp _myApp;
    [SerializeField] private TMP_Text _contactNameText;
    [SerializeField] private Image _contactImage;
    [SerializeField] private Image _background;
    [SerializeField] private GameObject _notificationObject;
    [SerializeField] private TMP_Text _notificationText;
    [SerializeField] private Button _button;
    [SerializeField] private EventTrigger _eventTrigger;

    [TitleGroup("Normal Colors")]
    [SerializeField] private Color32 _backgroundNormalColor;
    [SerializeField] private Color32 _textNormalColor = Color.black;
    [TitleGroup("Selected Colors")]
    [SerializeField] private Color32 _backgroundSelectedColor;
    [SerializeField] private Color32 _textSelectedColor = Color.white;

    private int _notificationCount;
    private ContactData _contact;

    private void Awake()
    {
        var entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Select
        };
        entry.callback.AddListener(Selected);
        _eventTrigger.triggers.Add(entry);
        
        entry = new EventTrigger.Entry
        {
            eventID = EventTriggerType.Deselect
        };
        entry.callback.AddListener(Deselected);
        _eventTrigger.triggers.Add(entry);
        
        _button.onClick.AddListener(OnClick);
    }

    public void InitWithData(ContactData contactData)
    {
        _contactNameText.text = contactData.ContactName;
        _contactImage.sprite = contactData.ContactImage;
        _contact = contactData;
    }

    private void UpdateNotification()
    {
        _notificationObject.SetActive(_notificationCount > 0);
        _notificationText.text = _notificationCount.ToString();
    }

    private void Selected(BaseEventData arg0)
    {
        _background.color = _backgroundSelectedColor;
        _contactNameText.color = _textSelectedColor;
    }
    
    private void Deselected(BaseEventData arg0)
    {
        _background.color = _backgroundNormalColor;
        _contactNameText.color = _textNormalColor;
    }

    private void OnClick()
    {
        _myApp.OpenChatPage(_contact);
        _notificationCount = 0;
        UpdateNotification();
    }

    public void AddNotification()
    {
        _notificationCount++;
        UpdateNotification();
    }
}
