using System.Collections;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineController : MonoBehaviour
{
    [SerializeField] private PlayableDirector _director;
    
    [Range(1, 10)]
    [SerializeField] private int _minIdleLoopCount;
    [Range(1, 10)]
    [SerializeField] private int _maxIdleLoopCount;
    
    [Range(0f, 1f)] [SerializeField] private float _chanceToStartIdle;
    
    [SerializeField] private PlayableAsset _idleTimeline;
    [SerializeField] private PlayableAsset[] _timelines;

    private int _currentLoop;
    private int _desiredLoopCountBeforeChoosingAnotherTimeline;
    
    private void Awake()
    {
        CalculateWhatToPlayNext();
    }

    private void CalculateWhatToPlayNext()
    {
        if (Random.Range(0f, 1f) <= _chanceToStartIdle)
        {
            PlayIdleTimeline();
        }
        else
        {
            StartCoroutine(StartRandomTimeline());
        }
    }

    private void PlayIdleTimeline()
    {
        _desiredLoopCountBeforeChoosingAnotherTimeline = Random.Range(_minIdleLoopCount, _maxIdleLoopCount + 1);
        _director.playableAsset = _idleTimeline;
        _director.Play();
    }

    private IEnumerator StartRandomTimeline(float delay = 0f)
    {
        _desiredLoopCountBeforeChoosingAnotherTimeline = 1;
        yield return new WaitForSeconds(delay);
        var timelineToPlay = _timelines[Random.Range(0, _timelines.Length)];
        
        _director.playableAsset = timelineToPlay;
        _director.Play();
    }

    public void OnTimelineEnded()
    {
        _currentLoop++;
        if (_currentLoop >= _desiredLoopCountBeforeChoosingAnotherTimeline)
        {
            CalculateWhatToPlayNext();
        }
        else
        {
            PlayIdleTimeline();
        }
    }
}