using UnityEngine;
using UnityEngine.InputSystem;

public class OpenExternalURL : MonoBehaviour
{
   [SerializeField] private string _url;
   
   private void OpenURL(InputAction.CallbackContext _)
   {
      System.Diagnostics.Process.Start(_url);
   }

   public void OpenURL()
   {
      System.Diagnostics.Process.Start(_url);
   }
}
