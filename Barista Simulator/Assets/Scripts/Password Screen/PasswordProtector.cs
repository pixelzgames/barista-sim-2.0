using UnityEngine;
using UnityEngine.SceneManagement;

public class PasswordProtector : MonoBehaviour
{
    [SerializeField] private string _password;

    public void ValidatePassword(string password)
    {
        if (string.Equals(password, _password))
        {
            Destroy(gameObject);
            SceneManager.LoadScene(2);
        }
    }
}
