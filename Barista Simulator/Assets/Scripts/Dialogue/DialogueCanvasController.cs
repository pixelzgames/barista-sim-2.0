using System;
using Febucci.UI;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Class that handles the lifetime of a single dialogue
/// </summary>
public class DialogueCanvasController : BRAGCanvasController
{
    public event Action<DialogueConfig> OnFinished;

    [SerializeField] private TMP_Text _dialogueText;
    [SerializeField] private TypewriterByCharacter _typewriter;
    [SerializeField] private TMP_Text _nameText;

    
    private DialogueConfig _config;
    private bool _canSkip = true;
    private int _dialogueLineCount;
    private int _currentDialogueLine;
    
    public void Init(DialogueConfig config)
    {
        _config = config;
        _dialogueText.text = "";
        _nameText.text = _config.Contact.ContactData.ContactName;
        _dialogueLineCount = _config.DialogueLines.Length;

        var input = SingletonManager.Instance.InputManager.InputMaster.UI;
        input.Confirm.performed += ConfirmOnPerformed;
        SingletonManager.Instance.InputManager.ChangeInputMapping(input);
        EnableCanvas(0.5f);
        
        StartDialogue();
    }

    public void OnTextShowed()
    {
        _canSkip = false;
    }

    private void ConfirmOnPerformed(InputAction.CallbackContext obj)
    {
        if (!_canSkip)
        {
            NextDialogueLine();
            return;
        }

        SkipDialogueLine();
    }

    private void SkipDialogueLine()
    {
        _canSkip = false;
        _typewriter.SkipTypewriter();
    }

    private void NextDialogueLine()
    {
        _canSkip = true;

        if (_currentDialogueLine + 1 > _dialogueLineCount - 1)
        {
            CloseDialogue();
            return;
        }

        _currentDialogueLine++;
        _dialogueText.text = _config.DialogueLines[_currentDialogueLine];
    }

    private void StartDialogue()
    {
        _currentDialogueLine = -1;
        NextDialogueLine();
    }

    private void CloseDialogue()
    {
        var input = SingletonManager.Instance.InputManager.InputMaster.UI;
        input.Confirm.performed -= ConfirmOnPerformed;
        SingletonManager.Instance.InputManager.SetInputMappingToDefault();
        OnFinished?.Invoke(_config);
        Destroy(gameObject);
    }
}
