/* MIT License
Copyright (c) 2016 RedBlueGames
*/

using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;


public class BuildToolExample : MonoBehaviour
{
    [InitializeOnLoadMethod]
    public static void MyBuild()
    {
        // This gets the Build Version from Git via the `git describe` command
        PlayerSettings.bundleVersion = Git.BuildVersion;
        Debug.Log("version " + Git.BuildVersion);
    }

    [MenuItem("BRAG/Build/Build with Version")]
    public static void Build()
    {
        // ===== This sample is taken from the Unity scripting API here:
        // https://docs.unity3d.com/ScriptReference/BuildPipeline.BuildPlayer.html
        var buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = new[] { "Assets/Scenes/Boot.unity", "Assets/Main_Menu.unity", "Assets/Scenes/Home_01.unity", "Assets/Scenes/Cafe_01.unity" },
            locationPathName = "Builds",
            target = BuildTarget.StandaloneWindows,
            options = BuildOptions.None
        };

        var report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        var summary = report.summary;
    
        switch (summary.result)
        {
            case BuildResult.Succeeded:
                Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
                break;
            case BuildResult.Failed:
                Debug.Log("Build failed");
                break;
        }
    }
}