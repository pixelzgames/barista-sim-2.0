using DTT.COLOR;
using UnityEngine;

[CreateAssetMenu(fileName = "NewThoughtDisplayConfig", menuName = "BRAG/AI/Thoughts")]
public class ThoughtsDisplayConfig : ScriptableObject
{
    [field: SerializeField]
    public Sprite ThoughtIcon { get; private set; }
    
    [field: SerializeField, Min(0)]
    public int DisplayPriority { get; private set; }

    [field: SerializeField, TextArea] 
    public string DisplayText { get; private set; }
    
    [field: SerializeField] 
    public PaletteColorSelection DisplayColor { get; private set; }
    
    [field: SerializeField]
    public bool IsWaitingThought { get; private set; }
    
    [field: SerializeField]
    public bool IsOrderThought { get; private set; }
}
