using System;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "TagDataConfig", menuName = "BRAG/UI/Tag Data")]
public class TagConfig : ScriptableObject
{
    [field: SerializeField]
    public TagData TagData { get; private set; }
}

[Serializable]
public class TagData
{
    public Color32 GradiantColorA;
    public Color32 GradientColorB;
    public string TagName;
    public Color32 NameColor;
    public bool IsShiny;
    public bool HasShadow;
    [ShowIf("HasShadow")]
    public Color32 ShadowColor;
    public bool HasDropShadow;
}
