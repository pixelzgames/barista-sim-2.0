using System;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Gameplay Mechanic/Knocking Mechanic", fileName = "NewKnockingConfig")]
public class KnockingMechanicConfig : ScriptableObject
{
   [field: SerializeField]
   public KnockingMechanicData KnockingMechanicData { get; private set; }
}

[Serializable]
public class KnockingMechanicData
{
   public float NormalKnockNormalizedChargeThreshold = 0.45f;
   public float PerfectKnockNormalizedChargeThreshold = 0.65f;
   public float BadKnockNormalizedChargeThreshold = 0.75f;
   public float ChargingSpeed = 0.8f;
   public float DelayBetweenKnock = 0.2f;
   public float TimeForInactiveInputBeforeReset = 0.2f;
   public float SmoothingInputFactorUp = 20f;
   public float SmoothingInputFactorDown = 5f;
   public float ThresholdToKnock = 0.8f;
   public float ThresholdToCharged = 0.3f;
   public float ThresholdToGoingDown = 0.52f;
   public float MaxTimeToTriggerKnock = 0.5f;
   public AnimationCurve ChargingAnimationCurve;
   public int NoChargeKnocksToAdd = 1;
   public int NormalChargeKnocksToAdd = 2;
   public int MaxChargeKnocksToAdd = 4;
}
