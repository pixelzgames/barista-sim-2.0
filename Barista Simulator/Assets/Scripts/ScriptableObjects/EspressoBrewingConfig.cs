using GD.MinMaxSlider;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/EspressoUIConfigs")]
public class EspressoBrewingConfig : ScriptableObject
{
    [field: SerializeField, Range(90f, 360f)] public float EspressoRangePosition { get; private set; }
    [field: SerializeField, Range(0f, 50f)] public float EspressoRangeAnglePercentage { get; private set; }
    [field: SerializeField, Range(0f, 50f)] public float LungoRangeAnglePercentage { get; private set; }
    [field: SerializeField, MinMaxSlider(0f, 1f)] public Vector2 GodshotAngleAngleAndPosition { get; private set; }
    [field: SerializeField] public float ExtractionTime { get; private set; }
    [field: SerializeField] public AnimationCurve ExtractionCurve { get; private set; }

    public EspressoBrewingResult CalculateBrewingResult(float fillAmount)
    {
        EspressoBrewingResult result;

        var tooQuickRange = EspressoRangePosition / 360;
        var espressoRange = EspressoRangeAnglePercentage / 100;
        var lungoRange = LungoRangeAnglePercentage / 100;
        if (fillAmount < tooQuickRange)
        {
            result = EspressoBrewingResult.Bad;
        }
        else if (fillAmount < tooQuickRange + espressoRange)
        {
            var x = GodshotAngleAngleAndPosition.x * espressoRange;
            var y = GodshotAngleAngleAndPosition.y * espressoRange;
            
            if (fillAmount > tooQuickRange + x && fillAmount < tooQuickRange + y)
            {
                result = EspressoBrewingResult.Godshot;
            }
            else
            {
                result = EspressoBrewingResult.Espresso;
            }
        }
        else if (fillAmount < tooQuickRange + espressoRange + lungoRange)
        {
            result = EspressoBrewingResult.Lungo;
        }
        else
        {
            result = EspressoBrewingResult.Bad;
        }
            
        return result;
    }

    // Return an ideal filling amount to where to stop for an espresso shot
    public float GetEspressoTarget()
    {
        var tooQuickRange = EspressoRangePosition / 360;
        var espressoRange = EspressoRangeAnglePercentage / 100;
        var target = tooQuickRange + espressoRange/2;

        return target;
    }
}

public enum EspressoBrewingResult
{
    None,
    Espresso,
    Godshot,
    Lungo,
    Bad
}
