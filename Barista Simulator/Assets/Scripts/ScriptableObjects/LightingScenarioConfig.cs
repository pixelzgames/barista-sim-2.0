using UnityEngine;

[CreateAssetMenu(fileName = "LightingScenarioConfig", menuName = "BRAG/LightingScenarioConfig")]
public class LightingScenarioConfig : ScriptableObject
{
    public string ScenarioName;
    public Vector3 LightDirection;
    public Color32 LightColor;
    public float LightIntensity;
    public Color32 EnvironmentColor;
    public Material Skybox;
    public float AmbientLightIntensity;
}
