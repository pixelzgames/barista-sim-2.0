using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/XP System/XP Configs", fileName = "NewXPConfig")]
public class XPSystemConfig : ScriptableObject
{
    [field: SerializeField] public AnimationCurve XPNeededPerLevelAnimationCurve { get; private set; }
    [field: SerializeField] public LevelReward[] RewardsPerLevel { get; private set; }
}

[System.Serializable]
public struct LevelReward
{
    public int BeanPoints;
}

