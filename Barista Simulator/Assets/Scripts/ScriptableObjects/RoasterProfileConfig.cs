using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "BRAG/Roaster/Profile", fileName = "NewRoasterProfile")]
public class RoasterProfileConfig : ScriptableObject
{
   public RoasterProfile[] RoastProfiles;
}

[System.Serializable]
public class RoasterProfile
{
   public RoastType Roast;
   [Tooltip("In seconds")]
   public float TimeToReach;
}
