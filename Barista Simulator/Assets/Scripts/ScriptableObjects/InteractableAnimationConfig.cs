using UnityEngine;

[CreateAssetMenu(fileName = "NewInteractableAnimationConfig", menuName = "BRAG/Interactable Animation Config")]
public class InteractableAnimationConfig : ScriptableObject
{
    public bool LockPlayerMovement;
    public bool PositionPlayerInFront;
    public bool PlaceInMachineSocketWhileInteracting;
    public bool ForceLookAt = true;
    public InteractableAnimationType AnimationToPlay;
    public AnimationActionDelayType AnimationDelay;
    public bool NeedsBothHands;
    public Pickable ObjectToSpawnInHands;
}

public enum AnimationActionDelayType
{
    Instant,
    InputLength
}

public enum InteractableAnimationType
{
    None,
    Pickup,
    Charge,
    Throw,
    Fill,
    Interact,
    Pour,
    Grinder_PressButton,
    Grinder_Fill,
    Grinder_PlaceAndGrind
}
