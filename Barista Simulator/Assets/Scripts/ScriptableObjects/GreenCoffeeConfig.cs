using System;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCoffee", menuName = "BRAG/Coffee/Coffee")]
public class GreenCoffeeConfig : ScriptableObject
{
    [field: SerializeField] public CoffeeOriginConfig CoffeeOrigin { get; private set; }
    [field: SerializeField, Space(10), Title("Quality"),ListDrawerSettings(ShowFoldout = false)] 
    public CoffeeQualityConfig[] PossibleCoffeeQualityConfigs { get; private set; }
    [field: SerializeField]  public GreenCoffeeData PossibleFlavourNotes { get; private set; }
}

[Serializable]
public class GreenCoffeeData
{
    [HorizontalGroup(Title = "Notes per roast level", Gap = 10), LabelText("Light Notes")]
    public TastingDescriptor[] PossibleLightRoastNotes;

    [HorizontalGroup, LabelText("Medium Notes")]
    public TastingDescriptor[] PossibleMediumRoastNotes;

    [HorizontalGroup, LabelText("Dark Notes")]
    public TastingDescriptor[] PossibleDarkRoastNotes;
}

