using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/AI/Tasting Note List", fileName = "AvailableTastingNotesList")]
public class TastingNotesToChooseFromConfig : ScriptableObject
{
   [field: SerializeField] public TastingDescriptor[] AvailableTastingNotes { get; private set; }

   public TastingDescriptor DrawRandomTastingNote()
   {
      var randomNumber = Random.Range(0, AvailableTastingNotes.Length);
      return AvailableTastingNotes[randomNumber];
   }
}
