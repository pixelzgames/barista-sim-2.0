﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "NewRecipeListConfig", menuName = "BRAG/Recipes/Recipe List")]
public class RecipeConfigList : ScriptableObject
{
    public List<RecipeConfig> Recipes;
    
    public RecipeConfig GetRecipeConfigByType(RecipeType type)
    {
        return Recipes.Where(recipe => recipe.RecipeType == type).Select(recipe => recipe).FirstOrDefault();
    }
}