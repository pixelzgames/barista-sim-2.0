using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/AI/Expectations Config", fileName = "NewExpectationsConfig")]
public class ExpectationsConfig : ScriptableObject
{
    [field: SerializeField, Header("Expectation with traits")]
    public int MinExpectationTraitAmount { get; private set; }
    [field: SerializeField]
    public int MaxExpectationTraitAmount { get; private set; }
    
    [field: SerializeField,Header("Expectation binaries"),Range(0,100)]
    public float ChanceOfGettingBinary { get; private set; }
    [field: SerializeField]
    public int MinExpectationBinaryAmount { get; private set; }
    [field: SerializeField]
    public int MaxExpectationBinaryAmount { get; private set; }
    [field: SerializeField]
    public ExpectationListConfig Binaries{ get; private set; }
    
    [field: SerializeField,Header("Needs"),Range(0,100)]
    public float ChanceOfGettingNeed { get; private set; }
    [field: SerializeField]
    public int MinNeedAmount { get; private set; }
    [field: SerializeField]
    public int MaxNeedAmount { get; private set; }
    [field: SerializeField]
    public ExpectationListConfig Needs { get; private set; }
}