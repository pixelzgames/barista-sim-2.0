using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCoffeeMarket", menuName = "BRAG/Store/Coffee Store Market")]
public class CoffeeMarketStoreConfig : ScriptableObject
{
    [field: SerializeField] public CoffeeMarketStore CoffeeProductsMarket { get; private set; }
}

[System.Serializable]
public class CoffeeMarketStore
{
    [MinMaxSlider(5, 50, true)] public Vector2 MinMaxPrice;
    public GreenCoffeeConfig[] PossibleCoffeesOffering;
}