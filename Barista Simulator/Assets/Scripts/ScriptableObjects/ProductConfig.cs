﻿using Sirenix.OdinInspector;
using UnityEngine;
// ReSharper disable InconsistentNaming

/// <summary>
/// Scriptable Object to hold the data needed to spawn and display products bought by the player in the PurchaseMenu.
/// </summary>
[CreateAssetMenu(fileName = "New Product", menuName = "BRAG/Store/Product")]
public class ProductConfig : ScriptableObject
{
    [field: SerializeField] public Product Product { get; private set; }
}

[System.Serializable]
public class Product
{
    public string ProductName;
    [PreviewField]
    public Sprite Icon;
    public int Price;
    public int DeliveryTimeInDays;
    public GameObject ProductPrefab;
    
    public CoffeeData CoffeeData { get; private set; }
    
    public void SetCoffeeData(GreenCoffeeConfig data)
    {
        CoffeeData = CoffeeConfigUtility.ConvertConfigToData(data);
    }
}