using UnityEngine;

[CreateAssetMenu(fileName = "RadialMenuElementData",menuName = "BRAG/Radial Menu/Element Data")]
public class RadialMenuElementConfig : ScriptableObject
{
   [field: SerializeField] public string Name { get; private set; }
   [field: SerializeField] public Sprite Icon { get; private set; }
   [field: SerializeField] public Sprite SelectedIcon { get; private set; }

   public InteractionMode OnAction;
}
