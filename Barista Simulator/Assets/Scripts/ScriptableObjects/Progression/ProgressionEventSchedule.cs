using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Progression/Schedule", fileName = "NewProgressionSchedule")]
public class ProgressionEventSchedule : ScriptableObject
{
    [field: SerializeField]
    public ProgressionDayConfig[] ProgressionDays { get; private set; }
}