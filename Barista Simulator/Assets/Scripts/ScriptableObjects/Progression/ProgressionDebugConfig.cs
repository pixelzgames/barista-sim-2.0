using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Debug/Editor Debug Configs")]
public class ProgressionDebugConfig : ScriptableObject
{
    public int CurrentDay;
    public string HomeLevelName;
    public string CafeLevelName;
    public bool HasPhoneUnlocked;
}
