using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "BRAG/Progression/Day", fileName = "NewProgressionDay")]
public class ProgressionDayConfig : ScriptableObject
{
    [field: SerializeField] 
    public ProgressionEvent[] DailyProgressionEvents { get; private set; }
}

[System.Serializable]
public class ProgressionEvent
{
    [EnumToggleButtons] public ProgressionEventType EventType;
    
    [Space(20f)]
    [BoxGroup("Event Data")]
    [ShowIf("EventType", ProgressionEventType.Message)]
    [JsonIgnore]
    public BeanOSMessageConfig TextMessage;
    
    [Space(20f)]
    [BoxGroup("Event Data")]
    [ShowIf("EventType", ProgressionEventType.Task)]
    [JsonIgnore]
    public TaskConfig TaskConfig;
    
    [FormerlySerializedAs("CutsceneData")]
    [Space(20f)]
    [BoxGroup("Event Data")]
    [ShowIf("EventType", ProgressionEventType.Cutscene)]
    [JsonIgnore]
    public CutsceneConfig _cutsceneConfig;
    
    [FormerlySerializedAs("DialogueData")]
    [Space(20f)]
    [BoxGroup("Event Data")]
    [ShowIf("EventType", ProgressionEventType.Dialogue)]
    [JsonIgnore]
    public DialogueConfig _dialogueConfig;
    
    //[BoxGroup("Event to trigger")]
    //[HideIf("EventType", ProgressionEventType.Spawn)]
    // TODO 
    //public SpawnableData TextMessage;
    
    [BoxGroup("Event to trigger")]
    [HideIf("EventType", ProgressionEventType.None)]
    public GameEvent EventToTrigger;
    
    public bool Completed { get; set; }
}

public enum ProgressionEventType
{
    None = 0,
    Message = 1,
    Dialogue = 2,
    Spawn = 3,
    Task = 4,
    Cutscene = 5,
}
