using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Character Creator/Color Data", fileName = "NewCharacterCreatorColorData")]
public class CharacterCreatorColorConfig : ScriptableObject
{
    public Color32 MainColor;
    public Color32 AccentColor;
}
