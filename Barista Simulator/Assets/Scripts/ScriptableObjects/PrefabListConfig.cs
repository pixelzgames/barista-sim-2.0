using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Prefabs", fileName = "New Prefab List")]
public class PrefabListConfig : ScriptableObject
{
    public PrefabDictionary PrefabData => _prefabList;
    
    [SerializeField]
    private PrefabDictionary _prefabList;
}
