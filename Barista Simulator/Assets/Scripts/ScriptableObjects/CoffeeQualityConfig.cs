using System;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCoffeeQualityConfig", menuName = "BRAG/Coffee/Coffee Quality Config")]
public class CoffeeQualityConfig : ScriptableObject
{
   [HorizontalGroup, HideLabel]
   [field: SerializeField] public Quality Quality { get; private set; }
   [TableList, HorizontalGroup]
   [field: SerializeField] public int Slots { get; private set; }
}

[Serializable]
public class CoffeeQuality
{
   [HorizontalGroup, HideLabel]
   public Quality Quality;
   [TableList, HorizontalGroup]
   public CoffeeDescriptorSlot[] TastingDecriptorSlots;
}

[Serializable]
public class CoffeeDescriptorSlot
{
   public CoffeeDescriptor CoffeeDescriptor;
}
