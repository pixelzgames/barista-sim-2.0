using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Object Theme Data", fileName = "NewObjectThemeData")]
public class ObjectTheme : ScriptableObject
{
   [field: SerializeField] public string SkinName { get; private set; }
   [field: SerializeField] public List<MaterialList> MaterialsPerRenderer { get; private set; }
   [field: SerializeField] public Sprite Thumbnail { get; private set; }

   public string Id => name + "_" + SkinName;
}

[Serializable]
public struct MaterialList
{
   public Material[] Materials;
}
