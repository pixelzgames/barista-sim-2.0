using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/AI/Expectation By Traits List Config", fileName = "NewExpectationTraitList")]
public class ExpectationsTraitsList : ScriptableObject
{
    [field: SerializeField] public ExpectationConfig[] ExpectationsByTraits { get; private set; }
}
