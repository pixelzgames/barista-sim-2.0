using UnityEngine;
// ReSharper disable InconsistentNaming

[CreateAssetMenu(fileName = "NewCoffeeDatabase", menuName = "BRAG/Coffee/Coffee Database")]
public class CoffeeDatabaseConfig : ScriptableObject
{
    public GreenCoffeeConfig[] AllCoffees;
}
