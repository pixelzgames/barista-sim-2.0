using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSocialPostDisplayConfig", menuName = "BRAG/AI/SocialPostDisplayConfig")]
public class SocialPostDisplayConfig : ScriptableObject
{
    [field: SerializeField]
    public List<SocialPostContent> ContentPool { get; private set; }

    public SocialPostContent GetRandomInPool()
    {
        return ContentPool[Random.Range(0, ContentPool.Count)];
    }
}
