using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Config/Game Mode", fileName = "DefaultGameMode")]
public class GameModeConfig : ScriptableObject
{
    [field: SerializeField]
    public GameConfigData GameConfigToLoad { get; private set; }
}
