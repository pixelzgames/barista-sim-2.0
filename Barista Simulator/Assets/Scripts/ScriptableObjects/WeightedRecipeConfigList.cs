using UnityEngine;
using WeightedDistribution;

[System.Serializable]
[CreateAssetMenu(fileName = "NewWeightedRecipeListConfig", menuName = "BRAG/Recipes/Weighted Recipe List")]
public class WeightedRecipeConfigList : Distribution<RecipeConfig, RecipeDataListDistributionItem>
{
}

[System.Serializable]
public class RecipeDataListDistributionItem : DistributionItem<RecipeConfig> {}