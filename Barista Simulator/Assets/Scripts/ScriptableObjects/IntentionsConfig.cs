using UnityEngine;
using WeightedDistribution;

[CreateAssetMenu(menuName = "BRAG/AI/Intentions Config",fileName = "NewIntentionsConfig")]
public class IntentionsConfig : Distribution<CustomerIntention, IntentionDistributionItem>
{
}

[System.Serializable]
public class IntentionDistributionItem : DistributionItem<CustomerIntention> {}