using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Dialogue", fileName = "NewDialogueData")]
public class DialogueConfig : ScriptableObject
{
    [field: SerializeField]
    public BeanOSContactConfig Contact { get; private set; }
    
    [field: SerializeField, TextArea]
    public string[] DialogueLines { get; private set; }

    [field: SerializeField, Tooltip("Dialogue is popped as soon as it is added to Dialogue dictionary.")]
    public bool Instant { get; private set; }

    public void ScriptableObjectConstructor(string[] lines)
    {
        Contact = CreateInstance<BeanOSContactConfig>();
        DialogueLines = lines;
    }
}
