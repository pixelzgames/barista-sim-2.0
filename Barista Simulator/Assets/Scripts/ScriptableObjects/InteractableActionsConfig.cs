using UnityEngine;

[CreateAssetMenu(fileName = "InteractableActionsConfig", menuName = "BRAG/InteractableActionsConfig")]
public class InteractableActionsConfig : ScriptableObject
{
    [field: SerializeField] public ActionInputConfig[] Actions { get; private set; }
}
