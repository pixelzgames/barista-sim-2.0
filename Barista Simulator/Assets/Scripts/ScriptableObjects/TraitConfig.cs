using UnityEngine;
using WeightedDistribution;

[CreateAssetMenu(menuName = "BRAG/AI/Traits Config",fileName = "NewTraitsConfig")]
public class TraitConfig : Distribution<TraitType, TraitDistributionItem>
{
    [field: SerializeField]
    public int MinTraitAmount { get; private set; }
    [field: SerializeField]
    public int MaxTraitAmount { get; private set; }
}

[System.Serializable]
public class TraitDistributionItem : DistributionItem<TraitType> {}