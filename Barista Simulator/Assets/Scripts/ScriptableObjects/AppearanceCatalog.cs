using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/Appearance/AppearanceCatalog", fileName = "NewAppearanceCatalog")]
public class AppearanceCatalog : ScriptableObject
{
    [field: SerializeField] public List<CustomizableCatalog> CustomizableCatalogs { get; private set; }
    
    public Appearance GenerateRandomAppearance()
    {
        var glassesStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Glasses].Customizables.Count);
        var glassesColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Glasses].Customizables[glassesStyle].AvailableThemes.Count);

        var eyeStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Eyes].Customizables.Count);
        var eyeColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Eyes].Customizables[eyeStyle].AvailableThemes.Count);

        var facialHairStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.FacialHair].Customizables.Count);
        var facialHairColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.FacialHair].Customizables[facialHairStyle].AvailableThemes.Count);

        var apronStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Apron].Customizables.Count);
        var apronColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Apron].Customizables[apronStyle].AvailableThemes.Count);

        var shirtStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Shirt].Customizables.Count);
        var shirtColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Shirt].Customizables[shirtStyle].AvailableThemes.Count);

        var pantsStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Pants].Customizables.Count);
        var pantsColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Pants].Customizables[pantsStyle].AvailableThemes.Count);

        var hatStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Hat].Customizables.Count);
        var hatColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Hat].Customizables[hatStyle].AvailableThemes.Count);

        var skinStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Skin].Customizables.Count);
        var skinColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Skin].Customizables[0].AvailableThemes.Count);
        
        var shoeStyle = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Shoes].Customizables.Count);
        var shoeColor = Random.Range(0, CustomizableCatalogs[(int)CustomizableType.Shoes].Customizables[shoeStyle].AvailableThemes.Count);

        return new Appearance(glassesStyle, glassesColor, eyeStyle, eyeColor, facialHairStyle, facialHairColor, apronStyle, apronColor, shirtStyle, shirtColor, pantsStyle, pantsColor, hatStyle, hatColor, skinStyle, skinColor, shoeStyle, shoeColor);
    }
}
