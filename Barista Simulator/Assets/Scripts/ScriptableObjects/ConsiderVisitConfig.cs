using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/AI/Consider Visit Config",fileName = "NewConsiderConfig")]
public class ConsiderVisitConfig : ScriptableObject
{
   [field: SerializeField, Range(0,100)]
   public float BaseChanceToVisit { get; private set; }
}