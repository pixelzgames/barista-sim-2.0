using UnityEngine;

[CreateAssetMenu(fileName = "NewCutsceneData", menuName = "BRAG/CutsceneData")]
public class CutsceneConfig : ScriptableObject
{
    public CutscenePlayer CutscenePlayer;
}
