using UnityEngine;
using WeightedDistribution;

[System.Serializable]
[CreateAssetMenu(menuName = "BRAG/AI/Expectation List", fileName = "NewExpectationList")]
public class ExpectationListConfig : Distribution<ExpectationConfig, ExpectationListDistributionItem>
{
    
}

[System.Serializable]
public class ExpectationListDistributionItem : DistributionItem<ExpectationConfig> {}
