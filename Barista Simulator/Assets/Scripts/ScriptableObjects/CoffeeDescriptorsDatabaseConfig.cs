using System;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDescriptorDatabase", menuName = "BRAG/Coffee/Coffee Descriptor Database")]
public class CoffeeDescriptorsDatabaseConfig : ScriptableObject
{
    public CoffeeDescriptorsDictionary CoffeeDescriptorsDictionary;
}

[Serializable]
public class CoffeeDescriptor
{
    public TastingDescriptor TastingNote;
    public CoffeeDescriptorScheme ColorScheme { get; private set; }

    public CoffeeDescriptor(TastingDescriptor tastingDescriptor = TastingDescriptor.OPENED, CoffeeDescriptorScheme scheme = null)
    {
        TastingNote = tastingDescriptor;
        if (scheme != null)
        {
            ColorScheme = scheme;
            return;
        }
        
        if (CoffeeConfigUtility.CoffeeDescriptorsDatabaseConfig.CoffeeDescriptorsDictionary.TryGetValue(TastingNote, 
                out var colorValue))
        {
            ColorScheme = colorValue;
        }
    }
}

[Serializable]
public class CoffeeDescriptorScheme
{
    public Color32 FontColor;
    public Color32 ShadowColor;
    public Gradient Gradient;
}
