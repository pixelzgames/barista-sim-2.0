using UnityEngine;
using WeightedDistribution;

[System.Serializable]
[CreateAssetMenu(fileName = "NewChooseDrinkConfig", menuName = "BRAG/AI/Choose Drink Config")]
public class ChooseDrinkConfig : Distribution<ChooseCoffeeType, ChooseCoffeeDistributionItem>
{

}

[System.Serializable]
public class ChooseCoffeeDistributionItem : DistributionItem<ChooseCoffeeType> {}

public enum ChooseCoffeeType
{
   FromMenu,
   NotOnMenu
}