using UnityEngine;

[CreateAssetMenu(fileName = "NewCoffeeOrigin", menuName = "BRAG/Coffee/Origin")]
public class CoffeeOriginConfig : ScriptableObject
{
    [field: SerializeField] public CoffeeOrigin CoffeeOrigin { get; private set; }
}
