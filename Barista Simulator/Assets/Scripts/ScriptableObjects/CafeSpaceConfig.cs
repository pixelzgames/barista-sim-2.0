using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "BRAG/Cafe/ Space Config", fileName = "NewCafeSpaceConfig")]
public class CafeSpaceConfig : ScriptableObject
{
    [field: SerializeField]
    public float MaxAttractivenessForThisSpace { get; private set; }
    [field: SerializeField]
    public float MaxCleanlinessForThisSpace { get; private set; }
    [field: SerializeField]
    public float MaxComfortForThisSpace { get; private set; }
}
