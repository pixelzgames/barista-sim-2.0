using Sirenix.OdinInspector;
using UnityEngine;

[System.Serializable]
public class CoffeeOrigin
{
   [field:SerializeField]
   public Origin Origin { get; private set; }
   [field:SerializeField, PreviewField]
   public Sprite OriginSprite { get; private set; }

   public CoffeeOrigin(Origin origin, Sprite originSprite)
   {
      Origin = origin;
      OriginSprite = originSprite;
   }
}

public enum Origin
{
   Various,
   Burundi,
   Congo,
   Ethiopia,
   Kenya,
   Malawi,
   Rwanda,
   Tanzania,
   Uganda,
   Zambia,
   China,
   India,
   Indonesia,
   Philippines,
   Thailand,
   Vietnam,
   Yemen,
   Bolivia,
   Brazil,
   Colombia,
   CostaRica,
   Cuba,
   DominicanRepublic,
   Ecuador,
   ElSalvador,
   Guatemala,
   Haiti,
   Hawaii,
   Honduras,
   Jamaica,
   Mexico,
   Nicaragua,
   Panama,
   Peru,
   Venezuela
};


