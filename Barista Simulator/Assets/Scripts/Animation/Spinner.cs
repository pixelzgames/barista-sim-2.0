using UnityEngine;

public class Spinner : MonoBehaviour
{
    [SerializeField] private float _spinningDegreesPerSecond = 45f;
    [SerializeField] private Vector3 _spinningAxis;

    private bool _spinning;
    
    public void StartSpinning()
    {
        _spinning = true;
    }

    public void StopSpinning()
    {
        _spinning = false;
    }
    
    // Update is called once per frame
    private void Update()
    {
        if (!_spinning)
        {
            return;
        }
        
        transform.Rotate(_spinningAxis, _spinningDegreesPerSecond * Time.deltaTime);
    }
}
