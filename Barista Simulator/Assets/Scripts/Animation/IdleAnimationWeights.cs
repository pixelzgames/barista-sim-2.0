using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewIdleAnimationWeights", menuName = "BRAG/Animations/IdleAnimationWeights")]
public class IdleAnimationWeights : ScriptableObject
{
    [SerializeField]
    private List<IdleAnimationWeight> _idleAnimations;

    public int Count => _idleAnimations.Count;

    public IdleAnimationType GetRandomAnimationByWeight()
    {
        var sumOfWeights = 0;
        for (int i = 0; i < _idleAnimations.Count; i++)
        {
            sumOfWeights += _idleAnimations[i].Weight;
        }

        var randomValue = UnityEngine.Random.Range(0, sumOfWeights);
        
        foreach (var t in _idleAnimations)
        {
            if (randomValue < t.Weight)
            {
                return t.IdleAnimationType;
            }
            randomValue -= t.Weight;
        }

        return 0;
    }
}

public enum IdleAnimationType
{
    Default,
    ScratchingHead,
    LookingAtPhone,
    Stretching,
    LookingAround,
    ScratchingArm
}

[Serializable]
public class IdleAnimationWeight
{
    [field: SerializeField]
    public IdleAnimationType IdleAnimationType { get; private set; }

    [field: SerializeField]
    public int Weight { get; private set; }
}
