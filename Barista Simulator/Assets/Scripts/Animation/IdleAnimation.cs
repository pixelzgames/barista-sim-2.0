using UnityEngine;

public class IdleAnimation : StateMachineBehaviour
{
    [SerializeField]
    private Vector2 _timeRangeUntilIdleVariation;

    [SerializeField]
    private IdleAnimationWeights _idleAnimationWeights;

    private bool _isTimeToSwitch;
    private IdleAnimationType _idleAnimationType;
    private float _timeUntilVariation;
    private float _idleTime;
    
    private static readonly int WalkSpeed = Animator.StringToHash("walk_speed");
    private static readonly int Idles = Animator.StringToHash("idles");

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SetNewBoredTime();
        ResetIdle();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Reset Timer if walkspeed is above zero
        if (animator.GetFloat(WalkSpeed) > 0.01f)
        {
            ResetIdle();
        }

        if (!_isTimeToSwitch)
        {
            _idleTime += Time.deltaTime;

            if (_idleTime > _timeUntilVariation && stateInfo.normalizedTime % 1 < 0.02f)
            {
                _isTimeToSwitch = true;
                _idleAnimationType = _idleAnimationWeights.GetRandomAnimationByWeight();
                animator.SetFloat(Idles, (int)_idleAnimationType);
            }
        }
        else if (stateInfo.normalizedTime % 1 > 0.98)
        {
            ResetIdle();
        }

        animator.SetFloat(Idles, (int)_idleAnimationType);
    }

    private void ResetIdle()
    {
        if (_isTimeToSwitch)
        {
            _idleAnimationType = 0;
            SetNewBoredTime();
        }

        _isTimeToSwitch = false;
        _idleTime = 0f;
    }

    private void SetNewBoredTime()
    {
        _timeUntilVariation = Random.Range(_timeRangeUntilIdleVariation.x, _timeRangeUntilIdleVariation.y);
    }
}
