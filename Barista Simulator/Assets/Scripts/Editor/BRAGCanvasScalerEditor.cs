using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(BRAGCanvasScaler), true)]
[CanEditMultipleObjects]
public class BRAGCanvasScalerEditor : CanvasScalerEditor
{
   SerializedProperty _uiType;
   protected override void OnEnable()
   {
      base.OnEnable();
      
      _uiType = serializedObject.FindProperty("_uiType");
   }

   public override void OnInspectorGUI()
   {
      EditorGUILayout.PropertyField(_uiType);
      base.OnInspectorGUI();
   }
}
