﻿using UnityEditor;

[CustomEditor(typeof(Cup))]
public class CupEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space(10);

        Cup cupScript = (Cup)target;
        cupScript.RefreshCupLook();
    }
}
