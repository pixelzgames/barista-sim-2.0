using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

public class GameModeWindowTool : OdinEditorWindow
{
    [MenuItem("BRAG/Game Mode Settings")]
    public static void OpenWindow()
    {
        InspectObject(Resources.Load("CurrentGameMode"));
    }
}
