using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AI;
using AI.Structure;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable InconsistentNaming on struct

public class CustomersController : MonoBehaviour
{
    public GameConfigAIData CustomerConfigData { get; private set; }
    
    [Header("Customer")]
    [SerializeField] private GameObject _customerPrefab;
    [SerializeField] private CustomerLineup _customerLineup;
    [SerializeField] private Door[] _doors;   
    [SerializeField] private Transform _saleCounter;

    [Header("Waiting Area")] [SerializeField]
    private WaitingArea[] _waitingAreas;

    [SerializeField] 
    private Transform[] _entriesExits;

    public CustomerLineup Lineup => _customerLineup;
    public Door RandomDoor => _doors[Random.Range(0, _doors.Length)];
    public Transform SaleCounter => _saleCounter;
    
    private readonly List<NPCCustomer> _allocatedCustomers = new();
    
    private Coroutine _coroutine;
    private float _timer;
    private float _timeToWait;

    private void Awake()
    {
        // Hide the initial prefab used for pooling 
        _customerPrefab.gameObject.SetActive(false);
        CustomerConfigData = GameConfig.GetConfigData().AI;
    }
    
    private void Start()
    {
        // Start spawning npcs from start
        InitiateSpawning();
    }

    private void OnDestroy()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
            _coroutine = null;
        }
    }

    private void InitiateSpawning()
    {
        _timer = 0f;
        _timeToWait = Random.Range(CustomerConfigData.CustomersConfigs.CustomerSpawnDelayMinMax.x,
            CustomerConfigData.CustomersConfigs.CustomerSpawnDelayMinMax.y);
        
        _coroutine = StartCoroutine(RandomSpawner());
    }

    private IEnumerator RandomSpawner()
    {
        while (_timer < _timeToWait)
        {
            _timer += Time.deltaTime;
            yield return null;
        }

        if (!TryToSpawnCustomer())
        {
            Debug.LogError("Error: Could not spawn customer!");
        }
        
        InitiateSpawning();
    }

    private bool TryToSpawnCustomer()
    {
        if (SingletonManager.Instance.CafeManager == null || _allocatedCustomers.Count >= CustomerConfigData.CustomersConfigs.MaxAllocatedCustomersInWorld)
        {
            return false;
        }

        AllocateCustomer();

        if (SingletonManager.Instance.CafeManager.IsCafeOpen)
        {
            SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().CustomerData.TotalPassersby++;
        }

        return true;
    }
    
    private void AllocateCustomer()
    {
        //Allocate customer
        var npc = Instantiate(_customerPrefab, _customerPrefab.transform.position, Quaternion.identity).GetComponent<NPCCustomer>();
        npc.gameObject.SetActive(true);
        npc.InitAllocation(this);
        _allocatedCustomers.Add(npc);
    }
    
    /// <summary>
    /// Get the Entry position to spawn from and an Exit position to go
    /// </summary>
    /// <param name="exitPosition">The exit position that is NOT the entry one</param>
    /// <returns>An entry position to spawn from</returns>
    public Vector3 GetRandomEntryExitPosition(out Vector3 exitPosition)
    {
        var rand = Random.Range(0, _entriesExits.Length);
            
        var listWithoutEntryPoint = _entriesExits.Where((_, i) => i != rand).ToArray();
        exitPosition = GenerateRandomPointOnSurface(listWithoutEntryPoint);

        var entry = new [] {_entriesExits[rand]};
        return GenerateRandomPointOnSurface(entry);
    }

    /// <summary>
    /// Called by the NPC itself
    /// </summary>
    /// <param name="npc"></param>
    public void UnAllocateCustomer(NPCCustomer npc)
    {
        _allocatedCustomers.Remove(npc);
        Destroy(npc.gameObject);
    }
    
    /// <summary>
    /// Generates a random point in the waiting area and verifies that the new point is far enough away from existing points in _waitingAreaDict.
    /// </summary>
    /// <returns></returns>
    public Vector3 GenerateRandomPointOnSurface(Transform[] transforms)
    {
        var randomArea = transforms[Random.Range(0, transforms.Length)].GetComponent<Renderer>();
        
        // Generate random coord
        var position = randomArea.transform.position;
        var posX = position.x;
        var scale = randomArea.bounds;
        var extentX = scale.extents.x;
        var posZ = position.z;
        var extentZ = scale.extents.z;
            
        var randX = Random.Range(posX - extentX, posX + extentX);
        var randZ = Random.Range(posZ - extentZ, posZ + extentZ);

        var coord = new Vector3(randX, position.y, randZ);
        
        return coord;
    }

    public Transform[] GetTransformsForSurfaceType(AIPositionSurfaceAreaType type)
    {
        var areaArray = Array.Empty<Transform>();
        foreach (var area in _waitingAreas)
        {
            if (area.Type != type)
            {
                continue;
            }

            areaArray = area.WaitingTransforms;
        }

        return areaArray;
    }
}

[Serializable]
public class WaitingArea
{
    public Transform[] WaitingTransforms;
    public AIPositionSurfaceAreaType Type;
}

public enum AIPositionSurfaceAreaType
{
    ConsiderVisit,
    ConsiderOrder,
    Order,
}
