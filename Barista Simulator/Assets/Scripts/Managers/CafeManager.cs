﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI;
using UnityEngine;

public class CafeManager : MonoBehaviour
{
    public event Action<bool> CafeStateChange = delegate {  };
    public event Action TakingOrder = delegate { };
    
    [field: SerializeField] public Cup CupPrefab { get; private set; }
    
    public bool IsCafeOpen { get; private set; }
    public bool WasCafeEverOpenedThatDay { get; private set; }
    public float TimeLeftInDayNormalized { get; private set; }
    public bool IsCafeAtFullCapacity => _customersVisiting.Count >= GameConfig.Data.AI.CustomersConfigs.MaxCustomersVisiting;
    
    // World Attributes
    public List<WorldAttribute> CurrentWorldAttributes { get; private set; } = new();
    public int Attractiveness { get; private set; }
    public int Cleanliness { get; private set; }
    public int Comfort { get; private set; }
    public bool Wifi { get; private set; }
    public bool Music { get; private set; }
    public List<Seat> Seats { get; private set; }

    [SerializeField] private CafeSpaceConfig _cafeSpaceConfig;

    [SerializeField]
    [Tooltip("Reference to product box prefab.")]
    private GameObject _productBoxPrefab;

    private float _currentTimeDay;
    private float _dayLength;
    private readonly List<NPCCustomer> _customersVisiting = new ();

    private readonly List<Shipment> _shipments = new();
    private ShipmentHandler _shipmentHandler;
    
    private void OnEnable()
    {
        SingletonManager.Instance.GameManager.ChangedDay += OnChangedDay;
        SingletonManager.Instance.GameManager.LevelLoaded += OnLevelLoaded;
    }

    private void OnLevelLoaded(LevelData levelData)
    {
        if (levelData.LevelType != Level.Cafe)
        {
            return;
        }
        
        InitCafeData();
        ReceiveShipments();
        var gameEvent = new GameEvent(GameEventKey.ArrivedAtCafe);
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(gameEvent);
    }

    private void InitCafeData()
    {
        Seats = FindObjectsByType<Seat>(FindObjectsSortMode.None).ToList();
        _shipmentHandler = FindFirstObjectByType<ShipmentHandler>();
        _dayLength = GameConfig.GetConfigData().Gameplay.DayLength;
        SingletonManager.Instance.GameManager.ChangeTimeOfDayState(TimeOfDay.Day);
        TimeLeftInDayNormalized = _currentTimeDay / _dayLength;
        _currentTimeDay = 0f;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.GameManager.ChangedDay -= OnChangedDay;
        SingletonManager.Instance.GameManager.LevelLoaded -= OnLevelLoaded;
    }

    private void Update()
    {
         _currentTimeDay += Time.deltaTime;
         TimeLeftInDayNormalized = _currentTimeDay / _dayLength;

         if (_currentTimeDay >= _dayLength)
         {
             CloseCafe();
         }
    }

    /// <summary>
    /// Used to start the logic of AI and everything else.
    /// </summary>
    public void OpenCafe()
    {
        if (IsCafeOpen)
        {
            return;
        }
        
        SingletonManager.Instance.GameManager.ChangeGameplayState(GameplayState.CafeOpen);
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Cafe opened!"));
        IsCafeOpen = true;
        CafeStateChange?.Invoke(true);
        WasCafeEverOpenedThatDay = true;
    }
    
    public void CloseCafe()
    {
        if (!IsCafeOpen)
        {
            return;
        }
        
        SingletonManager.Instance.GameManager.ChangeGameplayState(GameplayState.CafeClosing);
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Cafe closed!"));
        IsCafeOpen = false;
        CafeStateChange(false);
    }

    private void OnChangedDay(int day)
    {
        WasCafeEverOpenedThatDay = false;
    }

    public void AddShipment(Product productConfig)
    {
        _shipments.Add(new Shipment(productConfig, productConfig.DeliveryTimeInDays));
    }
    
    public float NormalizedAttractiveness()
    {
        return Attractiveness / _cafeSpaceConfig.MaxAttractivenessForThisSpace;
    }
    
    public float NormalizedCleanliness()
    {
        return Attractiveness / _cafeSpaceConfig.MaxCleanlinessForThisSpace;
    }
    
    public float NormalizedComfort()
    {
        return Attractiveness / _cafeSpaceConfig.MaxComfortForThisSpace;
    }

    // Add or remove World Attribute
    public void AddRemoveWorldAttribute(WorldAttribute attribute)
    {
        var value = attribute.Value;
        if (!CurrentWorldAttributes.Contains(attribute))
        {
            CurrentWorldAttributes.Add(attribute);
        }
        else
        {
            value *= -1;
            CurrentWorldAttributes.Remove(attribute);
        }

        switch (attribute.WorldAttributeType)
        {
            case WorldAttributeType.Attractiveness:
                Attractiveness += value;
                break;
            case WorldAttributeType.Cleanliness:
                Cleanliness += value;
                break;
            case WorldAttributeType.Comfort:
                Comfort += value;
                break;
            case WorldAttributeType.Wifi:
                Wifi = CurrentWorldAttributes.Any(current => current.WorldAttributeType == WorldAttributeType.Wifi);
                break;
            case WorldAttributeType.Music:
                Music = CurrentWorldAttributes.Any(current => current.WorldAttributeType == WorldAttributeType.Music);
                break;
            case WorldAttributeType.None:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public float GetExternalTimeModifiers()
    {
        // TODO: Add these? 
        // Music is playing, waiting is more pleasant = +10 sec.
        // Someone spills drink = -10 sec.
        // Weather = +-X sec.
        // Barista talks to you = +30 sec.
        // This place is very comfortable = +[confort] in sec.
        // Wifi / Power outage = -20 sec.
        // This place is not cleanliness = -[cleanliness] in sec.
        return 0f;
    }

    public void CustomerEntered(NPCCustomer customer)
    {
        if (_customersVisiting.Contains(customer))
        {
            return;
        }
        
        _customersVisiting.Add(customer);
        SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().CustomerData.CustomersVisited++;
    }

    public void TakeOrder()
    {
        TakingOrder?.Invoke();
    }

    private void ReceiveShipments(bool ignoreDays = false)
    {
        foreach (var shipment in _shipments)
        {
            if (!ignoreDays)
            {
                --shipment.Days;
                Debug.Log(shipment.ProductConfig.ProductName + " " + shipment.Days);
                if (shipment.Days != 0)
                {
                    continue;
                }
            }
            else
            {
                shipment.Days = 0;
            }
            
            //Instantiate new product.
            var go = Instantiate(_productBoxPrefab, _shipmentHandler.transform);
            var productBox = go.GetComponent<ProductBox>();
            productBox.SetProduct(shipment.ProductConfig.ProductPrefab.GetComponent<Interactable>());
                
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData(shipment.ProductConfig.ProductName + " has arrived."));
        }

        _shipments.RemoveAll(shipment => shipment.Days == 0);
    }
    
    #if UNITY_EDITOR

    public bool TryForceReceiveShipments()
    {
        if (IsCafeOpen || SingletonManager.Instance.GameManager.CurrentGameplayState == GameplayState.Home)
        {
            return false;
        }
        
        ReceiveShipments(true);
        return true;
    }
    
    #endif
}
