using System;
using System.Collections.Generic;
using UnityEngine;

public class BeanOSManager : PersistentBaseData<BeanOSManagerData>
{
    public event Action<ProcessedMessage> NewMessage = delegate { };
    public event Action<SocialPostStubber> NewSocialPost = delegate { };
    
    public ContactList AllContacts => _allContacts;

    [SerializeField] private ContactList _allContacts;
    [SerializeField] private MessageProcessor _processor;

    public void CreateNewMessage(IEnumerable<MessageData> messagesData)
    {
        var processedMessages = _processor.ProcessMessages(messagesData);
        foreach (var processedMsg in processedMessages)
        {
            processedMsg.SendMessage += OnProcessedMessageTyping;
        }
    }

    private void OnProcessedMessageTyping(ProcessedMessage message)
    {
        message.SendMessage -= OnProcessedMessageTyping;
        NewMessage?.Invoke(message);
        Data.Conversations.AddMessage(message);
    }

    public void ReplyMessage(IEnumerable<MessageData> messagesData, ContactData contactToSendReplyTo)
    {
        var processedMessages = _processor.ProcessMessages(messagesData);
        foreach (var processedMsg in processedMessages)
        {
            processedMsg.ChangeState(ProcessedMessage.MessageState.Sent);
            Data.Conversations.AddMessage(processedMsg, contactToSendReplyTo);
            NewMessage?.Invoke(processedMsg);
        }
    }

    public void AddSocialPost(SocialPostStubber post)
    {
        NewSocialPost?.Invoke(post);
    }

    protected override void LoadData(PlayerData data)
    {
        Data = data.BeanOSManagerData;
    }
}