using System.Collections.Generic;
using UnityEngine;

public class WorldObjectsHandler
{
   public List<IWorldDataPersistence> WorldObjects => _worldObjects;
   
   private readonly List<IWorldDataPersistence> _worldObjects = new ();

   public void RegisterWorldObject(IWorldDataPersistence worldObject)
   {
       _worldObjects.Add(worldObject);
       Debug.Log(
           $"Added world object {worldObject.GetType()} to the world \n " +
           $"total world objects: {_worldObjects.Count}");
   }

   public void UnregisterWorldObject(IWorldDataPersistence worldObject)
   {
       if (worldObject == null)
       {
           return;
       }
       
       _worldObjects.Remove(worldObject);
       Debug.Log(
           $"Removed world object {worldObject.GetType()} from the world \n " +
           $"total world objects: {_worldObjects.Count}");
   }
}

[System.Serializable]
public class WorldObjectsData
{
    public List<BRAGObjectData> WorldObjects = new();
}
