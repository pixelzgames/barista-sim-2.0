using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioMixer _bragMixer;
    
    private const float MIN_VOLUME = -80;

    private void Awake()
    {
        GameOptions.GameOptionsChange += OnOptionsChange;
    }

    private void OnDestroy()
    {
        GameOptions.GameOptionsChange -= OnOptionsChange;
    }

    /// <summary>
    /// Takes a float in a 0-1 range and converts it to decibels (range (10 to -80))
    /// </summary>
    /// <param name="value">Value to convert</param>
    private static float ConvertToDecibels(float value)
    {
        var result = (1 - Mathf.Sqrt(value)) * MIN_VOLUME;
        return result;
    }

    private void OnOptionsChange()
    {
        _bragMixer.SetFloat("Music_Volume", ConvertToDecibels(GameOptions.MUSIC_VOLUME));
        _bragMixer.SetFloat("SFX_Volume", ConvertToDecibels(GameOptions.SFX_VOLUME));
        _bragMixer.SetFloat("UI_Volume", ConvertToDecibels(GameOptions.MENU_SFX_VOLUME));
    }
}
