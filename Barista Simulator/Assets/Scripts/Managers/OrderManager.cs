﻿using System;
using AI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class OrderManager : MonoBehaviour
{
    public Action OrderSold = delegate {  };
    public Action<Order> OrderGenerated = delegate {  };
    
    [SerializeField] private GameObject _orderUIPrefab;

    private readonly List<Order> _currentOrders = new();

    public Order GenerateRandomOrderFromMenu(OrderType type, NPCCustomer customer = null,
        TastingDescriptor requestedTastingNoteByCustomer = TastingDescriptor.OPENED)
    {
        var lastOrder = Instantiate(_orderUIPrefab).GetComponent<Order>();
        _currentOrders.Add(lastOrder);
        lastOrder.InitOrder(type, GenerateRandomRecipeForOrder(ChooseCoffeeType.FromMenu), customer, requestedTastingNoteByCustomer);
        OrderGenerated.Invoke(lastOrder);
        return lastOrder;
    }

    public Order GenerateSpecificOrder(OrderType orderType, RecipeConfig recipe, NPCCustomer customer,
        TastingDescriptor requestedTastingNoteByCustomer)
    {
        var lastOrder = Instantiate(_orderUIPrefab).GetComponent<Order>();
        _currentOrders.Add(lastOrder);
        lastOrder.InitOrder(orderType, recipe, customer, requestedTastingNoteByCustomer);
        OrderGenerated.Invoke(lastOrder);
        return lastOrder;
    }

    public void RemoveOrder(Order order)
    {
        _currentOrders.Remove(order);
    }

    public void SellOrder(Order order, int price)
    {
        RemoveOrder(order);

        SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().FinanceData.Revenue += price;
        SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().FinanceData.DrinksSold++;
        SingletonManager.Instance.GameStatsTracker.Data.AllTimeGameData.Revenue += price;
        SingletonManager.Instance.GameStatsTracker.Data.AllTimeGameData.CustomersServed++;
        
        SingletonManager.Instance.CurrenciesManager.ChangeCurrentMoneyBy(price);
        OrderSold();
    }

    public RecipeConfig GenerateRandomRecipeForOrder(ChooseCoffeeType type)
    {
        switch (type)
        {
            case ChooseCoffeeType.FromMenu:
                return GameConfig.GetConfigData().Gameplay.RecipesOnMenu.Draw();
            case ChooseCoffeeType.NotOnMenu:
                var allRecipes = GameConfig.GetConfigData().Gameplay.AllRecipes;
                var menuRecipes = GameConfig.GetConfigData().Gameplay.RecipesOnMenu;
                WeightedRecipeConfigList notOnMenu = new();
                
                // Build a new distributed list of recipes not on the menu
                foreach (var recipe in allRecipes.Recipes)
                {
                    foreach (var menuRecipe in menuRecipes.Items)
                    {
                        if (menuRecipe.Value.RecipeType == recipe.RecipeType)
                        {
                            continue;
                        }
                        
                        notOnMenu.Add(recipe, 1f);
                    }
                }
                
                return notOnMenu.Draw();
            default:
                return null;
        }
    }

    public OrderType GenerateRandomCupType()
    {
        return (OrderType) Random.Range(0, Enum.GetValues(typeof(OrderType)).Length);
    }

    public Order GetMostPressingValidOrder(Cup cup)
    {
        var orderedOrders = GetOrderedByMostPressingOrders();
        return orderedOrders.FirstOrDefault(order => cup.CurrentRecipe.Config.RecipeType == order.Recipe.RecipeType && 
                                                     cup.OrderType == order.Type);
    }

    public Order GetCustomerOrder(NPCCustomer customer)
    {
        return _currentOrders.FirstOrDefault(order => order.Customer == customer);
    }

    private Order[] GetOrderedByMostPressingOrders()
    {
        return _currentOrders.OrderBy(o => o.OrderTimer.TimeRemaining).ToArray();
    }
}
