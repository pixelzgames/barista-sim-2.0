using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Manages which UI canvases are active in different gameplay states and interaction modes.
/// </summary>
public class CafeUIController : MonoBehaviour
{
    [SerializeField] private BRAGCanvasController _ordersCanvas;
    [SerializeField] private BRAGCanvasController _interactionCanvas;
    [SerializeField] private BRAGCanvasController _pauseMenu;
    [SerializeField] private CafeStateUIController _cafeStateUIController;

    private InputMaster _inputMaster;
    
    private void Start()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        _inputMaster.GenericGameplay.Pause.performed += OnPauseMenuButton;
    }

    private void OnEnable()
    {
        // Subscribe to game mode change
        SingletonManager.Instance.GameManager.InteractionModeChanged += OnInteractionModeChanged;
        // Subscribe to Interaction mode change
        SingletonManager.Instance.GameManager.GameplayStateChanged += OnGameplayStateChanged;
        SingletonManager.Instance.GameManager.MechanicMode += OnMechanicModeChanged;
        
        SingletonManager.Instance.CafeManager.CafeStateChange += _cafeStateUIController.PlayCanvasAnimation;
        _cafeStateUIController.OnAnimationFinished += OnCafeClosedAnimFinished;
    }

    private void OnMechanicModeChanged(bool isOn)
    {
        if (isOn)
        {
            _interactionCanvas.DisableCanvas();
        }
        else
        {
            _interactionCanvas.EnableCanvas();
        }
    }

    private void OnDestroy()
    {
        // Unsubscribe to game mode change
        SingletonManager.Instance.GameManager.InteractionModeChanged -= OnInteractionModeChanged;
        // Unsubscribe to Interaction mode change
        SingletonManager.Instance.GameManager.GameplayStateChanged -= OnGameplayStateChanged;
        _inputMaster.GenericGameplay.Pause.performed -= OnPauseMenuButton;
        SingletonManager.Instance.CafeManager.CafeStateChange -= _cafeStateUIController.PlayCanvasAnimation;
    }

    private void OnGameplayStateChanged(GameplayState gameplayState)
    {
        switch (gameplayState)
        {
            case GameplayState.CafePreOpen:
                break;
            case GameplayState.CafeOpen:
                _ordersCanvas.EnableCanvas();
                break;
            case GameplayState.CafeClosing:
                _ordersCanvas.DisableCanvas();
                break;
        }
    }

    private void OnInteractionModeChanged(InteractionMode interactionMode)
    {
        switch (interactionMode)
        {
            case InteractionMode.None:
                break;
            case InteractionMode.Barista:
                break;
            case InteractionMode.Maintenance:
                break;
            case InteractionMode.Edit:
                break;
        }
    }
    
    private void OnCafeClosedAnimFinished()
    {
        SingletonManager.Instance.CameraManager.PhoneController.OpenPhone(BeanOSAppName.Reports);
    }

    private void OnPauseMenuButton(InputAction.CallbackContext obj)
    {
        _pauseMenu.EnableCanvas();
    }
}
