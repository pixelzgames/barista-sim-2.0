using System;

public static class GameOptions
{
    public static Action GameOptionsChange = delegate {  };
    
    //TODO there values should be changed and saved by the game options
    public static float UI_HUD_SCALER = 1f;
    public static float UI_GAMEPLAY_SCALER = 1f;
    public static float MUSIC_VOLUME = 1f;
    public static float SFX_VOLUME = 1f;
    public static float MENU_SFX_VOLUME = 1f;
}

[Serializable]
public enum UIOption
{
    Hud,
    Gameplay
}
