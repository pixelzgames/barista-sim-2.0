using System;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public Action<TimerState> TimerStateChanged = delegate { };
    public bool TimerStarted { get; private set; }
    public float TimeRemaining { get; private set; }
    public float Length { get; private set; }
    public float TimeElapsed => Length - TimeRemaining;
    public bool ModerateReached { get; private set; }
    public bool ImminentReached { get; private set; }
    public bool TimerFinished { get; private set; }

    private float _moderateLength;
    private float _imminentLength;

    private void Update()
    {
        if (!TimerStarted || TimerFinished)
        {
            return;
        }

        TimeRemaining -= Time.deltaTime;

        if (TimeRemaining <= _moderateLength && !ModerateReached)
        {
            TimerStateChanged(TimerState.Warning);
            ModerateReached = true;
        }

        if (TimeRemaining <= _imminentLength && !ImminentReached)
        {
            TimerStateChanged(TimerState.EndingImminent);
            ImminentReached = true;
        }

        if (TimeRemaining <= 0f)
        {
            TimerFinished = true;
            StopTimer();
        }
    }

    /// <summary>
    /// Starts the timer with the given length
    /// </summary>
    /// <param name="timerLength"></param>
    public void StartTimer(float timerLength)
    {
        TimeRemaining = timerLength;
        Length = timerLength;
        _moderateLength = timerLength * GameConfig.Data.AI.ModerateWaitRatio;
        _imminentLength = timerLength * GameConfig.Data.AI.ImminentWaitRatio;

        ModerateReached = false;
        ImminentReached = false;
        
        if (TimerStarted)
        {
            return;
        }
        
        TimerStateChanged(TimerState.Started);
        TimerStarted = true;
    }

    /// <summary>
    /// Stops and kills the timer without invoking the OnTimerEnded delegate.
    /// </summary>
    public void KillTimer()
    {
        TimerFinished = true;
        TimeRemaining = 0f;
        Destroy(this);
    }

    /// <summary>
    /// Kills the current timer and restarts it for the given length.
    /// </summary>
    /// <param name="length"></param>
    public void ResetTimer(float length)
    {
        KillTimer();
        StartTimer(length);
    }

    /// <summary>
    /// Adds time to the current timer.
    /// </summary>
    /// <param name="addedTime"></param>
    public void AddTime(float addedTime)
    {
        TimeRemaining += addedTime;
    }

    /// <summary>
    /// Stops timer and invokes OnTimerEnded delegate.
    /// </summary>
    private void StopTimer()
    {
        TimerStateChanged(TimerState.Ended);
    }
}
public enum TimerState
{
    Started,
    Warning,
    EndingImminent,
    Ended
}
