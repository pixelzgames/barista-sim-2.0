﻿using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public Action<Devices> DeviceChanged = delegate { };
    public Action<InputActionMap> InputChange = delegate { };
    public InputMaster InputMaster => _inputMaster;
    public InputActionMap CurrentActionMapping { get; private set; }
    public Devices CurrentDevice { get; private set; } = Devices.Xbox;
    
    private InputMaster _inputMaster;

    private void OnEnable()
    {
        _inputMaster.Enable();
        SetInputMappingToDefault();
        SingletonManager.Instance.GameManager.InteractionModeChanged += OnInteractionModeChanged;
    }

    private void OnDisable()
    {
        _inputMaster.Disable();
        foreach (var map in _inputMaster.asset.actionMaps)
        {
            map.Disable();
        }
        
        SingletonManager.Instance.GameManager.InteractionModeChanged -= OnInteractionModeChanged;
    }

    private void Awake()
    {
        _inputMaster = new InputMaster();
    }

    // Update is called once per frame
    private void Update()
    {
        CheckForDeviceChange();
    }

    private void OnInteractionModeChanged(InteractionMode mode)
    {
        SetInputMappingToDefault();
    }

    private void CheckForDeviceChange()
    {
        //Check for any button being pressed on keyboard
        if (Keyboard.current != null && Keyboard.current.anyKey.wasPressedThisFrame)
        {
            ChangeCurrentDevice(Devices.Keyboard);
        }
        //Check for left thumbstick to be moved to change input to gamepad
        if (Gamepad.current != null && Gamepad.current.leftStick.IsPressed(0.1f))
        {
            ChangeCurrentDevice(Devices.Xbox);
        }
    }

    private void ChangeCurrentDevice(Devices device)
    {
        if (CurrentDevice == device)
        {
            return;
        }
        
        CurrentDevice = device;
        DeviceChanged(CurrentDevice);
        Debug.Log("Device scheme changed to " + CurrentDevice);
    }

    /// <summary>
    /// Use to change the current active input map
    /// </summary>
    /// <param name="mapping">Use the CurrentActionMaps to get the list of available maps</param>
    public void ChangeInputMapping(InputActionMap mapping)
    {
        if (mapping == CurrentActionMapping)
        {
            return;
        }
        
        foreach (var map in _inputMaster.asset.actionMaps)
        {
            map.Disable();
        }
        
        mapping.Enable();
        CurrentActionMapping = mapping;
        InputChange?.Invoke(mapping);
        Debug.Log($"Input mapping changed to {mapping.name}");
    }

    public void SetInputMappingToDefault()
    {
        ChangeInputMapping(_inputMaster.GenericGameplay);
    }

    public enum Devices
    {
        Keyboard,
        Xbox
    };
}
