using System;
using Sirenix.OdinInspector;
using UnityEngine;

public static class GameConfig 
{
    public static GameConfigData Data { get; private set; }
    private const string CONFIG_PATH = "CurrentGameMode";

    [RuntimeInitializeOnLoadMethod]
    private static void LoadData()
    {
        var gameMode = (GameModeConfig) Resources.Load(CONFIG_PATH);
        Data = gameMode.GameConfigToLoad;
    }

    public static GameConfigData GetConfigData()
    {
        return Data;
    }
}

[Serializable]
public class GameConfigAIData
{
    [FoldoutGroup("Wait time")]
    [MinValue(0f)]
    public int BaseWaitTime;
    [FoldoutGroup("Wait time")]
    [MinMaxSlider(0,30, true)]
    public Vector2 ConsiderMinMaxWaitTime = new Vector2(20,40);
    [FoldoutGroup("Thoughts Display Timings")]
    [Header("When to display Yellow"),MinValue(0f),MaxValue(1f), GUIColor("#e8c413")]
    public float ModerateWaitRatio;
    [FoldoutGroup("Thoughts Display Timings")]
    [Header("When to display Red"),MinValue(0f),MaxValue(1f), GUIColor("#d12a00")]
    public float ImminentWaitRatio;
    [FoldoutGroup("Movement")]
    public Vector2 MinMaxNPCSpeed;
    [FoldoutGroup("Visit chance")]
    [Range(0f, 100f)] 
    public float ConsiderInsideChanceVersusOutside;
    
    [FoldoutGroup("Customers configs"), HideLabel]
    [Serializable]
    public struct CustomersConfig
    {
        public int MaxCustomersVisiting;
        public Vector2 CustomerSpawnDelayMinMax;
        public int MaxAllocatedCustomersInWorld;
    }

    [field: SerializeField, Required] public CustomersConfig CustomersConfigs { get; private set; }
    [field: SerializeField, Required] public ExpectationsTraitsList ExpectationsTraitsList { get; private set; }
    [field: SerializeField, Required] public ExpectationsConfig ExpectationsConfig { get; private set; }
    [field: SerializeField, Required] public TraitConfig TraitConfig { get; private set; }
    [field: SerializeField, Required] public ChooseDrinkConfig ChooseDrinkConfig { get; private set; }
    [field: SerializeField, Required] public TastingNotesToChooseFromConfig TastingNotesToChooseFromConfig { get; private set; }
    [field: SerializeField, Required] public ConsiderVisitConfig ConsiderVisitConfig { get; private set; }
    [field: SerializeField, Required] public IntentionsConfig IntentionsConfig { get; private set; }
    [field: SerializeField, Required] public FeedbackEntryTypeDisplayMapping FeedbackEntryTypeConfig { get; private set; }
}

[Serializable]
public class GameConfigGameplayData
{    
    [field: SerializeField]
    public int DrinkPrice { get; private set; }
    
    [field: SerializeField, Required]
    public GameplayStateInteractionModeDictionary GameplayStateDictionary { get; private set; }
    
    [field: SerializeField]
    public float DayLength { get; private set; }
    
    [field: SerializeField, InfoBox("Mostly for debug, should be on on release")]
    public bool DialingInEnabled { get; private set; }
    
    [field: SerializeField, Required]
    public XPSystemConfig XPSystemConfig { get; private set; }
    
    [field: SerializeField, Required]
    public RecipeConfigList AllRecipes  { get; private set; }
    //TODO REMOVE WHEN SYSTEM UNDERSTANDS WHAT EQUIPMENT PLAYER HAS
    [field: SerializeField, Required]
    public WeightedRecipeConfigList RecipesOnMenu  { get; private set; }
    [field: SerializeField]
    public bool OverrideCafeGridSerialization { get; private set; }
    [field: SerializeField, ShowIf("OverrideCafeGridSerialization")]
    public string CafeGridSaveFile { get; private set; }
    [field: SerializeField, ShowIf("OverrideCafeGridSerialization")]
    public string CafeWorldSaveFile { get; private set; }
    [field: SerializeField] public InteractableActionsConfig PickableActionsConfig { get; private set; }
    [field: SerializeField] public InteractableActionsConfig MovableActionsConfig { get; private set; }
    [field: SerializeField] public InteractableActionsConfig PowerableActionsConfig { get; private set; }
}
