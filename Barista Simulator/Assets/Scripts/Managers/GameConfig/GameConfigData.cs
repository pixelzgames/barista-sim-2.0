using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGameConfigData", menuName = "BRAG/GameConfig")]
public class GameConfigData : ScriptableObject
{
    [HideLabel,FoldoutGroup("AI")]
    public GameConfigAIData AI;
    [HideLabel,FoldoutGroup("Gameplay")]
    public GameConfigGameplayData Gameplay;
    [HideLabel,FoldoutGroup("Progression")]
    public ProgressionEventSchedule Progresion;
    [HideLabel,FoldoutGroup("Debug")]
    public ProgressionDebugConfig DebugConfig;
}
