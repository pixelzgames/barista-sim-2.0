using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : PersistentBaseData<GameManagerData>
{
    public event Action<Level> OnLevelLoadRequest = delegate { }; 
    public event Action<GameObject> SpawnedCharacter = delegate { };  
    public event Action<LevelData> LevelLoaded = delegate {  };
    public event Action<InteractionMode> InteractionModeChanged = delegate{  };
    public event Action<GameplayState> GameplayStateChanged = delegate{  };
    public event Action<int> ChangedDay = delegate {  };
    public event Action<TimeOfDay> ChangedTimeState = delegate {  };
    public event Action<bool> CinematicMode = delegate {  };
    public Action<bool> MechanicMode = delegate {  };

    [SerializeField] private Image _loadingImage;
    private static readonly int Fade = Shader.PropertyToID("_fade");

    [SerializeField] private PlayerController _characterPrefab;
    [SerializeField] private ModalWindowController _modalWindow;
    [SerializeField] private NewDayCanvasController _newDayCanvasController;
    
    public TimeOfDay CurrentTimeOfDayState { get; private set; } = TimeOfDay.None;
    public GameplayState CurrentGameplayState { get; private set; } = GameplayState.CafePreOpen;
    public InteractionMode CurrentInteractionMode { get; private set; } = InteractionMode.Barista;

    public Level CurrentSceneType { get; private set; } = Level.None;
    
    public void ChangeGameplayState(GameplayState newState)
    {
        CurrentGameplayState = newState;
        GameplayStateChanged?.Invoke(CurrentGameplayState);
        Debug.Log("Gameplay state changed to " + CurrentGameplayState);
    }
    
    public void ChangeInteractionMode(InteractionMode newMode)
    {
        if (!IsValidMode(newMode))
        {
            return;
        }
        
        CurrentInteractionMode = newMode;
        InteractionModeChanged?.Invoke(CurrentInteractionMode);
        
        if (SingletonManager.Instance.GameNotificationManager)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Changed to " + newMode + " Mode."));
        }
    }

    public bool IsValidMode(InteractionMode newMode)
    {
        return GameConfig.GetConfigData().Gameplay.GameplayStateDictionary[CurrentGameplayState].HasFlag(newMode);
    }

    /// <summary>
    /// Go to the next day
    /// </summary>
    public void ChangeDay()
    {
        StartCoroutine(NextDayScreen());
    }

    public void OnMechanicMode(bool isOn)
    {
        MechanicMode.Invoke(isOn);
    }

    private IEnumerator NextDayScreen()
    {
        var newDayCanvasInstance = Instantiate(_newDayCanvasController);
        
        // Wait until the UI has fully appeared on screen
        while (!newDayCanvasInstance.HasFadedIn)
        {
            yield return null;
        }

        SingletonManager.Instance.ProgressionManager.ChangeDay();
        ChangeTimeOfDayState(TimeOfDay.Morning);
        
        // Wait until the UI has played all it's animations
        while (!newDayCanvasInstance.IsDoneRunning)
        {
            yield return null;
        }
        
        // Wait until the UI has fully disappeared off screen
        while (!newDayCanvasInstance.HasFadedOut)
        {
            yield return null;
        }
        
        Destroy(newDayCanvasInstance.gameObject);
        SingletonManager.Instance.DataPersistenceManager.SaveAllPlayerData();
        ChangedDay?.Invoke(SingletonManager.Instance.ProgressionManager.Data.CurrentDay);
    }

    public void ChangeTimeOfDayState(TimeOfDay state)
    {
        if (state == TimeOfDay.Morning)
        {
            SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.Wakeup));
        }
        
        CurrentTimeOfDayState = state;
        ChangedTimeState?.Invoke(state);
        Debug.Log($"Changed Time Of Day State to {state}");
    }

    public void LoadALevel(LevelData levelData)
    {
        Debug.Log($"Requesting level load {levelData.LevelType}, current level is: " + CurrentSceneType); 
        OnLevelLoadRequest?.Invoke(CurrentSceneType);
        
        if (levelData.CommutingType != CommutingType.None)
        {
            StartCoroutine(LoadCommutingLevelAsynchronously(levelData));
            return;
        }
        
        StartCoroutine(LoadLevelAsynchronously(levelData));
    }

    public void SpawnCharacter(CharacterWorldData.DataPerSceneType characterData)
    {
        Vector3 position;
        Quaternion rotation;
        
        if (characterData == null)
        {
            var spawnPoint = GameObject.FindWithTag("Respawn");
            position = spawnPoint.transform.position;
            rotation = spawnPoint.transform.rotation;
        }
        else
        {
            position = characterData.Position.ToVector3();
            rotation = characterData.Rotation;
        }
        
        var character = Instantiate(_characterPrefab, position, rotation);
        SingletonManager.Instance.CameraManager.AssignCameraTarget(character.transform);
        SpawnedCharacter?.Invoke(character.gameObject);
    }
    
    public void ShowModalWindow(ModalWindowData data)
    {
        if (data.Image != null)
        {
            Instantiate(_modalWindow).ShowWindowWithImage(data);
            return;
        }
        
        Instantiate(_modalWindow).ShowWindow(data);
    }

    public void ToggleCinematicMode(bool isEnabled)
    {
        if (isEnabled)
        {
            var inputMapping = SingletonManager.Instance.InputManager.InputMaster.Cinematic;
            SingletonManager.Instance.InputManager.ChangeInputMapping(inputMapping);
        }
        else
        {
            SingletonManager.Instance.InputManager.SetInputMappingToDefault();
        }
        
        SingletonManager.Instance.CameraManager.ToggleCullingManager(!isEnabled);
        CinematicMode?.Invoke(isEnabled);
    }

    protected override void LoadData(PlayerData data)
    {
        Data = data.GameManagerData;
    }

    private IEnumerator LoadCommutingLevelAsynchronously(LevelData levelData)
    {
        yield return _loadingImage.material.DOFloat(1f,Fade, 1f).WaitForCompletion();
        var operation = SceneManager.LoadSceneAsync("Loading_metro");
        
        while (!operation.isDone)
        {
            yield return null;
        }
        
        _loadingImage.material.DOFloat(0f,Fade, 1f).onComplete = () =>
        {
            _loadingImage.material.SetFloat(Fade, 0f);
        };
        
        var timer = 3f;
        while (timer > 0f)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        
        StartCoroutine(LoadLevelAsynchronously(levelData));
    }
    
    private IEnumerator LoadLevelAsynchronously(LevelData levelData)
    {
        yield return _loadingImage.material.DOFloat(1f,Fade, 1f).WaitForCompletion();
        
        var loaderHandler = new LoaderHandler
        (
            new ILoaderWaitable[]
            { 
                // Add any new loaders sequentially
                new SceneLoader(levelData),
                new GridLoader(levelData.LevelType),
                new WorldLoader(levelData.LevelType)
            }
        );

        loaderHandler.LoadingCompleted += OnChainLoaderCompleted;
        loaderHandler.RunChainLoaders();
        yield break;

        void OnChainLoaderCompleted()
        {
            var shouldSpawnCharacter = levelData.LevelType != Level.MainMenu;
            CharacterWorldData.DataPerSceneType characterSpawnData = null;
            switch (levelData.LevelType)
            {
                case Level.Cafe:
                    SingletonManager.Instance.InputManager.SetInputMappingToDefault();
                    ChangeGameplayState(GameplayState.CafePreOpen);
                    characterSpawnData = SingletonManager.Instance.ProgressionManager.Data.CharacterWorldData.CafeData;
                    break;
                case Level.Home:
                    SingletonManager.Instance.InputManager.SetInputMappingToDefault();
                    ChangeTimeOfDayState(CurrentTimeOfDayState == TimeOfDay.Day ? TimeOfDay.Night : TimeOfDay.Morning);
                    ChangeGameplayState(GameplayState.Home);
                    characterSpawnData = SingletonManager.Instance.ProgressionManager.Data.CharacterWorldData.HomeData;
                    break;
                case Level.MainMenu:
                    ChangeGameplayState(GameplayState.MainMenu);
                    break;
                case Level.None:
                    SingletonManager.Instance.InputManager.SetInputMappingToDefault();
                    break;
            }
            
            if (shouldSpawnCharacter)
            {
                SpawnedCharacter += OnAllLoadingCompleted;
                SpawnCharacter(characterSpawnData);
            }
            else
            {
                OnAllLoadingCompleted(null);
            }

            loaderHandler.LoadingCompleted -= OnChainLoaderCompleted;
        }
        
        void OnAllLoadingCompleted(GameObject _)
        {
            SpawnedCharacter -= OnAllLoadingCompleted;
            _loadingImage.material.DOFloat(0f,Fade, 1f).onComplete = () =>
            {
                _loadingImage.material.SetFloat(Fade, 0f);
            };

            CurrentSceneType = levelData.LevelType;
            LevelLoaded?.Invoke(levelData);
        }
    }
    
    private void OnDestroy()
    {
        _loadingImage.material.SetFloat(Fade,0f);
    }
}

/// <summary>
/// Gameplay State: different states affect what the player can do. 
/// </summary>
[Serializable]
public enum GameplayState
{
    Home,
    CafePreOpen,
    CafeOpen,
    CafeClosing,
    DayOff,
    MainMenu
}

[Serializable]
public enum TimeOfDay
{
    None,
    Morning,
    Day,
    Night
}

/// <summary>
/// Affects the available interactions on interactables.
/// </summary>
[Flags, Serializable]
public enum InteractionMode
{
    None = 0,
    Barista = 1,
    Maintenance = 2,
    Edit = 4 
}

public enum Level
{
    None = 0,
    Cafe = 1,
    Home = 2,
    MainMenu = 3
}

public enum CommutingType
{
    None = 0,
    Metro = 1,
    Plane = 2
}

[Serializable]
public struct LevelData
{
    public string LevelName;
    public Level LevelType;
    public CommutingType CommutingType;
}
