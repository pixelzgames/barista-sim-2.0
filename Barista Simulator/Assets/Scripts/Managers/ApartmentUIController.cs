using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Manages which UI canvases are active in different gameplay states and interaction modes.
/// </summary>
public class ApartmentUIController : MonoBehaviour
{
    [SerializeField] private BRAGCanvasController _pauseMenu;
    [SerializeField] private BRAGCanvasController _characterCreator;
    [SerializeField] private BRAGCanvasController _hud;

    private InputMaster _inputMaster;
    
    private void Start()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        _inputMaster.GenericGameplay.Pause.performed += OnPauseMenuButton;
    }
    
    public void OpenCharacterCreator()
    {
        _characterCreator.EnableCanvas();
    }

    private void OnDestroy()
    {
        _inputMaster.GenericGameplay.Pause.performed -= OnPauseMenuButton;
    }

    private void OnPauseMenuButton(InputAction.CallbackContext obj)
    {
        _pauseMenu.EnableCanvas();
    }
}
