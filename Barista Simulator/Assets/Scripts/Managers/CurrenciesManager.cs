﻿using System;

public class CurrenciesManager : PersistentBaseData<CurrenciesData>
{
    public event Action<int> MoneyChangedBy = delegate { };
    public event Action<int> XPChangedBy = delegate {  };
    public event Action<int> BeanPointsChangedBy = delegate {  };
    public event Action OnLeveledUp = delegate {  };

    private void Start()
    {
        SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted += GameEventReceived;
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted -= GameEventReceived;
    }

    private void GameEventReceived(GameEvent gameEvent)
    {
        if (gameEvent.Key != GameEventKey.MoneyReceived)
        {
            return;
        }

        ChangeCurrentMoneyBy(gameEvent.Value);
    }

    public void ChangeCurrentMoneyBy(int value)
    {
        Data.CurrentMoney += value;
        MoneyChangedBy(value);
    }

    public void ChangeCurrentBeanPointsBy(int value)
    {
        Data.CurrentBeanPoints += value;
        BeanPointsChangedBy(value);
    }
    
    public void ChangeCurrentXPBy(int value)
    {
        Data.CurrentXP += value;
        XPChangedBy?.Invoke(value);
    }

    public bool LevelUp()
    {
        if (Data.CurrentLevel >= GameConfig.Data.Gameplay.XPSystemConfig.XPNeededPerLevelAnimationCurve.keys[
                GameConfig.Data.Gameplay.XPSystemConfig.XPNeededPerLevelAnimationCurve.length - 1].time)
        {
            //Max level reached
            return false;
        }
        
        Data.CurrentLevel++;
        OnLeveledUp?.Invoke();
        
        var rewardIndex = Data.CurrentLevel - 1;
        // If no more rewards were set up, use the last one
        if (GameConfig.Data.Gameplay.XPSystemConfig.RewardsPerLevel.Length >= Data.CurrentLevel)
        {
            rewardIndex = GameConfig.Data.Gameplay.XPSystemConfig.RewardsPerLevel.Length - 1;
        }
        
        var reward = GameConfig.Data.Gameplay.XPSystemConfig.RewardsPerLevel[rewardIndex].BeanPoints;
        ChangeCurrentBeanPointsBy(reward);
        return true;
    }

    public bool TryOrderingProduct(Product productConfig)
    {
        var notification = new NotificationData("Not enough money");
        if (Data.CurrentMoney < productConfig.Price)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(notification);
            return false;
        }

        SingletonManager.Instance.CafeManager.AddShipment(productConfig);
        ChangeCurrentMoneyBy(-productConfig.Price);
        notification = new NotificationData($"Ordered {productConfig.ProductName}");
        SingletonManager.Instance.GameNotificationManager.QueueNotification(notification);
        SingletonManager.Instance.GameStatsTracker.GetCurrentDailyGameData().FinanceData.Expenses += productConfig.Price;
        return true;
    }

    protected override void LoadData(PlayerData data)
    {
        Data = data.CurrenciesData;
    }
}
