using System.Collections;
using Cinemachine;
using UnityEngine;

public class CameraManager : MonoBehaviour, ILevelLoadingWaitable
{
    public CinemachineVirtualCamera MainGameplayCamera => _mainCamera;
    public CinemachineBrain CinemachineBrain => _cinemachineBrain;
    [field: SerializeField] public CameraController CameraController { get; private set; }
    
    [SerializeField] private CinemachineVirtualCamera _mainCamera;
    [SerializeField] private CinemachineBrain _cinemachineBrain;
    [SerializeField] private LayerMask _noPlayerMask;
    [SerializeField] private CullingManager _cullingManager;

    [field:SerializeField] 
    public PhoneController PhoneController { get; private set; }

    private LayerMask _defaultLayerMask;
    private ICinemachineCamera _lastCam;
    private bool _isReady;
    private Coroutine _coroutine;
    
    private void Awake()
    {
        SingletonManager.Instance.SetCameraManager(this);
        _defaultLayerMask = Camera.main.cullingMask;
        ChangeCamera(_mainCamera);
        _isReady = true;
    }

    public void ChangeCamera(CinemachineVirtualCamera cam, bool handleLayers = true)
    {
        if (handleLayers)
        {
            Camera.main.cullingMask = cam != _mainCamera ? _noPlayerMask : _defaultLayerMask;
        }
        
        if (_lastCam != null)
        {
            _lastCam.Priority = 0;
        }
        
        _lastCam = cam;
        cam.Priority = 1;
    }
    
    public void AssignCameraTarget(Transform target, float delay = 0f)
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }
        
        _coroutine = StartCoroutine(AssignTargetDelay(delay, target));
    }

    private IEnumerator AssignTargetDelay(float delay, Transform target)
    {
        var timer = 0f;
        while (timer < delay)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        
        AssignTarget(target);
    }
    
    private void AssignTarget(Transform target)
    {
        _mainCamera.Follow = target;
        _mainCamera.LookAt = target;
    }
    
    public void ToggleCullingManager(bool enableCulling)
    {
        _cullingManager.ToggleCulling(enableCulling);
    }

    public bool IsReady()
    {
        return _isReady;
    }
}
