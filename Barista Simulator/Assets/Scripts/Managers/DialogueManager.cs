using System;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
   public event Action<DialogueConfig> DialogueFinished;
   
   [SerializeField] private DialogueCanvasController _dialogueCanvasControllerPrefab;
   [SerializeField] private List<DialogueConfig> _defaultDialogues;

   private DialogueCanvasController _currentDialogueCanvasController;
   private Dictionary<ContactData, DialogueConfig> _currentDialogues = new();

   private void Start()
   {
      foreach (var dialogue in _defaultDialogues)
      {
         _currentDialogues.TryAdd(dialogue.Contact.ContactData, dialogue);
      }
   }

   /// <summary>
   /// Start a dialogue sequence
   /// </summary>
   /// <param name="dialogue">Dialogue data scriptable object</param>
   private void StartDialogueSequence(DialogueConfig dialogue)
   {
      if (_currentDialogueCanvasController != null)
      {
         Debug.LogError($"A dialogue {_currentDialogueCanvasController} is already running!");
      }
      
      _currentDialogueCanvasController = Instantiate(_dialogueCanvasControllerPrefab);
      _currentDialogueCanvasController.Init(dialogue);
      _currentDialogueCanvasController.OnFinished += OnDialogueCanvasFinished;
   }

   public void RequestDialogueSequence(ContactData contact)
   {
      if (!_currentDialogues.ContainsKey(contact))
      {
         return; 
      }
      
      StartDialogueSequence(_currentDialogues[contact]);
   }

   public void AddDialogueToDictionary(DialogueConfig config)
   {
      // If Dictionary already contains a dialogue data for this contact, replace it
      if (_currentDialogues.ContainsKey(config.Contact.ContactData))
      {
         _currentDialogues.Remove(config.Contact.ContactData);
      }

      _currentDialogues.TryAdd(config.Contact.ContactData, config);
   }

   private void OnDialogueCanvasFinished(DialogueConfig dialogueConfig)
   {
      DialogueFinished?.Invoke(dialogueConfig);
      SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.DialogueFinished));
      _currentDialogueCanvasController = null;
   }
}
