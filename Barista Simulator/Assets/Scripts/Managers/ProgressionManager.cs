using System.Collections.Generic;
using UnityEngine;

public class ProgressionManager : PersistentBaseData<ProgressionManagerData>
{
    private readonly List<ProgressionControllerBase> _activeControllers = new();
    
    private void Start()
    {
        SingletonManager.Instance.GameManager.ChangedDay += SetNewDayEvents;
        SingletonManager.Instance.GameManager.LevelLoaded += OnLevelChanged;
        SingletonManager.Instance.GameManager.OnLevelLoadRequest += GameManagerOnOnLevelLoadRequest;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        SingletonManager.Instance.GameManager.LevelLoaded -= OnLevelChanged;
        SingletonManager.Instance.GameManager.ChangedDay -= SetNewDayEvents;
        SingletonManager.Instance.GameManager.OnLevelLoadRequest -= GameManagerOnOnLevelLoadRequest;
    }

    private void OnLevelChanged(LevelData levelData)
    {
        if (levelData.LevelType == Level.MainMenu)
        {
            return;
        }
        
        Data.LastGameplayScene = levelData.LevelType;
    }
    
    public int ChangeDay()
    {
        return Data.CurrentDay++;
    }

    public string GetCurrentSceneToLoadType(Level type)
    {
        switch (type)
        {
            case Level.Cafe:
                return Data.CurrentCafeBaseLevel.SceneName;
            case Level.Home:
                return Data.CurrentHomeBaseLevel.SceneName;
        }

        return null;
    }

    private void SetNewDayEvents(int day)
    {
        Data.CurrentActiveDailyEvents.Clear();
        
        if (day >= GameConfig.Data.Progresion.ProgressionDays.Length)
        {
            Debug.LogWarning($"Trying to go to day {day} but configs don't exist in progression.");
            return;
        }
        
        if (GameConfig.Data.Progresion.ProgressionDays[day] == null)
        {
            Debug.LogWarning($"No progression data found in the config files for day {day}");
            return;
        }
        
        foreach (var dailyEvent in GameConfig.Data.Progresion.ProgressionDays[day].DailyProgressionEvents )
        {
            dailyEvent.Completed = false;
            Data.CurrentActiveDailyEvents.Add(dailyEvent);
        }
    }

    private void SetDayEvents()
    {
        foreach (var dayEvent in Data.CurrentActiveDailyEvents)
        {
            if (dayEvent.Completed)
            {
                continue;
            }
            
            var controller = new ProgressionControllerBase(dayEvent);
            switch (dayEvent.EventType)
            {
                case ProgressionEventType.None:
                    break;
                case ProgressionEventType.Message:
                    controller = new BeanChatController(dayEvent);
                    break;
                case ProgressionEventType.Dialogue:
                    controller = new DialogueController(dayEvent);
                    break;
                case ProgressionEventType.Spawn:
                    break;
                case ProgressionEventType.Task:
                    controller = new TaskController(dayEvent);
                    break;
                case ProgressionEventType.Cutscene:
                    controller = new CutsceneController(dayEvent);
                    break;
            }

            controller.OnProgressionEventFinished += ControllerOnProgressionEventFinished;
            _activeControllers.Add(controller);
        }
    }

    private void ControllerOnProgressionEventFinished(ProgressionControllerBase progressionControllerBase)
    {
        _activeControllers.Remove(progressionControllerBase);
        progressionControllerBase.ProgressionEvent.Completed = true;
    }
    
    private void GameManagerOnOnLevelLoadRequest(Level currentSceneType)
    {
        if (currentSceneType is Level.MainMenu or Level.None)
        {
            return;
        }
        
        var newData = new CharacterWorldData.DataPerSceneType
        {
            LevelType = currentSceneType,
            Position = PlayerInteractionManager.Instance.transform.position.ToSerializedVector3(),
            Rotation = PlayerInteractionManager.Instance.transform.rotation
        };
        
        switch (currentSceneType)
        {
            case Level.Cafe:
                Data.CharacterWorldData.CafeData = newData;
                break;
            case Level.Home:
                Data.CharacterWorldData.HomeData = newData;
                break;
        }
    }

    protected override void LoadData(PlayerData data)
    {
        Data = data.PlayerProgressionData;

        // Only to init when resetting save file (brand new)
        if (Data.CurrentActiveDailyEvents.Count == 0 && !FileGameDataHandler<PlayerData>.HasSaveFile())
        {
            for (var i = 0; i < _activeControllers.Count; i++)
            {
                _activeControllers[i].Destroy();
                _activeControllers[i] = null;
            }
            
            _activeControllers.Clear();
            SetNewDayEvents(Data.CurrentDay);
        }
        
        SetDayEvents();
    }
}
