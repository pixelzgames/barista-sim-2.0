using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameNotificationManager : MonoBehaviour
{
    [SerializeField] private NotificationMessageController _minorNotificationControllerPrefab;
    [SerializeField] private NotificationMessageController _mainNotificationControllerPrefab;
    
    private Queue<NotificationData> _minorNotifications = new();
    private Queue<NotificationData> _mainNotifications = new();

    /// <summary>
    /// Show a message to the player as a notification
    /// </summary>
    /// <param name="data">Data of the notification to show</param>
    public void QueueNotification(NotificationData data)
    {
        bool shouldTrigger;
        switch (data.Size)
        {
            case NotificationSize.Minor:
                shouldTrigger = _minorNotifications.Count == 0;
                _minorNotifications.Enqueue(data);
                break;
            case NotificationSize.Main:
                shouldTrigger = _mainNotifications.Count == 0;
                _mainNotifications.Enqueue(data);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        if (shouldTrigger)
        {
            ShowNextNotificationInQueue(data.Size);
        }
    }
    
    // TODO replace with above
    [Obsolete]
    public void QueueNotification(string title, float time = 2f)
    {
        QueueNotification(new NotificationData(title, timeToShow: time));
    }

    private void ShowNextNotificationInQueue(NotificationSize size)
    {
        NotificationData notificationData;
        NotificationMessageController message;
        switch (size)
        {
            case NotificationSize.Minor:
                notificationData = _minorNotifications.Peek();
                message = Instantiate(_minorNotificationControllerPrefab, transform);
                break;
            case NotificationSize.Main:
                notificationData = _mainNotifications.Peek();
                message = Instantiate(_mainNotificationControllerPrefab, transform);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        message.NotificationDisappeared = DequeueNotification;
        message.InitMessage(notificationData);
    }

    private void DequeueNotification(NotificationSize size)
    {
        NotificationData notificationData;
        switch (size)
        {
            case NotificationSize.Minor:
                notificationData = _minorNotifications.Dequeue();
                notificationData.NotificationFinished?.Invoke();
                
                if (_minorNotifications.Count > 0)
                {
                    ShowNextNotificationInQueue(size);
                }
                break;
            case NotificationSize.Main:
                notificationData = _mainNotifications.Dequeue();
                notificationData.NotificationFinished?.Invoke();
                
                if (_mainNotifications.Count > 0)
                {
                    ShowNextNotificationInQueue(size);
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}

public enum NotificationSize
{
    Minor = 0,
    Main = 1
}

public class NotificationData
{
    public Action NotificationFinished = delegate { };
        
    public NotificationData(string title, string subtitle = "", string description = "", float timeToShow = 2f, NotificationSize size = NotificationSize.Minor)
    {
        Title = title;
        Subtitle = subtitle;
        Description = description;
        TimeToShow = timeToShow;
        Size = size;
    }
    
    public string Title;
    public string Subtitle;
    public string Description;
    public float TimeToShow;
    public NotificationSize Size;
}