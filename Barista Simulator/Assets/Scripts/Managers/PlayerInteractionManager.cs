﻿using System;
using System.Collections;
using Backend.Grid;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.Serialization;

public class PlayerInteractionManager : MonoBehaviour
{
    #region Singleton
    public static PlayerInteractionManager Instance;

    [FormerlySerializedAs("playerController")] 
    [SerializeField] private PlayerController _playerController;

    public PlayerController PlayerController => _playerController;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(this);

        _playerController = GetComponent<PlayerController>();
    }
    private void OnDisable()
    {
        Instance = null;
    }
    #endregion
    
    public Action<bool> OnCloseUpMechanic = delegate{  };
    
    public LayerMask InteractionLayerMask => _mask;
    public Pickable CurrentlyInMainHand {get; private set;}
    private Pickable _currentlyInSecondHand;
    public Transform SocketForInteraction => _socketForInteraction;
    public bool InCloseUpMechanic { get; private set; }
    public bool CanUsePhone
    {
        get => SingletonManager.Instance.ProgressionManager.Data.HasPhone;
        set => SingletonManager.Instance.ProgressionManager.Data.HasPhone = value;
    }
    
    [SerializeField] private LayerMask _mask = 0;
    
    [SerializeField] private Transform _socketForInteraction;
    [SerializeField] private float _collisionRadius;
    
    [SerializeField]
    [Tooltip("Reference to MAIN hand socket where interactable are placed.")]
    private PlayerHand _mainHand;

    [SerializeField]
    [Tooltip("Reference to SECOND hand socket where interactable are placed.")]
    private PlayerHand _secondHand;
    
    [SerializeField] private GameObject _broom;

    private Interactable _currentHighlightedInteractable;
    private Animator _animator;
    private bool _animatingAnInteraction;
    private float _lastThrowTime;
    private InteractableAnimationConfig _currentAnimConfig;
    private static readonly int HoldingAnimID = Animator.StringToHash("Holding");
    private static readonly int ChargeThrowAnimID = Animator.StringToHash("ChargeThrow");
    private static readonly int ThrowAnimID = Animator.StringToHash("Throw");
    private static readonly int ExitAnimID = Animator.StringToHash("Exit");
    private static readonly int EditModeAnimID = Animator.StringToHash("EditMode");
    private static readonly int OnPhoneAnimID = Animator.StringToHash("OnPhone");

    private enum HighlightState
    {
        SphereCast,
        GridCell
    }
    private HighlightState _currentHighlightingState = HighlightState.SphereCast;
    private Interactable _lastSphereCastClosest;

    private Backend.Grid.Grid _grid;
    private GridCell _currentGridCell;
    private GridCell _lastGridCell;
    private readonly VerticalityHandler _verticalityHandler = new();
    private bool _isVerticalityEnabled;

    private void OnEnable()
    {
        _animator = GetComponent<Animator>();
        SingletonManager.Instance.InputManager.InputChange += OnInputChanged;
        SingletonManager.Instance.GameManager.InteractionModeChanged += InteractionModeChanged;
        SingletonManager.Instance.GameManager.MechanicMode += SetCloseUpMechanic;

        if (SingletonManager.Instance.CameraManager)
        {
            SingletonManager.Instance.CameraManager.PhoneController.PhoneStatusUpdated += OnPhoneStatusUpdated;
        }
    }

    private void OnDestroy()
    {
        SingletonManager.Instance.InputManager.InputChange -= OnInputChanged;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridUp.performed -= GridUpOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridDown.performed -= GridDownOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.Throw.performed -= ThrowInputOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.Drop.performed -= DropInputOnPerformed;
        SingletonManager.Instance.GameManager.InteractionModeChanged -= InteractionModeChanged;
        SingletonManager.Instance.GameManager.MechanicMode -= SetCloseUpMechanic;
        
        if (SingletonManager.Instance.CameraManager)
        {
            SingletonManager.Instance.CameraManager.PhoneController.PhoneStatusUpdated -= OnPhoneStatusUpdated;
        }
    }
    
    private void Start()
    {
        _grid = FindFirstObjectByType<Backend.Grid.Grid>();

        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.Throw.performed += ThrowInputOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.Drop.performed += DropInputOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridUp.performed += GridUpOnPerformed;
        SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridDown.performed += GridDownOnPerformed;
        _isVerticalityEnabled = true;
    }

    private void FixedUpdate()
    {
        if (_animatingAnInteraction)
        {
            return;
        }
        
        // Gather all colliders in a sphere radius
        var cols = new Collider[15];
        var colCount = Physics.OverlapSphereNonAlloc(_socketForInteraction.position, _collisionRadius, cols, _mask);
        
        if (colCount > 0 && _grid != null && !_grid.CursorController.IsCursorActive)
        {
            // Store the closest collider in that sphere
            var colToHighlight = FindClosest(cols, out var closestInteractable, colCount);

            // Check if there is in fact a collider that was stored in the sphere
            if (colToHighlight != null)
            {
                UpdateHighlightState(closestInteractable);
                Interactable interactableToHighlight = null;
                
                switch (_currentHighlightingState)
                {
                    case HighlightState.SphereCast:
                        _lastSphereCastClosest = closestInteractable;
                        interactableToHighlight = closestInteractable.GetInteractableDownTheChain();
                        break;
                    case HighlightState.GridCell:
                        closestInteractable = CalculateObjectToHighlightOnGrid();
                        interactableToHighlight = closestInteractable;
                        break;
                }

                if (closestInteractable == null)
                {
                    return;
                }
                
                var hasValidDirection = SingletonManager.Instance.GameManager.CurrentInteractionMode == InteractionMode.Edit || 
                                        closestInteractable.CompareDirection(GetDirectionRelativeToInteractable(closestInteractable));
                
                // TODO Highlight newly placed object when working with container and placing it
                if (interactableToHighlight != _currentHighlightedInteractable || !hasValidDirection)
                {
                    // Checks if there was a last Highlighted object, then UNHIGHLIGHT IT
                    if (_currentHighlightedInteractable != null)
                    {
                        if (_currentHighlightingState == HighlightState.SphereCast)
                        {
                            _currentGridCell = null;
                        }
                        
                        StopHighlightInteractable();
                    }

                    if (hasValidDirection)
                    {
                        if (_currentHighlightingState == HighlightState.GridCell)
                        {
                            SingletonManager.Instance.InteractionUIController.VerticalUIController.ShowVerticalUI(true);
                        }
                        
                        HighlightInteractable(interactableToHighlight);
                    }
                }
            }
            // If there is NO collider found nearby 
            else
            {
                //Checks if we had previously an object highlighted and Unhighlight it
                if (_currentHighlightedInteractable != null)
                { 
                    _currentGridCell = null;
                    StopHighlightInteractable();
                }
            }
        }
        // If there is NO collider found nearby and we had previously one object highlighted then UNHIGHLIGHT it
        else if (_currentHighlightedInteractable != null)
        {
            _currentGridCell = null;
            StopHighlightInteractable();
        }
    }

    public void ToggleVerticality(bool on)
    {
        if (on && !_isVerticalityEnabled)
        {
            _isVerticalityEnabled = true;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridUp.performed += GridUpOnPerformed;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridDown.performed += GridDownOnPerformed;
        }
        else
        {
            _isVerticalityEnabled = false;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridUp.performed -= GridUpOnPerformed;
            SingletonManager.Instance.InputManager.InputMaster.GenericGameplay.GridDown.performed -= GridDownOnPerformed;
        }
    }

    private void SetCloseUpMechanic(bool isOn)
    {
        InCloseUpMechanic = isOn;
    }

    /// <summary>
    /// Get the Interactable to highlight when changing verticality on grid
    /// </summary>
    /// <returns>The interactable on the grid cell</returns>
    private Interactable CalculateObjectToHighlightOnGrid()
    {
        if (!_verticalityHandler.IsCurrentIndexValidAndOccupied(_currentGridCell))
        {
            _verticalityHandler.GetIndexOfFirstOccupiedSocket(_currentGridCell);
        }
        
        return _currentHighlightingState == HighlightState.SphereCast ? null : 
            _currentGridCell.GridSockets[_verticalityHandler.VerticalIndex].GetInteractableInSocket();
    }

    // Check whether the last closest found has changed or not. If so, revert to SphereCast method
    private void UpdateHighlightState(Interactable closestInteractable)
    {
        var movable = closestInteractable as Movable;
        if (movable != null)
        {
            var gridCell = _grid.GetGridCellAtPosition(closestInteractable.transform.position);
            if (_currentGridCell != gridCell && gridCell != null)
            {
                _currentGridCell = gridCell;
            }
        }
        
        if (_currentHighlightingState != HighlightState.GridCell || _lastSphereCastClosest == closestInteractable)
        {
            return;
        }
    
        _currentHighlightingState = HighlightState.SphereCast;
    }

    private void ThrowInputOnPerformed(InputAction.CallbackContext ctx)
    {
        var button = (ButtonControl)ctx.control;
        if (button.wasPressedThisFrame)
        {
            ThrowStarted(ctx.time);
        }
        else if (button.wasReleasedThisFrame)
        {
            ThrowObject(ctx.time);
        }
    }

    private void DropInputOnPerformed(InputAction.CallbackContext ctx)
    {
        DropObject();
    }
    
    private void OnPhoneStatusUpdated(bool on)
    {
        _animator.SetBool(OnPhoneAnimID, on);
    }

    private void OnInputChanged(InputActionMap map)
    {
        _animator.SetBool(EditModeAnimID, map == SingletonManager.Instance.InputManager.InputMaster.Moveable.Get());
    }
    
    private void GridDownOnPerformed(InputAction.CallbackContext obj)
    {
        if (_currentGridCell == null)
        {
            return;
        }
        
        _currentHighlightingState = HighlightState.GridCell;
        _currentGridCell.MoveDownGridSocket(_verticalityHandler.MoveIndexDown(_currentGridCell));
    }

    private void GridUpOnPerformed(InputAction.CallbackContext obj)
    {
        if (_currentGridCell == null)
        {
            return;
        }
        
        _currentHighlightingState = HighlightState.GridCell;
        _currentGridCell.MoveUpGridSocket(_verticalityHandler.MoveIndexUp(_currentGridCell));
    }

    private InteractDirection GetDirectionRelativeToInteractable(Interactable interactable)
    {
        if (interactable == null)
        {
            return default;
        }
        
        var interactableTransform = interactable.transform;
        var angle = Vector3.SignedAngle(transform.position - interactableTransform.position, interactableTransform.right, Vector3.up);
        if (angle < 0)
        {
            angle = 360 - angle * -1;
        }

        if (angle > 45 && angle <= 135)
        {
            return InteractDirection.Front;
        }

        if ((angle > 0 && angle < 45) || (angle > 315 && angle < 360))
        {
            return InteractDirection.Left;
        }

        if (angle > 135 && angle <= 225)
        {
            return InteractDirection.Right;
        }

        if (angle > 225 && angle <= 315)
        {
            return InteractDirection.Back;
        }

        return 0;
    }

    private Collider FindClosest(Collider[] cols, out Interactable outInteractable, int maxCol)
    {       
        if (InCloseUpMechanic)
        {
            outInteractable = null;
            return null;
        }

        GridCell closestCell = _grid.GetGridCellAtPosition(_socketForInteraction.position);
        Interactable closestInteractable = null;
        Collider closest = null;
        var closestDistance = Mathf.Infinity;

        for (var i = 0; i < maxCol; i++)
        {
            var interactable = cols[i].GetComponent<Interactable>();
            if (!interactable || !interactable.enabled)
            {
                continue;
            }

            if (interactable.IgnoreFromInteraction)
            {
                continue;
            }

            var colliderCenterDistance = cols[i].bounds.center - _socketForInteraction.position;
            if (!(colliderCenterDistance.magnitude < closestDistance))
            {
                continue;
            }
            
            closest = cols[i];
            closestDistance = colliderCenterDistance.magnitude;
            closestInteractable = interactable;
        }

        // Check for a closer grid cell for empty-floor -> wall selection
        if (closestCell != null && Vector3.Distance(closestCell.GetWorldPosition(), _socketForInteraction.position) <
            closestDistance)
        {
            // Check if the grid cell has a wall gridsocket taken and empty floor
            if (closestCell.WallSocket != null && !closestCell.FloorSocket.IsOccupied() && closestCell.WallSocket.IsOccupied())
            {
                outInteractable = closestCell.WallSocket.GetInteractableInSocket();
                return outInteractable.Collider;
            }
        }
            
        outInteractable = closestInteractable;
        return closest;
    }

    public void TryPullPhoneOut()
    {
        if (!CanUsePhone)
        {
            return;
        }
        
        SingletonManager.Instance.CameraManager.PhoneController.PhoneButton(default);
    }
    
    public bool TryPlaceObject(Socket socket, bool alignRotation = true)
    {
        if (CurrentlyInMainHand != null)
        {
            CurrentlyInMainHand.PlaceOnASocket(socket, alignRotation);
            UnEquipObjectFromHands();
            return true;
        }
        
        
        return false;
    }

    public bool TryAddObjectsToHands(Pickable pickable, out PlayerHand hand)
    {
        if (CurrentlyInMainHand == null /*|| CurrentlyInMainHand.Equals(pickable)*/)
        {
            hand = _mainHand;
            pickable.OnPickUp += EquipObjectInHands;
            return true;
        }
        
        if (_currentlyInSecondHand == null /*|| CurrentlyInMainHand.Equals(pickable)*/)
        {
            hand = _secondHand;
            pickable.OnPickUp += EquipObjectInHands;
            return true;
        }

        hand = null;
        return false;
    }

    private void EquipObjectInHands(Pickable pickable, PlayerHand hand)
    {
        _animator.ResetTrigger(ChargeThrowAnimID);
        _animator.ResetTrigger(ThrowAnimID);
        
        switch (hand.Hand)
        {
            case PlayerHand.HandType.Main:
                CurrentlyInMainHand = pickable;
                CurrentlyInMainHand.Highlight(false);
                SingletonManager.Instance.InteractionUIController.SetHeldInteractable(pickable);
                break;
            case PlayerHand.HandType.Second:
                _currentlyInSecondHand = pickable;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        pickable.OnPickUp -= EquipObjectInHands;
    }
    
    public void UnEquipObjectFromHands(PlayerHand.HandType hand = PlayerHand.HandType.Main)
    {
        switch (hand)
        {
            case PlayerHand.HandType.Main:
                CurrentlyInMainHand = null;
                SingletonManager.Instance.InteractionUIController.SetHeldInteractable(null);
                _animator.SetBool(HoldingAnimID, false);
                break;
            case PlayerHand.HandType.Second:
                _currentlyInSecondHand = null;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(hand), hand, null);
        }
    }

    public void StopInteractionAnimation()
    {
        _animator.ResetTrigger(ExitAnimID);

        if (!_animatingAnInteraction)
        {
            return;
        }
        
        StopAllCoroutines();
        _animator.speed = 1f;

        if (_currentAnimConfig.LockPlayerMovement)
        {
            _playerController.ToggleMovement(true);
        }

        if (_currentAnimConfig.AnimationDelay == AnimationActionDelayType.InputLength)
        {
            _animator.SetTrigger(ExitAnimID);
            
            if (_currentAnimConfig.PlaceInMachineSocketWhileInteracting && _currentHighlightedInteractable is IAnimatableSocket machineSocket)
            {
                var pickable = machineSocket.RemoveFromAnimatableSocket();
                var pickableAnimator = pickable.GetComponent<Animator>();

                if (pickableAnimator)
                {
                    pickableAnimator.SetBool(_currentAnimConfig.AnimationToPlay.ToString(), false);
                }
                
                _animator.SetBool(HoldingAnimID, true);
            }
        }

        _animatingAnInteraction = false;

        if (_currentlyInSecondHand == null)
        {
            return;
        }
        
        _currentlyInSecondHand.OnCompletedInteraction();
        _currentlyInSecondHand = null;
    }

    private void HighlightInteractable(Interactable interactable)
    {
        _currentHighlightedInteractable = interactable;
        _currentHighlightedInteractable.Highlight(true);
        
        if (_currentHighlightingState == HighlightState.SphereCast && 
            _currentHighlightedInteractable is Containable containable && !containable.Socket.IsEmpty)
        {
            _currentHighlightedInteractable = containable.Socket.CurrentInteractable;
        }

        _currentHighlightedInteractable.BeginInteraction += OnPlayInteraction;
        _currentHighlightedInteractable.EndInteraction += OnPlayInteraction;
        
        _currentHighlightedInteractable.BindInput();
        
        if (_currentHighlightingState != HighlightState.SphereCast || _currentGridCell == null)
        {
            return;
        }
            
        var index = _verticalityHandler.GetIndexOfInteractable(_currentGridCell, _currentHighlightedInteractable);
        SingletonManager.Instance.InteractionUIController.VerticalUIController.InitializeUIWithGridCellData(
            _currentGridCell, index);
    }

    private void StopHighlightInteractable()
    {
        if (_currentGridCell == null)
        {
            SingletonManager.Instance.InteractionUIController.VerticalUIController.ShowVerticalUI(false);
        }
        
        _currentHighlightedInteractable.Highlight(false);
        
        _currentHighlightedInteractable.BeginInteraction -= OnPlayInteraction;
        _currentHighlightedInteractable.EndInteraction -= OnPlayInteraction;
        
        _currentHighlightedInteractable.UnbindInput();
        _currentHighlightedInteractable = null;
    } 

    private void OnPlayInteraction(ActionInputConfig actionInputConfig)
    {
        if (_animatingAnInteraction)
        {
            return;
        }
        
        if (actionInputConfig.AnimationConfig == null)
        {
            Debug.LogError($"Missing Animation Config on {actionInputConfig.name} Scriptable");
            return;
        }
        
        _animatingAnInteraction = true;

        if (actionInputConfig.AnimationConfig.NeedsBothHands && _currentHighlightedInteractable is Pickable pickable)
        {
            pickable.PickUp();
        }
        else if (actionInputConfig.AnimationConfig.ObjectToSpawnInHands != null && CurrentlyInMainHand == null)
        {
            var spawnedObject = Instantiate(actionInputConfig.AnimationConfig.ObjectToSpawnInHands, _mainHand.TransformSocket.position, Quaternion.identity);
            spawnedObject.PickUp();
        }

        if (actionInputConfig.AnimationConfig.LockPlayerMovement)
        {
            _playerController.ToggleMovement(false);
        }

        if (actionInputConfig.AnimationConfig.PositionPlayerInFront)
        {
            var interactionSocket = _currentHighlightedInteractable.InteractionSocket;
            
            if (interactionSocket == null)
            {
                Debug.LogWarning("AnimationConfig is set to PositionPlayerInFront but there is no InteractionSocket on the "
                                 + _currentHighlightedInteractable.InteractableName);
            }
            else
            {
                _playerController.SetPlayerInteractionSocket(interactionSocket, _currentHighlightedInteractable);
            }
        }

        if (actionInputConfig.AnimationConfig.ForceLookAt)
        {
            _playerController.SetPlayerLookDirection(_currentHighlightedInteractable.transform);
        }

        if (actionInputConfig.AnimationConfig.PlaceInMachineSocketWhileInteracting && _currentHighlightedInteractable is IAnimatableSocket animatableSocket)
        {
            var animator = CurrentlyInMainHand.GetComponent<Animator>();

            if (animator)
            {
                animator.SetBool(actionInputConfig.AnimationConfig.AnimationToPlay.ToString(), true);
            }
            
            animatableSocket.AddToAnimatableSocket(actionInputConfig.AnimationConfig.AnimationToPlay, CurrentlyInMainHand);
        }

        var anim = actionInputConfig.AnimationConfig.AnimationToPlay.ToString();
        var isHolding = actionInputConfig.AnimationConfig.AnimationToPlay == InteractableAnimationType.Pickup;
        var delay = 0f;
        var animSpeed = 1f;
        
        var length = 0f;
        var ac = _animator.runtimeAnimatorController;
        foreach (var clip in ac.animationClips)
        {
            if (clip.name == anim)
            {
                length = clip.length;
                break;
            }
        }

        if (actionInputConfig.AnimationConfig.AnimationDelay == AnimationActionDelayType.InputLength)
        {
            delay = actionInputConfig.HoldTime;
            if (length > 0f)
            {
                animSpeed = length / actionInputConfig.HoldTime;
            }
        }

        _currentAnimConfig = actionInputConfig.AnimationConfig;

        StartCoroutine(PlayInteractionAnimation(anim, delay, animSpeed, useArmLayer: isHolding));
    }
    
    private IEnumerator PlayInteractionAnimation(string animEvent, float playDelay = 0f, float animSpeed = 1f, bool useArmLayer = false)
    {
        if (useArmLayer)
        {
            _animator.SetTrigger(animEvent);
            _animator.SetBool(HoldingAnimID, true);
        }
        else if (!animEvent.Equals(InteractableAnimationType.None.ToString()))
        {
            _animator.CrossFade(animEvent, 0.10f);
        }
        
        _animator.speed = animSpeed;
        
        yield return new WaitForSeconds(playDelay);
        
        _animator.speed = 1f;

        if (_currentAnimConfig.LockPlayerMovement)
        {
            _playerController.ToggleMovement(true);
        }

        if (_currentAnimConfig.PlaceInMachineSocketWhileInteracting)
        {
            _animator.SetBool(HoldingAnimID, true);
        }
        
        _animatingAnInteraction = false;
        _currentAnimConfig = null;
    }
    
    private void ThrowStarted(double throwTime)
    {
        _animator.ResetTrigger(ChargeThrowAnimID);
        _animator.ResetTrigger(ThrowAnimID);
        
        _lastThrowTime = (float)throwTime;
        if (CurrentlyInMainHand == null || !CurrentlyInMainHand.CanThrow)
        {
            return;
        }
        
        _animator.SetTrigger(ChargeThrowAnimID);
    }

    private void ThrowObject(double throwTime)
    {
        if (CurrentlyInMainHand == null || !CurrentlyInMainHand.CanThrow)
        {
            return;
        }

        _animator.SetTrigger(ThrowAnimID);

        var power = throwTime - _lastThrowTime;
        CurrentlyInMainHand.Throw((float)power);
    }

    private void DropObject()
    {
        if (CurrentlyInMainHand == null || !CurrentlyInMainHand.CanThrow)
        {
            return;
        }
        
        CurrentlyInMainHand.Drop();
    }
    
    private void InteractionModeChanged(InteractionMode interactionMode)
    {
        _broom.SetActive(interactionMode == InteractionMode.Maintenance);
    }
}

[Serializable]
public class PlayerHand
{
    public enum HandType
    {
        Main,
        Second
    }

    [field: SerializeField]
    public HandType Hand { get; private set; }
    
    [field: SerializeField]
    public Transform TransformSocket { get; private set; }
}