using AI.Feedback;
using UnityEngine;

public class SingletonManager : MonoBehaviour
{
    public static SingletonManager Instance { get; private set; }
    [field: SerializeField] public OrderManager OrderManager { get; private set; }
    [field: SerializeField] public FeedbackCatalogManager FeedbackCatalogManager { get; private set; }
    [field: SerializeField] public GameManager GameManager { get; private set; }
    [field: SerializeField] public CurrenciesManager CurrenciesManager { get; private set; }
    [field: SerializeField] public CafeManager CafeManager { get; private set; }
    [field: SerializeField] public InputManager InputManager { get; private set; }
    [field: SerializeField] public DataPersistenceManager DataPersistenceManager { get; private set; }
    [field: SerializeField] public CharacterAppearanceManager CharacterAppearanceManager { get; private set; }
    [field: SerializeField] public AudioManager AudioManager { get; private set; }
    [field: SerializeField] public ProgressionManager ProgressionManager { get; private set; }
    [field: SerializeField] public BeanOSManager BeanOSManager { get; private set; }
    [field: SerializeField] public TaskManager TaskManager { get; private set; }
    [field: SerializeField] public UnlockableItemManager UnlockableItemManager { get; private set; }
    [field: SerializeField] public DialogueManager DialogueManager { get; private set; }
    [field: SerializeField] public GameNotificationManager GameNotificationManager { get; private set; }
    [field: SerializeField] public GameStatsTracker GameStatsTracker { get; private set; }
    [field: SerializeField] public GridManager GridManager { get; private set; }
    public WorldObjectsHandler WorldObjectsHandler { get; private set; } = new();
    public GameEventDispatcher GameEventDispatcher { get; private set; } = new();
    public InteractionUIController InteractionUIController { get; private set; }
    public CameraManager CameraManager { get; private set; }
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
        
        DontDestroyOnLoad(gameObject);
    }

    public void SetInteractionUIController(InteractionUIController interactionUIController)
    {
        if (!InteractionUIController)
        {
            InteractionUIController = interactionUIController;
        }
    }
    
    public void SetCameraManager(CameraManager cameraManager)
    {
        if (!CameraManager)
        {
            CameraManager = cameraManager;
        }
    }
}

