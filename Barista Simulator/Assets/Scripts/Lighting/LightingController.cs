using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode]
public class LightingController : MonoBehaviour
{
    public event Action<bool> EveningReached = delegate { }; 
    
    private static readonly int TopColor = Shader.PropertyToID("_TopColor");
    private static readonly int BottomColor = Shader.PropertyToID("_BottomColor");
    
    public float MorningValue => _morningValue;
    public float NightValue => _nightValue;
    
    [SerializeField] private bool _debug;
    [ProgressBar("_morningValue", "_nightValue"), SerializeField] private float _transitionSlider;
    [SerializeField] private float _morningValue, _noonValue, _eveningValue, _nightValue;
    [SerializeField] private Light _directionalLight;
    [SerializeField] private Light _ambientLight;
    [SerializeField] private LightingScenarioConfig _morningScenarioConfig, _noonScenarioConfig, _eveningScenarioConfig, _nightScenarioConfig;
    
    private ProbeReferenceVolume _probeReferenceVolume;
    private bool _eveningReached;
    
    private void Awake()
    {
        _probeReferenceVolume = ProbeReferenceVolume.instance;
        _probeReferenceVolume.BlendLightingScenario(_morningScenarioConfig.ScenarioName, 0f);
    }
    
    private void Update()
    {
        if (Application.IsPlaying(this) || !_debug)
        {
            _probeReferenceVolume ??= ProbeReferenceVolume.instance;
            return;
        }
        
        UpdateLighting(_transitionSlider);
    }
    
    public void UpdateLighting(float value)
    {
        if (value > _nightValue || value < _morningValue)
        {
            Debug.LogError($"Trying to change lighting with an invalid value {value}");
            return;
        }
        
        var blendValue = value;
        // Morning -> Noon
        if (value < _noonValue)
        {
            if (_eveningReached)
            {
                EveningReached?.Invoke(false);
                _eveningReached = false;
            }
            
            _probeReferenceVolume.lightingScenario = _morningScenarioConfig.ScenarioName;
            blendValue = Mathf.InverseLerp(_morningValue, _noonValue, blendValue);
            _directionalLight.transform.rotation = Quaternion.Lerp(Quaternion.Euler(_morningScenarioConfig.LightDirection)
                , Quaternion.Euler(_noonScenarioConfig.LightDirection), blendValue);
            _directionalLight.intensity = Mathf.Lerp(_morningScenarioConfig.LightIntensity,
                _noonScenarioConfig.LightIntensity, blendValue);
            _ambientLight.intensity = Mathf.Lerp(_morningScenarioConfig.AmbientLightIntensity,
                _noonScenarioConfig.AmbientLightIntensity, blendValue);
            _directionalLight.color =
                Color.Lerp(_morningScenarioConfig.LightColor, _noonScenarioConfig.LightColor, blendValue);
            RenderSettings.ambientLight = Color.Lerp(_morningScenarioConfig.EnvironmentColor,
                _noonScenarioConfig.EnvironmentColor, blendValue);
            RenderSettings.skybox.SetColor(TopColor,
                Color.Lerp(_morningScenarioConfig.Skybox.GetColor(TopColor), _noonScenarioConfig.Skybox.GetColor(TopColor),
                    blendValue));
            RenderSettings.skybox.SetColor(BottomColor,
                Color.Lerp(_morningScenarioConfig.Skybox.GetColor(BottomColor),
                    _noonScenarioConfig.Skybox.GetColor(BottomColor), blendValue));
            _probeReferenceVolume.BlendLightingScenario(_noonScenarioConfig.ScenarioName, blendValue);
        }
        // Noon -> Evening
        else if (value >= _noonValue && value < _eveningValue)
        {
            if (_eveningReached)
            {
                EveningReached?.Invoke(false);
                _eveningReached = false;
            }
            
            _probeReferenceVolume.lightingScenario = _noonScenarioConfig.ScenarioName;
            blendValue = Mathf.InverseLerp(_noonValue, _eveningValue, value);
            _directionalLight.transform.rotation = Quaternion.Lerp(Quaternion.Euler(_noonScenarioConfig.LightDirection)
                , Quaternion.Euler(_eveningScenarioConfig.LightDirection), blendValue);
            _directionalLight.intensity = Mathf.Lerp(_noonScenarioConfig.LightIntensity, _eveningScenarioConfig.LightIntensity, blendValue);
            _ambientLight.intensity = Mathf.Lerp(_noonScenarioConfig.AmbientLightIntensity,
                _eveningScenarioConfig.AmbientLightIntensity, blendValue);
            _directionalLight.color = Color.Lerp(_noonScenarioConfig.LightColor, _eveningScenarioConfig.LightColor, blendValue);
            RenderSettings.ambientLight = Color.Lerp(_noonScenarioConfig.EnvironmentColor, _eveningScenarioConfig.EnvironmentColor, blendValue);
            RenderSettings.skybox.SetColor(TopColor,
                Color.Lerp(_noonScenarioConfig.Skybox.GetColor(TopColor), _eveningScenarioConfig.Skybox.GetColor(TopColor),
                    blendValue));
            RenderSettings.skybox.SetColor(BottomColor,
                Color.Lerp(_noonScenarioConfig.Skybox.GetColor(BottomColor),
                    _eveningScenarioConfig.Skybox.GetColor(BottomColor), blendValue));
            _probeReferenceVolume.BlendLightingScenario(_eveningScenarioConfig.ScenarioName, blendValue);
        }
        // Evening -> Night
        else if (value >= _eveningValue)
        {
            if (!_eveningReached)
            {
                EveningReached?.Invoke(true);
                _eveningReached = true;
            }
            
            _probeReferenceVolume.lightingScenario = _eveningScenarioConfig.ScenarioName;
            blendValue = Mathf.InverseLerp(_eveningValue, _nightValue, value);
            _directionalLight.transform.rotation = Quaternion.Lerp(Quaternion.Euler(_eveningScenarioConfig.LightDirection)
                , Quaternion.Euler(_nightScenarioConfig.LightDirection), blendValue);
            _directionalLight.intensity = Mathf.Lerp(_eveningScenarioConfig.LightIntensity, _nightScenarioConfig.LightIntensity, blendValue);
            _ambientLight.intensity = Mathf.Lerp(_eveningScenarioConfig.AmbientLightIntensity,
                _nightScenarioConfig.AmbientLightIntensity, blendValue);
            _directionalLight.color = Color.Lerp(_eveningScenarioConfig.LightColor, _nightScenarioConfig.LightColor, blendValue);
            RenderSettings.ambientLight = Color.Lerp(_eveningScenarioConfig.EnvironmentColor, _nightScenarioConfig.EnvironmentColor, blendValue);
            RenderSettings.skybox.SetColor(TopColor,
                Color.Lerp(_eveningScenarioConfig.Skybox.GetColor(TopColor), _nightScenarioConfig.Skybox.GetColor(TopColor),
                    blendValue));
            RenderSettings.skybox.SetColor(BottomColor,
                Color.Lerp(_eveningScenarioConfig.Skybox.GetColor(BottomColor),
                    _nightScenarioConfig.Skybox.GetColor(BottomColor), blendValue));
            _probeReferenceVolume.BlendLightingScenario(_nightScenarioConfig.ScenarioName, blendValue);
        }
    }
}
