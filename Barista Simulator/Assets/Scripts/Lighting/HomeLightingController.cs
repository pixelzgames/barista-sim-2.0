using UnityEngine;

public class HomeLightingController : MonoBehaviour
{
   [SerializeField] private LightingController _lightingController;

   private void Awake()
   {
      SetLighting(SingletonManager.Instance.GameManager.CurrentTimeOfDayState);
      SingletonManager.Instance.GameManager.ChangedTimeState += SetLighting;
   }

   private void SetLighting(TimeOfDay timeOfDay)
   {
      switch (timeOfDay)
      {
         case TimeOfDay.Morning:
            _lightingController.UpdateLighting(_lightingController.MorningValue);
            break;
         case TimeOfDay.Night:
            _lightingController.UpdateLighting(_lightingController.NightValue);
            break;
      }
   }

   private void OnDestroy()
   {
      SingletonManager.Instance.GameManager.ChangedTimeState -= SetLighting;
   }
}
