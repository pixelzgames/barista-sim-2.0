using UnityEngine;

public class CafeLightingController : MonoBehaviour
{
    [SerializeField] LightingController _lightingController;

    private void Update()
    {
        var timeLeftInDayNormalized = SingletonManager.Instance.CafeManager.TimeLeftInDayNormalized;
        timeLeftInDayNormalized = Mathf.Clamp(timeLeftInDayNormalized * _lightingController.NightValue,
            _lightingController.MorningValue, _lightingController.NightValue);
        _lightingController.UpdateLighting(timeLeftInDayNormalized);
    }
}