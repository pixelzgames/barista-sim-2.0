#if UNITY_EDITOR

using System.IO;
using UnityEditor;
using UnityEngine;

public class IconCreatorToolComponent : MonoBehaviour
{
    [SerializeField] private Camera _cameraToRender;
    [SerializeField] private string _fileName;
    
    [ContextMenu("Save Icon")]
    private void GrabIcon()
    {
        var activeRenderTexture = RenderTexture.active;
        RenderTexture.active = _cameraToRender.targetTexture;
 
        _cameraToRender.Render();
 
        var image = new Texture2D(_cameraToRender.targetTexture.width, _cameraToRender.targetTexture.height);
        image.ReadPixels(new Rect(0, 0, _cameraToRender.targetTexture.width, _cameraToRender.targetTexture.height), 0, 0);
        image.Apply();
        RenderTexture.active = activeRenderTexture;
 
        var bytes = image.EncodeToPNG();
        DestroyImmediate(image);
 
        File.WriteAllBytes(Application.dataPath + "/Icons/" + _fileName + ".png", bytes);
        AssetDatabase.Refresh();
        
        var importer = (TextureImporter)AssetImporter.GetAtPath("Assets/Icons/" + _fileName + ".png");
 
        importer.isReadable = true;
        importer.textureType = TextureImporterType.Sprite;
 
        var importerSettings = new TextureImporterSettings();
        importer.ReadTextureSettings(importerSettings);
        
        importerSettings.spriteGenerateFallbackPhysicsShape = false;
        importerSettings.spriteMeshType = SpriteMeshType.Tight;
        importerSettings.spriteMode = (int)SpriteImportMode.Single;
        importerSettings.sRGBTexture = false;
        importer.SetTextureSettings(importerSettings);
 
        importer.spriteImportMode = SpriteImportMode.Single;
        importer.maxTextureSize = 256;
        importer.alphaIsTransparency = true;
        importer.textureCompression = TextureImporterCompression.CompressedHQ;
        importer.alphaSource = TextureImporterAlphaSource.FromInput;
 
        EditorUtility.SetDirty(importer);
        importer.SaveAndReimport();
    }
}

#endif