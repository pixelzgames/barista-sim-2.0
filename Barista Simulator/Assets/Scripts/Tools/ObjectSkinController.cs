#if UNITY_EDITOR

using System.IO;
using UnityEditor;
using UnityEngine;

public class ObjectSkinController : MonoBehaviour
{
    [SerializeField] private GameObject _objectToSpawn;
    [SerializeReference] private Customizable _customizable;
    [SerializeField] private Camera _cameraToRender;

    [SerializeField]
    private GameObject _spawnedObject;

    [ContextMenu("Spawn with default skin")]
    private void SpawnObject()
    {
        if (_spawnedObject)
        {
            return;
        }
        
        _spawnedObject = Instantiate(_objectToSpawn, Vector3.zero, Quaternion.Euler(0, 15, 0));

        var rend = _spawnedObject.GetComponent<Renderer>();
        if (rend == null)
        {
            rend = _spawnedObject.GetComponentInChildren<Renderer>();
        }
        
        var center = rend.bounds.center;
        Debug.Log($"Center = {center}");
        var emptyParent = new GameObject("empty")
        {
            transform =
            {
                position = center
            }
        };
        
        _spawnedObject.transform.SetParent(emptyParent.transform);
        emptyParent.transform.position = Vector3.zero;
        _spawnedObject.transform.SetParent(null);
        DestroyImmediate(emptyParent);

        // Assign first default skin
        var receiver = _spawnedObject.GetComponent<CustomizationReceiver>();
        for (var i = 0; i < receiver.Renderers.Count; i++)
        {
           receiver.Renderers[i].materials = _customizable.AvailableThemes[0].MaterialsPerRenderer[i].Materials;
        }
    }

    [ContextMenu("Reset")]
    private void Reset()
    {
        if (_spawnedObject)
        {
            DestroyImmediate(_spawnedObject);
            _spawnedObject = null;
        }
    }

    [ContextMenu("Save Icon")]
    private void GrabIcon()
    {
        var activeRenderTexture = RenderTexture.active;
        RenderTexture.active = _cameraToRender.targetTexture;
 
        _cameraToRender.Render();
 
        var image = new Texture2D(_cameraToRender.targetTexture.width, _cameraToRender.targetTexture.height);
        image.ReadPixels(new Rect(0, 0, _cameraToRender.targetTexture.width, _cameraToRender.targetTexture.height), 0, 0);
        image.Apply();
        RenderTexture.active = activeRenderTexture;
 
        var bytes = image.EncodeToPNG();
        DestroyImmediate(image);
 
        File.WriteAllBytes(Application.dataPath + "/Icons/" + _customizable.ItemName + ".png", bytes);
        AssetDatabase.Refresh();
        
        var importer = (TextureImporter)AssetImporter.GetAtPath("Assets/Icons/" + _customizable.ItemName + ".png");
 
        importer.isReadable = true;
        importer.textureType = TextureImporterType.Sprite;
 
        var importerSettings = new TextureImporterSettings();
        importer.ReadTextureSettings(importerSettings);
        
        importerSettings.spriteGenerateFallbackPhysicsShape = false;
        importerSettings.spriteMeshType = SpriteMeshType.Tight;
        importerSettings.spriteMode = (int)SpriteImportMode.Single;
        importerSettings.sRGBTexture = false;
        importer.SetTextureSettings(importerSettings);
 
        importer.spriteImportMode = SpriteImportMode.Single;
        importer.maxTextureSize = 256;
        importer.alphaIsTransparency = true;
        importer.textureCompression = TextureImporterCompression.CompressedHQ;
        importer.alphaSource = TextureImporterAlphaSource.FromInput;
 
        EditorUtility.SetDirty(importer);
        importer.SaveAndReimport();
    }
}
#endif