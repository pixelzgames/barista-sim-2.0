#if UNITY_EDITOR

using Sirenix.OdinInspector;
using UnityEngine;

[ExecuteInEditMode]
public class TastingTagToolController : MonoBehaviour
{
#if UNITY_EDITOR
    
    [SerializeField] private FlavorDescriptorUIController _tagPrefab;
    [SerializeField] private Transform _rect;
    [SerializeField] private CoffeeDescriptorsDatabaseConfig _coffeeDescriptors;
    
    [Button("Generate tasting note tags")]
    private void GenerateFlavourTags()
    {
        foreach (var descriptor in _coffeeDescriptors.CoffeeDescriptorsDictionary)
        {
            var descriptorObject = Instantiate(_tagPrefab, _rect);
            var descriptorData = new CoffeeDescriptor(descriptor.Key, descriptor.Value);
            descriptorObject.InitWithData(descriptorData);
            descriptorObject.gameObject.SetActive(true);
        }
    }

    [Button("Clear tasting note tags")]
    private void ClearAll()
    {
        for (var i = _rect.childCount - 1; i < _rect.childCount; i--)
        {
            if (i == 0)
            {
                break;
            }

            DestroyImmediate(_rect.GetChild(i).gameObject);
        }
    }

    [Button("Refresh the look", ButtonSizes.Large)]
    private void RefreshLook()
    {
        ClearAll();
        GenerateFlavourTags();
    }
    
#endif
}

#endif