using System.IO;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;


public class AppearanceTool : MonoBehaviour
{
    private const string PATH = "Assets/ScriptableObjects/CharacterCustomization/Appearances/";

    private int _glassesStyleRange,
        _glassesColorRange,
        _eyeColorRange,
        _facialHairStyleRange,
        _facialHairColorRange,
        _apronStyleRange,
        _apronColorRange,
        _shirtStyleRange,
        _shirtColorRange,
        _pantsStyleRange,
        _pantsColorRange,
        _hatStyleRange,
        _hatColorRange,
        _skinColorRange,
        _shoeStyleRange,
        _shoeColorRange;
        
    private Appearance _appearanceToCreate;
    
    [Title("Appearance Values")]
    
    [SerializeField, PropertyRange(0, "_glassesStyleRange")] private int _glassesStyle;
    [SerializeField, PropertyRange(0, "_glassesColorRange")] private int _glassesColor;

    private int _eyeStyle;
    [SerializeField, PropertyRange(0, "_eyeColorRange")] private int _eyeColor;

    [SerializeField, PropertyRange(0, "_facialHairStyleRange")] private int _facialHairStyle;
    [SerializeField, PropertyRange(0, "_facialHairColorRange")] private int _facialHairColor;

    [SerializeField, PropertyRange(0, "_apronStyleRange")] private int _apronStyle;
    [SerializeField, PropertyRange(0, "_apronColorRange")] private int _apronColor;

    [SerializeField, PropertyRange(0, "_shirtStyleRange")] private int _shirtStyle;
    [SerializeField, PropertyRange(0, "_shirtColorRange")] private int _shirtColor;

    [SerializeField, PropertyRange(0, "_pantsStyleRange")] private int _pantsStyle;
    [SerializeField, PropertyRange(0, "_pantsColorRange")] private int _pantsColor;

    [SerializeField, PropertyRange(0, "_hatStyleRange")] private int _hatStyle;
    [SerializeField, PropertyRange(0, "_hatColorRange")] private int _hatColor;
    
    [SerializeField, PropertyRange(0, "_skinColorRange")] private int _skinColor;

    [SerializeField, PropertyRange(0, "_shoeStyleRange")] private int _shoeStyle;
    [SerializeField, PropertyRange(0, "_shoeColorRange")] private int _shoeColor;
    
    [SerializeField] private AppearanceCatalog _appearanceCatalog;
    [SerializeField] private UtilityAppearanceController _appearanceController;

    [SerializeField] private Camera _cameraToRender;
    
    [Button(ButtonSizes.Large), GUIColor(1, 0, 1)]
    [PropertyOrder(0)]
    private void RandomizeAppearance()
    {
        var newAppearance = _appearanceController.RandomizeAppearance();

        _appearanceToCreate = newAppearance;
        SetColorRanges();
        
        _glassesStyle = newAppearance.GlassesStyle;
        _glassesColor = newAppearance.GlassesColor;
        _eyeStyle = newAppearance.EyeStyle;
        _eyeColor = newAppearance.EyeColor;
        _facialHairStyle = newAppearance.FacialHairStyle;
        _facialHairColor = newAppearance.FacialHairColor;
        _apronStyle = newAppearance.ApronStyle;
        _apronColor = newAppearance.ApronColor;
        _shirtStyle = newAppearance.ShirtStyle;
        _shirtColor = newAppearance.ShirtColor;
        _pantsStyle = newAppearance.PantsStyle;
        _pantsColor = newAppearance.PantsColor;
        _hatStyle = newAppearance.HatStyle;
        _hatColor = newAppearance.HatColor;
        _skinColor = newAppearance.SkinColor;
        _shoeStyle = newAppearance.ShoeStyle;
        _shoeColor = newAppearance.ShoeColor;
    }

    
    [Title("EDIT EXISTING APPEARANCE")]
    [EnableIf("@_contactToLoad != null")]
    [Button(ButtonSizes.Large), GUIColor(1, 1, 0)]
    [PropertyOrder(1)]
    private void LoadAppearanceFromFile()
    {
        _appearanceToCreate =  _contactToLoad.ContactData._appearanceConfig.Appearance;
        _appearanceController.ApplyAppearanceFromExternal(_appearanceToCreate);
        SetColorRanges();
        
        _glassesStyle = _appearanceToCreate.GlassesStyle;
        _glassesColor = _appearanceToCreate.GlassesColor;
        _eyeStyle = _appearanceToCreate.EyeStyle;
        _eyeColor = _appearanceToCreate.EyeColor;
        _facialHairStyle = _appearanceToCreate.FacialHairStyle;
        _facialHairColor = _appearanceToCreate.FacialHairColor;
        _apronStyle = _appearanceToCreate.ApronStyle;
        _apronColor = _appearanceToCreate.ApronColor;
        _shirtStyle = _appearanceToCreate.ShirtStyle;
        _shirtColor = _appearanceToCreate.ShirtColor;
        _pantsStyle = _appearanceToCreate.PantsStyle;
        _pantsColor = _appearanceToCreate.PantsColor;
        _hatStyle = _appearanceToCreate.HatStyle;
        _hatColor = _appearanceToCreate.HatColor;
        _skinColor = _appearanceToCreate.SkinColor;
        _shoeStyle = _appearanceToCreate.ShoeStyle;
        _shoeColor = _appearanceToCreate.ShoeColor;
    }
    
    [EnableIf("@_contactToLoad != null")]
    [Button(ButtonSizes.Large), GUIColor(0, 1, 1)]
    [PropertyOrder(2)]
    private void SaveAppearanceToExistingFile()
    {
        _contactToLoad.ContactData._appearanceConfig.Appearance = _appearanceToCreate;
        _contactToLoad.ContactData.ContactImage = SaveCharacterIcon();
        AssetDatabase.SaveAssets();
    }
    
    [PropertyOrder(3)]
    //[SerializeField] private AppearanceData _appearanceToLoad;
    [SerializeField] private BeanOSContactConfig _contactToLoad;
    

    [Title("CREATE NEW APPEARANCE", "Will be created in " + PATH)]
    [PropertyOrder(4)]
    [EnableIf("@FileName.Length > 0")]
    [Button(ButtonSizes.Large), GUIColor(0, 1, 0)]
    private void CreateAssetFromCurrent()
    {
        CreateAppearanceAsset();
    }
    
    [PropertyOrder(5)]
    [LabelText("New File Name")] public string FileName = "New File Name";
    
    private void Start()
    {
        SetStyleRanges();
        SetColorRanges();
    }

    private void OnValidate()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        _appearanceToCreate = new Appearance(_glassesStyle, _glassesColor, _eyeStyle, _eyeColor, _facialHairStyle,
            _facialHairColor, _apronStyle, _apronColor, _shirtStyle, _shirtColor, _pantsStyle, _pantsColor, _hatStyle,
            _hatColor, 0, _skinColor, _shoeStyle, _shoeColor);
        
        _appearanceController.ApplyAppearanceFromExternal(_appearanceToCreate);
        SetColorRanges();
    }

    private void SetStyleRanges()
    {
        _glassesStyleRange = _appearanceCatalog.CustomizableCatalogs[0].Customizables.Count - 1;
        _facialHairStyleRange = _appearanceCatalog.CustomizableCatalogs[2].Customizables.Count - 1;
        _apronStyleRange = _appearanceCatalog.CustomizableCatalogs[3].Customizables.Count - 1;
        _shirtStyleRange = _appearanceCatalog.CustomizableCatalogs[4].Customizables.Count - 1;
        _pantsStyleRange = _appearanceCatalog.CustomizableCatalogs[5].Customizables.Count - 1;
        _hatStyleRange = _appearanceCatalog.CustomizableCatalogs[6].Customizables.Count - 1;
        _shoeStyleRange = _appearanceCatalog.CustomizableCatalogs[8].Customizables.Count - 1;
    }

    private void SetColorRanges()
    {
        _glassesColorRange = _appearanceCatalog.CustomizableCatalogs[0].Customizables[_appearanceToCreate.GlassesStyle].AvailableThemes.Count - 1;
        _eyeColorRange = _appearanceCatalog.CustomizableCatalogs[1].Customizables[_appearanceToCreate.EyeStyle].AvailableThemes.Count - 1;
        _facialHairColorRange = _appearanceCatalog.CustomizableCatalogs[2].Customizables[_appearanceToCreate.FacialHairStyle].AvailableThemes.Count - 1;
        _apronColorRange = _appearanceCatalog.CustomizableCatalogs[3].Customizables[_appearanceToCreate.ApronStyle].AvailableThemes.Count - 1;
        _shirtColorRange = _appearanceCatalog.CustomizableCatalogs[4].Customizables[_appearanceToCreate.ShirtStyle].AvailableThemes.Count - 1;
        _pantsColorRange = _appearanceCatalog.CustomizableCatalogs[5].Customizables[_appearanceToCreate.PantsStyle].AvailableThemes.Count - 1;
        _hatColorRange = _appearanceCatalog.CustomizableCatalogs[6].Customizables[_appearanceToCreate.HatStyle].AvailableThemes.Count - 1;
        _skinColorRange = _appearanceCatalog.CustomizableCatalogs[7].Customizables[_appearanceToCreate.SkinStyle].AvailableThemes.Count - 1;
        _shoeColorRange = _appearanceCatalog.CustomizableCatalogs[8].Customizables[_appearanceToCreate.ShoeStyle].AvailableThemes.Count - 1;
    }
    
    private void CreateAppearanceAsset()
    {
        BeanOSContactConfig contactConfig = ScriptableObject.CreateInstance<BeanOSContactConfig>();
        contactConfig.ContactData._appearanceConfig.Appearance = _appearanceToCreate;
        AssetDatabase.CreateAsset(contactConfig, PATH + FileName + ".asset");
    }
    
    [ContextMenu("Save Icon")]
    private Sprite SaveCharacterIcon()
    {
        var activeRenderTexture = RenderTexture.active;
        RenderTexture.active = _cameraToRender.targetTexture;
 
        _cameraToRender.Render();
 
        var image = new Texture2D(_cameraToRender.targetTexture.width, _cameraToRender.targetTexture.height);
        image.ReadPixels(new Rect(0, 0, _cameraToRender.targetTexture.width, _cameraToRender.targetTexture.height), 0, 0);
        image.Apply();
        RenderTexture.active = activeRenderTexture;
 
        var bytes = image.EncodeToPNG();
        DestroyImmediate(image);
 
        File.WriteAllBytes(Application.dataPath + "/Icons/CharacterThumbnails/" + _contactToLoad.ContactData.ContactName + "_Icon.png", bytes);
        AssetDatabase.Refresh();
        
        var importer = (TextureImporter)AssetImporter.GetAtPath("Assets/Icons/CharacterThumbnails/" + _contactToLoad.ContactData.ContactName + "_Icon.png");
 
        importer.isReadable = true;
        importer.textureType = TextureImporterType.Sprite;
 
        var importerSettings = new TextureImporterSettings();
        importer.ReadTextureSettings(importerSettings);
        
        importerSettings.spriteGenerateFallbackPhysicsShape = false;
        importerSettings.spriteMeshType = SpriteMeshType.Tight;
        importerSettings.spriteMode = (int)SpriteImportMode.Single;
        importerSettings.sRGBTexture = false;
        importer.SetTextureSettings(importerSettings);
 
        importer.spriteImportMode = SpriteImportMode.Single;
        importer.maxTextureSize = 256;
        importer.alphaIsTransparency = true;
        importer.textureCompression = TextureImporterCompression.CompressedHQ;
        importer.alphaSource = TextureImporterAlphaSource.FromInput;
 
        EditorUtility.SetDirty(importer);
        importer.SaveAndReimport();

        Texture2D texture = new Texture2D(_cameraToRender.targetTexture.width,  _cameraToRender.targetTexture.height, TextureFormat.RGB24, false);
        texture.LoadImage(File.ReadAllBytes(Application.dataPath + "/Icons/CharacterThumbnails/" + _contactToLoad.ContactData.ContactName + "_Icon.png"));
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        return sprite;
    }
}