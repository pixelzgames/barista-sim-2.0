using Sirenix.OdinInspector;
using UnityEngine;

public class GenericProductRefillBox : Pickable
{
    public int CurrentProductsCount => _currentProductsCount;
    
    [DisableInPlayMode]
    [SerializeField] private int _productsCapacity;

    private int _currentProductsCount;

    protected override void Awake()
    {
        base.Awake();

        //TODO Change with serialization as this is for debugging purposes
        _currentProductsCount = _productsCapacity;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        //If you do have something in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    public void WithdrawProducts(int count)
    {
        _currentProductsCount -= count;
        if (_currentProductsCount < 0)
        {
            _currentProductsCount = 0;
        }
    }
    
    private float GetPercentageFilled()
    {
        return Mathf.Clamp01((float)_currentProductsCount / _productsCapacity);
    }
    
    public override string GetInfoToDisplay()
    {
        return "Box is " +  "<b>" + GetPercentageFilled()*100f + "% </b>full.";
    }
}
