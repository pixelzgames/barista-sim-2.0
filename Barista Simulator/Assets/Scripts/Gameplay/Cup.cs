﻿using System.Linq;
using UnityEngine;

public class Cup : Pickable, IContainer<Liquid>, ICleanable
{
    private static readonly int FillAmountID = Shader.PropertyToID("_FillAmount");
    
    public bool Dirty { get; set; }
    public bool IsEmpty => _liquidContainer == null || _liquidContainer.IsEmpty();
    
    [field: SerializeField ] public OrderType OrderType { get; private set; } = OrderType.Stay;
    
    
    [Header("Cup properties")]
    [Tooltip("If the object is manually placed")]
    [SerializeField] private bool _isLidOnOnStart;
    
    [Tooltip("If the object is manually placed")]
    [SerializeField] private GameObject _stay;
    [SerializeField] private GameObject _togo;

    [SerializeField]
    private Sprite _togoSprite;
    [SerializeField]
    private Sprite _staySprite;
    
    [Header("Handles")]
    [SerializeField] private Transform _togoHandle;
    [SerializeField] private Transform _stayHandle;
    
    private bool _hasLidOn;
    private bool _hasLiquidIn;

    private GameObject _currentCup;
    private GameObject _lidMesh;
    private GameObject _liquidMesh;
    private Material _coffeeLiquidMaterial;
    private LiquidContainer _liquidContainer;

    protected override void Awake()
    {
        base.Awake();
        RefreshCupLook();
        InitContainer();
    }

    private void OnDisable()
    {
        _liquidContainer.ContentChanged -= UpdateLiquidMesh;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
                _currentInteractions.Add(action);
        }
        else if (_inHand is MilkPitcher pitcher && CurrentRecipe != null)
        {
            if (!pitcher.GetContainer().IsEmpty() && pitcher.Milk.IsFrothed)
            {
                if (CurrentRecipe.Config.RecipeType == RecipeType.Espresso && pitcher.Milk.FoamType == FoamType.Large)
                {
                    var action = GetActionByName("MakeCappuccino");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
                if (CurrentRecipe.Config.RecipeType == RecipeType.Espresso && pitcher.Milk.FoamType == FoamType.Micro)
                {
                    var action = GetActionByName("MakeLatte");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
                if (CurrentRecipe.Config.RecipeType == RecipeType.DoubleEspresso && pitcher.Milk.FoamType == FoamType.Micro)
                {
                    var action = GetActionByName("MakeFlatWhite");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
            }
        }
        else if (_inHand is Kettle kettle && CurrentRecipe != null)
        {
            if (CurrentRecipe.Config.RecipeType == RecipeType.Espresso && kettle.GetContainer().Content.GetTemperatureState() == TemperatureState.Boiling)
            {
                var action = GetActionByName("MakeAmericano");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
            
            if (CurrentRecipe.Config.RecipeType == RecipeType.DoubleEspresso && kettle.GetContainer().Content.GetTemperatureState() == TemperatureState.Boiling)
            {
                var action = GetActionByName("MakeLongBlack");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        else if (_inHand is Kettle kettleInHand)
        {
            if (RecipeMaker<Liquid>.CanMakeInstantCoffeeRecipe(kettleInHand.GetContainer(), GetContainer()))
            {
                var action = GetActionByName("MakeInstantCoffee");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            } 
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        if (_inHand is MilkPitcher pitcher)
        {
            var recipeType = RecipeType.Cappuccino;
            switch (actionName)
            {
                case "MakeCappuccino":
                    break;
                case "MakeLatte":
                    recipeType = RecipeType.Latte;
                    break;
                case "MakeFlatWhite":
                    recipeType = RecipeType.FlatWhite;
                    break;
            }
            
            RecipeMaker<Liquid>.MakeRecipe(recipeType, pitcher.GetContainer(), GetContainer(),
                pitcher.GetContainer().Content.Temperature.Current, GetCoffeeData());
            return;
        }

        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
            case "MakeInstantCoffee":
                if (_inHand is IContainer<Liquid> liquidContainer)
                {
                    RecipeMaker<Liquid>.MakeRecipe(RecipeType.InstantCoffee, liquidContainer.GetContainer(), GetContainer(),
                        liquidContainer.GetContainer().Content.Temperature.Current, GetContainer().Ingredients.OfType<InstantGrounds>().First().CoffeeData);
                }
                else if (_inHand is IContainer<Solid> solidContainer)
                {
                    RecipeMaker<Solid>.MakeRecipe(RecipeType.InstantCoffee, solidContainer.GetContainer(), GetContainer(),
                        GetContainer().Content.Temperature.Current, _inHand.GetCoffeeData());
                }
      
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    public void RefreshCupLook()
    {
        InitializeCupPrefab(OrderType, _isLidOnOnStart);
        _interactableIcon = OrderType == OrderType.Stay ? _staySprite : _togoSprite;
        UpdateLiquidMesh();
    }

    public void InitializeCupPrefab(OrderType type, bool lid)
    {
        OrderType = type;
        _hasLidOn = lid;

        _currentCup = OrderType switch
        {
            OrderType.Stay => _stay,
            OrderType.Togo => _togo,
            _ => _currentCup
        };

        if (!_collider)
        {
            return;
        }
        
        var mesh = _currentCup.GetComponent<MeshRenderer>();
        var bounds = mesh.bounds;
        ((BoxCollider) _collider).size = bounds.size;
        ((BoxCollider) _collider).center = _currentCup.transform.localPosition + (Vector3.up * bounds.size.y / 2f);

        SetCupIKHandle(type);
        
        DisplayProperCup();
    }

    private void DisplayProperCup()
    {
        //Cycle through children and hide them all
        for (var i = 0; i < 4; i++)
        {
            transform.GetChild(i).gameObject.SetActive(_currentCup == transform.GetChild(i).gameObject);
        }

        _interactableName = $"{OrderType} Cup";

        _lidMesh = OrderType == OrderType.Togo ? _currentCup.transform.Find("Cup_Lid").gameObject : null;
        _liquidMesh = _currentCup.transform.Find("Cup_Liquid").gameObject;
        _coffeeLiquidMaterial = _liquidMesh.GetComponent<MeshRenderer>().material;
        
        ToggleLid(_hasLidOn);
        UpdateLiquidMesh();
    }

    private void SetCupIKHandle(OrderType type)
    {
        _ikHandle = type switch
        {
            OrderType.Stay => _stayHandle,
            OrderType.Togo => _togoHandle,
            _ => _ikHandle
        };
    }

    public void ToggleLid(bool on)
    {
        if (OrderType == OrderType.Stay)
        {
            _hasLidOn = false;
            return;
        }
        _hasLidOn = on;
        _lidMesh.SetActive(_hasLidOn);
    }

    public void RaiseObjectStateEvent(ObjectState state)
    {
        _objectStateEventController.RaiseEvent(state);
    }
    
    public void RemoveCupContents()
    {
        GetContainer().Content = null;
    }

    public void AddIngredient(Ingredient ingredient)
    {
        GetContainer().Ingredients.Add(ingredient);
    }
    
    public void UpdateLiquidMesh()
    {
        if (_coffeeLiquidMaterial == null)
        {
            return;
        }
        
        _coffeeLiquidMaterial.SetFloat(FillAmountID, CurrentRecipe?.Servings ?? 0f); 
    }

    public override string GetInfoToDisplay()
    {
        var info = "";
        foreach (var ingredient in GetContainer().Ingredients)
        {
            info += ingredient.IngredientName + System.Environment.NewLine;
        }

        if (!IsEmpty)
        {
            info += $"Drink temperature is <b>{CurrentRecipe.GetTemperatureState().ToString().ToLower()}</b>." +
                    System.Environment.NewLine;
        }
        
        return info;
    }

    public void InitContainer()
    {
        _liquidContainer = new LiquidContainer(CurrentRecipe,1, true);
        _liquidContainer.ContentChanged += UpdateLiquidMesh;
        UpdateLiquidMesh();
    }
    
    public override LiquidContainer GetLiquidWaterData()
    {
        if (CurrentRecipe != null || GetContainer().Ingredients.Count <= 0)
        {
            return null;
        }
        
        foreach (var ingredient in GetContainer().Ingredients)
        {
            if (ingredient is Water water)
            {
                return new LiquidContainer(water, 1 ,false);
            }
        }

        return _liquidContainer;
    }

    public Container<Liquid> GetContainer()
    {
        return _liquidContainer;
    }

    public override LiquidContainer GetLiquidCoffeeData()
    {
        return _liquidContainer;
    }

    public void Clean()
    {
        RemoveCupContents();
    }
}
