using UnityEngine;
using UnityEngine.Serialization;

public class BossInteractable : Interactable
{
    [FormerlySerializedAs("_contactData")] [SerializeField] private BeanOSContactConfig _contactConfig;
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
 
        var action = GetActionByName("Talk");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Talk":
                Talk();
                base.Highlight(false);
                break;
        }

        base.ExecuteAction(actionName);
    }
    
    private void Talk()
    {
        if (!_contactConfig)
        {
            return;
        }
        
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.TalkToBoss));
        SingletonManager.Instance.DialogueManager.RequestDialogueSequence(_contactConfig.ContactData);
    }
}
