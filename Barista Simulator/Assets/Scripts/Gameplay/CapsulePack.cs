public class CapsulePack : Pickable
{
    /// <summary>
    /// Called when refilling capsules 
    /// </summary>
    public void OnBeingUsed()
    {
        Drop();
        Destroy(gameObject);
    }
}
