using UnityEngine;

public class KnockBox : Movable, IKnockableMechanic
{
    [SerializeField] [Range(1,10)] private int _knocksToEmpty;
    private float _filledPercentage;
    
    [Header("Gameplay mechanic")]
    [SerializeField] private KnockingGameplayMechanic _gameplayMechanicController;
    
    private Portafilter _portafilter;

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null && _filledPercentage > 0f)
        {
            var action = GetActionByName("Empty");
            if (action != null)
                _currentInteractions.Add(action);
        }
        
        if (_inHand != null && _inHand is Portafilter portafilter && portafilter.Dirty)
        {
            var action = GetActionByName("Knock");
            if (action != null)
                _currentInteractions.Add(action);
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Knock":
                Knock();
                break;
            case "Empty":
                Empty();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void Empty() 
    {
        _filledPercentage = 0f;
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Bin emptied!"));
    }

    private void Knock()
    {
        _portafilter = _inHand as Portafilter;
        StartKnockingMechanic();
    }
    
    private void StartKnockingMechanic()
    {
        _gameplayMechanicController.OnFinishedKnocking += OnFinishedKnockingMechanic;
        _gameplayMechanicController.StartMechanic(this);
    }
    
    private void OnFinishedKnockingMechanic()
    {
        _gameplayMechanicController.OnFinishedKnocking -= OnFinishedKnockingMechanic;
        _portafilter.Clean();
    }
    
    public override string GetInfoToDisplay()
    {
        return $"Trash is {_filledPercentage}% full!";
    }
    
    public Interactable GetInteractable()
    {
        return this;
    }

    public int GetKnocksRequiredToEmpty()
    {
        return _knocksToEmpty;
    }
}
