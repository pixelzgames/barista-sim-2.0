using System;
using System.Linq;
using UnityEngine;

public class CuppingBowl : Pickable, IContainer<Liquid>
{
    private static readonly int FillAmountID = Shader.PropertyToID("_FillAmount");

    public Grounds CurrentGrounds => _liquidContainer.Ingredients.OfType<Grounds>().First();
    public bool IsEmpty => CurrentRecipe == null && _liquidContainer.Ingredients.Count == 0;
    
    [SerializeField] private MeshRenderer _coffeeLiquidMeshRenderer;

    private LiquidContainer _liquidContainer;
    private Material _coffeeLiquidMaterial;
    private bool _cupped;
    
    protected override void Awake()
    {
        base.Awake();
        _coffeeLiquidMaterial = _coffeeLiquidMeshRenderer.material;
        InitContainer();
    }

    private void OnDisable()
    {
        _liquidContainer.ContentChanged -= UpdateLiquidMesh;
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
            case "Pour":
                AddWater();
                break;
            case "CupCoffee":
                RevealCoffeeData();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void AddWater()
    {
        if (_inHand is not Kettle kettle)
        {
            return;
        }

        RecipeMaker<Liquid>.MakeRecipe(RecipeType.Cupping, kettle.GetContainer(), GetContainer(),
            kettle.Water.Temperature.Current, CurrentGrounds.CoffeeData);
    }
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        //If you dont have anything in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }

            if (CurrentRecipe != null && !_cupped)
            {
                action = GetActionByName("CupCoffee");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        else if (_inHand is Kettle kettle && _liquidContainer.Ingredients.Count > 0 && _liquidContainer.Ingredients.OfType<Grounds>().Any())
        {
            // Kettle in hands with Coffee Grounds in the bowl
            if (kettle.Water.Servings >= 1)
            {
                var action = GetActionByName("Pour");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }

        ShowInteractionUI();
    }

    private void RevealCoffeeData()
    {
        CurrentGrounds.CoffeeData.SetFlavorNote();
        _cupped = true;
    }
    
    private void UpdateLiquidMesh()
    {
        if (_coffeeLiquidMaterial == null)
        {
            return;
        }

        _coffeeLiquidMaterial.SetFloat(FillAmountID, CurrentRecipe != null ? 1f : 0f);
    }
    
    public override string GetInfoToDisplay()
    {
        var info = "";
        if (!IsEmpty)
        {
            return info + CoffeeDataUtility.GetCoffeeDataDisplay(CurrentGrounds);
        }
        
        info = "<b>Empty</b>." + Environment.NewLine;
        return info;
    }

    public void InitContainer()
    {
        _liquidContainer = new LiquidContainer(null, 1, true);
        _liquidContainer.ContentChanged += UpdateLiquidMesh;
    }

    public Container<Liquid> GetContainer()
    {
        return _liquidContainer;
    }
}
