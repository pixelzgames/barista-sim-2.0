using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

public class BatchBrewMachine : Powerable, IContainer<Liquid>
{
    private event Action BrewedCoffee;
    
    [Header("Coffee properties")]
    [SerializeField] private int _coffeeServingsCapacity = 15;
    [SerializeField] private MeshRenderer _coffeeLiquidMeshRenderer;

    [Header("Water properties")] [SerializeField]
    private bool _isPlumbedIn;
    [SerializeField, HideIf("_isPlumbedIn")] private int _maxWaterServingsCapacity = 50;
    [SerializeField] private MeshRenderer _waterLiquidMeshRenderer;

    [Header("Brewing properties")] 
    [SerializeField] private float _brewTime = 60f;
    [SerializeField] private float _brewTemperature = 94f;

    [Header("Other properties")] 
    [SerializeField] private Socket _filterSocket;
    [SerializeField] private BrewFilter _brewFilter;
    
    [Header("Pouring mechanic")]
    [SerializeField] private BatchBrewPouringGameplayMechanic _gameplayMechanicController;

    private LiquidContainer _waterLiquidContainer;
    private LiquidContainer _coffeeLiquidContainer;
    private bool _brewing;
    private Material _coffeeLiquidMaterial;
    private Material _waterLiquidMaterial;
    private static readonly int FillAmount = Shader.PropertyToID("_FillAmount");

    protected override void Awake()
    {
        base.Awake();
        if (_coffeeLiquidMeshRenderer)
        {
            _coffeeLiquidMaterial = _coffeeLiquidMeshRenderer.material;
        }

        if (_waterLiquidMeshRenderer)
        {
            _waterLiquidMaterial = _waterLiquidMeshRenderer.material;
        }

        _brewFilter.SetData(_coffeeServingsCapacity);
        InitContainer();
        BrewedCoffee = OnBrewedCoffee;
        _gameplayMechanicController.OnFinishedPour += FillCup;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        _waterLiquidContainer.ContentChanged -= UpdateContainerLevel;
        _coffeeLiquidContainer.ContentChanged -= UpdateContainerLevel;
        _gameplayMechanicController.OnFinishedPour -= FillCup;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        switch (CurrentPowerState)
        {
            case PowerState.Powering:
                return;
            case PowerState.Off:
            {
                var action = GetActionByName("Power On");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                break;
            }
            case PowerState.Powered:
            {
                if (!_brewing)
                {
                    if (IsReadyToBrew())
                    {
                        var action = GetActionByName("StartBrewing");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                }
                else
                {
                    var action = GetActionByName("StopBrewing");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
                
                //If you dont have anything in hands
                if (_inHand == null && !_brewing && !_filterSocket.IsEmpty)
                {
                    var action = GetActionByName("TakeFilter");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
        
                if (_inHand != null && !_brewing)
                {
                    if (_inHand is Kettle)
                    {
                        var action = GetActionByName("AddWater");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    else if (_inHand is BrewFilter)
                    {
                        var action = GetActionByName("PlaceFilter");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    else if (_inHand is Cup {IsEmpty: true} && CurrentRecipe?.Servings >= 1)
                    {
                        var action = GetActionByName("PourCoffee");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                }

                break;
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "TakeFilter":
                TakeFilter();
                break;
            case "AddWater":
                OnAddWaterToTank();
                break;
            case "PlaceFilter":
                PlaceFilter();
                break;
            case "PourCoffee":
                StartPouringMechanic();
                break;
            case "StartBrewing":
                StartStopBrew(true);
                break;
            case "StopBrewing":
                StartStopBrew(false);
                break;
            case "Power On":
                TryPowerOn();
                break;
            case "Power Off":
                TryPowerOff();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    private bool IsReadyToBrew()
    {
        return !_brewing && _filterSocket.CurrentInteractable == _brewFilter && GetLiquidWaterData().Content.Servings > 0f && !_brewFilter.GetContainer().IsEmpty() && !_brewFilter.Dirty;
    }
    
    private void OnAddWaterToTank()
    {
        if (_inHand is not Kettle kettle)
        {
            return;
        }
        
        var inKettle = kettle.Water.Servings;
        if (inKettle <= 0f)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Kettle is empty!"));
            return;
        }
        
        var toRemove = _maxWaterServingsCapacity > inKettle ? inKettle : _maxWaterServingsCapacity;
        AddWater(toRemove);
        kettle.GetContainer().RemoveFromContainer(toRemove);
    }
    
    private void AddWater(int servingsAmount)
    {
        _waterLiquidContainer.AddToContainer(servingsAmount);
        UpdateContainerLevel();
        UpdateLiquidMesh();
    }
    
    private void FillCup()
    {
        if (_inHand is Cup cup)
        {
            RecipeMaker<Liquid>.MakeRecipe(RecipeType.BlackCoffee, _coffeeLiquidContainer, cup.GetContainer(),
                _coffeeLiquidContainer.Content.Temperature.Current, GetCoffeeData());
        }
    }
    
    private void StartPouringMechanic()
    {
        _gameplayMechanicController.StartMechanic(this);
    }
    
    private void PlaceFilter()
    {
        PlayerInteractionManager.Instance.TryPlaceObject(_filterSocket);
    }

    private void TakeFilter()
    {
        _brewFilter.PickUp();
    }

    private void StartStopBrew(bool start)
    {
        if (start)
        {
            StartCoroutine(StartBrew(_brewTime));
        }
        else
        {
            _brewing = false;
            _objectStateEventController.RaiseEvent(ObjectState.Brewing, false);
            StopAllCoroutines();
        }
    }

    private IEnumerator StartBrew(float timeOfExtraction)
    {
        _objectStateEventController.RaiseEvent(ObjectState.Brewing,true, timeOfExtraction);
        _brewing = true;
        
        yield return new WaitForSeconds(timeOfExtraction);
        BrewedCoffee?.Invoke();
    }
    
    private void OnBrewedCoffee()
    {
        if (_brewFilter.GetContainer().Content is not Grounds content)
        {
            throw new NullReferenceException();
        }

        var isBad = content.Size != content.CoffeeData.IdealBrewingGrindSize;
        RecipeMaker<Liquid>.MakeRecipe(RecipeType.BlackCoffee, GetLiquidWaterData(), GetLiquidCoffeeData(),
            _brewTemperature, GetCoffeeData(), isBad, _coffeeServingsCapacity);
        
        _brewFilter.RemoveGrounds();
        _brewFilter.Dirty = true;
        
        _objectStateEventController.RaiseEvent(ObjectState.Brewing, false);
        _brewing = false;
        RefreshUI();
    }

    private void UpdateLiquidMesh()
    {
        if (_coffeeLiquidMaterial == null || CurrentRecipe == null)
        {
            return;
        }
        
        _coffeeLiquidMaterial.SetFloat(FillAmount, (float)CurrentRecipe.Servings / _coffeeServingsCapacity);
        _waterLiquidMaterial.SetFloat(FillAmount, (float)GetLiquidWaterData().Content.Servings / _maxWaterServingsCapacity);
    }
    
    public override string GetInfoToDisplay()
    {
        var water = $"Water level is <b>{_waterLiquidContainer.GetFullPercentage()}%</b> full!" + Environment.NewLine;
        if (_isPlumbedIn)
        {
            water = "Water: Plumbed in" + Environment.NewLine;
        }

        return water +
               $"Filter {(!_brewFilter.GetContainer().IsEmpty() ? $"has <color={Barista.Colors.OnHex}>grounds" : $"is <color={Barista.Colors.OffHex}>empty")} </color>";
    }

    public void InitContainer()
    {
        var water = new Water(_brewTemperature, _coffeeServingsCapacity);
        
        if (_isPlumbedIn)
        {
            water.Servings = int.MaxValue;
        }
        
        _waterLiquidContainer = new LiquidContainer(water, _maxWaterServingsCapacity, !_isPlumbedIn);
        _coffeeLiquidContainer = new LiquidContainer(null, _coffeeServingsCapacity, true);
        
        _waterLiquidContainer.ContentChanged += UpdateContainerLevel;
        _coffeeLiquidContainer.ContentChanged += UpdateContainerLevel;
        
        UpdateContainerLevel();
    }

    public Container<Liquid> GetContainer()
    {
        return _waterLiquidContainer;
    }

    private void UpdateContainerLevel()
    {
        var waterLevel =_waterLiquidContainer.GetLevel();
        var coffeeLevel = _coffeeLiquidContainer.GetLevel();

        switch (waterLevel)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowWater);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater);
                _objectStateEventController.RaiseEvent(ObjectState.LowWater, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowWater, false);
                break;
        }
        
        switch (coffeeLevel)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfCoffee, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfCoffee);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfCoffee, false);
                break;
        }

        UpdateLiquidMesh();
    }

    public override LiquidContainer GetLiquidWaterData()
    {
        return _waterLiquidContainer;
    }

    public override LiquidContainer GetLiquidCoffeeData()
    {
        return _coffeeLiquidContainer;
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        if (_filterSocket.IsEmpty)
        {
            return null;
        }
        
        return _brewFilter.GetContainer() as SolidContainer;
    }
}
