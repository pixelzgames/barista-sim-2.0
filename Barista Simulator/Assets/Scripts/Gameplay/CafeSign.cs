public class CafeSign : Interactable
{
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        ActionInputConfig action;

        if (SingletonManager.Instance.CafeManager.IsCafeOpen)
        {
            action = GetActionByName("CloseCafe");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }
        else if (!SingletonManager.Instance.CafeManager.WasCafeEverOpenedThatDay)
        {
            action = GetActionByName("OpenCafe");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "OpenCafe":
                SingletonManager.Instance.CafeManager.OpenCafe();
                break;
            case "CloseCafe":
                SingletonManager.Instance.CafeManager.CloseCafe();
                break;
        }

        base.ExecuteAction(actionName);
    }
}