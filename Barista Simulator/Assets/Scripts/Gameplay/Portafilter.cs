using System.Text.RegularExpressions;
using UnityEngine;

public class Portafilter : Pickable, IContainer<Solid>, ICleanable
{
    public Grounds CurrentGrounds => _solidContainer.Content as Grounds;
    public bool Dirty { get; set; }
    public void Clean()
    {
        RemoveGrounds();
        Dirty = false;
    }

    [SerializeField] 
    private GameObject _groundsMesh;
    
    private SolidContainer _solidContainer;

    protected override void Start()
    {
        base.Start();
        InitContainer();
    }

    private void OnDisable()
    {
        _solidContainer.ContentChanged -= UpdateContainerLevel;
    }

    public void SetDirty(bool dirty)
    {
        Dirty = dirty;
    }

    public void RemoveGrounds()
    {
        _solidContainer.RemoveAllContent();
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        //If you dont have anything in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
                _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
        }

        base.ExecuteAction(actionName);
    }

    public override string GetInfoToDisplay()
    {
        var grindSize =
            $"{(CurrentGrounds != null && CurrentGrounds.Servings > 0 ? "Grind size is " + "<b>" + Regex.Replace(CurrentGrounds.Size.ToString(), "(\\B[A-Z])", " $1") + "</b>" : "")}";
        var dirty = Dirty ? $"<color={Barista.Colors.OffHex}>DIRTY (" + GetContainer().Content.Servings + " Left)</color>" : "";
        
        return grindSize + "\n" + dirty;
    }

    public void InitContainer()
    {
        _solidContainer = new SolidContainer(new Grounds(), 2, true);
        _solidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        return _solidContainer;
    }

    private void UpdateContainerLevel()
    {
        if (_solidContainer.Content == null || _solidContainer.IsEmpty())
        {
            _groundsMesh.SetActive(false);
        }
        else
        {
            _groundsMesh.SetActive(true);
        }
    }
}