using Sirenix.OdinInspector;
using UnityEngine;

public class ProductDisplay : Movable
{
    [DisableInPlayMode]
    [SerializeField] private int _productsCapacity;
    [DisableInPlayMode]
    [SerializeField] private GameObject[] _productObjects;
    
    private int _currentProductsCount;
    
    protected override void Awake()
    {
        base.Awake();
        
        UpdateVisualRendering();
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
       
        //Create a new list of actions to be called based on current state
        _currentInteractions.Clear();

        if (GetPercentageFilled() < 1f)
        {
            //If you do have something in hands
            if (_inHand != null && _inHand is GenericProductRefillBox)
            {
                var action = GetActionByName("RefillProducts");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        
        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "RefillProducts":
                RefillProducts();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private float GetPercentageFilled()
    {
        return Mathf.Clamp01((float)_currentProductsCount / _productsCapacity);
    }
    
    private void RefillProducts()
    {
        if (GetPercentageFilled() >= 1f)
        {
            return;
        }
        
        if (_inHand is not GenericProductRefillBox box)
        {
            return;
        }
        
        var inBox = box.CurrentProductsCount;
        var toRemove = _productsCapacity - _currentProductsCount > inBox ? inBox : _productsCapacity - _currentProductsCount;
        box.WithdrawProducts(toRemove);
        _currentProductsCount += toRemove;
        
        UpdateVisualRendering();
        
        var gameEvent = new GameEvent(GameEventKey.ShelveRefilled, toRemove);
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(gameEvent);
    }

    private void UpdateVisualRendering()
    {
        var totalMeshCountToDisplay = Mathf.RoundToInt(_productObjects.Length * GetPercentageFilled());
        for (var i = 0; i < _productObjects.Length; i++)
        {
            if (i < totalMeshCountToDisplay)
            {
                _productObjects[i].SetActive(true);
                continue;
            }
            
            _productObjects[i].SetActive(false);
        }
    }
    
    public override string GetInfoToDisplay()
    {
        return "Shelves are " +  "<b>" + GetPercentageFilled()*100f + "% </b>full.";
    }
}