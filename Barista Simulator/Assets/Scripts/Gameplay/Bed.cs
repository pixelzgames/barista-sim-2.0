using UnityEngine;

public class Bed : Interactable
{
    [field:SerializeField] public Transform CharacterSocket { get; private set; }

    protected override void Start()
    {
        SingletonManager.Instance.GameManager.ChangedTimeState += OnTimeOfDayChanged;
        base.Start();
    }

    protected override void OnDestroy()
    {
        SingletonManager.Instance.GameManager.ChangedTimeState -= OnTimeOfDayChanged;
        base.OnDestroy();
    }

    private void OnTimeOfDayChanged(TimeOfDay timeOfDay)
    {
        IgnoreFromInteraction = timeOfDay == TimeOfDay.Morning;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        //If you dont have anything in hands
        if (_inHand == null && SingletonManager.Instance.GameManager.CurrentTimeOfDayState == TimeOfDay.Night)
        {
            var action = GetActionByName("Sleep");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Sleep":
                GoToSleep();
                break;
        }

        base.ExecuteAction(actionName);
    }

    private void GoToSleep()
    {
        SingletonManager.Instance.GameManager.ChangeDay();
        PlayerInteractionManager.Instance.PlayerController.Sleep(true);
        PlayerInteractionManager.Instance.PlayerController.SetPlayerTransform(CharacterSocket);
    }
}
