﻿using UnityEngine;

public class ObjectDestroyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        // Only destroy objects that are not on the player layer
        if (collision.gameObject.GetComponent<Pickable>() != null)
        {
            Destroy(collision.gameObject, 1f);
        }
    }
}
