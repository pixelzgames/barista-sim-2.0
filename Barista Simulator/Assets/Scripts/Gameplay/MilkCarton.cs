using UnityEngine;

public class MilkCarton : Pickable, IContainer<Liquid>
{
    public Milk Milk => _milk;
    
    private Milk _milk = new Milk(6f, 1);
    
    [SerializeField] private int _servingsCapacity = 10;

    private LiquidContainer _liquidContainer;
    private bool _spoiled;

    protected override void Start()
    {
        base.Start();
        InitContainer();
    }

    private void OnDisable()
    {
        _liquidContainer.ContentChanged -= UpdateContainerLevel;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        //If you dont have anything in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }
        
        if(_inHand != null && _inHand is MilkPitcher pitcher && pitcher.Milk.Servings <= 0f)
        {
            if (_milk.Servings >= pitcher.GetLiquidMilkData().ServingsCapacity)
            {
                var action = GetActionByName("FillPitcher");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }

        ShowInteractionUI();
    }
     
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
            case "FillPitcher":
                OnCompletedPour();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    private void OnCompletedPour()
    {
        BRAGUtilities.TransferContentTo(_inHand.GetLiquidMilkData(), _liquidContainer);
    }

    public override LiquidContainer GetLiquidMilkData()
    {
        return _liquidContainer;
    }

    private void UpdateContainerLevel()
    {
        var level =_liquidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowMilk);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk);
                _objectStateEventController.RaiseEvent(ObjectState.LowMilk, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowMilk, false);
                break;
        }
    }

    public void InitContainer()
    {
        _milk.Servings = _servingsCapacity;
        _liquidContainer = new LiquidContainer(_milk, _servingsCapacity, false);
        _liquidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Liquid> GetContainer()
    {
        return _liquidContainer;
    }
}
