using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

public class EspressoMachineSection : Interactable
{
    public EspressoMachine ParentMachine => _parentEspressoMachine;
    public Portafilter ClippedPortafilter => _portafilterSocket.CurrentInteractable as Portafilter;

    [SerializeField, Required] 
    private MilkPitcher _milkPitcher;
    [SerializeField] 
    private EspressoMachine _parentEspressoMachine;
    [SerializeField] 
    private Socket _cupSocket;
    [SerializeField] 
    private Socket _portafilterSocket;
    [SerializeField] 
    private Socket _milkPitcherSocket;

    [Header("Frothing Mechanic")]
    [SerializeField] 
    private FrothingGameplayMechanic _gameplayMechanicController;
    
    private bool _brewing;
    private Coroutine _extractionCoroutine;
    private float _brewingTimeNormalized;
    private int _lastShotDose;
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null)
        {
            if (IsReadyToPullEspresso())
            {
                if (!_brewing)
                {
                    if (ClippedPortafilter.GetContainer().Content.Servings == 1)
                    {
                        var action = GetActionByName("PullSingleShot");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    else if (ClippedPortafilter.GetContainer().Content.Servings == 2)
                    {
                        var action = GetActionByName("PullDoubleShot");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                }
                else
                {
                    var action = GetActionByName("StopExtraction");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
            }

            if (!_brewing)
            {
                //If there is a portafilter in the socket
                var portafilter = _portafilterSocket.CurrentInteractable as Portafilter;
                if (portafilter != null && portafilter.Dirty)
                {
                    var action = GetActionByName("TakePortafilter");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }

                //If there is a cup there
                if (_cupSocket.CurrentInteractable != null)
                {
                    var action = GetActionByName("TakeCup");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
            }
            
            if (!_milkPitcher.GetContainer().IsEmpty() && !_milkPitcher.Milk.IsFrothed)
            {
                var action = GetActionByName("StartFrothingMilk");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
            
            if (!_milkPitcher.GetContainer().IsEmpty())
            {
                var action = GetActionByName("TakeMilkPitcher");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        else
        {
            switch (_inHand)
            {
                case Cup cup:
                {
                    if (cup.IsEmpty && _cupSocket.IsEmpty)
                    {
                        var action = GetActionByName("PlaceCup");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }

                        // Has a an empty cup in hand and water spout available
                        if (_parentEspressoMachine.EspressoMachineConfig.HasHotWaterSpout)
                        {
                            action = GetActionByName("AddHotWater");
                            if (action != null)
                            {
                                _currentInteractions.Add(action);
                            }

                            break;
                        }
                    }
                    
                    // Has a cup of espresso in hand and water spout is available
                    if (cup.CurrentRecipe.Config.RecipeType == RecipeType.Espresso &&
                        _parentEspressoMachine.EspressoMachineConfig.HasHotWaterSpout)
                    {
                        var action = GetActionByName("MakeAmericano");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    
                    // Has a cup of double espresso in hand and water spout is available
                    if (cup.CurrentRecipe.Config.RecipeType == RecipeType.DoubleEspresso &&
                        _parentEspressoMachine.EspressoMachineConfig.HasHotWaterSpout)
                    {
                        var action = GetActionByName("MakeLongBlack");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }

                    if (_milkPitcher.Milk.IsFrothed && cup.CurrentRecipe != null)
                    {
                        switch (cup.CurrentRecipe.Config.RecipeType)
                        {
                            case RecipeType.Espresso:
                            {
                                switch (_milkPitcher.Milk.FoamType)
                                {
                                    case FoamType.Large:
                                    {
                                        var action = GetActionByName("MakeCappuccino");
                                        if (action != null)
                                        {
                                            _currentInteractions.Add(action);
                                        }

                                        break;
                                    }
                                    case FoamType.Micro:
                                    {
                                        var action = GetActionByName("MakeLatte");
                                        if (action != null)
                                        {
                                            _currentInteractions.Add(action);
                                        }

                                        break;
                                    }
                                }

                                break;
                            }
                            case RecipeType.DoubleEspresso:
                            {
                                if (_milkPitcher.Milk.FoamType == FoamType.Micro)
                                {
                                    var action = GetActionByName("MakeFlatWhite");
                                    if (action != null)
                                    {
                                        _currentInteractions.Add(action);
                                    }
                                }

                                break;
                            }
                        }
                    }

                    break;
                }
                case Portafilter
                    when _portafilterSocket.IsEmpty:
                {
                    var action = GetActionByName("PlacePortafilter");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                    break;
                }
                case MilkCarton carton when !carton.GetContainer().IsEmpty() &&
                                            carton.GetContainer().Content.GetTemperatureState() ==
                                            TemperatureState.Cold && !_milkPitcher.GetContainer().IsFull() && !_milkPitcher.Milk.IsFrothed:
                {
                    var action = GetActionByName("AddMilk");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                    
                    break;
                }
                case GenericGroundsContainer when !ClippedPortafilter.Dirty && !ClippedPortafilter.GetContainer().IsFull():
                {
                    var action = GetActionByName("AddGrounds");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                    
                    break;
                }
                case MilkPitcher pitcher when pitcher == _milkPitcher:
                {
                    var action = GetActionByName("PlacePitcher");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                    
                    break;
                }
            }
        }
        
        ShowInteractionUI();
    }

    private bool IsReadyToPullEspresso()
    {
        return _portafilterSocket.CurrentInteractable != null && _cupSocket.CurrentInteractable != null &&
               _cupSocket.CurrentInteractable is Cup { IsEmpty: true } &&
               !ClippedPortafilter.GetContainer().IsEmpty() && !ClippedPortafilter.Dirty;
    }

    protected override void ExecuteAction(string actionName)
    {
        if (_inHand is Cup cup)
        {
            switch (actionName)
            {
                case "MakeCappuccino":
                    RecipeMaker<Liquid>.MakeRecipe(RecipeType.Cappuccino, GetLiquidMilkData(), cup.GetContainer(),
                        GetLiquidMilkData().Content.Temperature.Current,
                        cup.GetCoffeeData());
                
                    break;
                case "MakeLatte":
                    RecipeMaker<Liquid>.MakeRecipe(RecipeType.Latte, GetLiquidMilkData(), cup.GetContainer(),
                        GetLiquidMilkData().Content.Temperature.Current,
                        cup.GetCoffeeData());
                
                    break;
                case "MakeFlatWhite":
                    RecipeMaker<Liquid>.MakeRecipe(RecipeType.FlatWhite, GetLiquidMilkData(), cup.GetContainer(),
                        GetLiquidMilkData().Content.Temperature.Current,
                        cup.GetCoffeeData());
                
                    break;
                case "MakeAmericano":
                    RecipeMaker<Liquid>.MakeRecipe(RecipeType.Americano, GetLiquidWaterData(), cup.GetContainer(),
                        GetLiquidWaterData().Content.Temperature.Current,
                        cup.GetCoffeeData());
                    break;
                case "MakeLongBlack":
                    RecipeMaker<Liquid>.MakeRecipe(RecipeType.LongBlack, GetLiquidWaterData(), cup.GetContainer(),
                        GetLiquidWaterData().Content.Temperature.Current,
                        cup.GetCoffeeData());
                    break;
            }
        }
        
        switch (actionName)
        {
            case "PullSingleShot":
                PullShot(1);
                break;
            case "PullDoubleShot":
                PullShot(2);
                break;
            case "AddGrounds":
                AddGroundsInPortafilter();
                break;
            case "StopExtraction":
                StopExtraction();
                break;
            case "TakePortafilter":
                PickupPortafilter();
                break;
            case "PlacePortafilter":
                PlacePortafilterOnSocket();
                break;
            case "PlaceCup":
                PlaceCupOnSocket();
                break;
            case "TakeCup":
                PickupCup();
                break;
            case "StartFrothingMilk":
                FrothMilk();
                break;
            case "AddMilk":
                AddMilk();
                break;
            case "AddHotWater":
                PourWater();
                break;
            case "TakeMilkPitcher":
                _milkPitcher.PickUp();
                break;
            case "PlacePitcher":
                PlacePitcher();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    #region Milk Pitcher

    private void AddMilk()
    {
        BRAGUtilities.TransferContentTo(_inHand.GetLiquidMilkData(), _milkPitcher.GetLiquidMilkData(), 1);
    }
    
    private void FrothMilk()
    {
        _gameplayMechanicController.StartMechanic(this);
        _gameplayMechanicController.OnMilkFrothed += MilkFrothed;
    }

    private void MilkFrothed(FoamType foamType, float temperatureValue)
    {
        _milkPitcher.Milk.SetIsFrothed(true, foamType);
        _milkPitcher.Milk.Temperature.Current = temperatureValue;
        _gameplayMechanicController.OnMilkFrothed -= MilkFrothed;
    }
    
    private void PlacePitcher()
    {
        PlayerInteractionManager.Instance.TryPlaceObject(_milkPitcherSocket);
    }

    #endregion
    
    #region Portafilter

    private void PlacePortafilterOnSocket()
    {
        PlayerInteractionManager.Instance.TryPlaceObject(_portafilterSocket);
    }
    
    private void PickupPortafilter()
    {
        if (_portafilterSocket.CurrentInteractable != null && _portafilterSocket.CurrentInteractable is Pickable pickable)
        {
            pickable.PickUp();
        }
    }
    
    private void AddGroundsInPortafilter()
    {
        var groundsContainer = _inHand as GenericGroundsContainer;
        if (groundsContainer == null)
        {
            return;
        }

        if (groundsContainer.GetContainer().Content is not Grounds)
        {
            Debug.LogError("Error: invalid cast from generic grounds container content to grounds");
            return;
        }

        BRAGUtilities.TransferContentTo(groundsContainer.GetSolidBeansContainer(),
            ClippedPortafilter.GetSolidBeansContainer());
    }

    #endregion
    
    #region Cup

    private void PickupCup()
    {
        if (_cupSocket.CurrentInteractable is not Cup cup)
        {
            return;
        }
        
        cup.PickUp();
    }

    private void PlaceCupOnSocket()
    {
        PlayerInteractionManager.Instance.TryPlaceObject(_cupSocket);
    }

    #endregion

    /// <summary>
    /// Start making the recipe
    /// </summary>
    /// <param name="timeOfExtraction">Time it takes to output said recipe</param>
    /// <returns></returns>
    private IEnumerator StartExtraction(float timeOfExtraction)
    {
        var target = _parentEspressoMachine.EspressoBrewingConfig.GetEspressoTarget();
        _brewing = true;
        
        _objectStateEventController.RaiseEvent(ObjectState.EspressoBrewing, true, timeOfExtraction);
        var timer = 0f;
        while (timer < timeOfExtraction)
        {
            if (_parentEspressoMachine.EspressoMachineConfig.AutoBrewing && Mathf.Approximately(target, _brewingTimeNormalized))
            {
                StopExtraction();
                yield break;
            }
            
            _brewingTimeNormalized = _parentEspressoMachine.EspressoBrewingConfig.ExtractionCurve.Evaluate(timer / timeOfExtraction);
            timer += Time.deltaTime;
            yield return null;
        }

        StopExtraction();
    }
    
    private void PullShot(int dose)
    {
        if (_cupSocket.CurrentInteractable == null)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("There are no cups to brew into"));
            return;
        }

        if (_cupSocket.CurrentInteractable is Cup { IsEmpty: false })
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Cup already has something in it"));
            return;
        }

        _lastShotDose = dose;
        _extractionCoroutine = StartCoroutine(StartExtraction(ParentMachine.EspressoBrewingConfig.ExtractionTime));
    }

    private void PourWater()
    {
        if (_inHand is not Cup cup)
        {
            return;
        }
        
        cup.AddIngredient(new Water(GetLiquidWaterData().Content.Temperature.Current, 1));
        GetLiquidWaterData().RemoveFromContainer(1);
    }

    private void StopExtraction()
    {
        if (!_brewing)
        {
            return;
        }
        
        StopCoroutine(_extractionCoroutine);
        
        //Calculate Dialing In Results
        var dialedInState = GetGrindSizeDelta();
        var isBadQuality = dialedInState != ObjectState.DialedIn;
        
        _objectStateEventController.RaiseEvent(ObjectState.EspressoBrewing, false);

        var extractionResult = EspressoBrewingResult.Bad;
        if (!isBadQuality)
        {
            extractionResult = _parentEspressoMachine.EspressoMachineConfig.AutoBrewing ? 
                EspressoBrewingResult.Espresso : _parentEspressoMachine.EspressoBrewingConfig.CalculateBrewingResult(_brewingTimeNormalized);
        }

        var recipeType = _lastShotDose == 1 ? RecipeType.Espresso : RecipeType.DoubleEspresso;
        
        var cup = _cupSocket.CurrentInteractable as Cup;
        if (cup == null)
        {
            return;
        }
        
        // If cup has water in it
        if (cup.GetLiquidWaterData() != null && !cup.GetLiquidWaterData().IsEmpty())
        {
           recipeType = _lastShotDose == 1 ? RecipeType.Americano : RecipeType.LongBlack;
        }

        switch (extractionResult)
        {
            case EspressoBrewingResult.None:
            case EspressoBrewingResult.Bad:
                RecipeMaker<Solid>.MakeRecipe(recipeType, GetSolidBeansContainer(), cup.GetContainer(), 94f,
                    ClippedPortafilter.CurrentGrounds.CoffeeData, true);
                break;
            case EspressoBrewingResult.Espresso:
            case EspressoBrewingResult.Godshot:
            case EspressoBrewingResult.Lungo:
                RecipeMaker<Solid>.MakeRecipe(recipeType, GetSolidBeansContainer(), cup.GetContainer(), 94f,
                    ClippedPortafilter.CurrentGrounds.CoffeeData, isBadQuality);
                break;
        }
        
        if (extractionResult == EspressoBrewingResult.Godshot &&
            ClippedPortafilter.CurrentGrounds.CoffeeData.IsEspressoDialedIn)
        {
            dialedInState = ObjectState.GodShot;
        }
        
        if (dialedInState != ObjectState.None)
        {
            cup.RaiseObjectStateEvent(dialedInState);
            if (dialedInState == ObjectState.DialedIn &&
                !ClippedPortafilter.CurrentGrounds.CoffeeData.IsEspressoDialedIn)
            {
                ClippedPortafilter.CurrentGrounds.CoffeeData.RevealEspressoDialIn();
            }
        }
        
        ClippedPortafilter.SetDirty(true);
        
        _brewing = false;
        _brewingTimeNormalized = 0f;
    }

    private ObjectState GetGrindSizeDelta()
    {
        var portafilterGrindSize = ClippedPortafilter.CurrentGrounds.Size;
        var coffeeDialInGrindSize = ClippedPortafilter.CurrentGrounds.CoffeeData.IdealEspressoGrindSize;
        var grindSizeDelta = portafilterGrindSize - coffeeDialInGrindSize;

        // Check for dialing in
        var cupObjectState = grindSizeDelta switch
        {
            <= -2 => ObjectState.VeryUnderExtracted,
            -1 => ObjectState.UnderExtracted,
            0 => ObjectState.DialedIn,
            1 => ObjectState.OverExtracted,
            >= 2 => ObjectState.VeryOverExtracted,
        };
        
        return cupObjectState;
    }

    public override string GetInfoToDisplay()
    {
        var grindSize = _portafilterSocket.CurrentInteractable != null
            ? $"{(ClippedPortafilter.CurrentGrounds != null ? "Grind size is " + "<b>" + ClippedPortafilter.CurrentGrounds.Size.ToString().ToLower() + "</b>" : "")}"
            : "";
        return grindSize;
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        return ClippedPortafilter?.GetSolidBeansContainer();
    }

    public override LiquidContainer GetLiquidWaterData()
    {
        return ParentMachine.GetContainer() as LiquidContainer;
    }

    public override LiquidContainer GetLiquidMilkData()
    {
        return _milkPitcher.GetLiquidMilkData();
    }

    public void SetIgnoreInteraction(bool on)
    {
        IgnoreFromInteraction = on;
    }
}