public class Sink : Movable
{
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
       
        //If you have a cup in hands
        if (_inHand is Kettle)
        {
            var action = GetActionByName("FillKettle");
            if (action != null)
                _currentInteractions.Add(action);
        }
        
        if (_inHand is ICleanable)
        {
            var action = GetActionByName("Clean");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "FillKettle":
                FillKettle();
                break;
            case "Clean":
                Clean();
                break;
        }

        base.ExecuteAction(actionName);
    }

    private void FillKettle()
    {
        if (_inHand is not Kettle kettle)
        {
            return;
        }
        
        if (kettle.Water.Servings >= kettle.KettleMaxCapacity - (kettle.KettleMaxCapacity * 0.9f))
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Kettle is already too full"));
            return;
        }

        kettle.GetContainer().AddToContainer(kettle.KettleMaxCapacity);
    }

    private void Clean()
    {
        // TODO: have a more generic way of cleaning for all cleanable pickables
        var pickable = _inHand;
        switch (pickable)
        {
            case Cup cup:
                cup.Clean();
                break;
            case Portafilter portafilter:
                portafilter.Clean();
                break;
            case MilkPitcher _milkPitcher:
                _milkPitcher.GetContainer().RemoveFromContainer(_milkPitcher.GetContainer().ServingsCapacity);
                break;
        }
    }

    public override string GetInfoToDisplay()
    {
        return "Some info about the Sink could go here :)";
    }
}