using UnityEngine;

[CreateAssetMenu(fileName = "NewEspressoMachineProperties", menuName = "BRAG/Coffee Machine Configs/Espresso Machine Properties")]
public class EspressoMachineConfig : ScriptableObject
{
    [field: Range(1,10)] 
    [field:SerializeField]
    public int HeatingSpeedModifier { get; private set; }
    
    [field: Range(1,200)] 
    [field:SerializeField]
    public int WaterServingsCapacity { get; private set; }

    [field:SerializeField]
    public bool PlumbedIn { get; private set; }

    [field:SerializeField]
    public bool AutoBrewing { get; private set; }

    [field:SerializeField]
    public bool HasSteamWand { get; private set; }

    [field:SerializeField]
    public bool HasHotWaterSpout { get; private set; }

    [field:SerializeField]
    public int GroupHeadCount { get; private set; }
}
