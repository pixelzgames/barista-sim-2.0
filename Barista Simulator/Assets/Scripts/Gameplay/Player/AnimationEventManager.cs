using UnityEngine;

/// <summary>
/// Contains All Animation Event method callbacks
/// </summary>
public class AnimationEventManager : MonoBehaviour
{
    [SerializeField]
    private Transform _rightHandSocket;

    [SerializeField]
    private Transform _leftHandSocket;

    public void AddToRightHand(Object obj)
    {
        Instantiate(obj as GameObject, _rightHandSocket);
    }

    public void AddToLeftHand(Object obj)
    {
        Instantiate(obj as GameObject, _leftHandSocket);
    }

    public void WokeUp()
    {
        PlayerInteractionManager.Instance.PlayerController.Sleep(false);
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.PlayerGainedControl));
    }

    public void DestroyRightHandObject()
    {
        if (_rightHandSocket.transform.childCount == 0)
        {
            return;
        }

        Destroy(_rightHandSocket.transform.GetChild(0).gameObject);
    }

    public void DestroyLeftHandObject()
    {
        if (_leftHandSocket.transform.childCount == 0)
        {
            return;
        }

        Destroy(_leftHandSocket.transform.GetChild(0).gameObject);
    }

    public void DestroyAnimationObjectInHands()
    {
        DestroyRightHandObject();
        DestroyLeftHandObject();
    }
}
