using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;

public class Grinder : Powerable, IContainer<Solid>, IAnimatableSocket
{
    public GrindSize CurrentGrindSize
    {
        get => _currentGrindSize;
        set => _currentGrindSize = value; 
    }
    public GrindSize[] PossibleGrindSizes => _possibleGrindSizes;
    [field: SerializeField] public AnimationSocketDictionary AnimationSocketDictionary { get; set; }
    
    [SerializeField] private Animator _animator;
    [SerializeField] private int _beanHopperServingsCapacity = 30;
    [SerializeField] private GrindSize[] _possibleGrindSizes;
    [SerializeField] private float _automaticGrindTime = 5f;

    [Header("Beans meshes")]
    [SerializeField] private GameObject _beansMeshFull;
    [SerializeField] private GameObject _beansMeshMed;
    [SerializeField] private GameObject _beansMeshLow;
    [SerializeField] private ParticleSystem _groundsParticle;

    [Header("Change grind mechanic")]
    [SerializeField] private GrinderGameplayMechanic _gameplayMechanicController;

    private static readonly int Filling = Animator.StringToHash("Filling");
    private bool _grinding;
    private SolidContainer _solidContainer;
    private GrindSize _currentGrindSize = GrindSize.Coarse;
    private Socket _currentAnimationSocket;
    private Coroutine _grindingCoroutine;
    private Socket _grindIntoSocket;

    protected override void Awake()
    {
        base.Awake();

        InitContainer();

        BeginInteraction += StartedInput;
        EndInteraction += StoppedInput;
    }

    private void OnDisable()
    {
        BeginInteraction -= StartedInput;
        EndInteraction -= StoppedInput;
        _solidContainer.ContentChanged -= UpdateContainerLevel;
    }

    private void StartedInput(ActionInputConfig obj)
    {
        if (obj.AnimationConfig.AnimationToPlay == InteractableAnimationType.Grinder_PressButton)
        {
            _groundsParticle.Play();
        }
        
        if (obj.AnimationConfig.AnimationToPlay == InteractableAnimationType.Grinder_Fill)
        {
            _animator.SetBool(Filling, true);
        }
    }
    
    private void StoppedInput(ActionInputConfig obj)
    {
        if (obj.AnimationConfig.AnimationToPlay == InteractableAnimationType.Grinder_PressButton)
        {
            _groundsParticle.Stop();
        }
        
        if (obj.AnimationConfig.AnimationToPlay == InteractableAnimationType.Grinder_Fill)
        {
            _animator.SetBool(Filling, false);
        }
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        switch (CurrentPowerState)
        {
            case PowerState.Powering:
                break;
            case PowerState.Off:
            {
                var action = GetActionByName("Power On");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
                
                break;
            }
            case PowerState.Powered:
            {
                var action = GetActionByName("Power Off");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                action = GetActionByName("ChangeGrind");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                if (_inHand == null)
                {
                    if (!_grinding && !_solidContainer.IsEmpty() && (_currentAnimationSocket == null || _currentAnimationSocket.IsEmpty))
                    {
                        action = GetActionByName("GrindInto");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }

                        action = GetActionByName("PlaceAndFill");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    else if (_grinding)
                    {
                        action = GetActionByName("StopGrinding");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    else if (_currentAnimationSocket != null && !_currentAnimationSocket.IsEmpty)
                    {
                        action = GetActionByName("Take");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                }
                else
                {
                    if (_inHand is BrewFilter && !_grinding && !_solidContainer.IsEmpty() && (_currentAnimationSocket == null || _currentAnimationSocket.IsEmpty))
                    {
                        action = GetActionByName("GrindInto");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                        
                        action = GetActionByName("PlaceAndFill");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                    
                    // If Hopper is not empty
                    if (!_solidContainer.IsEmpty())
                    {
                        break;
                    }

                    if (_inHand is CoffeeBag bag && !bag.GetContainer().IsEmpty())
                    {
                        action = GetActionByName("FillHopper");
                        if (action != null)
                        {
                            _currentInteractions.Add(action);
                        }
                    }
                }

                break;
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "GrindInto":
                FillContainerInSocket();
                break;
            case "PlaceAndFill":
                PlaceAndGrindOnSocket();
                break;
            case "GrindIntoContainer":
                FillContainerInSocket();
                break;
            case "PlaceAndFillContainer":
                PlaceAndGrindOnSocket();
                break;
            case "Take":
                PickupObjectOnSocket();
                break;
            case "ChangeGrind":
                StartChangeGrindSizeMechanic();
                break;
            case "FillHopper":
                FillHopper();
                break;
            case "Power On":
                TryPowerOn();
                break;
            case "Power Off":
                TryPowerOff();
                break;
            case "StopGrinding":
                StopGrinding();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void StopGrinding()
    {
        _grinding = false;
        StopCoroutine(_grindingCoroutine);
        _grindingCoroutine = null;
        _objectStateEventController.RaiseEvent(ObjectState.Brewing, false);
        OnStoppedGrinding();
    }

    private void PlaceAndGrindOnSocket()
    {
        _grindingCoroutine = StartCoroutine(StartAutomaticGrinding(_automaticGrindTime));
    }
    
    private IEnumerator StartAutomaticGrinding(float timeOfExtraction)
    {
        _groundsParticle.Play();
        _objectStateEventController.RaiseEvent(ObjectState.Brewing, true, timeOfExtraction);
        _grinding = true;

        var timer = 0f;
        while (timer < timeOfExtraction)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        
        _grinding = false;
        _objectStateEventController.RaiseEvent(ObjectState.Brewing, false);
  
        GrindOneServingInContainer(_currentAnimationSocket.CurrentInteractable.GetSolidBeansContainer());
        OnStoppedGrinding();
        
        if (!_currentAnimationSocket.CurrentInteractable.GetSolidBeansContainer().IsFull() && !_solidContainer.IsEmpty())
        {
            _grindingCoroutine = null;
            PlaceAndGrindOnSocket();
            yield break;
        }

        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Grinding done!"));
        _grindingCoroutine = null;
    }

    private void PickupObjectOnSocket()
    {
        var pickable = _currentAnimationSocket.CurrentInteractable as Pickable;
        if (!pickable)
        {
            return;
        }
        
        pickable.PickUp();
        _currentAnimationSocket = null;
    }

    private void StartChangeGrindSizeMechanic()
    {
        _gameplayMechanicController.StartMechanic(this);
    }
    
    private void FillContainerInSocket()
    {
        _inHand = PlayerInteractionManager.Instance.CurrentlyInMainHand;
        if (_inHand is GenericGroundsContainer container)
        {
            GrindOneServingInContainer(container.GetContainer());
        }
        else if (_inHand is BrewFilter filter)
        {
            GrindOneServingInContainer(filter.GetContainer());
        }
        
        OnStoppedGrinding();
    }

    private void OnStoppedGrinding()
    {
        ChangeBeansMeshBasedOnQuantity();
        _groundsParticle.Stop();
    }

    private void GrindOneServingInContainer(Container<Solid> solidContainer)
    {
        _solidContainer.RemoveFromContainer(1);
        var currentWholeSeeds = _solidContainer.Content as WholeBeans;

        if (solidContainer.IsEmpty())
        {
            solidContainer.Content = new Grounds(0, currentWholeSeeds?.CoffeeData, _currentGrindSize);
        }
       
        solidContainer.AddToContainer(1);
    }

    private void FillHopper()
    {
        if (_currentAnimationSocket.CurrentInteractable is not CoffeeBag { } bag)
        {
            return;
        }
        
        var servingsCountInBag = bag.CurrentBeans.Servings;
        var servingsToRemove = _beanHopperServingsCapacity > servingsCountInBag ? servingsCountInBag : _beanHopperServingsCapacity;

        _solidContainer.Content = new WholeBeans(servingsToRemove, bag.CurrentBeans.CoffeeData);
        bag.GetContainer().RemoveFromContainer(servingsToRemove);
        _animator.SetBool(Filling, false);
    }

    private void ChangeBeansMeshBasedOnQuantity()
    {
        if (_solidContainer.Content == null)
        {
            _beansMeshFull.SetActive(false);
            _beansMeshMed.SetActive(false);
            _beansMeshLow.SetActive(false);
            return;
        }
        
        if (_solidContainer.Content.Servings > _beanHopperServingsCapacity * 0.95f)
        {
            _beansMeshFull.SetActive(true);
            _beansMeshMed.SetActive(false);
            _beansMeshLow.SetActive(false);
        }
        else if (_solidContainer.Content.Servings > _beanHopperServingsCapacity * 0.50f)
        {
            _beansMeshFull.SetActive(false);
            _beansMeshMed.SetActive(true);
            _beansMeshLow.SetActive(false);
        }
        else if (_solidContainer.Content.Servings > _beanHopperServingsCapacity * 0.0f)
        {
            _beansMeshFull.SetActive(false);
            _beansMeshMed.SetActive(false);
            _beansMeshLow.SetActive(true);
        }
        else
        {
            _beansMeshFull.SetActive(false);
            _beansMeshMed.SetActive(false);
            _beansMeshLow.SetActive(false);
        }
    }

    public override string GetInfoToDisplay()
    {
        return $"Grind size is set to: <b>{Regex.Replace(_currentGrindSize.ToString(), "(\\B[A-Z])" , " $1")}</b>.";
    }

    public void InitContainer()
    {
        _solidContainer = new SolidContainer(new WholeBeans(), _beanHopperServingsCapacity, true);
        _solidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    private void UpdateContainerLevel()
    {
        var level =_solidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
        }

        ChangeBeansMeshBasedOnQuantity();
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        return _solidContainer;
    }

    public void AddToAnimatableSocket(InteractableAnimationType animationType, Pickable pickable)
    {
        if (!AnimationSocketDictionary.TryGetValue(animationType, out Socket socket))
        {
            Debug.Log("Couldn't find socket in dictionary.");
            return;
        }

        _currentAnimationSocket = socket;

        PlayerInteractionManager.Instance.TryPlaceObject(socket);
    }

    public Pickable RemoveFromAnimatableSocket()
    {
        if (_currentAnimationSocket == null)
        {
            return null;
        }
        
        var pickable = _currentAnimationSocket.CurrentInteractable as Pickable;
        pickable?.PickUp();
        
        _currentAnimationSocket = null;
        return pickable;
    }

    public void AnimateSocketInteractable()
    {
        
    }
}
