using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicController : MonoBehaviour
{
    [SerializeField] private MusicCatalog _catalog;
    [SerializeField] private float _fadeTime = 5f;
    [SerializeField] private float _windUpTime = 5f;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private WorldAttribute _worldAttribute;
    
    private AudioClip _currentClip;
    private int _currentGenreIndex;
    private int _currentSongIndex;

    private void Start()
    {
        // Load a clip
        _currentGenreIndex = Random.Range(0, _catalog.Genres.Count);
        _currentSongIndex = Random.Range(0, _catalog.Genres[_currentGenreIndex].SongList.Count);

        if (_audioSource.clip == null)
        {
            _currentClip = _catalog.Genres[_currentGenreIndex].SongList[_currentSongIndex];
            _audioSource.clip = _currentClip;
        }
    }

    public string GetCurrentMusicInfo()
    {
        return _catalog.Genres[_currentGenreIndex].SongList[_currentSongIndex].name + "\nGenre: " + _catalog.Genres[_currentGenreIndex].name;
    }

    public void PlayMusic()
    {
        StopAllCoroutines();
        ChangeSong(true);
        _worldAttribute.enabled = true;
    }

    public void StopMusic()
    {
        StopAllCoroutines();
        StartCoroutine(FadeOut(fadeTime: _windUpTime, playNextSong: false, changePitch: true));
        _worldAttribute.enabled = false;
    }

    public void ChangeGenre()
    {
        _currentGenreIndex = (_currentGenreIndex + 1) % _catalog.Genres.Count;
        ChangeSong();
    }

    private void ChangeSong(bool firstSong = false)
    {
        var time = firstSong ? _windUpTime : _fadeTime;
        StartCoroutine(FadeIn(fadeTime: time, changePitch: firstSong));

        _currentSongIndex = (_currentSongIndex + 1) % _catalog.Genres[_currentGenreIndex].SongList.Count;
        _audioSource.clip = _catalog.Genres[_currentGenreIndex].SongList[_currentSongIndex];
        _audioSource.Play();

        StartCoroutine(FadeOut(_fadeTime, delay: _audioSource.clip.length - _fadeTime, playNextSong: true));
    }

    private IEnumerator FadeIn(float fadeTime, bool changePitch = false)
    {
        float timeElapsed = 0;

        while (timeElapsed < fadeTime)
        {
            _audioSource.volume = Mathf.Lerp(0f, 1f, timeElapsed / fadeTime);

            if (changePitch)
                _audioSource.pitch = Mathf.Lerp(0f, 1f, timeElapsed / fadeTime);

            timeElapsed += Time.deltaTime;

            yield return null;
        }
    }

    private IEnumerator FadeOut(float fadeTime, float delay = 0f, bool changePitch = false, bool playNextSong = true)
    {
        yield return new WaitForSeconds(delay);

        float timeElapsed = 0;

        while (timeElapsed < fadeTime)
        {
            _audioSource.volume = Mathf.Lerp(1f, 0f, timeElapsed / fadeTime);

            if (changePitch)
                _audioSource.pitch = Mathf.Lerp(1f, 0f, timeElapsed / fadeTime);

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        if (playNextSong)
        {
            ChangeSong();
        }
        else
        {
            _audioSource.Stop();
        }
    }
}
