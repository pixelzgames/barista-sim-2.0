﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Pixelz.Plants
{
    public class PlantHealth : MonoBehaviour
    {
        [FormerlySerializedAs("healthColors")] [SerializeField]
        private Gradient _healthColors;

        /// <summary>
        /// 0 is dying 1 is very healthy
        /// </summary>
        [FormerlySerializedAs("currentHealthStatus")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _currentHealthStatus = 1f;

        private Material _mat;

        private void Awake()
        {
            _mat = GetComponent<Renderer>().material;
        }

        // Update is called once per frame
        private void Update()
        {
            //TODO Remove when the other codes controls call SetGrowth below.
            SetHealth(_currentHealthStatus);
        }

        /// <summary>
        /// Sets the health of the plant normalized from 0-1
        /// </summary>
        /// <param name="health"> Health from 0-1 </param>
        public void SetHealth(float health)
        {
            _mat.color = _healthColors.Evaluate(health);
        }

    }
}
