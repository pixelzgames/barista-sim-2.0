﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Pixelz.Plants
{
    public class PlantGrowth : MonoBehaviour
    {
        [FormerlySerializedAs("growthCurve")] [SerializeField]
        private AnimationCurve _growthCurve;

        [FormerlySerializedAs("currentGrowth")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _currentGrowth = 1f;

        // Update is called once per frame
        private void Update()
        {
            //TODO Remove when the other codes controls call SetGrowth below.
            SetGrowth(_currentGrowth);
        }

        /// <summary>
        /// Hard sets the current growth of a plant from 0-1
        /// </summary>
        /// <param name="growth">Float from 0-1 growth, where 1 is fully grown, 0 is invisible</param>
        public void SetGrowth(float growth)
        {
            transform.localScale = Vector3.one * _growthCurve.Evaluate(growth);
        }

        public float GetCurrentGrowthNormalized()
        {
            return _currentGrowth;
        }
    }
}
