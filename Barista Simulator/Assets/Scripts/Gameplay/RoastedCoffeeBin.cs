using UnityEngine;

public class RoastedCoffeeBin : Interactable, IContainer<Solid>
{
    [SerializeField] private int _servingsCapacity = 100;
    [SerializeField] private GameObject _coffeeBagPrefab;

    private WholeBeans _roastedBeans = new();
    private SolidContainer _solidContainer;

    protected override void Start()
    {
        base.Start();
        InitContainer();
    }

    private void OnDisable()
    {
        _solidContainer.ContentChanged -= UpdateContainerLevel;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (!_solidContainer.IsEmpty())
        {
            var action = GetActionByName("BagCoffee");
            if (action != null)
                _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "BagCoffee":
                BagCoffee();
                break;
        }

        base.ExecuteAction(actionName);
    }

    private void BagCoffee()
    {
        if (_inHand != null)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("You have something in your hands! Empty them first"));
            return;
        }

        var bag = Instantiate(_coffeeBagPrefab, transform.position, Quaternion.identity)
            .GetComponentInChildren<Pickable>() as CoffeeBag;
        if (bag == null)
        {
            return;
        }

        var toRemove = bag.ServingsCapacity;
        // Is there enough beans currently to fill a bag
        if (toRemove > _roastedBeans.Servings)
        {
            toRemove = _roastedBeans.Servings;
        }

        //Create a copy
        var beans = new WholeBeans(toRemove, _roastedBeans.CoffeeData);

        bag.GetContainer().Content = beans;
        _solidContainer.RemoveFromContainer(toRemove);
        bag.PickUp();
    }

    public override string GetInfoToDisplay()
    {
        return !_solidContainer.IsEmpty() ? $"Bin has {_roastedBeans.Servings}g of " +
                                            $"{_roastedBeans.CoffeeData.Roast} roasted coffee" : "Bin is empty";
    }

    public void InitContainer()
    {
        _solidContainer = new SolidContainer(_roastedBeans, _servingsCapacity, true);
        _solidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    private void UpdateContainerLevel()
    {
        var level =_solidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
        }
    }
}