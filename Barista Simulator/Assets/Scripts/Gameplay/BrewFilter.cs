using System.Text.RegularExpressions;
using UnityEngine;

public class BrewFilter : Pickable, IContainer<Solid>, ICleanable
{
    public bool Dirty { get; set; }
    [SerializeField] private GameObject _paperFilterMesh;

    private SolidContainer _solidContainer;
    private int _servingsCapacity;
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        //If you dont have anything in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
        }

        base.ExecuteAction(actionName);
    }
    
    public override string GetInfoToDisplay()
    {
        var content = _solidContainer.Content as Grounds;
        var filterInside ="Filter " +  (content?.Servings > 0 ? $"is <color={Barista.Colors.OnHex}>full!</color>" : $"is <color={Barista.Colors.OffHex}>empty!</color>");
        var grindSize = $"{(content is { Servings: > 0 } ? "Grind size is " + "<b>" + Regex.Replace(content.Size.ToString(), "(\\B[A-Z])" , " $1") + "</b>" : "")}";
        return filterInside + System.Environment.NewLine +
               grindSize;
    }
    
    public void RemoveGrounds()
    {
        _solidContainer.RemoveAllContent();
    }
    
    public void SetData(int servingsCapacity)
    {
        _servingsCapacity = servingsCapacity;
        InitContainer();
    }
    
    public void InitContainer()
    {
        _solidContainer = new SolidContainer(new Grounds(), _servingsCapacity, true);
        _solidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    private void UpdateContainerLevel()
    {
        _paperFilterMesh.SetActive(!_solidContainer.IsEmpty());
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        return GetContainer() as SolidContainer;
    }

    public void Clean()
    {
        RemoveGrounds();
        UpdateContainerLevel();
        Dirty = false;
    }
}
