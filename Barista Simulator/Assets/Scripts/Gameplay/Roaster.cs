using System;
using System.Collections;
using UnityEngine;

public class Roaster : Movable
{
    [SerializeField] 
    private int _roasterServingsCapacity = 100;

    [SerializeField]
    private RoasterProfileConfig _roastProfile;

    [SerializeField] 
    private RoastedCoffeeBin _roastedCoffeeBin;

    [SerializeField]
    private Spinner _coolingTray;

    [SerializeField] private Animator _animator;
    
    private GreenBeans _greenCoffee;
    private WholeBeans _roastedBeans;
    private RoastType _currentRoastType;
    private Coroutine _roastingCoroutine;
    
    private enum RoasterState
    {
        Idling,
        Filled,
        Roasting,
        Cooling
    }
    
    private RoasterState _roasterState = RoasterState.Idling;
    private static readonly int _open = Animator.StringToHash("Open");

    protected override void SetCurrentPossibleInteractions()
    {
        if (!_currentlyHighlighted)
        {
            return;
        }
        
        switch (_roasterState)
        {
            //If you do have green coffee bag in hands
            case RoasterState.Idling:
            {
                if (_inHand != null && _inHand is GreenCoffeeBag bag && !bag.GetContainer().IsEmpty())
                {
                    var action = GetActionByName("FillBeans");
                    if (action != null)
                        _currentInteractions.Add(action);
                }
                
                break;
            }
            //Show the possible interactions!
            case RoasterState.Filled:
            {
                var action = GetActionByName("StartRoast");
                if (action != null)
                    _currentInteractions.Add(action);
                break;
            }
            case RoasterState.Roasting:
            {
                if (_currentRoastType != RoastType.Raw)
                {
                    var action = GetActionByName("DropRoast");
                    if (action != null)
                        _currentInteractions.Add(action);
                }
                
                break;
            }
            case RoasterState.Cooling:
            {
                var action = GetActionByName("TransferInBin");
                if (action != null)
                    _currentInteractions.Add(action);
                break;
            }
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "FillBeans":
                FillBeans();
                break;
            case "StartRoast":
                _roastingCoroutine = StartCoroutine(StartRoast());
                break;
            case "DropRoast":
                StopRoast();
                break;

            case "TransferInBin":
                TransferInBin();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void TransferInBin()
    {
        _roastedCoffeeBin.GetContainer().Content = _roastedBeans;
        _roastedBeans = null;
        _roasterState = RoasterState.Idling;
        Debug.Log("Transferred to storage bin! Roaster is ready for next roast");
    }

    private void FillBeans()
    {
        if (_inHand is not GreenCoffeeBag bag)
        {
            return;
        }
        
        var inBag = bag.ContainerContent.Servings;
        var toRemove = _roasterServingsCapacity > inBag ? inBag : _roasterServingsCapacity;
        
        AddBeans(new GreenBeans(toRemove,bag.ContainerContent.CoffeeData));
        bag.GetContainer().RemoveFromContainer(toRemove);
        
        _roasterState = RoasterState.Filled;
        Debug.Log("Green Beans Added!");
    }

    private IEnumerator StartRoast()
    { 
        _roasterState = RoasterState.Roasting;
        Debug.Log("Roast Started!");

        foreach (var profile in _roastProfile.RoastProfiles)
        {
            yield return new WaitForSeconds(profile.TimeToReach);
            _currentRoastType = profile.Roast;
            Debug.Log($"Roast is now : {_currentRoastType}");
        }
    }
    
    private void StopRoast()
    {
        StopCoroutine(_roastingCoroutine);
        
        _roastedBeans = new WholeBeans(_greenCoffee.Servings, _greenCoffee.CoffeeData);
        _roastedBeans.CoffeeData.SetRoast(_currentRoastType);
        //TODO with roast score, hard coded for now
        _roastedBeans.CoffeeData.SetQualityFromRoastScore(0);
        _greenCoffee = null;
        _roasterState = RoasterState.Cooling;
        _currentRoastType = RoastType.Raw;
        _coolingTray.StartSpinning();
        _animator.SetTrigger(_open);
        Debug.Log("Roast Dropped!");
    }

    private bool HasBeans()
    {
        return _roasterState != RoasterState.Idling;
    }

    private void AddBeans(GreenBeans beans)
    {
        _greenCoffee = beans;
        _coolingTray.StopSpinning();
        CheckBeansLevel();
    }

    private void CheckBeansLevel()
    {
        if (!HasBeans() || _roastedBeans.Servings <= 0f)
        {
            _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds);
        }
        else if (_roastedBeans.Servings >= _roasterServingsCapacity)
        {
            _objectStateEventController.RaiseEvent(ObjectState.Full);
        }
    }

    public override string GetInfoToDisplay()
    {
        var roasterState = $"Roaster is in {_roasterState.ToString()} state." + Environment.NewLine;
        if (HasBeans())
        {
            roasterState += _roasterState switch
            {
                RoasterState.Filled => $"Roaster has {_greenCoffee.Servings}g of coffee.",
                RoasterState.Roasting => $"Roaster has {_greenCoffee.Servings}g of coffee.",
                RoasterState.Cooling => $"Roaster has {_roastedBeans.Servings}g of coffee.",
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        
        return roasterState;
    }
}
