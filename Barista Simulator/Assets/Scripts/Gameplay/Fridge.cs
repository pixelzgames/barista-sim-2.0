using System.Collections;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class Fridge : Movable
{
    private const float ANIM_OPEN_TIME = 0.5f;
    private const float ANIM_CLOSE_TIME = 1f;
    
    [SerializeField] private Transform _door;
    [SerializeField] private GameObject _inventoryPrefab;
    [SerializeField] private Socket[] _slotsInFridge;
    [SerializeField] private float _coolingDegreePerSecond = 1f;
    [SerializeField] private float _fridgeTemperature = 6f;
    
    private bool _isOpened;

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        if (!_isOpened)
        {
            //If you do have something in hands
            if (_inHand != null)
            {
                //If you have any GroundsContainer in hand
                var action = GetActionByName("Store");
                if (action != null)
                    _currentInteractions.Add(action);
            }
            else
            {
                var action = GetActionByName("Open");
                if (action != null)
                    _currentInteractions.Add(action);
            }
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Open":
                OpenFridge();
                break;
            case "Store":
                StartCoroutine(AddToSlot());
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void OpenFridge(bool displayUI = true)
    {
        if (_isOpened)
        {
            return;
        }

        _door.DOLocalRotate(new Vector3(0f, -120f, 0f), ANIM_OPEN_TIME).SetEase(Ease.OutBounce);
        _collider.enabled = false;
        _isOpened = true;

        if (!displayUI)
        {
            return;
        }
        
        SingletonManager.Instance.InputManager.ChangeInputMapping(SingletonManager.Instance.InputManager.InputMaster.UI);
        var inventory = Instantiate(_inventoryPrefab).GetComponent<InventoryControllerSpawn>();
        inventory.OnClosed += CloseFridge;
        inventory.OnButtonPressed += PickupItemInFridge;
        var items = _slotsInFridge.Select(slot => slot.CurrentInteractable as Pickable).ToArray();
        inventory.InitializeUI(items, _slotsInFridge.Length);
    }

    private void CloseFridge()
    {
        _isOpened = false;
        _door.DOLocalRotate(Vector3.zero, ANIM_CLOSE_TIME).SetEase(Ease.OutBounce);
        _collider.enabled = true;
        SingletonManager.Instance.InputManager.ChangeInputMapping(SingletonManager.Instance.InputManager.InputMaster.GenericGameplay);
    }

    private void PickupItemInFridge(InventorySlot slotItemPressed)
    {
        RemoveFromSlot(slotItemPressed.SlotIndex);
        slotItemPressed.PickableInSlot.PickUp();
    }

    private IEnumerator AddToSlot()
    {
        if (_inHand == null)
        {
            yield break;
        }
        
        foreach (var slot in _slotsInFridge)
        {
            if (slot.CurrentInteractable != null)
            {
                continue;
            }
            
            OpenFridge(false);
            yield return new WaitForSeconds(ANIM_OPEN_TIME);
            PlayerInteractionManager.Instance.TryPlaceObject(slot);
            
            // Cool liquids :) 
            if (slot.CurrentInteractable is IContainer<Liquid> temperatureInteractable )
            {
                temperatureInteractable.GetContainer().Content.SetTemperatureGoal(_fridgeTemperature, _coolingDegreePerSecond);
            }
            
            CloseFridge();
            break;
        }
    }

    private void RemoveFromSlot(int id)
    {
        // Set to room temperature when removing from fridge :(
        if (_slotsInFridge[id].CurrentInteractable is IContainer<Liquid> temperatureInteractable )
        {
            temperatureInteractable.GetContainer().Content.SetTemperatureGoal(Temperature.ROOM_TEMP, Liquid.DEFAULT_ROOMTEMP_CHANGE_RATE);
        }
    }
}
