using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.VFX;

public class Streetlight : MonoBehaviour
{
   private static readonly int IsOn = Shader.PropertyToID("_IsOn");
   
   [SerializeField] private MeshRenderer _lightRenderer;
   [SerializeField] private Light _light;
   [SerializeField] private VisualEffect _fliesVFX;
   [SerializeField, MinMaxSlider(1f, 60f)] private Vector2 _minMaxDelayBeforeTurningOn;

   private bool _eveningReached;
   private bool _isOn;
   private float _delay;
   private float _timer;
   private float _initialIntensity;
   private Material _material;
   private LightingController _lightingController;

   private void OnEnable()
   {
      _initialIntensity = _light.intensity;
      _material = _lightRenderer.material;
      _lightingController = FindAnyObjectByType<LightingController>();
      _lightingController.EveningReached += OnEveningLighting;
      ToggleLight(false);
   }

   private void OnDisable()
   {
      _lightingController.EveningReached -= OnEveningLighting;
   }

   private void OnEveningLighting(bool eveningReached)
   {
      _eveningReached = eveningReached;
      if (_eveningReached)
      {
         _delay = Random.Range(_minMaxDelayBeforeTurningOn.x, _minMaxDelayBeforeTurningOn.y);
      }
   }

   private void Update()
   {
      if (_eveningReached && !_isOn)
      {
         if (_timer > _delay)
         {
            ToggleLight(true);
         }
         
         _timer += Time.deltaTime;
      }

      if (!_eveningReached && _isOn)
      {
         ToggleLight(false);
      }
   }

   private void ToggleLight(bool on)
   {
      _timer = 0f;
      _light.intensity = on ? _initialIntensity : 0f;
      _material.SetInt(IsOn, on ? 1 : 0);
      _isOn = on;
      _fliesVFX.enabled = on;
   }
}
