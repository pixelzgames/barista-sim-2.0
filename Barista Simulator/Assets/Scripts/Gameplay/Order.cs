﻿using AI;
using System;
using UnityEngine;

public class Order : MonoBehaviour
{
    public float InitialTime { get; private set; }
    public RecipeConfig Recipe{get; private set;}
    public OrderType Type { get; private set;}
    public int Price { get; private set;}
    public NPCCustomer Customer { get; private set;}
    public TastingDescriptor RequiredTastingNoteByCustomer { get; private set; }

    public Timer OrderTimer { get; private set; }

    public Action OnInitialized = delegate { };
    public Action OnSuccess = delegate { };
    public Action OnFailure = delegate { };

    /// <summary>
    /// Call this method before using this class somehow
    /// </summary>
    /// <param name="type"></param>
    /// <param name="recipe"></param>
    /// <param name="customer"></param>
    /// <param name="requiredTastingNoteByCustomer">Required Tasting Note by The Customer</param>
    public void InitOrder(OrderType type, RecipeConfig recipe, NPCCustomer customer, TastingDescriptor requiredTastingNoteByCustomer)
    {
        Type = type;
        Recipe = recipe;
        Customer = customer;
        Price = GameConfig.GetConfigData().Gameplay.DrinkPrice;
        RequiredTastingNoteByCustomer = requiredTastingNoteByCustomer;
        
        if (Customer == null)
        {
            SetOrderTimer(SetupTimer());
        }
    }

    // Used only to debug when NO customers are available
    private Timer SetupTimer()
    {
        OrderTimer = gameObject.AddComponent<Timer>();
        OrderTimer.StartTimer(GameConfig.Data.AI.BaseWaitTime + Recipe.NPCWaitTimeMinimum);

        return OrderTimer;
    }

    public void SetOrderTimer(Timer timer)
    {
        OrderTimer = timer;
        InitialTime = timer.TimeRemaining;
        OrderTimer.TimerStateChanged += FailOrder;
        OnInitialized?.Invoke();
    }

    private void FailOrder(TimerState state)
    {
        if (state == TimerState.Ended)
        {
            OnFailure?.Invoke();
            SingletonManager.Instance.OrderManager.RemoveOrder(this);
        }
    }
}
