using UnityEngine;

public class OrderGenerator : Movable
{
    [SerializeField] private Animator _animator;
    private static readonly int Pushed = Animator.StringToHash("Pushed");
    
    protected override void SetCurrentPossibleInteractions()
    {
       base.SetCurrentPossibleInteractions();
        
        var action = GetActionByName("TakeOrder");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "TakeOrder":
                // Take next order in lineup
                if (!SingletonManager.Instance.CafeManager.IsCafeOpen)
                {
                    SingletonManager.Instance.CafeManager.OpenCafe();
                }
                
                SingletonManager.Instance.OrderManager.GenerateRandomOrderFromMenu(Random.Range(0, 2) > 0 ? OrderType.Stay : OrderType.Togo);
                _animator.SetTrigger(Pushed);
                break;
        }
        
        base.ExecuteAction(actionName);
    }
}
