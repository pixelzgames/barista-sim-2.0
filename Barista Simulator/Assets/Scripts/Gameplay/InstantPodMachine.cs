using System.Collections;
using DG.Tweening;
using UnityEngine;

public class InstantPodMachine : Powerable
{
    private int _capsulesLeft;
    private bool _isCapsuleIn;
    private bool _brewing;
    
    [SerializeField] private Socket _cupSocket;
    
    [Header("Brewing properties")] 
    [SerializeField] private float _brewTime;

    [Header("Other properties")] 
    [SerializeField] private int _maxCapsuleCapacity = 10;

    protected override void Awake()
    {
        base.Awake();
        _capsulesLeft = _maxCapsuleCapacity;
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "InsertCapsule":
                InsertCapsule();
                break;
            case "StartBrewing":
                StartStopBrew(true);
                break;
            case "TakeCup":
                PickupCup();
                break;
            case "PlaceCup":
                PlaceCupOnSocket();
                break;
            case "RefillCapsules":
                RefillCapsules();
                break;
            case "Power On":
                TryPowerOn();
                break;
            case "Power Off":
                TryPowerOff();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        switch (CurrentPowerState)
        {
            case PowerState.Powering:
                return;
            case PowerState.Off:
            {
                var action = GetActionByName("Power On");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                ShowInteractionUI();
                return;
            }
            case PowerState.Powered:
            {
                var action = GetActionByName("Power Off");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
        
                //If you have nothing in hands
                if (_inHand == null && !_brewing)
                {
                    if (_capsulesLeft > 0 && !_isCapsuleIn)
                    {
                        action = GetActionByName("InsertCapsule");
                        if (action != null)
                            _currentInteractions.Add(action);
                    }

                    if (_cupSocket.CurrentInteractable != null)
                    {
                        if (_cupSocket.CurrentInteractable is Cup)
                        {
                            action = GetActionByName("TakeCup");
                            if (action != null)
                                _currentInteractions.Add(action);
                        }
                    }
                }
                else if(_inHand != null)
                {
                    //Else if you have a cup in hands
                    if (_inHand is Cup {IsEmpty: true})
                    {
                        if (_cupSocket.CurrentInteractable == null)
                        {
                            action = GetActionByName("PlaceCup");
                            if (action != null)
                                _currentInteractions.Add(action);
                        }
                    }
                    if (_inHand is CapsulePack)
                    {
                        action = GetActionByName("RefillCapsules");
                        if (action != null)
                            _currentInteractions.Add(action);
                    }
                }

                if (IsReadyToBrew())
                {
                    action = GetActionByName("StartBrewing");
                    if (action != null)
                        _currentInteractions.Add(action);
                }

                ShowInteractionUI();
                break;
            }
        }
    }
    
    private bool IsReadyToBrew()
    {
        return !_brewing && _capsulesLeft > 0 && _isCapsuleIn && _cupSocket.CurrentInteractable != null &&
               _cupSocket.CurrentInteractable is Cup
               {
                   IsEmpty: true
               };
    }
    
    private IEnumerator StartBrew(float timeOfExtraction)
    {
        _objectStateEventController.RaiseEvent(ObjectState.Brewing, true, timeOfExtraction);
        _brewing = true;

        if (_cupSocket.CurrentInteractable is not Cup cup)
        {
            yield break;
        }

        var recipeToCreate = GameConfig.GetConfigData().Gameplay.AllRecipes
            .GetRecipeConfigByType(RecipeType.InstantCoffee);

        var grounds = new InstantGrounds(1);
        Container<Solid> sourceContainer = new SolidContainer(grounds, 1, false);
        RecipeMaker<Solid>.MakeRecipe(RecipeType.InstantCoffee, sourceContainer, cup.GetContainer(), 95f, grounds.CoffeeData);

        DOTween.To(() => cup.CurrentRecipe.Servings, x => cup.CurrentRecipe.Servings = x, 200,
            timeOfExtraction).OnUpdate(cup.UpdateLiquidMesh);
            
        yield return new WaitForSeconds(timeOfExtraction);
            
        _brewing = false;
        _isCapsuleIn = false;
        _objectStateEventController.RaiseEvent(ObjectState.Brewing, false);
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData($"Made {recipeToCreate.RecipeName}!"));
    }
    
    private void StartStopBrew(bool start)
    {
        if (start)
            StartCoroutine(StartBrew(_brewTime));
        else
        {
            _objectStateEventController.RaiseEvent(ObjectState.Brewing, false);
            _brewing = false;
            StopAllCoroutines();
        }
    }

    private void PickupCup()
    {
        if (_cupSocket.CurrentInteractable is not Cup cup)
        {
            return;
        }
        
        cup.PickUp();
    }

    private void PlaceCupOnSocket()
    {
        PlayerInteractionManager.Instance.TryPlaceObject(_cupSocket);
    }

    private void RefillCapsules()
    {
        if (_inHand is not CapsulePack pack)
        {
            return;
        }
        
        pack.OnBeingUsed();
        _capsulesLeft = _maxCapsuleCapacity;
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Instant Coffee capsules refilled!"));
    }
    
    private void InsertCapsule()
    {
        _isCapsuleIn = true;
        _capsulesLeft--;
    }
    
    public override string GetInfoToDisplay()
    {
        return $"Has <b>{_capsulesLeft}</b> instant coffee capsules left.";
    }
}
