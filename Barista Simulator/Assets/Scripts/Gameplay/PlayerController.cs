﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController : MonoBehaviour
{
    private const float MOVEMENT_THRESHOLD = 0.01f;
    
    #region Inspector variables

    [SerializeField] private float _movementSpeed = 3.5f;
    [SerializeField] private float _headMaxThresholdAngle = 75f;
    [SerializeField] private Transform _torsoBone;
    [SerializeField] private Transform _headIKTarget;
    [SerializeField] private float _collisionRaycastLength;
    [SerializeField] private LayerMask _collisionRaycastLayerMask;
    
    [SerializeField] private AnimationCurve _animationSpeedMultiplier;
    [SerializeField] private Rig _headRig;
    
    #endregion

    #region Private variables
    private Rigidbody _rb;
    private float _h;
    private float _v;
    private Animator _animator;
    private Vector3 _inputDirectionJoystick;
    private bool _canMove = true;
    private static readonly int WalkSpeed = Animator.StringToHash("walk_speed");
    private static readonly int Sitting = Animator.StringToHash("Sitting");
    private static readonly int Sleeping = Animator.StringToHash("Sleeping");
    private Vector3 _cameraRelativeInput;
    private Vector3 _directionalInputs;
    private InputMaster _inputMaster;
    private Camera _cam;
    private bool _sleeping;
    #endregion
    
    private void OnEnable()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
    }

    private void Start()
    {
        PlayerInteractionManager.Instance.OnCloseUpMechanic += OnCloseUpMechanic;
    }

    private void OnDisable()
    {
        if (PlayerInteractionManager.Instance)
        {
            PlayerInteractionManager.Instance.OnCloseUpMechanic -= OnCloseUpMechanic;
        }
    }

    private void Awake()
    {
        if (!_cam)
        {
           _cam = Camera.main;
        }

        _animator = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody>();
    }

    private void OnCloseUpMechanic(bool on)
    {
        _canMove = !on;
    }
    
    private void Update()
    {
        if (!_canMove)
        {
            if (_sleeping)
            {
                if (Mathf.Abs(_inputMaster.GenericGameplay.Move.ReadValue<Vector2>().magnitude) > 0)
                {
                    WakeUp();
                }
            }
            
            SyncAnimatorMove(0f);
        }
        else
        {
            var movementVector = _inputMaster.GenericGameplay.Move.ReadValue<Vector2>();
            _h = movementVector.x;
            _v = movementVector.y;

            //Calculate Camera relative inputs
            var camFacing = _cam.transform.eulerAngles.y;
            var inputDirection = new Vector3(_h, 0f, _v);
            _cameraRelativeInput = Quaternion.Euler(0, camFacing, 0) * inputDirection;
            _directionalInputs = Vector3.ClampMagnitude(_cameraRelativeInput, 1f);
        
            //Smoothing out joystick inputs for rotation
            var inputUpdateMultiplier = IsThereCollisionInFront() ? 0f : 1f;
            var targetInput = new Vector3(_h * inputUpdateMultiplier, 0f, _v * inputUpdateMultiplier);
            _inputDirectionJoystick = Vector3.Lerp(_inputDirectionJoystick, targetInput, Time.deltaTime * 5f);
            SyncAnimatorMove(_inputDirectionJoystick.magnitude);
        }
    }

    private void FixedUpdate()
    {
        if (!_rb.isKinematic)
        {
            Rotate();
        }
        
        if (IsThereCollisionInFront())
        {
            return;
        }
        
        Move();
    }

    private bool IsThereCollisionInFront()
    {
        var myTransform = transform;
        return Physics.Raycast(myTransform.position + (Vector3.up * 0.5f), myTransform.forward, _collisionRaycastLength,
            _collisionRaycastLayerMask, QueryTriggerInteraction.Ignore);
    }

    public void ToggleMovement(bool on)
    {
        if (_sleeping)
        {
            on = false;
        }
        
        _canMove = on;
        _headRig.weight = on ? 1 : 0;
        if (!on)
        {
            _directionalInputs = Vector3.zero;
        }
    }

    private void Move()
    {
        if (_directionalInputs.magnitude > MOVEMENT_THRESHOLD && !_rb.isKinematic)
        {
            _rb.MovePosition(transform.position + _directionalInputs * Time.fixedDeltaTime * _movementSpeed);
        }
    }

    private void SyncAnimatorMove(float speed)
    {
        var anim = _animationSpeedMultiplier.Evaluate(speed);
        _animator.SetFloat(WalkSpeed, anim);
    }

    private void Rotate()
    {
        // IK for the Torso
        var charTransform = transform;
        var lookDirection = charTransform.position + _cameraRelativeInput;
        Vector3 posTogo;
        if (_cameraRelativeInput.magnitude > 0.1f)
        {
            posTogo = lookDirection;
        }
        else
        {
            posTogo = charTransform.position + charTransform.forward;
        }
        
        _headIKTarget.position = Vector3.Lerp(_headIKTarget.position, posTogo, 10f * Time.fixedDeltaTime);

        if (!(_directionalInputs.magnitude > MOVEMENT_THRESHOLD) &&
            !(Vector3.Angle(_torsoBone.forward, charTransform.forward) >= _headMaxThresholdAngle))
        {
            return;
        }
        
        if (!((lookDirection - charTransform.position).magnitude > Vector3.kEpsilon))
        {
            return;
        }
            
        var lookQuaternion = Quaternion.LookRotation(lookDirection - charTransform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookQuaternion, 10f * Time.fixedDeltaTime);
    }

    public void Sit(bool sit)
    {
        ToggleMovement(!sit);
        _rb.isKinematic = sit;
        _animator.SetBool(Sitting, sit);
    }

    public void Sleep(bool sleep)
    {
        _sleeping = sleep;
        ToggleMovement(!sleep);
        _rb.isKinematic = sleep;
        _animator.SetBool(Sleeping, sleep);
    }
    
    private void WakeUp()
    {
        _animator.SetBool(Sleeping, false);
    }

    public void SetPlayerLookDirection(Transform lookDirection)
    {
        var lookVector = lookDirection.position;
        lookVector.y = transform.position.y;

        transform.DOLookAt(lookVector, 0.2f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(_targetPos, 0.5f);
    }

    private Vector3 _targetPos;
    
    public void SetPlayerTransform(Transform targetTransform, float forwardOffset = 0f, bool applyRotation = true)
    {
        // TODO: Fix look at, DoRotate vs SetPlayerLookDirection
        var target = new Vector3(targetTransform.position.x, 0f, targetTransform.position.z) +
                         targetTransform.forward * forwardOffset;
        transform.DOMove(target, 0.2f).onComplete = () =>
        {
            if (applyRotation)
            {
                transform.DORotate(targetTransform.rotation.eulerAngles, 0.2f);
            }
            else
            {
                SetPlayerLookDirection(targetTransform);  
            }
        };
    }

    public void SetPlayerInteractionSocket(InteractionSocket interactionSocket, Interactable interactable)
    {
        var target = new Vector3(interactionSocket.transform.position.x, 0f, interactionSocket.transform.position.z);
        transform.DOMove(target, 0.2f).onComplete = () =>
        {
            SetPlayerLookDirection(interactable.transform);
        };
    }
}

