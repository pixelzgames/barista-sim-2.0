using DG.Tweening;
using UnityEngine;

public class CashRegister : Interactable
{
    [SerializeField] private CanvasGroup _imageToTween;
    protected override void Awake()
    {
        base.Awake();
        
        SingletonManager.Instance.OrderManager.OrderSold += OnOrderSold;
    }

    private void OnDisable()
    {
        SingletonManager.Instance.OrderManager.OrderSold -= OnOrderSold;
    }

    private void OnOrderSold()
    {
        _imageToTween.alpha = 1f;
        var mySequence = DOTween.Sequence();
        mySequence.Append(_imageToTween.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutElastic))
            .Append(_imageToTween.transform.DOLocalMoveY(25f, 1f).SetEase(Ease.OutBack))
            .Append(_imageToTween.DOFade(0f, 0.5f).SetEase(Ease.OutBack));
        mySequence.onComplete = ResetPopupUI;
        mySequence.Play();
    }

    private void ResetPopupUI()
    {
        _imageToTween.alpha = 0f;
        var trans = _imageToTween.transform;
        trans.localPosition = Vector3.one;
        trans.localScale = Vector3.zero;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        var action = GetActionByName("TakeOrder");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "TakeOrder":
                SingletonManager.Instance.CafeManager.TakeOrder();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
}
