using UnityEngine;

public class LightInteractable : Interactable
{
    [SerializeField] private Light[] _lights;
    [SerializeField] private GameObject[] _renderers;

    private bool _isOn;

    // TODO: DELETE COCK STUFF
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Press switch":
                ToggleLight();
                break;
            case "Press Cock":
                Debug.Log("Pressed COCK.");
                break;
            case "Hold Cock":
                Debug.Log("Held COCK.");
                break;
            case "Y Cock":
                Debug.Log("Y COCK.");
                break;
            case "Y Cock 2":
                Debug.Log("Y COCK 2");
                break;
            case "X Cock 1":
                Debug.Log("X COCK 1");
                break;
        }

        base.ExecuteAction(actionName);
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        var action = GetActionByName("Press switch");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        action = GetActionByName("Hold Cock");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        action = GetActionByName("Press Cock");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        action = GetActionByName("Y Cock");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        action = GetActionByName("Y Cock 2");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
        action = GetActionByName("X Cock 1");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }
       
        ShowInteractionUI();
    }

    private void ToggleLight()
    {
        _isOn = !_isOn;

        foreach (var lightComponent in _lights)
        {
            lightComponent.enabled = _isOn;
        }
        
        foreach (var rendererObject in _renderers)
        {
            rendererObject.SetActive(_isOn);
        }
    }
}
