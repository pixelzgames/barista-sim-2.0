using UnityEngine;

public class LevelPorter : Interactable
{
    [SerializeField] private Level _levelType;
    [SerializeField] private CommutingType _commutingType = CommutingType.Metro;

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        if (SingletonManager.Instance.GameManager.CurrentTimeOfDayState == TimeOfDay.Day)
        {
            var action = GetActionByName("GoHome");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "GoHome":
                var levelNameToLoad = 
                    SingletonManager.Instance.ProgressionManager.GetCurrentSceneToLoadType(_levelType);
        
                var data = new LevelData
                {
                    LevelName = levelNameToLoad,
                    LevelType = _levelType,
                    CommutingType = _commutingType
                };
                
                SingletonManager.Instance.GameManager.LoadALevel(data);
                break;
        }

        base.ExecuteAction(actionName);
    }
}
