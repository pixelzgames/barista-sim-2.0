using Sirenix.OdinInspector;
using UnityEngine;

public class EspressoMachine : Powerable, IContainer<Liquid>
{
    public EspressoBrewingConfig EspressoBrewingConfig => _brewingConfig;
    public EspressoMachineConfig EspressoMachineConfig => _espressoMachineConfig;
    
    [SerializeField, Required] private EspressoBrewingConfig _brewingConfig;
    [SerializeField] private EspressoMachineSection[] _espressoSections;
    [SerializeField, Required] private EspressoMachineConfig _espressoMachineConfig;
    
    private Material _lightMaterial;
    private LiquidContainer _waterContainer;

    protected override void Awake()
    {
        base.Awake();
        InitContainer();
        OnPoweredOn += PoweredOn;
        foreach (var section in _espressoSections)
        {
            section.SetIgnoreInteraction(true);
        }
        
        ApplyProperties();
    }

    public override Interactable GetInteractableDownTheChain()
    {
        if (IsPowered && (_espressoMachineConfig.PlumbedIn || (GetContainer() != null && !GetContainer().IsEmpty())))
        {
            if (_espressoSections.Length <= 1)
            {
                return _espressoSections[0].GetInteractableDownTheChain();
            }

            //TODO Add a way to find the section closest to the player if needs be
            //Returning the first one for now
            return _espressoSections[0].GetInteractableDownTheChain();
        }
        
        return base.GetInteractableDownTheChain();
    }

    private void OnDisable()
    {
        _waterContainer.ContentChanged -= UpdateContainerLevel;
    }

    private void ApplyProperties()
    {
        _timeForPowering /= _espressoMachineConfig.HeatingSpeedModifier;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        switch (CurrentPowerState)
        {
            case PowerState.Powering:
                return;
            case PowerState.Off:
            {
                var action = GetActionByName("Power On");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                ShowInteractionUI();
                break;
            }
            case PowerState.Powered:
            {
                var action = GetActionByName("Power Off");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                break;
            }
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Power On":
                TryPowerOn();
                break;
            case "Power Off":
                TryPowerOff();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void PoweredOn(PowerState state)
    {
        base.Highlight(false);
    }

    public override string GetInfoToDisplay()
    {
        return $"Machine is: {(IsPowered? $"<color={Barista.Colors.OnHex}>on" : $"<color={Barista.Colors.OffHex}>off")}";
    }

    public void InitContainer()
    {
        var servingCapacity = _espressoMachineConfig.PlumbedIn
            ? int.MaxValue
            : _espressoMachineConfig.WaterServingsCapacity;

        _waterContainer =
            new LiquidContainer(new Water(95f, servingCapacity), servingCapacity, !_espressoMachineConfig.PlumbedIn,
                _espressoMachineConfig.PlumbedIn);
        _waterContainer.ContentChanged += UpdateContainerLevel;
    }

    public Container<Liquid> GetContainer()
    {
        return _waterContainer;
    }

    private void UpdateContainerLevel()
    {
        if (_espressoMachineConfig.PlumbedIn)
        {
            return;
        }
        
        var level = LevelResult.Empty;
        
        if (GetContainer() != null)
        {
            level = GetContainer().GetLevel();
        }

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowWater);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater);
                _objectStateEventController.RaiseEvent(ObjectState.LowWater, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowWater, false);
                break;
        } 
    }

    public override void InitializeData()
    {
        base.InitializeData();
        Debug.Log($"Servings left from save: {_data.EspressoMachineData.ServingsLeft}");
    }

    public override BRAGObjectData UpdateObjectData()
    {
        _data.EspressoMachineData.ServingsLeft = 4;
        return base.UpdateObjectData();
    }
}
