using UnityEngine;

public class CoffeeBag : Pickable, IContainer<Solid>
{
    [SerializeField] private int _servingsBagCapacity = 340;
    [SerializeField] private bool _isDialedIn;
    public WholeBeans CurrentBeans
    {
        get
        {
            if (_solidContainer == null)
            {
                InitContainer();
            }
            
            return _solidContainer.Content as WholeBeans;
        }
    }

    public int ServingsCapacity => _servingsBagCapacity;

    private SolidContainer _solidContainer;

    protected override void Awake()
    {
        base.Awake();
        InitContainer();
        //TODO find a better way to fill a bag on play without breaking the Save/Load system
        if (_solidContainer.IsEmpty())
        {
            _solidContainer.Content = CoffeeConfigUtility.GenerateRandomCoffeeBag(_servingsBagCapacity, _isDialedIn);
        }
    }

    private void OnDisable()
    {
        _solidContainer.ContentChanged -= UpdateContainerLevel;
    }

    protected override void SetCurrentPossibleInteractions()
    {
       base.SetCurrentPossibleInteractions();
       
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    public override string GetInfoToDisplay()
    {
        return CoffeeDataUtility.GetCoffeeDataDisplay(CurrentBeans);
    }

    public void InitContainer()
    {
        _solidContainer = new SolidContainer(new WholeBeans(), _servingsBagCapacity, false);
        _solidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    private void UpdateContainerLevel()
    {
        var level =_solidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
        }
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        return _solidContainer;
    }
}
