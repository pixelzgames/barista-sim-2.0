using System.Collections;
using DG.Tweening;
using UnityEngine;

public class ProductBox : Interactable
{
    [SerializeField] private Animator _animator;

    private Interactable _productPrefab;
    private CursorController _cursorController;
    private Movable _spawnedMovable;

    protected override void Awake()
    {
        base.Awake();
        _cursorController = FindFirstObjectByType<CursorController>();
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null)
        {
            var action = GetActionByName("OpenBox");
            if (action != null)
                _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }

    public override string GetInfoToDisplay()
    {
        return $"In the box: {_productPrefab.InteractableName}";
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "OpenBox":
                OpenBox();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void OpenBox()
    {
        _animator.Play("OpenBox");
        _collider.enabled = false;
        _rb.isKinematic = true;
        
        var interactable = Instantiate(_productPrefab, transform.position, Quaternion.identity);

        switch (interactable)
        {
            case Pickable pickable:
            {
                SingletonManager.Instance.GameManager.ChangeInteractionMode(InteractionMode.Barista);
                pickable.PickUp();
                StartCoroutine(DestroyBox(1f));
                break;
            }
            case Movable movable:
                SingletonManager.Instance.GameManager.ChangeInteractionMode(InteractionMode.Edit);
                movable.Select(true);
                _spawnedMovable = movable;
                _cursorController.Cancelled += OnPlacementCancelled;
                _cursorController.Placed += OnPlaced;
                break;
        }
    }

    private void OnPlaced()
    {
        _cursorController.Cancelled -= OnPlacementCancelled;
        _cursorController.Placed -= OnPlaced;
        StartCoroutine(DestroyBox(1f));
    }

    private void OnPlacementCancelled()
    {
        _cursorController.Cancelled -= OnPlacementCancelled;
        _cursorController.Placed -= OnPlaced;
        
        _spawnedMovable.transform.DOKill();
        
        _animator.Play("Appear");
        _collider.enabled = true;
        _rb.isKinematic = false;
        _spawnedMovable.Deselect();
        _spawnedMovable.DeleteMovable();
        _spawnedMovable = null;
    }

    public void SetProduct(Interactable product)
    {
        _productPrefab = product;
    }

    private IEnumerator DestroyBox(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
