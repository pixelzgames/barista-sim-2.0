using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    public float NormalizedZoom => Mathf.Clamp01((_currentZoom - _minMaxZoom.x) / _minMaxZoom.y);
    
    [SerializeField] private float _zoomSpeed = 1f;
    [SerializeField] private float _zoomSmoothness = 0.5f;
    [SerializeField] private Vector2 _minMaxZoom;
    [SerializeField] private CinemachineVirtualCamera _camera;
    [SerializeField] private AnimationCurve _zoomCurve;
    
    private InputMaster _inputMaster;
    private CinemachineComponentBase _componentBase;
    private float _currentZoom = 15f;
    private InputActionMap _currentInputMap;

    public Direction CameraDirection { get; private set; }
    
    private void Start()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        _currentInputMap = SingletonManager.Instance.InputManager.CurrentActionMapping;
        _componentBase = _camera.GetCinemachineComponent(CinemachineCore.Stage.Body);
    }

    private void OnEnable()
    {
        SingletonManager.Instance.InputManager.InputChange += OnInputChanged;
    }

    private void OnDisable()
    {
        SingletonManager.Instance.InputManager.InputChange -= OnInputChanged;
    }

    private void OnInputChanged(InputActionMap inputMap)
    {
        _currentInputMap = inputMap;
    }

    private float ZoomInputValue()
    {
        if (_currentInputMap == _inputMaster.GenericGameplay.Get())
        {
            return _inputMaster.GenericGameplay.ZoomCamera.ReadValue<float>();
        }
        
        if (_currentInputMap == _inputMaster.Moveable.Get())
        {
            return _inputMaster.Moveable.ZoomCamera.ReadValue<float>();
        }

        return 0;
    }
    
    private float RotateInputValue()
    {
        if (_currentInputMap == _inputMaster.GenericGameplay.Get())
        {
            return _inputMaster.GenericGameplay.RotateCamera.ReadValue<float>();
        }
        
        if (_currentInputMap == _inputMaster.Moveable.Get())
        {
            return _inputMaster.Moveable.RotateCamera.ReadValue<float>();
        }

        return 0;
    }

    private void FixedUpdate()
    {
        if (!PlayerInteractionManager.Instance || PlayerInteractionManager.Instance.InCloseUpMechanic)
        {
            return;
        }

        if (_componentBase is not CinemachineOrbitalTransposer body)
        {
            return;
        }
        
        body.m_XAxis.m_InputAxisValue = RotateInputValue();
        
        _currentZoom = Mathf.Clamp(_currentZoom + ZoomInputValue() * Time.unscaledDeltaTime * _zoomSpeed, _minMaxZoom.x, _minMaxZoom.y);
        body.m_FollowOffset.y = Mathf.SmoothStep(body.m_FollowOffset.y, _currentZoom, _zoomSmoothness);
        body.m_FollowOffset.z = _zoomCurve.Evaluate(body.m_FollowOffset.y);
        
        CalculateCameraDirection();
    }
    
    private void CalculateCameraDirection()
    {
        var directionAngle = transform.rotation.eulerAngles.y;

        CameraDirection = directionAngle switch
        {
            // North
            >= 0f and < 22.5f or >= 337.5f => Direction.North,
            // North east
            >= 22.5f and < 67.5f => Direction.NorthEast,
            // East
            >= 67.5f and < 112.5f => Direction.East,
            // South east
            >= 112.5f and < 157.5f => Direction.SouthEast,
            // South
            >= 157.5f and < 202.5f => Direction.South,
            // South west
            >= 202.5f and < 247.5f => Direction.SouthWest,
            // West
            >= 247.5f and < 292.5f => Direction.West,
            // North west
            >= 292.5f and < 337.5f => Direction.NorthWest,
            _ => CameraDirection
        };
    }
}

public enum Direction
{
    North,
    NorthEast,
    NorthWest,
    South,
    SouthEast,
    SouthWest,
    East,
    West
}
