public class Dirt : Interactable
{
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Clean":
                Clean();
                break;
        }

        base.ExecuteAction(actionName);
    }

    protected override void SetCurrentPossibleInteractions()
    { 
        base.SetCurrentPossibleInteractions();
        
        //If you dont have anything in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Clean");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }
        
        ShowInteractionUI();
    }

    private void Clean()
    {
        IgnoreFromInteraction = true;
        Destroy(gameObject,0.1f);
    }

    public override string GetInfoToDisplay()
    {
        return "Dirt";
    }
}
