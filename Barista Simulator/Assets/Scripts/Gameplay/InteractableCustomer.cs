using System;
using AI;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class InteractableCustomer : Interactable
{
    [FormerlySerializedAs("_contactData")] [SerializeField] private BeanOSContactConfig _contactConfig;
    [SerializeField] private NPCCustomer _customer;
    [SerializeField] private ThoughtsDisplayConfig[] _randomTalkThoughts;

    private void Sell()
    {
        var saleItem = _inHand;
        if (saleItem != null && saleItem.CurrentRecipe != null)
        {
            var currentOrder = _customer.CurrentOrder;

            if (saleItem.CurrentRecipe.Config == currentOrder.Recipe)
            {
                // Get customer that ordered the cup 
                _customer.SetOrderObject(saleItem.gameObject);
                _customer.SetWaitingForOrder(false);
                _customer.ReceiveOrder();

                PlayerInteractionManager.Instance.UnEquipObjectFromHands();
            }
            else
            {
                SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData($"Wrong order."));
            }
        }
        else
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Can't sell that"));
        }
    }

    private void Talk()
    {
        // Check if this is a scripted NPC character or just a random customer
        // Add a random moment reaction if random, trigger a dialogue if scripted
        if (!_contactConfig)
        {
            _customer.ThoughtsUIController.RequestDisplay(GetRandomThought());
            return;
        }
        
        SingletonManager.Instance.DialogueManager.RequestDialogueSequence(_contactConfig.ContactData);
    }

    private ThoughtsDisplayConfig GetRandomThought()
    {
        return _randomTalkThoughts[Random.Range(0, _randomTalkThoughts.Length)];
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand != null && !_customer.ReceivedOrder)
        {
            var action = GetActionByName("Sell");
            if (action != null)
                _currentInteractions.Add(action);
        }
        else
        {
            var action = GetActionByName("Talk");
            if (action != null)
                _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Sell":
                Sell();
                base.Highlight(false);
                break;
            case "Talk":
                Talk();
                base.Highlight(false);
                break;
        }

        base.ExecuteAction(actionName);
    }

    public override string GetInfoToDisplay()
    {
        return _customer.TraitsString + Environment.NewLine
            + $"I expect {(_customer.ExpectationsString == string.Empty ? "nothing" : _customer.ExpectationsString)}";
    }
}
