﻿
public class Counter : Containable
{
    protected override void OnSocketStateChange()
    {
        if (Socket.CurrentInteractable == null)
        {
            _data.ContainableData.ObjectInSocket = null;
            return;
        }
        
        _data.ContainableData.ObjectInSocket = Socket.CurrentInteractable.UpdateObjectData();
    }
}
