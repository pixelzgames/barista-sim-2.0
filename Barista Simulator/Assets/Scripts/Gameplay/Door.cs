using System.Linq;
using Pathfinding;
using UnityEngine;
using Random = UnityEngine.Random;

public class Door : Interactable, IInteractionTransformSocket
{
    [Header("Door Properties")] 
    [SerializeField] private Level _levelTypeToLoad;
    [SerializeField] private CommutingType _commutingType = CommutingType.Metro;
    
    [SerializeField] private Level _doorType;

    [SerializeField] private Transform[] _interactionSockets;

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        ActionInputConfig action;

        switch (_doorType)
        {
            case Level.Home:
                if (SingletonManager.Instance.GameManager.CurrentTimeOfDayState != TimeOfDay.Morning)
                {
                    break;
                }

                action = GetActionByName("Leave");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }

                break;

            case Level.Cafe:
                if (!SingletonManager.Instance.CafeManager.IsCafeOpen)
                {
                    action = GetActionByName("GoHome");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }

                break;
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Leave":
                Leave();
                break;
            case "GoHome":
                GoHome();
                break;
        }

        base.ExecuteAction(actionName);
    }

    private void Leave()
    {
        var levelNameToLoad = 
            SingletonManager.Instance.ProgressionManager.GetCurrentSceneToLoadType(_levelTypeToLoad);
        
        var data = new LevelData
        {
            LevelName = levelNameToLoad,
            LevelType = _levelTypeToLoad,
            CommutingType = _commutingType
        };
        
        SingletonManager.Instance.GameManager.LoadALevel(data);
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.LeftApartment));
    }
    
    private void GoHome()
    {
        var levelNameToLoad = 
            SingletonManager.Instance.ProgressionManager.GetCurrentSceneToLoadType(_levelTypeToLoad);
        
        var data = new LevelData
        {
            LevelName = levelNameToLoad,
            LevelType = _levelTypeToLoad,
            CommutingType = _commutingType
        };
        
        SingletonManager.Instance.GameManager.LoadALevel(data);
    }

    public Transform GetRandomInteractionPoint()
    {
        return _interactionSockets[Random.Range(0, _interactionSockets.Length)];
    }

    public Transform GetClosestInteractionPoint(Vector3 position)
    {
        var targets = _interactionSockets.Select(socket => socket.position).ToArray();
        var path = MultiTargetPath.Construct(position, targets, null);
        AstarPath.StartPath(path);
        path.BlockUntilCalculated();
        
        var closestSocket = _interactionSockets[path.chosenTarget];
        return closestSocket;
    }
}
