using UnityEngine;

public class MilkPitcher : Pickable, IContainer<Liquid>, ICleanable
{
    private static readonly int FillAmount = Shader.PropertyToID("_FillAmount");

    public bool Dirty { get; set; }
    
    public Milk Milk => _liquidContainer.Content as Milk;
    
    [SerializeField] private int _servingsCapacity = 4;
    [SerializeField] private MeshRenderer _milkLiquidMeshRenderer;
    
    private LiquidContainer _liquidContainer;
    private Material _liquidMaterial;

    protected override void Awake()
    {
        base.Awake();
        _liquidMaterial = _milkLiquidMeshRenderer.material;
        InitContainer();
    }

    private void OnDisable()
    {
        _liquidContainer.ContentChanged -= UpdateContainerLevel;
    }
    
    private void UpdateLiquidMesh()
    {
        if (_liquidMaterial == null || Milk == null)
        {
            _liquidMaterial.SetFloat(FillAmount,0);
            return;
        }
        
        _liquidMaterial.SetFloat(FillAmount, Milk.Servings / (float)_servingsCapacity);
    }

    private void UpdateContainerLevel()
    {
        var level =_liquidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowMilk);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk);
                _objectStateEventController.RaiseEvent(ObjectState.LowMilk, false);
                Milk.SetIsFrothed(false, FoamType.None);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfMilk, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowMilk, false);
                break;
        }
        
        UpdateLiquidMesh();
    }

    public override string GetInfoToDisplay()
    {
        var temp = $"Milk temperature is <b>{Milk.GetTemperatureState().ToString().ToLower()}</b>." + System.Environment.NewLine;
        var foamType = Milk.FoamType;
        var frothed = $"Milk is: {(Milk.IsFrothed ? $"<b>Frothed (<b>{foamType}</b>)" : $"<b>not Frothed")}.";
        return temp + frothed;
    }

    public void InitContainer()
    {
        _liquidContainer = new LiquidContainer(new Milk(6f,0), _servingsCapacity, true);
        _liquidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Liquid> GetContainer()
    {
        return _liquidContainer;
    }

    public override LiquidContainer GetLiquidMilkData()
    {
        return _liquidContainer;
    }

    public void Clean()
    {  
        Milk.SetIsFrothed(false, FoamType.None);
        _liquidContainer.RemoveAllContent();
        Dirty = false;
    }
}
