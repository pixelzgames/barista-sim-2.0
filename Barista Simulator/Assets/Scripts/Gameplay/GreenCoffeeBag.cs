using UnityEngine;

public class GreenCoffeeBag : Pickable, IContainer<Solid>
{
    public GreenBeans ContainerContent => _solidContainer.Content as GreenBeans;
    
    [SerializeField] private int _maxBagCapacity = 60000;

    private SolidContainer _solidContainer;
    
    protected override void Start()
    {
        base.Start();
        InitContainer();
        
        if (_solidContainer.IsEmpty())
        {
            _solidContainer.Content = new GreenBeans(_maxBagCapacity,CoffeeConfigUtility.GetRandomCoffeeData(
                false));
        }
    }

    private void OnDisable()
    {
        _solidContainer.ContentChanged -= UpdateContainerLevel;
    }

    public void RemoveBeans(int amount)
    {
        _solidContainer.RemoveFromContainer(amount);
        UpdateContainerLevel();
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
                _currentInteractions.Add(action);
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    public override string GetInfoToDisplay()
    {
        var bag ="Bag is " +  "<b>" +(_solidContainer.Content?.Servings / _maxBagCapacity * 100f) + "% </b>full.";
        return bag + System.Environment.NewLine + CoffeeDataUtility.GetCoffeeDataDisplay(_solidContainer.Content as GreenBeans);
    }

    public void InitContainer()
    {
        _solidContainer = new SolidContainer(new GreenBeans(), _maxBagCapacity, false);
        _solidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    private void UpdateContainerLevel()
    {
        var level =_solidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfSeeds, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowSeed, false);
                break;
        }
    }
}
