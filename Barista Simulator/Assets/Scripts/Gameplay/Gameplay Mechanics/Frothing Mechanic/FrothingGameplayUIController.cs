using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FrothingGameplayUIController : GameplayMechanicUIBase<EspressoMachineSection>
{
    [field: SerializeField] public FrothingMechanicConfig FrothingMechanicConfig;
    [SerializeField] private Image _timer;
    [SerializeField] private Slider _slider;
    [SerializeField] private TMP_Text _feedbackText;
    [SerializeField] private RectTransform _background;
    [SerializeField] private RectTransform _milk;
    [SerializeField] private RectTransform _micro;
    [SerializeField] private RectTransform _large;
    [SerializeField] private GameObject _aPrompt;

    private Tweener _temperatureTween;
    private Tweener _frothTween;
    private float _totalWidth;
    private float _smoothedInput;

    /// <summary>
    /// Called from FrothingGameplayMechanic ONLY when frothing
    /// </summary>
    public override void OnGameplayTick() { }

    public void SetupUI()
    {
        ResetUI();
    }

    public void SetTimerFillAmount(float value)
    {
        _timer.fillAmount = value;
    }

    public void MoveHandleOverValue(float rawInput)
    {
        _smoothedInput = Mathf.Lerp(_smoothedInput, rawInput, Time.deltaTime * FrothingMechanicConfig.InputSmoothing);
        _slider.value = _smoothedInput;

        var milkRect = _milk.rect;
        var centerMultiplier = FrothingMechanicConfig.CenterGrowingRatePerSecond.Evaluate(_timer.fillAmount);
        _milk.sizeDelta = new Vector2(milkRect.width + centerMultiplier * Time.deltaTime, milkRect.height);

        var largeAnchoredPosition = _large.anchoredPosition;
        largeAnchoredPosition.x = milkRect.width / 2;
        _large.anchoredPosition = largeAnchoredPosition;
        
        var microAnchoredPosition = _micro.anchoredPosition;
        microAnchoredPosition.x = milkRect.width / -2;
        _micro.anchoredPosition = microAnchoredPosition;

        var micro = _micro.rect;
        if (IsInMicro())
        {
            _micro.sizeDelta =
                new Vector2(
                    Mathf.Clamp(micro.width + FrothingMechanicConfig.InZoneGrowingRatePerSecond * Time.deltaTime,
                        FrothingMechanicConfig.MinWidthForFoam, FrothingMechanicConfig.MaxWidthForFoam), micro.height);
        }
        else
        {
            _micro.sizeDelta =
                new Vector2(
                    Mathf.Clamp(micro.width - FrothingMechanicConfig.ShrinkingRatePerSecond * Time.deltaTime, FrothingMechanicConfig.MinWidthForFoam,
                        FrothingMechanicConfig.MaxWidthForFoam), micro.height);
        }

        var large = _large.rect;
        if (IsInLarge())
        {
            _large.sizeDelta = new Vector2(
                Mathf.Clamp(large.width + FrothingMechanicConfig.InZoneGrowingRatePerSecond * Time.deltaTime, FrothingMechanicConfig.MinWidthForFoam,
                    FrothingMechanicConfig.MaxWidthForFoam),
                large.height);
        }
        else
        {
            _large.sizeDelta =
                new Vector2(
                    Mathf.Clamp(large.width - FrothingMechanicConfig.ShrinkingRatePerSecond * Time.deltaTime, FrothingMechanicConfig.MinWidthForFoam,
                        FrothingMechanicConfig.MaxWidthForFoam), large.height);
        }
    }

    private bool IsInMicro()
    {
        var min = _micro.anchoredPosition.x / (_totalWidth/2);
        var max = min - _micro.rect.width/(_totalWidth/2);
        return _slider.value.IsInRange(max, min);
    }

    private bool IsInLarge()
    {
        var min = _large.anchoredPosition.x / (_totalWidth/2);
        var max = min + _large.rect.width/(_totalWidth/2);
        return _slider.value.IsInRange(min, max);
    }

    private bool IsInHotMilk()
    {
        var min = -_milk.rect.width / 2 / _totalWidth;
        var max = _milk.rect.width / 2 / _totalWidth;
        return _slider.value.IsInRange(min, max);
    }

    public FoamType GetFrothResult()
    {
        if (IsInMicro())
        {
            return FoamType.Micro;
        }
        
        if (IsInLarge())
        {
            return FoamType.Large;
        }
        
        if (IsInHotMilk())
        {
            return FoamType.HotMilk;
        }

        return FoamType.Yogurt;
    }

    public void ShowFeedback(FoamType foamType)
    {
        _feedbackText.gameObject.SetActive(true);
        _aPrompt.gameObject.SetActive(false);

        switch (foamType)
        {
            case FoamType.Micro:
                _feedbackText.text = "Made Micro Foamed Milk";
                break;
            case FoamType.Large:
                _feedbackText.text = "Made Large Foamed Milk";
                break;
            case FoamType.HotMilk:
                _feedbackText.text = "Made Hot Milk...";
                break;
            case FoamType.Yogurt:
                _feedbackText.text = "Made Yogurt...";
                break;
        }
    }

    public void ResetUI()
    {
        // Reset all positions
        var milkRect = _milk.rect;
        _milk.sizeDelta = new Vector2(FrothingMechanicConfig.CenterInitialWidth, milkRect.height);

        var largeAnchoredPosition = _large.anchoredPosition;
        largeAnchoredPosition.x = milkRect.width / 2;
        _large.anchoredPosition = largeAnchoredPosition;
        
        var microAnchoredPosition = _micro.anchoredPosition;
        microAnchoredPosition.x = milkRect.width / -2;
        _micro.anchoredPosition = microAnchoredPosition;
        
        var large = _large.rect;
        _large.sizeDelta = new Vector2(FrothingMechanicConfig.LargeInitialWidth, large.height);
        
        var micro = _micro.rect;
        _micro.sizeDelta = new Vector2(FrothingMechanicConfig.MicroInitialWidth, micro.height);
        
        _totalWidth = _background.rect.width;
        _smoothedInput = 0f;
        _slider.value = 0f;
        _timer.fillAmount = 0f;
        _feedbackText.gameObject.SetActive(false);
        _aPrompt.gameObject.SetActive(true);
    }
}
