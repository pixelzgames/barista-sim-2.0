using UnityEngine;

[CreateAssetMenu(fileName = "NewFrothingMechanicConfig", menuName = "BRAG/FrothingMechanicConfig")]
public class FrothingMechanicConfig : ScriptableObject
{
    [field: SerializeField] public float MicroInitialWidth { get; set; } = 20f;
    [field: SerializeField] public float LargeInitialWidth { get; set; } = 20f;
    [field: SerializeField] public float CenterInitialWidth { get; set; } = 60f;
    
    [field: SerializeField] public float MaxWidthForFoam { get; set; }
    [field: SerializeField] public float MinWidthForFoam { get; set; }

    [field: SerializeField] public AnimationCurve CenterGrowingRatePerSecond { get; set; }
    [field: SerializeField] public float InZoneGrowingRatePerSecond { get; set; }
    [field: SerializeField] public float ShrinkingRatePerSecond { get; set; }
    [field:SerializeField] public float GameDuration { get; set; }
    [field: SerializeField] public float InputSmoothing { get; set; }
    [field:SerializeField] public float TimeToShowFeedback { get; set; }
    [field: SerializeField] public float FrothingSensitivity { get; set; }
    [field:SerializeField] public FoamTypeConfig FoamTypeConfig { get; set; }
}
