using System;
using System.Collections;
using DG.Tweening;
using GameplayMechanic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FrothingGameplayMechanic : GameplayMechanicController<EspressoMachineSection>
{
    public Action<FoamType, float> OnMilkFrothed = delegate { };
    
    [SerializeField] private GameObject _milkPitcher;
    [SerializeField] private Animator _pitcherAnimator;
    [SerializeField] private Animator _knobAnimator;

    private FrothingGameplayUIController _frothingGameplayUIController;
    private InputMaster _inputMaster;
    private float _verticalRawInput;
    private float _targetFrothingValue;
    private bool _frothing;
    private float _timer;

    private Tweener _tween;
    private static readonly int Frothing = Animator.StringToHash("Frothing");
    private static readonly int AnimationTime = Animator.StringToHash("animationTime");

    public override void StartMechanic(EspressoMachineSection espressoMachineSection)
    {
        _milkPitcher.SetActive(true);
        _timer = 1f;
        
        base.StartMechanic(espressoMachineSection);
        
        _frothingGameplayUIController = _gameplayUIController as FrothingGameplayUIController;
        _frothingGameplayUIController?.SetupUI();
        
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        SingletonManager.Instance.InputManager.ChangeInputMapping(SingletonManager.Instance.InputManager.InputMaster.Frothing);
        _inputMaster.Frothing.Frothing.started += BeginFrothing;
    }

    protected override void StopMechanic()
    {
        base.StopMechanic();
        
        _milkPitcher.SetActive(false);

        _frothing = false;
        _inputMaster.Frothing.Frothing.started -= BeginFrothing;

        _targetFrothingValue = 0f;
        DOTween.Kill(_tween);
        _frothingGameplayUIController.ResetUI();
    }

    public override void OnGameplayTick()
    {
        if (!_frothing)
        {
            return;
        }

        _verticalRawInput = _inputMaster.Frothing.PitcherHeight.ReadValue<float>();
        
        _tween = DOTween.To(() => _targetFrothingValue, x => _targetFrothingValue = x, _verticalRawInput,
            _frothingGameplayUIController.FrothingMechanicConfig.FrothingSensitivity);

        _pitcherAnimator.SetFloat(AnimationTime, (_targetFrothingValue + 1) / 2);

        _frothingGameplayUIController.MoveHandleOverValue(_verticalRawInput);
        _frothingGameplayUIController.SetTimerFillAmount(_timer);
    }

    private void BeginFrothing(InputAction.CallbackContext obj)
    {
        _frothing = true;

        if (_knobAnimator != null)
        {
            _knobAnimator?.SetBool(Frothing, true);
        }
        
        _inputMaster.Frothing.Frothing.started -= BeginFrothing;
        DOTween.To(() => _timer, x => _timer = x, 0f,
            _frothingGameplayUIController.FrothingMechanicConfig.GameDuration).SetEase(Ease.Linear).onComplete = StoppedFrothing;
    }

    private void StoppedFrothing()
    {
        _frothing = false;
        
        if (_knobAnimator != null)
        {
            _knobAnimator.SetBool(Frothing, false);
        }
        
        DOTween.Kill(_tween);
        var frothResult = _frothingGameplayUIController.GetFrothResult();
        OnMilkFrothed.Invoke(frothResult, 65f);

        _frothingGameplayUIController.ShowFeedback(frothResult);

        if (frothResult == FoamType.Micro || frothResult == FoamType.Large)
        {
            SingletonManager.Instance.CurrenciesManager.ChangeCurrentXPBy(30);
        }
        
        StartCoroutine(StopMechanicAfterDelay(_frothingGameplayUIController.FrothingMechanicConfig.TimeToShowFeedback));
    }

    private IEnumerator StopMechanicAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        StopMechanic();
    }
}
