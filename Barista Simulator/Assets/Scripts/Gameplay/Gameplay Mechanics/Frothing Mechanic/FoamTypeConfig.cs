using GD.MinMaxSlider;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFoamTypeConfig", menuName = "BRAG/FoamTypeConfig")]
public class FoamTypeConfig : ScriptableObject
{
    [field:SerializeField, MinMaxSlider(0,100)] public Vector2 NoneRange { get; private set; }
    [field:SerializeField, MinMaxSlider(0,100)] public Vector2 LargeRange { get; private set; }
    [field:SerializeField, MinMaxSlider(0,100)] public Vector2 HotMilkRange { get; private set; }
    [field:SerializeField, MinMaxSlider(0,100)] public Vector2 MicroRange { get; private set; }
    [field:SerializeField, MinMaxSlider(0,100)] public Vector2 YogurtRange { get; private set; }
}
