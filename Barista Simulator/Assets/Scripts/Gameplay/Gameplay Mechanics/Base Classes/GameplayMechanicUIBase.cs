using UnityEngine;

public abstract class GameplayMechanicUIBase<T> : MonoBehaviour
{
   [SerializeField] private CanvasGroup _canvasGroup;
   
   protected T _data;

   public virtual void InitUIWithData(T data)
   {
      _data = data;
   }

   public void ShowCanvas()
   {
      _canvasGroup.alpha = 1;
      _canvasGroup.interactable = true;
      _canvasGroup.blocksRaycasts = true;
   }

   public void HideCanvas()
   {
      _canvasGroup.alpha = 0f;
      _canvasGroup.interactable = false;
      _canvasGroup.blocksRaycasts = false;
   }
   
   public abstract void OnGameplayTick();
}