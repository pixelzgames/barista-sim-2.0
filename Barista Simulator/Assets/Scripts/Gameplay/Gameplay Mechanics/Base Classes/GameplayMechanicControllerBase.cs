using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameplayMechanic
{
    public class GameplayMechanicController<T> : MonoBehaviour, IGameplayMechanic
    {
        [SerializeReference] protected GameplayMechanicUIBase<T> _gameplayUIController;
        [SerializeField] private CinemachineVirtualCamera _gameplayCamera;

        protected T _data;
        
        public bool IsPlaying { get; private set; }

        public virtual void StartMechanic(T data)
        {
            _data = data;
            
            if (IsPlaying)
            {
                return;
            }

            if (_gameplayCamera)
            {
                SingletonManager.Instance.CameraManager.ChangeCamera(_gameplayCamera);
                SingletonManager.Instance.CameraManager.ToggleCullingManager(false);
            }
            
            if (_gameplayUIController)
            {
                _gameplayUIController.InitUIWithData(_data);
                _gameplayUIController.ShowCanvas();
            }
            
            SingletonManager.Instance.GameManager.MechanicMode?.Invoke(true);
            SingletonManager.Instance.InputManager.ChangeInputMapping(
                SingletonManager.Instance.InputManager.InputMaster.GameplayMechanics);
            SingletonManager.Instance.InputManager.InputMaster.GameplayMechanics.Back.performed += BackButton;
             
            IsPlaying = true;
        }
        
        private void BackButton(InputAction.CallbackContext callbackContext)
        {
            StopMechanic();
        }
        
        private void Update()
        {
            if (IsPlaying)
            {
                OnGameplayTick();
            }
        }
        
        protected virtual void StopMechanic()
        {
            if (!IsPlaying)
            {
                return;
            }
            
            if (_gameplayCamera)
            {
                SingletonManager.Instance.CameraManager.ChangeCamera(SingletonManager.Instance.CameraManager.MainGameplayCamera);
                SingletonManager.Instance.CameraManager.ToggleCullingManager(true);
            }
            
            if (_gameplayUIController)
            {
               _gameplayUIController.HideCanvas();
            }
            
            SingletonManager.Instance.InputManager.InputMaster.GameplayMechanics.Back.performed -= BackButton;
            SingletonManager.Instance.InputManager.SetInputMappingToDefault();
            SingletonManager.Instance.GameManager.MechanicMode.Invoke(false);
            IsPlaying = false;
        }

        /// <summary>
        /// Implement any gameplay logic that needs Update here.
        /// </summary>
        public virtual void OnGameplayTick()
        {
            if (_gameplayUIController)
            {
                _gameplayUIController.OnGameplayTick();
            }
        }
    }
}

