using GameplayMechanic;
using UnityEngine;

public class GrinderGameplayMechanic: GameplayMechanicController<Grinder>
{
    [SerializeReference] private BaseAnimInputEffectController _animController;

    public override void OnGameplayTick()
    {
        base.OnGameplayTick();
        var _myController = _gameplayUIController as GrinderGameplayUIController;
        if (!_myController)
        {
            Debug.LogError("Wrong controller is being used as cast was unsuccessful");
            return;
        }
        
        _animController.UpdatePosition(_myController.NormalizedValue);
    }
}
