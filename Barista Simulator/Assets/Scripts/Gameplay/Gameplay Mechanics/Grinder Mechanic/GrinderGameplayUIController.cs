using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

public class GrinderGameplayUIController : GameplayMechanicUIBase<Grinder>
{
    public float NormalizedValue => _canvasMechanic.NormalizedValue;
    
    [SerializeField] private RadialKnobUIController _canvasMechanic;
    [SerializeField] private TextMeshProUGUI _grindSizeText;
    
    public override void OnGameplayTick()
    {
        UpdateUI();
        _canvasMechanic.CalculateRotation();
    }
    
    private void UpdateUI()
    {
        var possibleGrindSizes = _data.PossibleGrindSizes;
        for (var i = 0; i < possibleGrindSizes.Length; i++)
        {
            if (!(_canvasMechanic.NormalizedValue <= (i + 1f) / possibleGrindSizes.Length))
            {
                continue;
            }
            
            _data.CurrentGrindSize = possibleGrindSizes[i];
            break;
        }
        
        _grindSizeText.text = Regex.Replace(_data.CurrentGrindSize.ToString(), "(\\B[A-Z])" , " $1");
    }
}
