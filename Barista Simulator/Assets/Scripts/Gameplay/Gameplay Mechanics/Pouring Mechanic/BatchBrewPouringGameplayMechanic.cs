using System;
using System.Collections;
using GameplayMechanic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BatchBrewPouringGameplayMechanic: GameplayMechanicController<BatchBrewMachine>
{
    [SerializeField] private GameObject _cup;
    [SerializeField] private MeshRenderer _liquidMeshRenderer;
    [SerializeField] private Animator _animator;
    [SerializeField] private float _pourSpeed;
    [SerializeField] private float _pourGoal;
    [SerializeField] private float _perfectThreshold = 0.1f;
    [SerializeField] private float _goodThreshold = 0.1f;
    [SerializeField] private float _stopPourDelay = 0.1f;
    [SerializeField] private AnimationCurve _pourCurve;
    [SerializeField] private ParticleSystem _pourParticleSystem;
    [SerializeField] private AudioSource _pourSound;
    [SerializeField] private AudioSource _badPourResult;
    [SerializeField] private AudioSource _goodPourResult;
    [SerializeField] private AudioSource _perfectPourResult;

    public event Action OnFinishedPour = delegate { };

    private InputMaster _inputMaster;
    private BatchBrewPouringUIController _batchBrewPouringUIController;
    private static readonly int PourRate = Animator.StringToHash("PourRate");
    private bool _isPouring;
    private float _verticalInput;
    private float _pourValue;
    private Material _liquidMaterial;
    private static readonly int FillAmount = Shader.PropertyToID("_FillAmount");

    public override void StartMechanic(BatchBrewMachine batchBrewMachine)
    {
        _cup.SetActive(true);
        _liquidMaterial = _liquidMeshRenderer.material;
        
        base.StartMechanic(batchBrewMachine);
        
        _batchBrewPouringUIController = _gameplayUIController as BatchBrewPouringUIController;
        _batchBrewPouringUIController.Reset();
        _pourValue = 0f;
        
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        SingletonManager.Instance.InputManager.ChangeInputMapping(_inputMaster.GameplayMechanics);
        _inputMaster.GameplayMechanics.Pour.started += StartPour;
        _inputMaster.GameplayMechanics.Pour.canceled += StopPour;
    }

    private void StartPour(InputAction.CallbackContext obj)
    {
        _batchBrewPouringUIController.ShowHoldPrompt(false);
        _isPouring = true;
        _pourParticleSystem.Play();
        _pourSound.Play();
        StartCoroutine(SpoutAnimation(true, 0.3f));
    }

    private IEnumerator SpoutAnimation(bool pouring, float lerpTime)
    {
        var elapsedTime = 0f;

        while (elapsedTime < lerpTime)
        {
            _animator.SetFloat(PourRate, pouring ? Mathf.Lerp(0f,1f, elapsedTime / lerpTime) : Mathf.Lerp(1f,0f, elapsedTime / lerpTime));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
    
    private void StopPour(InputAction.CallbackContext obj)
    {
        _inputMaster.GameplayMechanics.Pour.started -= StartPour;
        _inputMaster.GameplayMechanics.Pour.canceled -= StopPour;
        
        StartCoroutine(SpoutAnimation(false, _stopPourDelay));
        CompletedPour();
    }

    private void CompletedPour()
    {
        _isPouring = false;
        _pourParticleSystem.Stop();
        _pourSound.Stop();
        
        var result = EvaluatePour();
        var xp = 0;
        
        switch (result)
        {
            case PourResult.Bad:
                xp = 0;
                _badPourResult.Play();
                break;
            case PourResult.Good:
                xp = 70;
                _goodPourResult.Play();
                break;
            case PourResult.Perfect:
                xp = 110;
                _perfectPourResult.Play();
                break;
        }

        _batchBrewPouringUIController.SetPourResult(result, AnimationDone);
        return;

        void AnimationDone()
        {
            SingletonManager.Instance.CurrenciesManager.ChangeCurrentXPBy(xp);
            StopMechanic();
        }
    }

    protected override void StopMechanic()
    {
        base.StopMechanic();
        _cup.SetActive(false);
        OnFinishedPour.Invoke();
    }

    public override void OnGameplayTick()
    {
        base.OnGameplayTick();

        if (_isPouring)
        {
            _pourValue += Time.deltaTime * _pourSpeed;
            _pourSound.pitch = 0.5f + _pourValue;
        }

        _liquidMaterial.SetFloat(FillAmount, _pourCurve.Evaluate(_pourValue)); 
        _batchBrewPouringUIController.OnGameplayTick();
    }

    private PourResult EvaluatePour()
    {
        if (_pourValue < _pourGoal + _goodThreshold + _perfectThreshold && _pourValue > _pourGoal + _perfectThreshold)
        {
            return PourResult.Good;
        }

        if (_pourValue < _pourGoal + _perfectThreshold && _pourValue > _pourGoal - _perfectThreshold)
        {
            return PourResult.Perfect;
        }
            
        if (_pourValue > _pourGoal - _perfectThreshold - _goodThreshold && _pourValue < _pourGoal - _perfectThreshold)
        {
            return PourResult.Good;
        }

        return PourResult.Bad;
    }
}

public enum PourResult
{
    Bad,
    Good,
    Perfect
}
