using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class BatchBrewPouringUIController : GameplayMechanicUIBase<BatchBrewMachine>
{
    [SerializeField] private TextMeshProUGUI _goodPourText;
    [SerializeField] private TextMeshProUGUI _badPourText;
    [SerializeField] private TextMeshProUGUI _perfectPourText;
    [SerializeField] private TextMeshProUGUI _holdText;

    private Action _onAnimationDone;
    private float _pourRate;
    

    public void Reset()
    {
        _goodPourText.gameObject.SetActive(false);
        _badPourText.gameObject.SetActive(false);
        _perfectPourText.gameObject.SetActive(false);
        _holdText.gameObject.SetActive(true);
    }

    public void ShowHoldPrompt(bool show)
    {
        _holdText.gameObject.SetActive(show);
    }

    public void SetPourResult(PourResult result, Action animationDone)
    {
        _onAnimationDone = animationDone;
        switch (result)
        {
            case PourResult.Bad:
                
                _badPourText.transform.localScale = Vector3.zero;
                _badPourText.gameObject.SetActive(true);
                _badPourText.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack).OnComplete(_onAnimationDone.Invoke);
                break;
            case PourResult.Good:
                _goodPourText.transform.localScale = Vector3.zero;
                _goodPourText.gameObject.SetActive(true);
                _goodPourText.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack).OnComplete(_onAnimationDone.Invoke);
                break;
            case PourResult.Perfect:
                _perfectPourText.transform.localScale = Vector3.zero;
                _perfectPourText.gameObject.SetActive(true);
                _perfectPourText.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack).OnComplete(_onAnimationDone.Invoke);
                break;
        }
    }

    public override void OnGameplayTick() { }
}
