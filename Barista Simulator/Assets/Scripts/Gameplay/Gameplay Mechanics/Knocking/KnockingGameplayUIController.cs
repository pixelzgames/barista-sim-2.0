using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KnockingGameplayUIController : GameplayMechanicUIBase<IKnockableMechanic>
{
    [SerializeField] private KnockingGameplayMechanic _controller;
    [SerializeField] private Slider _slider;
    [SerializeField] private TMP_Text _chargedText;
    [SerializeField] private Slider _chargingSlider;
    [SerializeField] private RectTransform _tweenText;
    [SerializeField] private GameObject _perfectText, _goodText, _okayText;

    private void OnEnable()
    {
        _controller.Knocked += OnKnocked;
        _controller.ChargeStatusUpdate += OnChargeStatusUpdated;
    }

    public override void InitUIWithData(IKnockableMechanic data)
    {
        base.InitUIWithData(data);
        _slider.maxValue = data.GetKnocksRequiredToEmpty();
        _slider.value = _slider.maxValue;
    }

    private void OnDestroy()
    {
        _controller.Knocked -= OnKnocked;
        _controller.ChargeStatusUpdate -= OnChargeStatusUpdated;
    }

    private void OnChargeStatusUpdated(float chargingPower, ChargingState chargingState)
    {
        _chargingSlider.value = chargingPower;
        if (chargingState == ChargingState.None)
        {
            _chargedText.alpha = 0f;
            return;
        }
        
        _chargedText.alpha = 1f;
    }

    private void OnKnocked(int knockToRemove, ChargingState chargingState)
    {
        _slider.value -= knockToRemove;
        switch (chargingState)
        {
            case ChargingState.None:
                _okayText.SetActive(true);
                break;
            case ChargingState.Normal:
                _goodText.SetActive(true);
                break;
            case ChargingState.Max:
                _perfectText.SetActive(true);
                break;
        }
        
        _tweenText.DOScale(Vector3.one * 1.3f, 0.2f).SetEase(Ease.OutBack).onComplete = ResetTexts;
    }

    private void ResetTexts()
    {
        _tweenText.DOScale(Vector3.zero, 0.2f).SetDelay(1f).onComplete = () =>
        {
            _okayText.SetActive(false);
            _goodText.SetActive(false);
            _perfectText.SetActive(false);
        };
    }

    public override void OnGameplayTick() { }
}
