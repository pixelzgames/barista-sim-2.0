using System;
using System.Collections;
using Cinemachine;
using GameplayMechanic;
using UnityEngine;


public class KnockingGameplayMechanic : GameplayMechanicController<IKnockableMechanic>
{
    public event Action<float, ChargingState> ChargeStatusUpdate;
    public event Action<int, ChargingState> Knocked;
    public event Action OnFinishedKnocking = delegate { };

    [SerializeField] private KnockingMechanicConfig _knockingConfig;
    [SerializeField] private CinemachineImpulseSource _impulse;
    [SerializeField] private GameObject _portafilter;
    [SerializeField] private Animator _animator;
    
    private float _inactiveInputTimer;
    private bool _charging;
    private bool _goingDown;
    private float _timeSinceGoingDown;
    private int _knocksCounter;
    private InputMaster _inputMaster;
    private float _smoothedInput;
    private static readonly int InputID = Animator.StringToHash("Input");
    private bool _knocked;
    private float _chargingPower;
    private float _currentChargingTime;
    private ChargingState _chargingState;

    public override void StartMechanic(IKnockableMechanic data)
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
        _portafilter.SetActive(true);
        _knocksCounter = 0;
        SetCharge(false);
        
        base.StartMechanic(data);
    }

    private IEnumerator Knock()
    {
        _knocked = true;
        _impulse.GenerateImpulse(0.3f);
        
        var knocksToAdd = 1;
        switch (_chargingState)
        {
            case ChargingState.None:
                knocksToAdd = _knockingConfig.KnockingMechanicData.NoChargeKnocksToAdd;
                break;
            case ChargingState.Normal:
                knocksToAdd = _knockingConfig.KnockingMechanicData.NormalChargeKnocksToAdd;
                break;
            case ChargingState.Max:
                knocksToAdd = _knockingConfig.KnockingMechanicData.MaxChargeKnocksToAdd;
                break;
        }
        
        _knocksCounter += knocksToAdd;

        Knocked?.Invoke(knocksToAdd, _chargingState);
        
        yield return new WaitForSeconds(_knockingConfig.KnockingMechanicData.DelayBetweenKnock);
        
        if (_knocksCounter >= _data.GetKnocksRequiredToEmpty())
        {
            yield return new WaitForSeconds(1f);
            OnFinishedKnocking?.Invoke();
            StopMechanic();
        }
    }

    protected override void StopMechanic()
    {
        _portafilter.SetActive(false);
        SingletonManager.Instance.CurrenciesManager.ChangeCurrentXPBy(10);
        base.StopMechanic();
    }

    public override void OnGameplayTick()
    {
        base.OnGameplayTick();
        
        var rawInput = _inputMaster.GameplayMechanics.Knock.ReadValue<float>();
        
        // Check if the inputs are sleeping
        CheckIfInputSleeping(_smoothedInput);
        
        // Remap axis to 0-1 values
        var remappedInput = Mathf.Clamp((rawInput + 1) / 2, 0f, 1f);
        _smoothedInput = Mathf.Lerp(_smoothedInput, remappedInput, (_charging ? _knockingConfig.KnockingMechanicData.SmoothingInputFactorUp : _knockingConfig.KnockingMechanicData.SmoothingInputFactorDown) * Time.deltaTime);
        _animator.SetFloat(InputID, _smoothedInput);
        
        if (_knocked)
        {
            if (_smoothedInput <= _knockingConfig.KnockingMechanicData.ThresholdToGoingDown)
            {
                ResetPortafilter();
            }
            
            return;
        }

        if (_smoothedInput <= _knockingConfig.KnockingMechanicData.ThresholdToCharged)
        {
            SetCharge(true);
        }

        if (_smoothedInput >= _knockingConfig.KnockingMechanicData.ThresholdToGoingDown)
        {
            if (!_goingDown)
            {
                SetGoingDown(true);
            }
      
            _timeSinceGoingDown += Time.deltaTime;
        }
      
        if (_smoothedInput >= _knockingConfig.KnockingMechanicData.ThresholdToKnock && _timeSinceGoingDown < _knockingConfig.KnockingMechanicData.MaxTimeToTriggerKnock)
        {
            StartCoroutine(Knock());
        }
    }

    private void SetGoingDown(bool status)
    {
        _goingDown = status;
    }

    private void SetCharge(bool charging)
    {
        _chargingState = ChargingState.None;
        if (charging)
        {
            _currentChargingTime += Time.deltaTime * _knockingConfig.KnockingMechanicData.ChargingSpeed;
            _chargingPower = _knockingConfig.KnockingMechanicData.ChargingAnimationCurve.Evaluate(_currentChargingTime);
        }
        else
        {
            _chargingPower = 0f;
            _currentChargingTime = 0f;
        }
        
        if (_chargingPower >= _knockingConfig.KnockingMechanicData.NormalKnockNormalizedChargeThreshold)
        {
            _chargingState = ChargingState.Normal;
        }

        if (_chargingPower >= _knockingConfig.KnockingMechanicData.PerfectKnockNormalizedChargeThreshold)
        {
            _chargingState = ChargingState.Max;
        }

        if (_chargingPower >= _knockingConfig.KnockingMechanicData.BadKnockNormalizedChargeThreshold)
        {
            _chargingState = ChargingState.None;
        }
        
        _charging = charging;
        ChargeStatusUpdate?.Invoke(_chargingPower, _chargingState);
    }

    private void CheckIfInputSleeping(float input)
    {
        if (input.IsInRange(0.45f, 0.55f))
        {
            _inactiveInputTimer += Time.deltaTime;
            if (_inactiveInputTimer >= _knockingConfig.KnockingMechanicData.TimeForInactiveInputBeforeReset)
            {
                ResetPortafilter();
            }
        }
    }

    private void ResetPortafilter()
    {
        _knocked = false;
        SetCharge(false);
        SetGoingDown(false);
        _timeSinceGoingDown = 0f;
        _inactiveInputTimer = 0f;
    }
}

public enum ChargingState
{
    None,
    Normal,
    Max
}
