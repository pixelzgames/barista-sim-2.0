using UnityEngine;
using DG.Tweening;
using UnityEngine.VFX;

public class TrashBin : Movable, IKnockableMechanic
{
    [SerializeField] [Range(1,10)] private int _knocksToEmpty;
    [SerializeField] private VisualEffect _nastyVFX;
    [SerializeField] private WorldAttribute _worldAttribute;
    
    [Header("Gameplay mechanic")]
    [SerializeField] private KnockingGameplayMechanic _gameplayMechanicController;
    
    private float _trashFilledPercentage;
    private Transform _hinge;
    private Portafilter _portafilter;

    protected override void Start() 
    {
        base.Start();
        _hinge = transform.GetChild(0);
        _nastyVFX.Stop();
    }

    public void Trash(float percentageAmount) 
    {
        if (_trashFilledPercentage + percentageAmount > 100f) 
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Bin is full, empty it first!"));
            return;
        }

        if (!_inHand.CanTrash)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Can't throw this out."));
            return;
        }
        
        _trashFilledPercentage = Mathf.Clamp(_trashFilledPercentage + percentageAmount, 0f, 100f);
        TrashAnimation();

        if (_trashFilledPercentage >= 100f)
        {
            _objectStateEventController.RaiseEvent(ObjectState.Nasty);
            _nastyVFX.Play();
            _worldAttribute.enabled = false;
        }
        
        if(_inHand == null)
        {
            return;
        }
        
        _inHand.Drop();
        Destroy(_inHand.gameObject);
    }
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
       
        if (_inHand == null && _trashFilledPercentage > 0f)
        {
            var action = GetActionByName("Empty");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        if (_inHand != null)
        {
            if (_inHand is Portafilter portafilter && portafilter.Dirty)
            {
                var action = GetActionByName("Knock");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
            
            if (_inHand is BrewFilter filter && filter.Dirty)
            {
                var action = GetActionByName("TrashPaperFilter");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        
            if (_inHand.CanTrash)
            {
                var action = GetActionByName("Trash out");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        
        

        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Trash out":
                Trash(5f);
                break;
            case "Empty":
                Empty();
                break;
            case "TrashPaperFilter":
                TrashPaperFilter();
                break;
            case "Knock":
                _portafilter = _inHand as Portafilter;
                StartKnockingMechanic();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void StartKnockingMechanic()
    {
        var seq = DOTween.Sequence();
        seq.Append(_hinge.DOBlendableLocalRotateBy(new Vector3(-110f, 0f, 0f), 0.5f).SetEase(Ease.OutBounce));
        _gameplayMechanicController.OnFinishedKnocking += OnFinishedKnockingMechanic;
        _gameplayMechanicController.StartMechanic(this);
    }

    private void OnFinishedKnockingMechanic()
    {
        _gameplayMechanicController.OnFinishedKnocking -= OnFinishedKnockingMechanic;
        _portafilter.Clean();
        var seq = DOTween.Sequence();
        seq.Append(_hinge.DOBlendableLocalRotateBy(new Vector3(110f, 0f, 0f), 0.5f).SetEase(Ease.OutBounce));
        seq.Play();
    }

    private void Empty() 
    {
        _trashFilledPercentage = 0f;
        _objectStateEventController.RaiseEvent(ObjectState.Nasty, false);
        _nastyVFX.Stop();
        _worldAttribute.enabled = true;
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Bin emptied!"));
    }

    private void TrashPaperFilter()
    {
        if (_inHand is BrewFilter filter)
        {
            filter.Clean();
        }
    }

    private void TrashAnimation() 
    {
        var seq = DOTween.Sequence();
        seq.Append(_hinge.DOBlendableLocalRotateBy(new Vector3(-110f, 0f, 0f), 0.5f).SetEase(Ease.OutBounce));
        seq.Append(_hinge.DOBlendableLocalRotateBy(new Vector3(110f, 0f, 0f), 0.5f).SetEase(Ease.OutBounce));
        seq.Play();
    }

    public override string GetInfoToDisplay()
    {
        return $"Trash is {_trashFilledPercentage}% full!";
    }

    public Interactable GetInteractable()
    {
        return this;
    }

    public int GetKnocksRequiredToEmpty()
    {
        return _knocksToEmpty;
    }
}
