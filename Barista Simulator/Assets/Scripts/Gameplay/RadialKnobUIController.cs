using UnityEngine;
using UnityEngine.UI;

public class RadialKnobUIController : MonoBehaviour
{
    /// <summary>
    /// Returns the normalized position of the knob
    /// </summary>
    public float NormalizedValue => (_rotationEuler.z - MAX_ANGLE) / (-MAX_ANGLE - MAX_ANGLE);
    
    private const float MAX_ANGLE = 115f;
    private InputMaster _inputMaster;
    private Vector3 _rotationEuler;
    
    [SerializeField] private float _rotatingDegreesPerSeconds = 150f;
    [SerializeField] private Image _knob;
    
    private void Start()
    {
        _inputMaster = SingletonManager.Instance.InputManager.InputMaster;
    }

    public void CalculateRotation()
    {
        var movementVector = _inputMaster.GameplayMechanics.Move.ReadValue<Vector2>();
        var h = movementVector.x;
        _rotationEuler += Vector3.forward * (_rotatingDegreesPerSeconds * Time.deltaTime * -h);
        _rotationEuler.z = Mathf.Clamp(_rotationEuler.z, -MAX_ANGLE, MAX_ANGLE);
        _knob.transform.rotation = Quaternion.Euler(0,0,_rotationEuler.z);
    }
}
