﻿using UnityEngine;
using DG.Tweening;

public class DoorTrigger : MonoBehaviour
{
    [SerializeField] private float _checkForDoorStateRateInSeconds = 1f;
    [SerializeField] private GameObject _door;
    [SerializeField] private Ease _easeType;
    [SerializeField] private float _animationDuration = 0.5f;
    [SerializeField] private Vector3 _rotationDirection;
    
    private bool _opened;
    private int _entities;
    private DoorSide _lastSideOpened;

    private void Start()
    {
        InvokeRepeating(nameof(IntermittentCheck),0f,_checkForDoorStateRateInSeconds);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") && !other.CompareTag("AI"))
        {
            return;
        }

        ++_entities;

        if (_opened || _entities <= 0)
        {
            return;
        }

        var rotationSideMultiplier = DoorSide.Left;
        if (transform.InverseTransformPoint(other.transform.position).y > transform.localPosition.y)
        {
            rotationSideMultiplier = DoorSide.Right;
        }
        
        OpenDoor(rotationSideMultiplier);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player") && !other.CompareTag("AI"))
        {
            return;
        }

        --_entities;
        
        if (!_opened || _entities != 0)
        {
            return;
        }

        CloseDoor();
    }

    private void IntermittentCheck()
    {
        switch (_entities)
        {
            case >= 1:
                OpenDoor();
                break;
            case 0:
                CloseDoor();
                break;
        }
    }

    private void OpenDoor(DoorSide side = DoorSide.None)
    {
        if (_opened)
        {
            return;
        }
        
        if (side == DoorSide.None)
        {
            side = _lastSideOpened;
        }

        var intSide = side == DoorSide.Left ? 1 : -1;
        _door.transform.DOLocalRotateQuaternion(Quaternion.Euler(0f,0f,90f * intSide * _rotationDirection.z), _animationDuration)
            .SetUpdate(UpdateType.Fixed).SetEase(_easeType);
        
        _opened = true;
        _lastSideOpened = side;
    }

    private void CloseDoor()
    {
        if (!_opened)
        {
            return;
        }
        
        _door.transform.DOLocalRotateQuaternion(Quaternion.identity, _animationDuration)
            .SetUpdate(UpdateType.Fixed).SetEase(_easeType);
        
        _opened = false;
        _lastSideOpened = DoorSide.None;
    }
    
    private enum DoorSide
    {
        None,
        Left,
        Right
    }
}
