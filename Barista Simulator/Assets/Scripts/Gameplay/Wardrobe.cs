﻿using Cinemachine;
using DG.Tweening;
using UnityEngine;

public class Wardrobe : Interactable
{
    [SerializeField] private ApartmentUIController _uiController;
    [SerializeField] private CinemachineVirtualCamera _virtualCamera;
    [SerializeField] private Transform _characterPosition;

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        var action = GetActionByName("ChangeAppearance");
        if (action != null)
        {
            _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "ChangeAppearance":
                OpenCharacterCreator();
                break;
        }

        base.ExecuteAction(actionName);
    }

    public void OpenCharacterCreator()
    {
        _uiController.OpenCharacterCreator();
        
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(new GameEvent(GameEventKey.CharacterCreatorOpened));

        // Set player rotation towards interactable
        PlayerInteractionManager.Instance.PlayerController.SetPlayerLookDirection(_virtualCamera.transform);
        PlayerInteractionManager.Instance.PlayerController.transform.DOMove(_characterPosition.position, 0.2f);
    }
}