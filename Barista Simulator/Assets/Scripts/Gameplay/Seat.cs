using System.Linq;
using AI;
using DG.Tweening;
using Pathfinding;
using UnityEngine;

public class Seat : Movable, IInteractionTransformSocket
{
    [SerializeField] private Transform _seatSocket;
    [SerializeField] private Transform[] _interactionSockets;

    public bool Occupied { get; private set; }
    
    /// <summary>
    /// Sit a player
    /// </summary>
    /// <param name="transformToSit">Transform to move to the correct position</param>
    private void Sit(Transform transformToSit)
    {
        Occupied = true;
        const float animSpeed = 0.2f;
        
        PlayerInteractionManager.Instance.PlayerController.Sit(true);
        transformToSit.DOMove(_seatSocket.position, animSpeed).SetUpdate(UpdateType.Fixed, false);
        transformToSit.DORotate(_seatSocket.rotation.eulerAngles, animSpeed).SetUpdate(UpdateType.Fixed, false);
    }
    
    /// <summary>
    /// Sit an NPC
    /// </summary>
    /// <param name="npc"></param>
    public void Sit(NPCCustomer npc)
    {
        Occupied = true;
        const float animSpeed = 0.2f;
        npc.Sit(true);
        npc.transform.DOMove(_seatSocket.position, animSpeed);
        npc.transform.DORotate(_seatSocket.rotation.eulerAngles, animSpeed);
    }

    /// <summary>
    /// Stand a player or an AI
    /// </summary>
    /// <param name="transformToStand">Transform to move to the correct position</param>
    public void Stand(Transform transformToStand)
    {
        Occupied = false;
        var animSpeed = 0.2f;

        PlayerInteractionManager.Instance.PlayerController.Sit(false);
 
        transformToStand.DORotate(GetRandomInteractionPoint().rotation.eulerAngles, animSpeed);
        transformToStand.DOMove(GetRandomInteractionPoint().position, animSpeed);
    }

    /// <summary>
    /// Stand an AI
    /// </summary>
    /// <param name="npc">Transform to move to the correct position</param>
    public void Stand(NPCCustomer npc)
    {
        Occupied = false;
        var animSpeed = 0.2f;

        npc.Sit(false);

        npc.transform.DOMove(GetRandomInteractionPoint().position, animSpeed);
    }

    public void SetOccupied()
    {
        Occupied = true;
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (!Occupied)
        {
            //If you dont have anything in hands
            var action = GetActionByName("SitDown");
            if (action != null)
                _currentInteractions.Add(action);
        }
        else
        {
            //If you dont have anything in hands
            var action = GetActionByName("StandUp");
            if (action != null)
                _currentInteractions.Add(action);
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "SitDown":
                Sit(PlayerInteractionManager.Instance.PlayerController.transform);
                break;
            case "StandUp":
                Stand(PlayerInteractionManager.Instance.PlayerController.transform);
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    public Transform GetRandomInteractionPoint()
    {
        return _interactionSockets[Random.Range(0, _interactionSockets.Length)];
    }

    public Transform GetClosestInteractionPoint(Vector3 position)
    {
        var targets = _interactionSockets.Select(socket => socket.position).ToArray();
        var path = MultiTargetPath.Construct(position, targets, null);
        AstarPath.StartPath(path);
        path.BlockUntilCalculated();
        
        var closestSocket = _interactionSockets[path.chosenTarget];
        return closestSocket;
    }

    public override string GetInfoToDisplay()
    {
        return $"A seat";
    }
}
