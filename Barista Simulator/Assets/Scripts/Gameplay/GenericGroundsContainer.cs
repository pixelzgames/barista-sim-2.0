using System;
using System.Text.RegularExpressions;
using UnityEngine;

public class GenericGroundsContainer : Pickable, IContainer<Solid>
{
    [SerializeField] private GameObject _coffeeGroundsMesh;
    private Grounds _currentGrounds => _solidContainer.Content as Grounds;
    
    private readonly SolidContainer _solidContainer = new (new Grounds(), 500, true);

    protected override void Awake()
    {
        base.Awake();
        _solidContainer.ContentChanged += OnContentChanged;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        _solidContainer.ContentChanged -= OnContentChanged;
    }

    private void OnContentChanged()
    {
        _coffeeGroundsMesh.SetActive(!_solidContainer.IsEmpty());
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();

        //If you dont have anything in hands
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
                _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
        }

        base.ExecuteAction(actionName);
    }
    
    public void InitContainer()
    {
        //_solidContainer = new SolidContainer(new Grounds(), 500, true);
    }

    public Container<Solid> GetContainer()
    {
        return _solidContainer;
    }

    public override string GetInfoToDisplay()
    {
        var grindSize =
            $"{(_currentGrounds != null && _currentGrounds.Servings > 0 ? "Grind size is " + "<b>" + Regex.Replace(_currentGrounds.Size.ToString(), "(\\B[A-Z])", " $1") + "</b>" : "")}";
        var dirty = GetContainer().Content.Servings + " servings in.";

        return grindSize + "\n" + dirty;
    }

    public override SolidContainer GetSolidBeansContainer()
    {
        return _solidContainer;
    }
}
