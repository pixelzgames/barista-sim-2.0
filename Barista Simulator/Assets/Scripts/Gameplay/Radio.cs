using UnityEngine;

[RequireComponent(typeof(MusicController))]
public class Radio : Movable
{
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private ParticleSystem _musicNotesParticles;

    private bool _isOn;
    private MusicController _musicController;
    private static readonly int _playing = Animator.StringToHash("Playing");

    protected override void Awake()
    {
        base.Awake();
        _musicController = GetComponent<MusicController>();
    }

    public override string GetInfoToDisplay()
    {
        return _isOn ? "Now Playing: \n" + _musicController.GetCurrentMusicInfo() : "Radio is Off";
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null)
        {
            if (_isOn)
            {
                var action = GetActionByName("ChangeGenre");
                if (action != null)
                    _currentInteractions.Add(action);

                action = GetActionByName("TurnOff");
                if (action != null)
                    _currentInteractions.Add(action);
            }
            else
            {
                var action = GetActionByName("TurnOn");
                if (action != null)
                    _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "ChangeGenre":
                _musicController.ChangeGenre();
                break;
            case "TurnOff":
                ToggleRadioOn();
                break;
            case "TurnOn":
                ToggleRadioOn();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void ToggleRadioOn()
    {
        _isOn = !_isOn;

        if (_isOn)
        {
            _musicController.PlayMusic();
        }
        else
        {
            _musicController.StopMusic();
        }

        _animator.SetBool(_playing, _isOn);
        
        // Music particles
        if (_musicNotesParticles != null)
        {
            var emission = _musicNotesParticles.emission;
            emission.enabled = _isOn;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude < 1f)
        {
            return;
        }

        if (_isOn)
        {
            _musicController.ChangeGenre();
        }
        else
        {
            ToggleRadioOn();
        }
    }
}
