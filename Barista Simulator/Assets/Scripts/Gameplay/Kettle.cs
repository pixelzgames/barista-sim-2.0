﻿using System.Linq;
using UnityEngine;

public class Kettle : Pickable, IContainer<Liquid>
{
    public Water Water => _liquidContainer.Content as Water;
    public int KettleMaxCapacity => _maxServings;
    
    [SerializeField] private int _maxServings = 20;
    [SerializeField] private float _heatingSpeedMultiplier = 1f;
    [SerializeField] private int _pourCostInServings = 1;
    
    private bool _isHeating;
    
    private LiquidContainer _liquidContainer;
    
    protected override void Awake()
    {
        base.Awake();
        InitContainer();
    }
    
    private void OnDisable()
    {
        _liquidContainer.ContentChanged -= UpdateContainerLevel;
    }

    private void StartHeating()
    {
        if (_liquidContainer.GetFullPercentage() <= 0)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Not enough water to heat up!"));
            return;
        }
        
        _isHeating = true;
        Water.SetTemperatureGoal(100f,_heatingSpeedMultiplier);
    }

    private void StopHeating()
    {
        _isHeating = false;
        Water.SetTemperatureGoal(Temperature.ROOM_TEMP, Liquid.DEFAULT_ROOMTEMP_CHANGE_RATE);
    }

    private void OnCompletedPour()
    {
        _liquidContainer.RemoveFromContainer(_pourCostInServings);
        if (_inHand is Cup cup)
        {
            cup.AddIngredient(new Water(Water.Temperature.Current, _pourCostInServings));
        }
    }

    private void UpdateContainerLevel()
    {
        var level =_liquidContainer.GetLevel();

        switch (level)
        {
            case LevelResult.Low:
                _objectStateEventController.RaiseEvent(ObjectState.LowWater);
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater, false);
                break;
            case LevelResult.Empty:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater);
                _objectStateEventController.RaiseEvent(ObjectState.LowWater, false);
                break;
            case LevelResult.None:
                _objectStateEventController.RaiseEvent(ObjectState.OutOfWater, false);
                _objectStateEventController.RaiseEvent(ObjectState.LowWater, false);
                break;
        }
    }

    private void OnCompletedInstantCoffee() 
    {
        if (_inHand is Cup cup)
        {
            RecipeMaker<Liquid>.MakeRecipe(RecipeType.InstantCoffee, GetContainer(), cup.GetContainer(),
                Water.Temperature.Current, cup.GetContainer().Ingredients.OfType<InstantGrounds>().First().CoffeeData);
        }
    }

    private void OnCompletedAmericano()
    {
        if (_inHand is Cup inHand)
        {
            RecipeMaker<Liquid>.MakeRecipe(RecipeType.Americano, GetContainer(), inHand.GetContainer(),
                Water.Temperature.Current, inHand.GetCoffeeData());
        }
    }
    
    private void OnCompletedLongBlack()
    {
        if (_inHand is Cup cup)
        {
            RecipeMaker<Liquid>.MakeRecipe(RecipeType.LongBlack, GetContainer(), cup.GetContainer(),
                Water.Temperature.Current, cup.GetCoffeeData());
        }
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Take":
                PickUp();
                break;
            case "StartHeat":
                StartHeating();
                break;
            case "StopHeat":
                StopHeating();
                break;
            case "Pour":
                OnCompletedPour();
                break;
            case "MakeInstantCoffee":
                OnCompletedInstantCoffee();
                break;
            case "MakeAmericano":
                OnCompletedAmericano();
                break;
            case "MakeLongBlack":
                OnCompletedLongBlack();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        if (_inHand == null)
        {
            var action = GetActionByName("Take");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
            
            if (Water.Temperature.Current < Liquid.MAX_TEMP && !_isHeating)
            {
                action = GetActionByName("StartHeat");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
            else if (_isHeating)
            {
                action = GetActionByName("StopHeat");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
        }
        else if (_inHand is Cup cup)
        {
            if (RecipeMaker<Liquid>.CanMakeInstantCoffeeRecipe(GetContainer(), cup.GetContainer()))
            {
                var action = GetActionByName("MakeInstantCoffee");
                if (action != null)
                {
                    _currentInteractions.Add(action);
                }
            }
            
            if (cup.IsEmpty)
            {
                if (cup.GetContainer().Ingredients.Count <= 0)
                {
                    var action = GetActionByName("Pour");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }
                }
            }

            switch (cup.CurrentRecipe.Config.RecipeType)
            {
                case RecipeType.Espresso:
                {
                    var action = GetActionByName("MakeAmericano");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }

                    break;
                }
                case RecipeType.DoubleEspresso:
                {
                    var action = GetActionByName("MakeLongBlack");
                    if (action != null)
                    {
                        _currentInteractions.Add(action);
                    }

                    break;
                }
            }
        }

        ShowInteractionUI();
    }

    public override string GetInfoToDisplay()
    {
        return $"Kettle is <b>{_liquidContainer.GetFullPercentage()}%</b> full." + System.Environment.NewLine +
               $"Water temperature is <b>{Water.GetTemperatureState().ToString().ToLower()}</b>." + System.Environment.NewLine +
               $"Kettle is: {(_isHeating ? $"<color={Barista.Colors.OnHex}>heating" : $"<color={Barista.Colors.OffHex}>not heating")}.";
    }

    public void InitContainer()
    {
        _liquidContainer = new LiquidContainer(new Water(Temperature.ROOM_TEMP, 0), _maxServings, true);
        _liquidContainer.ContentChanged += UpdateContainerLevel;
        UpdateContainerLevel();
    }

    public Container<Liquid> GetContainer()
    {
        return _liquidContainer;
    }
}
