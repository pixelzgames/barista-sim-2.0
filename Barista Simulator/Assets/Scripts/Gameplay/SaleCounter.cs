﻿using UnityEngine;

public class SaleCounter : Interactable
{
    [SerializeField] private bool _isDebug;
    [SerializeField] private Socket _containableSocket;

    private void Place()
    {
        var saleItem = _inHand as Cup;

        if (saleItem && saleItem.CurrentRecipe != null)
        {
            var currentOrder = SingletonManager.Instance.OrderManager.GetMostPressingValidOrder(saleItem);

            if (currentOrder)
            {
                if (_isDebug)
                {
                    SingletonManager.Instance.OrderManager.SellOrder(currentOrder, 10);
                    PlayerInteractionManager.Instance.TryPlaceObject(_containableSocket, false);
                    currentOrder.OnSuccess();
                    Destroy(saleItem.gameObject, 2f);
                    return;
                }
                
                if (currentOrder.Type == OrderType.Stay)
                {
                    SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData($"Customer is sitting, waiting for it..."));
                    return;
                }
                
                saleItem.ToggleLid(true);
                
                // Get customer that ordered the cup by time priority
                currentOrder.Customer.SetOrderObject(saleItem.gameObject);
                currentOrder.Customer.SetWaitingForOrder(false);

                PlayerInteractionManager.Instance.TryPlaceObject(_containableSocket, false);
            }
            else
            {
                SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData($"No one ordered a {saleItem.CurrentRecipe.Config.RecipeName} yet"));
                Debug.Log("No order for " + saleItem.CurrentRecipe.Config.RecipeName);
            }
        }
        else
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Can't sell that"));
        }
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        //If the containable is empty
        if (_containableSocket.IsEmpty)
        {
            //if hands are not empty
            if (_inHand != null)
            {
                var action = GetActionByName("Sell");
                if (action != null)
                    _currentInteractions.Add(action);
            }
        }
        
        ShowInteractionUI();
    }

    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Sell":
                Place();
                base.Highlight(false);
                break;
        }
        
        base.ExecuteAction(actionName);
    }
}
