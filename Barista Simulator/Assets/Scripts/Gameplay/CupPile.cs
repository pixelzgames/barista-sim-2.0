﻿using UnityEngine;

public class CupPile : Movable
{
    [Space(10f)]
    [SerializeField] private GameObject _cupPrefab;
    [SerializeField] private int _maxCupCount = 20;

    private int _cupsInPile;
    private OrderType _type;

    protected override void Start()
    {
        base.Start();
        _cupsInPile = _maxCupCount;
        CheckCupPile();
    }

    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        //If you dont have anything in hands
        if (_inHand == null && _cupsInPile > 0)
        {
            var action = GetActionByName("Stay");
            if (action != null)
                _currentInteractions.Add(action);
            action = GetActionByName("Togo");
            if (action != null)
                _currentInteractions.Add(action);
        }
        else if (_cupsInPile <= 0)
        {
            var action = GetActionByName("Refill Cups");
            if (action != null)
                _currentInteractions.Add(action);
        }

        if (_inHand is Cup { IsEmpty: true })
        {
            var action = GetActionByName("Add");
            if (action != null)
                _currentInteractions.Add(action);
        }

        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "Togo":
                SpawnCup(OrderType.Togo);
                break;
            case "Stay":
                SpawnCup(OrderType.Stay);
                break;
            case "Refill Cups":
                RefillCups();
                break;
            case "Add Cup":
                AddCupToPile();
                break;
        }
        
        base.ExecuteAction(actionName);
    }
    
    private void SpawnCup(OrderType type)
    {
        _type = type;
        TryAddCupsInHands();
    }

    private void RefillCups()
    {
        _cupsInPile = 20;
        SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("Cups pile refilled!"));
        CheckCupPile();
    }

    private void AddCupToPile()
    {
        if(_inHand == null || _inHand is not Cup)
        {
            return;
        }
        
        _inHand.Drop();
        Destroy(_inHand.gameObject);
        _cupsInPile++;
        CheckCupPile();
    }

    private void TryAddCupsInHands()
    {
        if (_cupsInPile <= 0)
        {
            SingletonManager.Instance.GameNotificationManager.QueueNotification(new NotificationData("No more cups in the pile"));
            return;
        }

        var cup = Instantiate(_cupPrefab, transform.position, Quaternion.identity)
            .GetComponentInChildren<Pickable>();


        ((Cup)cup).InitializeCupPrefab(_type, false);
        cup.PickUp();
        _cupsInPile--;
        CheckCupPile();
    }
    
    private void CheckCupPile()
    {
        if (_cupsInPile <= 0)
        {
            _objectStateEventController.RaiseEvent(ObjectState.Empty);
            _objectStateEventController.RaiseEvent(ObjectState.Low, false);
        }
        else if ((float)_cupsInPile / _maxCupCount * 100f <= ObjectStateConfig.LOW_PERCENTAGE)
        {
            _objectStateEventController.RaiseEvent(ObjectState.Low);
            _objectStateEventController.RaiseEvent(ObjectState.Empty, false);
        }
        else
        {
            _objectStateEventController.RaiseEvent(ObjectState.Low, false);
            _objectStateEventController.RaiseEvent(ObjectState.Empty, false);
        }
    }

    public override string GetInfoToDisplay()
    {
        return "Cups in pile left: " + _cupsInPile;
    }
}
