using UnityEngine;

public class AnimatorNormalizedController : BaseAnimInputEffectController
{
    [SerializeField] private Animator _animator;
    private static readonly int PlaybackTime = Animator.StringToHash("PlaybackTime");

    public override void UpdatePosition(float normalizedValue)
    {
        _animator.SetFloat(PlaybackTime, normalizedValue);
    }
}
