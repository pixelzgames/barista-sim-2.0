using UnityEngine;

public abstract class BaseAnimInputEffectController : MonoBehaviour
{
    public abstract void UpdatePosition(float normalizedValue);
}
