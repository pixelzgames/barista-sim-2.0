using System;

public class TaskManager : PersistentBaseData<TaskManagerData>
{
    public event Action<TaskInstance> OnTaskAdded = delegate { };
    public event Action<TaskData> OnTaskRemoved = delegate { };
    
    public void AddTask(TaskData taskData, bool loadingFromSaveFile = false)
    {
        var taskInstance = new TaskInstance(taskData);
        taskInstance.SubscribeToSubTasks();
        taskInstance.SubTasksCompleted += RemoveTask;
        
        if (loadingFromSaveFile)
        {
            OnTaskAdded?.Invoke(taskInstance);
            return;
        }
        
        var data = new NotificationData("New Task", taskData.TaskName, size: NotificationSize.Main);
        SingletonManager.Instance.GameNotificationManager.QueueNotification(data);
        data.NotificationFinished = () =>
        {
            OnTaskAdded?.Invoke(taskInstance);
        };
        
        Data.ActiveTasks.Add(taskInstance);
    }

    private void RemoveTask(TaskInstance taskInstance)
    {
        SingletonManager.Instance.GameEventDispatcher.LogGameEvent(taskInstance.TaskData.GameEventSentOnTaskCompletion);
        OnTaskRemoved?.Invoke(taskInstance.TaskData);
        taskInstance.SubTasksCompleted -= RemoveTask;
        
        Data.ActiveTasks.Remove(taskInstance);
    }
    
    protected override void LoadData(PlayerData data)
    {
        if (Data is { ActiveTasks: not null })
        {
            for (var i = 0; i < Data.ActiveTasks.Count; i++)
            {
                Data.ActiveTasks[i].Destroy();
                Data.ActiveTasks[i] = null;
            }
        
            Data.ActiveTasks.Clear();
        }

        Data = data.TaskManagerData;
        foreach (var task in Data.ActiveTasks)
        {
            AddTask(task.TaskData, true);
        }
    }
}