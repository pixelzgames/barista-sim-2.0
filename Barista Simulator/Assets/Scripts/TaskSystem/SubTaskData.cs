using System;
using UnityEngine;

[Serializable]
public class SubTaskData
{
    [field: SerializeField, TextArea]
    public string TaskDescription { get; set; }
    [field: SerializeField]
    public int ProgressCompletionAmount { get; set; }
    [field: SerializeField]
    public TaskProgressDisplayConfig DisplayConfig { get; set; }
    [field: SerializeField]
    public GameEventKey CompletionEventKey { get; set; }
}

public enum TaskProgressDisplayConfig
{
    None = 0,
    Percentage = 1,
    Fraction = 2,
    FillBar = 3
}
