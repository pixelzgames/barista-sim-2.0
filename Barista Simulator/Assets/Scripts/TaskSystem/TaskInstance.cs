using System;
using System.Collections.Generic;

public class TaskInstance
{
    public event Action<TaskInstance> SubTasksCompleted = delegate { };
    
    public List<SubTaskInstance> SubTasks { get; set; }
    public TaskData TaskData { get; set; }
    
    public TaskInstance(TaskData taskData)
    {
        TaskData = taskData;
        SubTasks = new List<SubTaskInstance>();

        foreach (var subTask in taskData.SubTaskDataList)
        {
            SubTasks.Add(new SubTaskInstance(subTask));
        }

        SubscribeToSubTasks();
    }

    public void SubscribeToSubTasks()
    {
        foreach (var subtask in SubTasks)
        {
            subtask.SubTaskCompleted += CheckIfAllSubTasksComplete;
        }
    }

    private void CheckIfAllSubTasksComplete(SubTaskInstance obj)
    {
        // Check if all tasks are complete
        foreach (var subTask in SubTasks)
        {
            if (!subTask.Completed)
            {
                return;
            }
        }
        
        SubTasksCompleted?.Invoke(this);
        foreach (var subtask in SubTasks)
        {
            subtask.SubTaskCompleted -= CheckIfAllSubTasksComplete;
        }
    }

    public void Destroy()
    {
        SubTasksCompleted = delegate { };
        TaskData = null;
        
        for (var i = 0; i < SubTasks.Count; i++)
        {
            SubTasks[i].Destroy();
            SubTasks[i] = null;
        }
        
        SubTasks.Clear();
    }
}