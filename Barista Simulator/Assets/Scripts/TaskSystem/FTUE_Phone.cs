public class FTUE_Phone : Interactable
{
    protected override void SetCurrentPossibleInteractions()
    {
        base.SetCurrentPossibleInteractions();
        
        if (_inHand == null)
        {
            var action = GetActionByName("PickupPhone");
            if (action != null)
            {
                _currentInteractions.Add(action);
            }
        }

        ShowInteractionUI();
    }
    
    protected override void ExecuteAction(string actionName)
    {
        switch (actionName)
        {
            case "PickupPhone":
                PickupPhone();
                break;
        }
        
        base.ExecuteAction(actionName);
    }

    private void PickupPhone()
    {
        PlayerInteractionManager.Instance.CanUsePhone = true;
        PlayerInteractionManager.Instance.TryPullPhoneOut();
        
        gameObject.SetActive(false);
    }
}
