using System;

public class SubTaskInstance
{
    public event Action<SubTaskInstance> SubTaskCompleted = delegate { };
    public event Action<int> ProgressionIncremented = delegate { };
    public SubTaskData SubTaskData { get; set; }
    public bool Completed { get; set; }
    public int CurrentValue { get; set; }
    
    public SubTaskInstance(SubTaskData subTaskData)
    {
        SubTaskData = subTaskData;
        SetupTaskGoal();
    }
    
    protected void SetupTaskGoal()
    {
        SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted += GameEventReceived;
    }

    private void GameEventReceived(GameEvent gameEvent)
    {
        if (gameEvent.Key == SubTaskData.CompletionEventKey)
        {
            CurrentValue += gameEvent.Value;
            ProgressionIncremented?.Invoke(CurrentValue);
            
            if (CurrentValue < SubTaskData.ProgressCompletionAmount)
            {
                return;
            }
            
            Completed = true;
            SubTaskCompleted?.Invoke(this);
            SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted -= GameEventReceived;
        }
    }

    public void Destroy()
    {
        SubTaskCompleted = delegate { };
        ProgressionIncremented = delegate { };
        SingletonManager.Instance.GameEventDispatcher.GameEventBroadcasted -= GameEventReceived;
    }
}
