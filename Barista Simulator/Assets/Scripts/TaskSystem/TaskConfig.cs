using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BRAG/TaskConfig", fileName = "NewTask")]
public class TaskConfig : ScriptableObject
{
    [field: SerializeField] public TaskData TaskData { get; private set; }
}

[Serializable]
public class TaskData
{
    [field: SerializeField] public string TaskName { get; set; }
    [field: SerializeField] public List<SubTaskData> SubTaskDataList { get; set; }

    [SerializeField] public GameEvent GameEventSentOnTaskCompletion = new(GameEventKey.TaskCompleted);
}