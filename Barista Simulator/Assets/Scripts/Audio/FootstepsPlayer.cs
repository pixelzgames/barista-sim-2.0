using UnityEngine;

public class FootstepsPlayer : MonoBehaviour
{
    [SerializeField] private AudioSource _audioPlayer;
    [SerializeField] private AudioClip[] _footstepClips;

    private void OnFootstepEvent()
    {
        var randomIndex = Random.Range(0, _footstepClips.Length);
        _audioPlayer.PlayOneShot(_footstepClips[randomIndex]);
    }
}
