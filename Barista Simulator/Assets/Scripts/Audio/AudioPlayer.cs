using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour
{
    [Required("Needs an Audio Source in here")]
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private bool _randomAudioClip;
    
    [HideIf("_randomAudioClip")]
    [SerializeField] private AudioClip _clipToPlay;
    
    [ShowIfGroup("_randomAudioClip")]
    [SerializeField] private AudioClip[] _clipsToPlay;
    
    [SerializeField] 
    private bool _randomizePitch;

    [ShowIfGroup("_randomizePitch")]
    [BoxGroup(nameof(_randomizePitch) + "/Randomize Pitch Values")]
    [SerializeField]
    private float _minPitch = 0.9f;
    
    [BoxGroup(nameof(_randomizePitch) + "/Randomize Pitch Values")]
    [SerializeField]
    private float _maxPitch = 1.1f;
    
    public void PlayAudio()
    {
        var pitch = 1f;
        var clip = _clipToPlay;
        
        if (_randomizePitch)
        {
            pitch = Random.Range(_minPitch, _maxPitch);
        }

        if (_randomAudioClip)
        {
            var randomClipIndex = Random.Range(0, _clipsToPlay.Length);
            clip = _clipsToPlay[randomClipIndex];
        }

        _audioSource.pitch = pitch;
        _audioSource.PlayOneShot(clip);
    }
}
