using System;

public class GameEventDispatcher
{
    public event Action<GameEvent> GameEventBroadcasted = delegate { };

    public void LogGameEvent(GameEvent gameEvent)
    {
        GameEventBroadcasted?.Invoke(gameEvent);
    }
}

[Serializable]
public enum GameEventKey
{
    PhoneOpened = 0,
    CharacterCreatorOpened = 1,
    CapUniformEquipped = 2,
    ApronUniformEquipped = 3,
    LeftApartment = 4,
    ChatOSOpened = 5,
    Wakeup = 6,
    CutscenePlayed = 7,
    MessageReceived = 8,
    TalkToBoss = 9,
    ArrivedAtCafe = 10,
    DialogueFinished = 11,
    TaskCompleted = 12,
    Sleep = 13,
    ArrivedHome = 14,
    ShelveRefilled = 15,
    MoneyReceived = 16,
    PlayerGainedControl = 17
}