using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ObjectStateUIController : MonoBehaviour
{
    [SerializeField] private float _distanceFromCameraToStartFading = 10f;
    [SerializeField] private float _distanceFromCameraToBeCulled = 15f;
    [SerializeField] private ObjectStateEventController _eventController;
    [SerializeField] private Transform _panel;
    [SerializeField] private CanvasGroup _canvasGroup;
    
    private readonly Dictionary<ObjectStateConfig, Image> _spawnedStateUI = new();
    private Tweener _tween;
    private Tweener _canvasGroupTween;
    private bool _canvasGroupShown = true;
    
    private void ObjectStateChange(ObjectStateConfig stateConfig, bool status, float genericValue = 0f)
    {
        if (_spawnedStateUI.ContainsKey(stateConfig) && status)
        {
            return;
        }
        
        if (status)
        {
            // Spawn new Image for State
            var ui = Instantiate(stateConfig.IconPrefab, _panel);
            _tween = ui.transform.DOPunchScale(ui.transform.localScale * 0.2f, 0.2f, 1, 0f);
            ui.gameObject.AddComponent<TweenTransformHandler>();
            _spawnedStateUI.Add(stateConfig, ui);
            HandleImageDataConfig(stateConfig, ui, genericValue);
        }
        else
        {
            if (!_spawnedStateUI.TryGetValue(stateConfig, out var stateImage))
            {
                return;
            }

            RemoveStateAndImage(stateConfig, stateImage);
        }
        
        if (_spawnedStateUI.Count > 0)
        {
            HandleVisibilityByPriority();
            ShowCanvas(true);
        }
    }

    private void HideImage(Image stateImage)
    {
        if (!stateImage.gameObject.activeInHierarchy)
        {
            return;
        }

        stateImage.gameObject.SetActive(false);
    }

    private void ShowImage(Image stateImage)
    {
        if (stateImage.gameObject.activeInHierarchy)
        {
            return;
        }
        
        stateImage.gameObject.SetActive(true);
        stateImage.transform.DOPunchScale(stateImage.transform.localScale * 0.2f, 0.2f, 1, 0f);
    }
    
    private void RemoveStateAndImage(ObjectStateConfig stateConfig, Image stateImage)
    {
        if (stateConfig.DisplayType == DisplayType.Filling)
        {
            stateImage.GetComponent<FillingUIController>().StopFilling();
        }
        
        _spawnedStateUI.Remove(stateConfig);
        if (_spawnedStateUI.Count == 1)
        {
            stateImage.transform.DOScale(Vector3.zero, 0.3f).onComplete = DestroyElement;
            return;
        }

        DestroyElement();

        void DestroyElement()
        {
            Destroy(stateImage.gameObject);
            HandleVisibilityByPriority();
        }
    }

    private void HandleImageDataConfig(ObjectStateConfig stateConfig, Image stateImage, float genericValue = 0f)
    {
        switch (stateConfig.DisplayType)
        {
            case DisplayType.Pulsing:
                stateImage.transform.DOScale(stateImage.transform.localScale * 0.8f,
                    stateConfig.PulsingSpeed).SetLoops(-1, LoopType.Yoyo);
                break;
            case DisplayType.Momentarily:
                var sequence = DOTween.Sequence();
                sequence.PrependInterval(stateConfig.MomentarilyDisplayTime).onComplete = () =>
                {
                    RemoveStateAndImage(stateConfig, stateImage);
                };
                break;
            case DisplayType.Filling:
                stateImage.GetComponent<FillingUIController>().StartFilling(genericValue);
                break;
            case DisplayType.Brewing:
            {
                if (_eventController.InteractableReference is EspressoMachineSection espressoMachine)
                {
                    var grounds = (Grounds)espressoMachine.ClippedPortafilter.GetContainer().Content;
                    var isDialedIn = grounds.CoffeeData.IsEspressoDialedIn;
                    stateImage.GetComponent<EspressoBrewingUIController>().SetData(espressoMachine.ParentMachine.EspressoBrewingConfig, isDialedIn);
                }
            }
                break;   
        }
    }

    private void HandleVisibilityByPriority()
    {
        if (_spawnedStateUI.Count <= 0)
        {
            ShowCanvas(false);
            return;
        }

        var orderedByPriority = _spawnedStateUI.Keys.OrderByDescending(o => o.Priority).ToArray();
        var highestPriority = orderedByPriority[0].Priority;
        for (var i = 0; i < orderedByPriority.Length; i++)
        {
            if (i == 0)
            {
                ShowImage(_spawnedStateUI[orderedByPriority[i]]);
                continue;
            }
            
            if (orderedByPriority[i].Priority >= highestPriority)
            {
                ShowImage(_spawnedStateUI[orderedByPriority[i]]);
                continue;
            }
            
            HideImage(_spawnedStateUI[orderedByPriority[i]]);
        }
    }

    private void OnEnable()
    {
        _eventController.ObjectStateChange += ObjectStateChange;
        ShowCanvas(false);
    }
    
    private void OnDisable()
    {
        foreach (var ui in _spawnedStateUI.Values)
        {
            HideImage(ui);
        }
        
        _eventController.ObjectStateChange -= ObjectStateChange;
        _tween?.Kill();
        _canvasGroupTween?.Kill();
    }

    private void ShowCanvas(bool show)
    {
        if (_canvasGroupShown == show)
        {
            return;
        }

        _canvasGroupShown = show;
        _canvasGroupTween = _canvasGroup.DOFade(show ? 1f : 0f, 0.3f);
    }

    private void Update()
    {
        if (_spawnedStateUI.Count <= 0 || !_eventController.InteractableReference || !PlayerInteractionManager.Instance)
        {
            return;
        }

        var cameraZoom = SingletonManager.Instance.CameraManager.CameraController.NormalizedZoom;
        var playerPosition = PlayerInteractionManager.Instance.transform.position;
        var distance = Vector3.Distance(playerPosition, _eventController.InteractableReference.transform.position);
        if (distance >= _distanceFromCameraToStartFading)
        {
            var value = Mathf.Lerp(0f + cameraZoom, 1f,
                1 - (distance - _distanceFromCameraToStartFading) /
                (_distanceFromCameraToBeCulled - _distanceFromCameraToStartFading));

            ShowCanvas(value > 0.5f);
        }
    }
}
  