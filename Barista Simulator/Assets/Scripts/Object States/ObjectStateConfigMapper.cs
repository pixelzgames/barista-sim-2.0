using UnityEngine;

[CreateAssetMenu (menuName = "BRAG/Object State/Onject State Mapper")]
public class ObjectStateConfigMapper : ScriptableObject
{
    public ObjectStatesDictionary ObjectStatesDictionary => _objectStatesDictionary;
    
    [SerializeField] private ObjectStatesDictionary _objectStatesDictionary;
}
