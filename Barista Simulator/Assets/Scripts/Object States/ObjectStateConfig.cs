using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "BRAG/Object State/State", fileName = "NewObjectState")]
public class ObjectStateConfig : ScriptableObject
{
    public const float LOW_PERCENTAGE = 20f;

    public Image IconPrefab;
    /// <summary>
    /// Higher priority prevents anything lower to be displayed, -1 ignores that rule
    /// </summary>
    public int Priority;
    public DisplayType DisplayType;
    /// <summary>
    /// Display type for MOMENTARILY only
    /// </summary>
    public float MomentarilyDisplayTime;
    public float PulsingSpeed;
}

public enum ObjectState
{
    PoweredOn,
    PoweredOff,
    Powering,
    Broken,
    OutOfWater,
    LowWater,
    OutOfSeeds,
    LowSeed,
    Brewing,
    Full,
    Nasty,
    OutOfMilk,
    LowMilk,
    Empty,
    Low,
    EspressoBrewing,
    GodShot,
    DialedIn,
    OverExtracted,
    UnderExtracted,
    VeryUnderExtracted,
    VeryOverExtracted,
    OutOfCoffee,
    None
}

public enum DisplayType
{
    Constant,
    Pulsing,
    Momentarily,
    Filling,
    Brewing
}