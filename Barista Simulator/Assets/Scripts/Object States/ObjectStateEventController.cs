using System;
using UnityEngine;

public class ObjectStateEventController : MonoBehaviour
{
    public Action<ObjectStateConfig, bool, float> ObjectStateChange = delegate {  };

    [field: SerializeField] public Interactable InteractableReference { get; private set; }

    [SerializeField] private ObjectStateConfigMapper _objectStates;
    
    public void RaiseEvent(ObjectState state, bool status = true, float genericValue = 0f)
    {
        var stateConfig = MapObjectStateByEnum(state);
        if (stateConfig == null)
        {
            return;
        }
        
        ObjectStateChange.Invoke(stateConfig, status, genericValue);
    }

    private ObjectStateConfig MapObjectStateByEnum(ObjectState state)
    {
        return _objectStates.ObjectStatesDictionary[state];
    }
}
