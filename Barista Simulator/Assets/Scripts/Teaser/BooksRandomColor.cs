using UnityEngine;

public class BooksRandomColor : MonoBehaviour
{
    [SerializeField] private Material[] _materials;
    
    [ContextMenu("Assign Random Color To Books")]
    public void AssignColor()
    {
        foreach (var rend in GetComponentsInChildren<Renderer>())
        {
            rend.material = _materials[Random.Range(0, _materials.Length)];
        }
    }
}