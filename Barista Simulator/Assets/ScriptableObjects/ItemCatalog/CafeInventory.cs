using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CafeInventory", fileName = "NewCafeInventory")]
public class CafeInventory : ScriptableObject
{
    [Header("Moveables")]
    [SerializeField] public List<Movable> Moveables;
}
