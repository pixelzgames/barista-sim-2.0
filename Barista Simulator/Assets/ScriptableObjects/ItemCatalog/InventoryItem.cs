using UnityEngine;

public class InventoryItem : MonoBehaviour
{
    private Movable _itemToSpawn;
    private CursorController _cursorController;

    public void SetupCatalogItem(Movable movable, CursorController cursorController)
    {
        _itemToSpawn = movable;
        _cursorController = cursorController;
    }

    public void SpawnMoveable()
    {
        _cursorController.SpawnMovableOnGrid(_itemToSpawn);
    }
}
