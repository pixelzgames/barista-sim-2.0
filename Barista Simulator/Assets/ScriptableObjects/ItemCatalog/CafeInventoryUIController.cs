using TMPro;
using UnityEngine;

public class CafeInventoryUIController : MonoBehaviour
{
    [SerializeField] private CafeInventory _cafeInventory;
    [SerializeField] private GameObject _inventoryItemPrefab;
    [SerializeField] private GameObject _inventoryPanelGO;
    [SerializeField] private CursorController _cursorController;

    private void Awake()
    {
        AddCatalogItems();
    }

    private void AddCatalogItems()
    {
        foreach (var item in _cafeInventory.Moveables)
        {
            GameObject go = Instantiate(_inventoryItemPrefab, _inventoryPanelGO.transform);
            TextMeshProUGUI text = go.GetComponentInChildren<TextMeshProUGUI>();
            text.text = item.InteractableName;

            go.GetComponent<InventoryItem>().SetupCatalogItem(item, _cursorController);
        }
    }
}
