using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Music Catalog", menuName = "BRAG/MusicCatalog")]
public class MusicCatalog : ScriptableObject
{
    public List<MusicGenre> Genres => _genres;
    
    [SerializeField] private List<MusicGenre> _genres;
}
