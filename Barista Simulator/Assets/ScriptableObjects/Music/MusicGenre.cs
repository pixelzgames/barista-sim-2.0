using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collection of AudioClips (Songs) of a certain genre
/// </summary>
[CreateAssetMenu(fileName = "New Music Genre", menuName = "BRAG/MusicGenre")]
public class MusicGenre : ScriptableObject
{
    public List<AudioClip> SongList => _songList;

    [SerializeField] private List<AudioClip> _songList;
}
